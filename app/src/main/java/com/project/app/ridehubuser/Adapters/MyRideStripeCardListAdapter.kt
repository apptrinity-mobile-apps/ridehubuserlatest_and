package com.project.app.ridehubuser.Adapters

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.project.app.ridehubuser.PojoResponse.StripeCardListPojo
import com.project.app.ridehubuser.R
import java.util.*

class MyRideStripeCardListAdapter(private val context: Context, private val data: ArrayList<StripeCardListPojo>, val payment_type : String) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var reason: TextView? = null
        internal var card_type: ImageView? = null
        internal var card_expiry: TextView? = null
        internal var my_ride_stripe_card_check_box: CheckBox? = null


    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.recycle_item_payment_card_type, parent, false)
            holder = ViewHolder()
            holder.reason = view.findViewById(R.id.myride_payment_list_textview) as TextView
            holder.card_type = view.findViewById(R.id.my_ride_stripe_card_type_image_view) as ImageView
            holder.card_expiry = view.findViewById(R.id.myride_stripe_card_expiry_textview) as TextView
            holder.my_ride_stripe_card_check_box = view.findViewById(R.id.my_ride_stripe_card_check_box) as CheckBox
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.reason!!.text = "Card ending with " + data[position].getLast4_digits()

        Log.e("CARDTYPE",data[position].getCredit_card_type())
        //holder.card_type!!.setText(data[position].getCredit_card_type())

        if(data[position].getCredit_card_type().equals("Visa")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.card_type!!.setImageDrawable(context.getDrawable(R.drawable.ic_visa_card))
            }
        }else if(data[position].getCredit_card_type().equals("MasterCard")){
            holder.card_type!!.setImageDrawable(context.getDrawable(R.drawable.ic_master_card))
        }else{
            holder.card_type!!.setImageDrawable(context.getDrawable(R.drawable.ic_amex_card))
        }

        holder.card_expiry!!.setText(data[position].getExp_month() + " / " + data[position].getExp_year())

        if(payment_type.equals("payment")){
            holder.my_ride_stripe_card_check_box!!.visibility = View.GONE
        }else{
            holder.my_ride_stripe_card_check_box!!.visibility = View.GONE
        }


        return view
    }

}