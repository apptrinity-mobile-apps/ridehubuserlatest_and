package com.project.app.ridehubuser.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.dialog.PkDialogWithoutButton
import com.project.app.mylibrary.gps.GPSTracker
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.AppInfoSessionManager
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.ImageLoader
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.*
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class SplashPage : ActivityHockeyApp(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    internal lateinit var session: SessionManager
    internal lateinit var context: Context
    internal lateinit var gps: GPSTracker

    internal lateinit var mLocationRequest: LocationRequest
    internal lateinit var mGoogleApiClient: GoogleApiClient
    internal lateinit var result: PendingResult<LocationSettingsResult>
    private var userID = ""
    private var sLatitude = ""
    private var sLongitude = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var mRequest: ServiceRequest? = null

    private var Str_HashKey = ""

    internal lateinit var appInfo_Session: AppInfoSessionManager

    private var isAppInfoAvailable = false

    internal val PERMISSION_REQUEST_CODE = 111

    internal lateinit var mInfoDialog: PkDialogWithoutButton
    internal var sPendingRideId = ""
    internal var sRatingStatus = ""
    internal var sCategoryImage = ""
    internal var sOngoingRide = ""
    internal var sOngoingRideId = ""
    private val package_name = "indo.com.ridehub_lyft_uber"
    internal lateinit var dialog: Dialog
    internal lateinit var imageLoader: ImageLoader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)

        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        context = applicationContext
        imageLoader = ImageLoader(context)
        handleSSLHandshake()

        cd = ConnectionDetector(this@SplashPage)
        isInternetPresent = cd!!.isConnectingToInternet

        try {
            val info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))

                Str_HashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT)


                println("Str_HashKey--------------$Str_HashKey")


            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }



        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
        session = SessionManager(applicationContext)
        appInfo_Session = AppInfoSessionManager(this@SplashPage)
        gps = GPSTracker(applicationContext)

        mGoogleApiClient = GoogleApiClient.Builder(this@SplashPage)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        mGoogleApiClient.connect()
        Handler().postDelayed({
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                    requestPermission()
                } else {
                    setLocation()
                }
            } else {
                setLocation()
            }
        }, SPLASH_TIME_OUT.toLong())

    }

    override fun onConnected(bundle: Bundle?) {
        // enableGpsService();
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}


    private fun setLocation() {
        cd = ConnectionDetector(this@SplashPage)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            if (gps.isgpsenabled() && gps.canGetLocation()) {
                if (session.isLoggedIn) {
                    //starting XMPP service

                    try{
                        ChatService.startUserAction(this@SplashPage)
                    }catch (e:Exception){
                        e.printStackTrace()
                    }



                    session.setLocationSearchValue("no")

                    val user = session.getUserDetails()
                    userID = user[SessionManager.KEY_USERID]!!
                    sLatitude = (gps.getLatitude().toString())
                    sLongitude = (gps.getLongitude().toString())

                    facebook_details_PostRequest(Iconstant.app_facebook_post_url)

                    postRequest_AppInformation(Iconstant.app_info_url)

                } else {
                    postRequest_AppInformation(Iconstant.app_info_url)
                }
            } else {
                enableGpsService()
            }
        } else {
            val mDialog = PkDialog(this@SplashPage)
            mDialog.setDialogTitle(resources.getString(R.string.alert_nointernet))
            mDialog.setDialogMessage(resources.getString(R.string.alert_nointernet_message))
            mDialog.setPositiveButton(
                resources.getString(R.string.timer_label_alert_retry),
                View.OnClickListener {
                    mDialog.dismiss()
                    setLocation()
                })
            mDialog.setNegativeButton(
                resources.getString(R.string.timer_label_alert_cancel),
                View.OnClickListener {
                    mDialog.dismiss()
                    finish()
                })
            mDialog.show()

        }
    }

    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@SplashPage, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
                    Toast.makeText(this@SplashPage, "Location enabled!", Toast.LENGTH_LONG).show()
                    if (session.isLoggedIn) {
                        //starting XMPP service

                        try{
                            ChatService.startUserAction(this@SplashPage)
                        }catch (e:Exception){
                            e.printStackTrace()
                        }

                        Handler().postDelayed({
                            session = SessionManager(applicationContext)
                            gps = GPSTracker(this@SplashPage)

                            val user = session.getUserDetails()
                            userID = user[SessionManager.KEY_USERID]!!
                            sLatitude = (gps.getLatitude().toString())
                            sLongitude = (gps.getLongitude().toString())

                            postRequest_AppInformation(Iconstant.app_info_url)
                        }, 2000)

                    } else {
                        postRequest_AppInformation(Iconstant.app_info_url)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    finish()
                }
                else -> {
                }
            }
        }
    }


    //-----------------------User Current Location Post Request-----------------
    private fun postRequest_SetUserLocation(Url: String) {

        println("----------sLatitude----------$sLatitude")
        println("----------sLongitude----------$sLongitude")
        println("----------userID----------$userID")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["latitude"] = sLatitude
        jsonParams["longitude"] = sLongitude

        println("-------------Splash UserLocation Url----------------$Url")
        mRequest = ServiceRequest(this@SplashPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------Splash UserLocation Response----------------$response")

                    var Str_status = ""
                    var sCategoryID = ""
                    var sTripProgress = ""
                    var sRideId = ""
                    var sRideStage = ""
                    var assign_as = ""
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        assign_as = `object`.getString("assign_as")
                        println("$assign_as--------assign_as--------$Str_status")
                        if (Str_status.equals("1", ignoreCase = true)) {
                            session.setAgent(assign_as)

                        }
                        sCategoryID = `object`.getString("category_id")
                        sTripProgress = `object`.getString("trip_in_progress")
                        sRideId = `object`.getString("ride_id")
                        sRideStage = `object`.getString("ride_stage")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    /*if(web_update()){

                        Alert("Update Available","Do you want to update.?" );
                    }else {*/
                    val i = Intent(this@SplashPage, NavigationDrawer::class.java)
                    startActivity(i)
                    finish()
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                    //}

                }

                override fun onErrorListener() {
                    val i = Intent(this@SplashPage, NavigationDrawer::class.java)
                    startActivity(i)
                    finish()
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                }
            })
    }


    //-----------------------App Information Post Request-----------------
    private fun postRequest_AppInformation(Url: String) {

        println("-------------Splash App Information Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_type"] = "user"
        jsonParams["id"] = userID
        mRequest = ServiceRequest(this@SplashPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------appinfo----------------$response")
                    var Str_status = ""
                    var sContact_mail = ""
                    var sCustomerServiceNumber = ""
                    var sSiteUrl = ""
                    var sXmppHostUrl = ""
                    var sHostName = ""
                    var sFacebookId = ""
                    var sGooglePlusId = ""
                    val sPhoneMasking = ""
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        if (Str_status.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            if (response_object.length() > 0) {
                                val info_object = response_object.getJSONObject("info")
                                if (info_object.length() > 0) {
                                    sContact_mail = info_object.getString("site_contact_mail")
                                    sCustomerServiceNumber =
                                        info_object.getString("customer_service_number")

                                    sSiteUrl = info_object.getString("site_url")

                                    sXmppHostUrl = info_object.getString("xmpp_host_url")

                                    sHostName = info_object.getString("xmpp_host_name")

                                    if (sFacebookId.equals("", ignoreCase = true)) {
                                        sFacebookId = "468945646630814"
                                    } else {
                                        sFacebookId = info_object.getString("facebook_id")
                                    }
                                    if (sGooglePlusId.equals("", ignoreCase = true)) {
                                        sGooglePlusId = "468945646630814"
                                    } else {
                                        sGooglePlusId = info_object.getString("google_plus_app_id")
                                    }

                                    isAppInfoAvailable = true
                                } else {
                                    isAppInfoAvailable = false
                                }
                            } else {
                                isAppInfoAvailable = false
                            }
                        } else {
                            isAppInfoAvailable = false
                        }

                        if (Str_status.equals("1", ignoreCase = true) && isAppInfoAvailable) {
                            appInfo_Session.setAppInfo(
                                sContact_mail,
                                sCustomerServiceNumber,
                                sSiteUrl,
                                sXmppHostUrl,
                                sHostName,
                                sFacebookId,
                                sGooglePlusId,
                                sCategoryImage,
                                sOngoingRide,
                                sOngoingRideId,
                                sPendingRideId,
                                sRatingStatus
                            )

                            if (session.isLoggedIn) {
                                postRequest_SetUserLocation(Iconstant.setUserLocation)
                            } else {
                                val i = Intent(this@SplashPage, LauncherActivity::class.java)
                                i.putExtra("HashKey", Str_HashKey)
                                startActivity(i)
                                finish()
                                overridePendingTransition(R.anim.enter, R.anim.exit)
                            }
                        } else {
                            mInfoDialog = PkDialogWithoutButton(this@SplashPage)
                            mInfoDialog.setDialogTitle(resources.getString(R.string.app_info_header_textView))
                            mInfoDialog.setDialogMessage(resources.getString(R.string.app_info_content))
                            mInfoDialog.show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }

                override fun onErrorListener() {
                    mInfoDialog = PkDialogWithoutButton(this@SplashPage)
                    mInfoDialog.setDialogTitle(resources.getString(R.string.app_info_header_textView))
                    mInfoDialog.setDialogMessage(resources.getString(R.string.app_info_content))
                    mInfoDialog.show()
                }
            })
    }


    private fun checkAccessFineLocationPermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkAccessCoarseLocationPermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkWriteExternalStoragePermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setLocation()
            } else {
                finish()
            }
        }
    }


    override fun onResume() {

        try{
            super.onResume()
            ChatService.startUserAction(this@SplashPage)
        }catch (e:Exception){
            e.printStackTrace()
        }


    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            finish()
            return true
        }
        return false
    }


    //-----------------------code for detail ButtonAction Post request---------------------
    private fun facebook_details_PostRequest(url: String) {
        val jsonParams = HashMap<String, String>()
        jsonParams["hash_key"] = Str_HashKey
        jsonParams["package_name"] = "com.casperon.app.cabily"

        val mservicerequest = ServiceRequest(this@SplashPage)
        mservicerequest.makeServiceRequest(
            url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {

                    Log.e("facebookpost------", response)

                }

                override fun onErrorListener() {

                }
            })
    }


    private fun value(string: String): Long {
        var string = string
        string = string.trim { it <= ' ' }
        if (string.contains(".")) {
            val index = string.lastIndexOf(".")
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1))
        } else {
            return java.lang.Long.valueOf(string)
        }
    }

    private fun Alert(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_dialog_library, null)
        val alert_title =
            dialogView.findViewById(R.id.custom_dialog_library_title_textview) as TextView
        val alert_message =
            dialogView.findViewById(R.id.custom_dialog_library_message_textview) as TextView
        val Bt_action = dialogView.findViewById(R.id.custom_dialog_library_ok_button) as Button
        Bt_action.text = "Update now"
        val Bt_dismiss = dialogView.findViewById(R.id.custom_dialog_library_cancel_button) as Button
        Bt_dismiss.visibility = View.GONE
        builder.setView(dialogView)
        alert_title.text = title
        alert_message.text = message
        builder.setCancelable(false)
        Bt_action.setOnClickListener {
            imageLoader.clearCache()
            dialog.dismiss()
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$package_name")
                    )
                )
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$package_name")
                    )
                )
            }
        }

        builder.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP)
                finish()
            false
        }

        dialog = builder.create()
        dialog.show()
    }

    companion object {

        // Splash screen timer
        private val SPLASH_TIME_OUT = 2000
        internal val REQUEST_LOCATION = 199
        /**
         * Enables https connections
         */
        @SuppressLint("TrulyRandom")
        fun handleSSLHandshake() {
            try {
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                    override fun getAcceptedIssuers(): Array<X509Certificate?> {
                        return arrayOfNulls(0)
                    }

                    override fun checkClientTrusted(
                        certs: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    override fun checkServerTrusted(
                        certs: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                })

                val sc = SSLContext.getInstance("SSL")
                sc.init(null, trustAllCerts, SecureRandom())
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
            } catch (ignored: Exception) {
            }

        }
    }

}