package com.project.app.ridehubuser.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.project.app.ridehubuser.R;
import com.project.app.ridehubuser.Utils.SessionManagerJava;

import java.util.HashMap;


public class ChatActivity extends AppCompatActivity {

    private static final String TAG ="WebViewLog";
    private ProgressDialog progressBar;
    private WebView webview;
    private EditText editText;
    private Button button;
    private RelativeLayout back;
    String data,UserID,UserName,UserEmail,UserImage,driverID,driverName,driverImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatwebjava);
        webview = (WebView) findViewById(R.id.webView);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        back = (RelativeLayout) findViewById(R.id.my_rides_chat_header_back_layout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("");
            }
        });

        SessionManagerJava session = new SessionManagerJava(this);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        UserID = user.get(SessionManagerJava.KEY_USERID);
        UserName = user.get(SessionManagerJava.KEY_USERNAME);
        UserEmail = user.get(SessionManagerJava.KEY_EMAIL);
        UserImage = user.get(SessionManagerJava.KEY_USERIMAGE);

        Log.e("UserID",UserID+"----"+UserName+"----"+UserEmail+"-----"+UserImage);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        driverID = intent.getStringExtra("driverID");
        driverName = intent.getStringExtra("driverName");
        driverImage = intent.getStringExtra("driverImage");


        data = "<script>\n" +
                "(function(t,a,l,k,j,s){\n" +
                "s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n" +
                ";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n" +
                ".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n" +
                "</script>\n" +
                "\n" +
                "<!-- container element in which TalkJS will display a chat UI -->\n" +
                "<div id=\"talkjs-container\" style=\"width: 100%; margin: 0px; height: 500px\"><i>Loading chat...</i></div>\n" +
                "\n" +
                "<!-- TalkJS initialization code, which we'll customize in the next steps -->\n" +
                "<script>\n" +
                "Talk.ready.then(function() {\n" +
                "    var me = new Talk.User({\n" +
                "        id: '"+ UserID +"',\n" +
                "        name: '"+UserName+"',\n" +
                "        role: \"user\",\n"+
                "        email: '"+UserEmail+"',\n" +
                "        photoUrl: '"+UserImage+"',\n" +
                "        welcomeMessage: \"Hey, how can I help?\"\n" +
                "    });\n" +
                "    window.talkSession = new Talk.Session({\n" +
                "        appId: \"M5iybBK9\",\n" +
                "        me: me\n" +
                "    });\n" +
                "    var other = new Talk.User({\n" +
                "        id: '"+ driverID +"',\n" +
                "        name: '"+driverName+"',\n" +
                "        role: \"user\",\n"+
                "        email: '"+UserEmail+"',\n" +
                "        photoUrl: '"+driverImage+"',\n" +
                "        welcomeMessage: \"Hey, how can I help?\"\n" +
                "    });\n" +
                "\n" +
                "    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n" +
                "    conversation.setParticipant(me);\n" +
                "    conversation.setParticipant(other);\n" +
                "    var inbox = talkSession.createInbox({selected: conversation});\n" +
                "    inbox.mount(document.getElementById(\"talkjs-container\"));\n" +
                "});\n" +
                "</script>\n";


        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        progressBar = ProgressDialog.show(ChatActivity.this, "WebView Android", "Loading...");

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "shouldOverrideUrlLoading() DEPRECATED Processing webview url click...");
                writeToLog("Processing webview url " + url);
                if(url.contains("moneyintoken")){
                    Log.i(TAG, "****contiene token ");
                }

                view.loadUrl(url);

                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    writeToLog("shouldOverrideUrlLoading() Processing webview url " + request.getUrl().toString());

                    if( request.getUrl().toString().contains("moneyintoken")){
                        Log.i(TAG, "2 ****contiene token ");
                    }

                }else{
                    writeToLog("Processing webview url " + request.toString());

                    if( request.toString().contains("moneyintoken")){
                        Log.i(TAG, "2 ****contiene token ");
                    }
                }


                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " + url);
                writeToLog("Finished loading URL: " + url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "onReceivedError DEPRECATED Error: " + description);
                Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                writeToLog("Error: " + description, 1);
            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                String newUrl = view.getUrl();
                Log.i(TAG,"onPageStarted() URL: " + url + " newURL: " + newUrl);
                writeToLog("onPageStarted() URL: " + url + " newURL: " + newUrl);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += "\"SSL Certificate Error\" Do you want to continue anyway?.. YES";

                writeToLog(message, 1);
                handler.proceed();

                Log.e(TAG, "onReceivedError DEPRECATED Error: " + message);
            }

        });
       // webview.loadUrl(url);

        webview.loadDataWithBaseURL("https://talkjs.com/dashboard/login", data, "text/html", "UTF-8", "");
    }

    private void writeToLog(String msg) {
        writeToLog(msg, 0);
    }

    private void writeToLog(String msg, int error){
        String color ="#00AA00";
        if(error == 1)color = "#00AA00";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
             editText.setText(Html.fromHtml("<font color=" + color + "> • " + msg + "</font><br>" + editText.getText().toString() + "<br>", Html.FROM_HTML_MODE_LEGACY));
        } else {
            editText.setText(Html.fromHtml("<font color=" + color + "> • " + msg + "</font><br>" + editText.getText().toString() + "<br>"));
        }
    }

}
