package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.MyRideStripeCardListAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.StripeCardListPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.stripe.android.model.Card
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class AddCashToWalletActivity : ActivityHockeyApp(){

    lateinit var iv_close_wallet:ImageView
    lateinit var tv_add_cash:TextView
    lateinit var tv_add_payment_method:TextView
    lateinit var et_edit_amount_id:EditText
    lateinit var rv_other_cards:ListView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    lateinit var userID: String
    private var dialog: Dialog? = null
    private var mRequest: ServiceRequest? = null

    private var itemStripeCardList: ArrayList<StripeCardListPojo>? = null
    private var isPaymentAvailable = false
    private var adapter: MyRideStripeCardListAdapter? = null

    private var card: Card? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_cash_wallet)

        initialize()

        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()

        iv_close_wallet.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        tv_add_payment_method.setOnClickListener {
            val intent = Intent(this@AddCashToWalletActivity, AddPaymentActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }

        tv_add_cash.setOnClickListener {

            if(!et_edit_amount_id.text.toString().trim().equals("")){
                val intent = Intent(this@AddCashToWalletActivity, AddFundsActivity::class.java)
                intent.putExtra("amount",et_edit_amount_id.text.toString())
                startActivity(intent)
                overridePendingTransition(R.anim.slideup, R.anim.slidedown)
            }
        }

        if (isInternetPresent!!) {
            postRequest_StripeCardList(Iconstant.makepayment_stripe_cardList)
        } else {
            alert(
                resources.getString(R.string.alert_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

    }

    private fun initialize(){
        session = SessionManager(this@AddCashToWalletActivity)
        cd = ConnectionDetector(this@AddCashToWalletActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_close_wallet = findViewById(R.id.iv_close_wallet)
        tv_add_cash = findViewById(R.id.tv_add_cash)
        tv_add_payment_method = findViewById(R.id.tv_add_payment_method)
        rv_other_cards = findViewById(R.id.rv_other_cards)
        et_edit_amount_id = findViewById(R.id.et_edit_amount_id)

        val manager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
      //  rv_other_cards.layoutManager = manager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rv_other_cards!!.isNestedScrollingEnabled = false
        }

        itemStripeCardList = ArrayList()

    }

    private fun postRequest_StripeCardList(Url: String) {
        dialog = Dialog(this@AddCashToWalletActivity)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(resources.getString(R.string.action_pleasewait))

        println("-------------StripeCardList Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        mRequest = ServiceRequest(this@AddCashToWalletActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------StripeCardList Response----------------$response")
                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")

                            if (response_object.length() > 0) {
                                val card_array = response_object.getJSONArray("cardlist")
                                if (card_array.length() > 0) {
                                    itemStripeCardList!!.clear()
                                    for (i in 0 until card_array.length()) {
                                        val reason_object = card_array.getJSONObject(i)
                                        val pojo = StripeCardListPojo()
                                        pojo.setLast4_digits(reason_object.getString("last4_digits"))
                                        pojo.setCredit_card_type(reason_object.getString("credit_card_type"))
                                        pojo.setExp_year(reason_object.getString("exp_year"))
                                        pojo.setExp_month(reason_object.getString("exp_month"))
                                        pojo.setCard_id(reason_object.getString("card_id"))

                                        itemStripeCardList!!.add(pojo)
                                    }
                                    adapter = MyRideStripeCardListAdapter(
                                        this@AddCashToWalletActivity, itemStripeCardList!!,""
                                    )
                                    rv_other_cards.adapter = adapter
                                    isPaymentAvailable = true
                                } else {
                                    isPaymentAvailable = false
                                }
                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                        if (Sstatus.equals("1", ignoreCase = true) && isPaymentAvailable) {

                        }

                    } catch (e: JSONException) {
                        Log.e("payment", "" + e)
                    }

                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    private fun alert(title: String, alert: String) {
        val mDialog = PkDialog(this@AddCashToWalletActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

}
