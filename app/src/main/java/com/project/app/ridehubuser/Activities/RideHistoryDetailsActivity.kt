package com.project.app.ridehubuser.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.project.app.mylibrary.customRatingBar.RotationRatingBar
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.Adapters.HelpCategoryAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.CancelTripPojo
import com.project.app.ridehubuser.PojoResponse.HelpCategoryPojo
import com.project.app.ridehubuser.PojoResponse.MyRideDetailPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.squareup.picasso.Picasso
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class RideHistoryDetailsActivity : ActivityHockeyApp() {

    lateinit var ll_trip_details_driver_profile: LinearLayout
    lateinit var ll_trip_details_help_receipt: LinearLayout
    lateinit var ll_main_track_cancleride: LinearLayout

    lateinit var ll_ridedetail_trackride: LinearLayout
    lateinit var ll_ridedetail_payment: LinearLayout
    lateinit var ll_ridedetail_mail_invoice: LinearLayout
    lateinit var ll_ridedetail_cancelride: LinearLayout
    lateinit var ll_ridedetail_reportissue: LinearLayout
    lateinit var ll_ridedetail_shareride: LinearLayout


    lateinit var ll_trip_details_help: LinearLayout
    lateinit var ll_help: LinearLayout
    lateinit var ll_help_header: LinearLayout
    lateinit var ll_receipt: LinearLayout
    lateinit var ll_receipt_header: LinearLayout
    lateinit var ll_help_trip_accident: LinearLayout
    lateinit var ll_help_trip_review: LinearLayout
    lateinit var ll_help_trip_lost_item: LinearLayout
    lateinit var ll_help_driver_unprofessional: LinearLayout
    lateinit var ll_help_trip_vehicle_different: LinearLayout
    lateinit var ll_help_trip_issue_different: LinearLayout
    lateinit var ll_help_trip_issue_animal: LinearLayout
    lateinit var ll_help_trip_safety_response: LinearLayout
    lateinit var tv_trip_details_time: TextView
    lateinit var tv_trip_details_car_type: TextView
    lateinit var tv_trip_details_vehicle_no: TextView
    lateinit var tv_trip_details_amount: TextView
    lateinit var tv_trip_details_pickup_header: TextView
    lateinit var tv_trip_details_pickup_details: TextView
    lateinit var tv_trip_details_destination_header: TextView
    lateinit var tv_trip_details_destination_details: TextView
    lateinit var tv_trip_details_pickup_time: TextView
    lateinit var tv_trip_details_drop_time: TextView
    lateinit var tv_trip_details_driver_name: TextView
    lateinit var tv_trip_details_last_trip_edit_payment: TextView
    lateinit var tv_trip_details_last_trip_edit_rating: TextView
    lateinit var tv_help_header: TextView
    lateinit var tv_receipt_header: TextView
    lateinit var iv_close_trip_details: ImageView
    lateinit var iv_trip_details_driver: ImageView
    lateinit var iv_ridedetail_map: ImageView
    lateinit var tv_trip_details_ride_rating: RotationRatingBar
    lateinit var tv_trip_details_driver_rating: RotationRatingBar
    lateinit var cv_trip_details_edit_payment: CardView
    lateinit var cv_trip_details_edit_rating: CardView
    lateinit var view_help_header: View
    lateinit var view_receipt_header: View

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    var rideID = ""
    private var userID = ""

    lateinit var dialog: Dialog
    lateinit var itemlist: ArrayList<MyRideDetailPojo>
    lateinit var itemlist_reason: ArrayList<CancelTripPojo>
    private var mRequest: ServiceRequest? = null

    private var currencySymbol = ""
    private var isPickUpAvailable = false
    private var isDropAvailable = false
    private var isSummaryAvailable = false
    private var isReasonAvailable = false
    private var isFareAvailable = false
    private var isTrackRideAvailable = false
    private var TotalAmount = ""
    //------Invoice Dialog Declaration-----
    private var Et_dialog_InvoiceEmail: EditText? = null
    private var invoice_dialog: MaterialDialog? = null
    private var completejob_dialog: Dialog? = null
    private var Et_share_trip_mobileno: EditText? = null
    private var UserID = ""
    private var refreshReceiver: RefreshReceiver? = null

    private var isRidePickUpAvailable = false
    private var isRideDropAvailable = false


    lateinit var tv_tip_amount: TextView
    lateinit var tv_totalfare: TextView
    lateinit var tv_subtotal: TextView
    lateinit var tv_timetaken: TextView
    lateinit var tv_waittime: TextView
    lateinit var tv_ridedistance: TextView
    lateinit var tv_paymenttype: TextView
    lateinit var tv_pointsusage: TextView
    lateinit var tv_walletusage: TextView

    lateinit var ll_receipt_tips: LinearLayout
    lateinit var ll_waittime: LinearLayout
    lateinit var ll_walletusage: LinearLayout
    lateinit var ll_receipt_pointsusage: LinearLayout
    lateinit var ll_paymenttype: LinearLayout
    lateinit var ll_ridedistance: LinearLayout
    lateinit var ll_timetaken: LinearLayout
    lateinit var ll_receipt_subtotalfare: LinearLayout
    lateinit var ll_receipt_totalfare: LinearLayout
    lateinit var rv_helpcategory: RecyclerView

    var itemlist_all: java.util.ArrayList<HelpCategoryPojo> = java.util.ArrayList()



    //----------------------Code for TextWatcher-------------------------
    private val mailInvoice_EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Et_dialog_InvoiceEmail!!.text.length > 0) {
                Et_dialog_InvoiceEmail!!.error = null
            }
        }
    }

    inner class RefreshReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_REFRESH") {

                postRequest_MyRides(Iconstant.myride_details_url)
                /*if (isInternetPresent!!) {
                    postRequest_MyRides(Iconstant.myride_details_url)
                }*/
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride_history_details)
        myrideDetail_class = this@RideHistoryDetailsActivity
        initialise()


        //Start XMPP Chat Service
        try{
            ChatService.startUserAction(this@RideHistoryDetailsActivity)
        }catch (e:Exception){
            //e.printStackTrace()
        }


        rideID = intent.getStringExtra("RideID")!!
        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID]!!

        postRequest_MyRides(Iconstant.myride_details_url)

        postRequest_HelpCategory(Iconstant.help_category_url)

        /*if (isInternetPresent!!) {
            postRequest_MyRides(Iconstant.myride_details_url)
        } else {
            alert(
                resources.getString(R.string.alert_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }*/

        iv_close_trip_details.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.slidedown, R.anim.slideup)
        }

        ll_trip_details_driver_profile.setOnClickListener {
            val intent = Intent(this@RideHistoryDetailsActivity, DriverDetailsActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        ll_help_header.setOnClickListener {
            ll_help.visibility = View.VISIBLE
            ll_receipt.visibility = View.GONE
            tv_help_header.setTextColor(ContextCompat.getColor(this, R.color.header_help_text))
            tv_receipt_header.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.header_help_text_light
                )
            )
            view_help_header.visibility = View.VISIBLE
            view_receipt_header.visibility = View.GONE
        }

        ll_receipt_header.setOnClickListener {
            ll_help.visibility = View.GONE
            ll_receipt.visibility = View.VISIBLE
            tv_help_header.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.header_help_text_light
                )
            )
            tv_receipt_header.setTextColor(ContextCompat.getColor(this, R.color.header_help_text))
            view_help_header.visibility = View.GONE
            view_receipt_header.visibility = View.VISIBLE
        }


        ll_ridedetail_mail_invoice!!.setOnClickListener { mailInvoice() }

        ll_ridedetail_shareride!!.setOnClickListener { shareTrip() }

        ll_ridedetail_reportissue!!.setOnClickListener { sendEmail() }

        ll_ridedetail_payment!!.setOnClickListener {
            cd = ConnectionDetector(this@RideHistoryDetailsActivity)
            isInternetPresent = cd!!.isConnectingToInternet




            val passIntent = Intent(this@RideHistoryDetailsActivity, MyRidePaymentList::class.java)
            passIntent.putExtra("RideID", rideID)
            passIntent.putExtra("TotalAmount", TotalAmount)
            startActivity(passIntent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

            /*if (isInternetPresent!!) {
                val passIntent = Intent(this@RideHistoryDetailsActivity, MyRidePaymentList::class.java)
                passIntent.putExtra("RideID", rideID)
                passIntent.putExtra("TotalAmount", TotalAmount)
                startActivity(passIntent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
            }*/
        }

        ll_ridedetail_cancelride!!.setOnClickListener {


            completejob_dialog = Dialog(this@RideHistoryDetailsActivity)
            val view = LayoutInflater.from(this@RideHistoryDetailsActivity)
                .inflate(R.layout.cancel_trip_popup, null)
            completejob_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val Bt_Submit = view.findViewById(R.id.jsharetrip_popup_submit) as LinearLayout
            val Bt_Cancel = view.findViewById(R.id.sharetrip_popup_cancel) as LinearLayout
            val btn_close = view.findViewById(R.id.iv_share_close) as ImageView
            completejob_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            completejob_dialog!!.setContentView(view)
            completejob_dialog!!.show()
            //completejob_dialog!!.setView(view).show()
            Bt_Submit.setOnClickListener {
                postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url)
            }
            Bt_Cancel.setOnClickListener { completejob_dialog!!.dismiss() }
            btn_close.setOnClickListener { completejob_dialog!!.dismiss() }

        }


        ll_ridedetail_trackride!!.setOnClickListener {
            cd = ConnectionDetector(this@RideHistoryDetailsActivity)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                postRequest_TrackRide(Iconstant.myride_details_track_your_ride_url)
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
            }
        }



    }

    private fun initialise() {
        session = SessionManager(this@RideHistoryDetailsActivity)
        cd = ConnectionDetector(this@RideHistoryDetailsActivity)
        // isInternetPresent = cd!!.isConnectingToInternet

        ll_trip_details_driver_profile = findViewById(R.id.ll_trip_details_driver_profile)
        iv_close_trip_details = findViewById(R.id.iv_close_trip_details)
        tv_trip_details_time = findViewById(R.id.tv_trip_details_time)
        tv_trip_details_car_type = findViewById(R.id.tv_trip_details_car_type)
        tv_trip_details_vehicle_no = findViewById(R.id.tv_trip_details_vehicle_no)
        tv_trip_details_amount = findViewById(R.id.tv_trip_details_amount)
        tv_trip_details_pickup_header = findViewById(R.id.tv_trip_details_pickup_header)
        tv_trip_details_pickup_details = findViewById(R.id.tv_trip_details_pickup_details)
        tv_trip_details_destination_header = findViewById(R.id.tv_trip_details_destination_header)
        tv_trip_details_destination_details = findViewById(R.id.tv_trip_details_destination_details)
        tv_trip_details_pickup_time = findViewById(R.id.tv_trip_details_pickup_time)
        tv_trip_details_drop_time = findViewById(R.id.tv_trip_details_drop_time)
        tv_trip_details_driver_name = findViewById(R.id.tv_trip_details_driver_name)
        iv_trip_details_driver = findViewById(R.id.iv_trip_details_driver)
        tv_trip_details_ride_rating = findViewById(R.id.tv_trip_details_ride_rating)
        tv_trip_details_driver_rating = findViewById(R.id.tv_trip_details_driver_rating)
        iv_ridedetail_map = findViewById(R.id.iv_ridedetail_map)

        /* cv_trip_details_edit_rating = findViewById(R.id.cv_trip_details_edit_rating)
         cv_trip_details_edit_payment = findViewById(R.id.cv_trip_details_edit_payment)
         tv_trip_details_last_trip_edit_rating =
             findViewById(R.id.tv_trip_details_last_trip_edit_rating)
         tv_trip_details_last_trip_edit_payment =
             findViewById(R.id.tv_trip_details_last_trip_edit_payment)*/


        ll_main_track_cancleride = findViewById(R.id.ll_main_track_cancleride)
        ll_ridedetail_trackride = findViewById(R.id.ll_ridedetail_trackride)
        ll_ridedetail_payment = findViewById(R.id.ll_ridedetail_payment)
        ll_ridedetail_mail_invoice = findViewById(R.id.ll_ridedetail_mail_invoice)
        ll_ridedetail_cancelride = findViewById(R.id.ll_ridedetail_cancelride)
        ll_ridedetail_shareride = findViewById(R.id.ll_ridedetail_shareride)
        ll_ridedetail_reportissue = findViewById(R.id.ll_ridedetail_reportissue)


        ll_trip_details_help_receipt = findViewById(R.id.ll_trip_details_help_receipt)
        ll_help = findViewById(R.id.ll_help)
        ll_help_header = findViewById(R.id.ll_help_header)
        ll_receipt = findViewById(R.id.ll_receipt)
        ll_receipt_header = findViewById(R.id.ll_receipt_header)
        ll_trip_details_help = findViewById(R.id.ll_trip_details_help)

        tv_help_header = findViewById(R.id.tv_help_header)
        view_receipt_header = findViewById(R.id.view_receipt_header)
        view_help_header = findViewById(R.id.view_help_header)
        tv_receipt_header = findViewById(R.id.tv_receipt_header)



        ll_receipt_tips = findViewById(R.id.ll_receipt_tips)
        ll_receipt_pointsusage = findViewById(R.id.ll_receipt_pointsusage)
        ll_paymenttype = findViewById(R.id.ll_paymenttype)
        ll_ridedistance = findViewById(R.id.ll_ridedistance)
        ll_timetaken = findViewById(R.id.ll_timetaken)
        ll_receipt_subtotalfare = findViewById(R.id.ll_receipt_subtotalfare)
        ll_receipt_totalfare = findViewById(R.id.ll_receipt_totalfare)
        rv_helpcategory = findViewById(R.id.rv_helpcategory)
        ll_waittime = findViewById(R.id.ll_waittime)
        ll_walletusage = findViewById(R.id.ll_walletusage)

        tv_tip_amount = findViewById(R.id.tv_tip_amount)
        tv_totalfare = findViewById(R.id.tv_totalfare)
        tv_walletusage = findViewById(R.id.tv_walletusage)
        tv_subtotal = findViewById(R.id.tv_subtotal)
        tv_timetaken = findViewById(R.id.tv_timetaken)
        tv_waittime = findViewById(R.id.tv_waittime)
        tv_ridedistance = findViewById(R.id.tv_ridedistance)
        tv_paymenttype = findViewById(R.id.tv_paymenttype)
        tv_pointsusage = findViewById(R.id.tv_pointsusage)
        tv_tip_amount = findViewById(R.id.tv_tip_amount)





        itemlist = ArrayList()
        itemlist_reason = ArrayList()


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = RefreshReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.package.ACTION_CLASS_REFRESH")
        registerReceiver(refreshReceiver, intentFilter)

        val user = session!!.getUserDetails()
        UserID = user[SessionManager!!.KEY_USERID!!].toString()

    }


    private fun postRequest_MyRides(Url: String) {
        dialog = Dialog(this@RideHistoryDetailsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------MyRide Detail Url----------------$Url")
        println("-------------MyRide Detail user_id----------------$userID")
        println("-------------MyRide Detail ride_id----------------$rideID")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["ride_id"] = rideID

        mRequest = ServiceRequest(this@RideHistoryDetailsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                @SuppressLint("SetTextI18n")
                override fun onCompleteListener(response: String) {

                    println("-------------MyRide Detail Response----------------$response")

                    var Sstatus = ""
                    val currencycode: Currency

                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            if (response_object.length() > 0) {

                                val check_details_object = response_object.get("details")
                                if (check_details_object is JSONObject) {

                                    val detail_object = response_object.getJSONObject("details")
                                    if (detail_object.length() > 0) {
                                        itemlist.clear()
                                        val pojo = MyRideDetailPojo()

                                        currencycode =
                                            Currency.getInstance(getLocale(detail_object.getString("currency")))
                                        currencySymbol = currencycode.symbol

                                        pojo.setCurrrencySymbol(currencycode.symbol)
                                        pojo.setCarType(detail_object.getString("cab_type"))
                                        pojo.setRideId(detail_object.getString("ride_id"))
                                        pojo.setRideStatus(detail_object.getString("ride_status"))
                                        pojo.setDisplayStatus(detail_object.getString("disp_status"))
                                        pojo.setDoCancelAction(detail_object.getString("do_cancel_action"))
                                        pojo.setDoTrackAction(detail_object.getString("do_track_action"))
                                        pojo.setIsFavLocation(detail_object.getString("is_fav_location"))
                                        pojo.setPay_status(detail_object.getString("pay_status"))
                                        pojo.setDistance_unit(detail_object.getString("distance_unit"))
                                        pojo.setVehicle_number(detail_object.getString("vehicle_number"))
                                        pojo.setPayment_type(detail_object.getString("payment_type"))
                                        pojo.setDriver_rating(detail_object.getString("driver_rating"))
                                        pojo.setImage(detail_object.getString("image"))
                                        pojo.setUser_rating(detail_object.getString("user_rating"))
                                        pojo.setDriver_name(detail_object.getString("driver_name"))
                                        pojo.setDriver_image(detail_object.getString("driver_image"))
                                        pojo.setVehicle_model(detail_object.getString("vehicle_model"))
                                        pojo.setPickup(
                                            resources.getString(R.string.my_rides_detail_pickup_textview) + " " + detail_object.getString(
                                                "pickup_date"
                                            )
                                        )
                                        pojo.setPickup_date(detail_object.getString("pickup_date"))
                                        pojo.setRidebegin_time(detail_object.getString("ride_begin_time"))
                                        pojo.setRideend_time(detail_object.getString("ride_end_time"))
                                        pojo.setDrop_date(detail_object.getString("drop_date"))



                                        println("-------------MyRide Detail Response1----------------" + detail_object.toString())

                                        tv_trip_details_pickup_time.text = detail_object.getString("ride_begin_time")
                                        tv_trip_details_drop_time.text = detail_object.getString("ride_end_time")

                                        val check_pickup_object = detail_object.get("pickup")
                                        if (check_pickup_object is JSONObject) {


                                            val pickup_object =
                                                detail_object.getJSONObject("pickup")
                                            val drop_object = detail_object.getJSONObject("drop")

                                            if (pickup_object.length() > 0) {
                                                tv_trip_details_pickup_details.text =
                                                    pickup_object.getString(
                                                        "location"
                                                    )
                                            }
                                            if (drop_object.length() > 0) {
                                                tv_trip_details_destination_details.text =
                                                    drop_object.getString("location")
                                            }

                                            if (pickup_object.length() > 0) {
                                                pojo.setAddress(pickup_object.getString("location"))
                                                val latlong_object =
                                                    pickup_object.getJSONObject("latlong")
                                                if (latlong_object.length() > 0) {
                                                    pojo.setLocationLat(latlong_object.getString("lat"))
                                                    pojo.setLocationLong(latlong_object.getString("lon"))
                                                }

                                                isPickUpAvailable = true
                                            } else {
                                                isPickUpAvailable = false
                                            }
                                        }
                                        println("-------------MyRide Detail Response2----------------" + check_pickup_object.toString())
                                        val check_drop_object = detail_object.get("drop")
                                        if (check_drop_object is JSONObject) {

                                            val drop_object = detail_object.getJSONObject("drop")
                                            if (drop_object.length() > 0) {
                                                pojo.setDropAddress(drop_object.getString("location"))

                                                isDropAvailable = true
                                            } else {
                                                isDropAvailable = false
                                            }
                                        }

                                        println("-------------MyRide Detail Response3----------------" + check_drop_object.toString())


                                        if (detail_object.getString("ride_status").equals(
                                                "Completed",
                                                ignoreCase = true
                                            ) || detail_object.getString("ride_status").equals(
                                                "Finished",
                                                ignoreCase = true
                                            )
                                        ) {
                                            pojo.setDrop(
                                                resources.getString(R.string.my_rides_detail_drop_textview) + " " + " " + detail_object.getString(
                                                    "drop_date"
                                                )
                                            )

                                            println("-------------MyRide Detail Response4----------------" + detail_object.toString())

                                                val check_summary_object = detail_object.get("summary")
                                                if (check_summary_object is JSONObject) {

                                                    val summary_object =
                                                        detail_object.getJSONObject("summary")
                                                    val pickup_object =
                                                        detail_object.getJSONObject("pickup")
                                                    val drop_object =
                                                        detail_object.getJSONObject("drop")

                                                    if (pickup_object.length() > 0) {
                                                        tv_trip_details_pickup_details.text =
                                                            pickup_object.getString("location")
                                                    }
                                                    if (drop_object.length() > 0) {
                                                        tv_trip_details_destination_details.text =
                                                            drop_object.getString("location")
                                                    }


                                                    if (summary_object.length() > 0) {
                                                        pojo.setRideDistance(summary_object.getString("ride_distance"))
                                                        pojo.setTimeTaken(summary_object.getString("ride_duration"))
                                                        pojo.setWaitTime(summary_object.getString("waiting_duration"))

                                                        isSummaryAvailable = true
                                                    } else {
                                                        isSummaryAvailable = false
                                                    }
                                                }




                                            val check_fare_object = detail_object.get("fare")
                                            if (check_fare_object is JSONObject) {

                                                val fare_object =
                                                    detail_object.getJSONObject("fare")
                                                if (fare_object.length() > 0) {
                                                    TotalAmount =
                                                        fare_object.getString("grand_bill")
                                                    pojo.setTotalBill(
                                                        "" + (fare_object.getString("grand_bill").toDouble()) + (fare_object.getString(
                                                            "coupon_discount"
                                                        ).toDouble())
                                                    )
                                                    pojo.setTotalPaid(fare_object.getString("total_paid"))
                                                    pojo.setCouponDiscount(fare_object.getString("coupon_discount"))
                                                    pojo.setWalletUsuage(fare_object.getString("wallet_usage"))
                                                    pojo.setPoints_usage(fare_object.getString("points_usage"))
                                                    pojo.setTip_amount(fare_object.getString("tips_amount"))
                                                    pojo.setGrand_bill(fare_object.getString("grand_bill"))
                                                    pojo.setTotal_Bill(fare_object.getString("total_bill"))
                                                    isFareAvailable = true
                                                } else {
                                                    isFareAvailable = false
                                                }
                                            }

                                        }

                                        itemlist.add(pojo)
                                    }
                                }

                            }
                        }

                        //------------OnPost Execute------------
                        if (Sstatus.equals("1", ignoreCase = true)) {

                            if (itemlist.size > 0) {



                                    println("-------------MyRide Detail Response4----------------" + "jdfdsfhsdf")
                                    //------Show and Hide the Button Layout------
                                    if (itemlist[0].getPay_status().equals("Pending") || itemlist[0].getPay_status().equals(
                                            "Processing"
                                        ) || itemlist[0].getDoCancelAction().equals("1") || itemlist[0].getRideStatus().equals(
                                            "Completed"
                                        ) || itemlist[0].getRideStatus().equals("Onride")
                                    ) {
                                        ll_main_track_cancleride!!.visibility = View.VISIBLE
                                    } else {
                                        ll_main_track_cancleride!!.visibility = View.GONE
                                    }


                                if (itemlist[0].getPay_status().equals("Pending") || itemlist[0].getPay_status().equals(
                                        "Processing"
                                    )
                                ) {
                                    ll_ridedetail_cancelride!!.visibility = View.GONE
                                    ll_ridedetail_payment!!.visibility = View.VISIBLE
                                    ll_ridedetail_mail_invoice!!.visibility = View.GONE
                                    ll_ridedetail_reportissue!!.visibility = View.GONE
                                }

                                if (itemlist[0].getDoCancelAction().equals("1")) {
                                    ll_ridedetail_cancelride!!.visibility = View.VISIBLE
                                    ll_ridedetail_payment!!.visibility = View.GONE
                                    ll_ridedetail_mail_invoice!!.visibility = View.GONE
                                    ll_ridedetail_reportissue!!.visibility = View.GONE
                                    /*Rl_main_tip!!.visibility = View.GONE
                                    Rl_tip!!.visibility = View.GONE
                                    Ll_deleteTip!!.visibility = View.GONE
                                    Cb_tip!!.isChecked = false*/
                                }

                                if (itemlist[0].getRideStatus().equals("Completed")) {
                                    ll_ridedetail_cancelride!!.visibility = View.GONE
                                    ll_ridedetail_payment!!.visibility = View.GONE
                                    ll_ridedetail_mail_invoice!!.visibility = View.VISIBLE
                                    ll_ridedetail_reportissue!!.visibility = View.VISIBLE

                                }

                                //------Show and Hide Track Ride Button Layout------
                                if (itemlist[0].getDoTrackAction().equals("1")) {
                                    ll_ridedetail_trackride!!.visibility = View.VISIBLE
                                    ll_ridedetail_shareride!!.visibility = View.VISIBLE
                                } else {
                                    ll_ridedetail_trackride!!.visibility = View.GONE
                                    ll_ridedetail_shareride!!.visibility = View.GONE
                                }

                                    if (isFareAvailable) {

                                    if (itemlist[0].getRideStatus().equals("Completed") || itemlist[0].getRideStatus().equals(
                                            "Finished"
                                        )
                                    ) {
                                        tv_trip_details_car_type.text =
                                            itemlist[0].getCarType().toString()
                                        tv_trip_details_vehicle_no.text =
                                            itemlist[0].getVehicle_model().toString()+"  "+ itemlist[0].getVehicle_number().toString()
                                        tv_trip_details_time.text =
                                            itemlist[0].getPickup_date().toString()

                                        if (isSummaryAvailable) {
                                            tv_ridedistance!!.text =
                                                itemlist[0].getRideDistance() + " " + itemlist[0].getDistance_unit()
                                            tv_timetaken!!.text =
                                                itemlist[0].getTimeTaken() + " " + resources.getString(
                                                    R.string.my_rides_detail_mins_textview
                                                )
                                            tv_waittime!!.text =
                                                itemlist[0].getWaitTime() + " " + resources.getString(
                                                    R.string.my_rides_detail_mins_textview
                                                )

                                            if(itemlist[0].getWalletUsuage().equals("") || itemlist[0].getWalletUsuage().equals("null") || itemlist[0].getWalletUsuage().equals("0")){
                                                ll_walletusage.visibility = View.GONE
                                            }else{
                                                ll_walletusage.visibility = View.VISIBLE
                                                tv_walletusage.setText("$"+itemlist[0].getWalletUsuage())
                                            }

                                            if(itemlist[0].getDriver_rating().equals("null") || itemlist[0].getDriver_rating().equals(null)|| itemlist[0].getDriver_rating().equals("")|| itemlist[0].getDriver_rating().equals("0")){
                                                tv_trip_details_driver_rating.visibility = View.GONE
                                            }else{
                                                val driver_rating = itemlist[0].getDriver_rating()
                                                if (driver_rating!!.length > 0) {
                                                    tv_trip_details_driver_rating!!.rating =
                                                        java.lang.Float.parseFloat(
                                                            driver_rating!!
                                                        )

                                                }
                                            }

                                            if(itemlist[0].getUser_rating().equals("null") || itemlist[0].getUser_rating().equals(null)|| itemlist[0].getUser_rating().equals("")|| itemlist[0].getUser_rating().equals("0")){
                                                tv_trip_details_ride_rating.visibility = View.GONE
                                            }else{
                                                val user_rating = itemlist[0].getUser_rating()
                                                if (user_rating!!.length > 0) {
                                                    tv_trip_details_ride_rating!!.rating =
                                                        java.lang.Float.parseFloat(
                                                            user_rating!!
                                                        )

                                                }
                                            }


                                           /* if (itemlist[0].getDriver_rating() != "null" || itemlist[0].getUser_rating() != "null") {
                                                val driver_rating = itemlist[0].getDriver_rating()
                                                val user_rating = itemlist[0].getUser_rating()
                                                if (driver_rating!!.length > 0) {
                                                    tv_trip_details_driver_rating!!.rating =
                                                        java.lang.Float.parseFloat(
                                                            driver_rating!!
                                                        )
                                                    tv_trip_details_ride_rating!!.rating =
                                                        java.lang.Float.parseFloat(
                                                            user_rating!!
                                                        )
                                                }
                                            } else {
                                                println("-------------Driverratingnull----------------" + "Driverratingnull")
                                                tv_trip_details_driver_rating.visibility = View.GONE

                                                tv_trip_details_ride_rating.visibility = View.GONE
                                                tv_trip_details_ride_rating!!.rating =
                                                    java.lang.Float.parseFloat(
                                                        0.toString()
                                                    )
                                            }*/

                                            if(itemlist[0].getDriver_name() != "null"){
                                                tv_trip_details_driver_name!!.setText("You rated " +itemlist[0].getDriver_name())
                                            }


                                            Log.e("DRIVERIMAGE",""+itemlist[0].getDriver_image())
                                            if(itemlist[0].getDriver_image() != "null"){
                                                Picasso.with(this@RideHistoryDetailsActivity).load("https://ridehub.co/images/users/"+itemlist[0].getDriver_image().toString())
                                                    .placeholder(R.drawable.user_icon_default).into(iv_trip_details_driver)

                                            }

                                            if(itemlist[0].getImage() != "null"){
                                                Picasso.with(this@RideHistoryDetailsActivity).load("https://ridehub.co/"+itemlist[0].getImage().toString())
                                                    .placeholder(R.drawable.map_demo).into(iv_ridedetail_map)

                                            }



                                        }

                                        if (isFareAvailable) {
                                            tv_trip_details_amount.text =
                                                "$${itemlist[0].getGrand_bill().toString()}"


                                            tv_totalfare!!.text =
                                               "$"+ TotalAmount
                                            tv_paymenttype!!.text = itemlist[0].getPayment_type()
                                            tv_subtotal!!.text =
                                                "$"+ itemlist[0].getTotal_Bill()
                                            //Tv_walletUsuage!!.text = itemlist[0].getCurrrencySymbol() + itemlist[0].getWalletUsuage()
                                            tv_pointsusage!!.text =
                                                "$" + itemlist[0].getPoints_usage()
                                            tv_tip_amount!!.text =
                                                "$" + itemlist[0].getTip_amount()


                                        } else {
                                            /*  Rl_priceBottom!!.visibility = View.GONE
                                    Tv_drop!!.visibility = View.GONE*/
                                        }

                                        if(itemlist[0].getImage() != "null"){
                                            Picasso.with(this@RideHistoryDetailsActivity).load("https://ridehub.co/"+itemlist[0].getImage().toString())
                                                .placeholder(R.drawable.map_demo).into(iv_ridedetail_map)

                                        }
                                    }
                                    //------------Button Change Function-------

                                } else if (itemlist[0].getRideStatus().equals("Cancelled") || itemlist[0].getRideStatus().equals(
                                        "cancelled"
                                    )
                                ) {

                                    tv_trip_details_driver_rating!!.visibility = View.GONE
                                    tv_trip_details_ride_rating!!.visibility = View.GONE
                                    tv_trip_details_car_type.text =
                                        itemlist[0].getCarType().toString()
                                    tv_trip_details_vehicle_no.text =
                                        itemlist[0].getVehicle_model().toString()+"  "+ itemlist[0].getVehicle_number().toString()
                                    tv_trip_details_time.text =
                                        itemlist[0].getPickup_date().toString()
                                    if (isFareAvailable) {
                                        tv_trip_details_amount.text =
                                            "$${"0"}"
                                    } else {
                                        tv_trip_details_amount.text = "Cancelled "
                                        tv_trip_details_amount.setTextColor(resources.getColor(R.color.red))
                                    }


                                } else if (itemlist[0].getRideStatus().equals("Booked") || itemlist[0].getRideStatus().equals(
                                        "booked"
                                    )
                                ) {
                                    tv_trip_details_car_type.text =
                                        itemlist[0].getCarType().toString()
                                    tv_trip_details_vehicle_no.text = " - "
                                    tv_trip_details_time.text =
                                        itemlist[0].getPickup_date().toString()
                                    if (isFareAvailable) {
                                        tv_trip_details_amount.text =
                                            "$${itemlist[0].getGrand_bill().toString()}"
                                    } else {
                                        tv_trip_details_amount.text = "$ -- "
                                    }


                                }
                            }else{
                                if (itemlist[0].getRideStatus().equals("Cancelled") || itemlist[0].getRideStatus().equals(
                                        "cancelled"
                                    )
                                ) {

                                    tv_trip_details_driver_rating!!.visibility = View.GONE
                                    tv_trip_details_ride_rating!!.visibility = View.GONE
                                    tv_trip_details_car_type.text =
                                        itemlist[0].getCarType().toString()
                                    tv_trip_details_vehicle_no.text =
                                        itemlist[0].getVehicle_number().toString()
                                    tv_trip_details_time.text =
                                        itemlist[0].getPickup_date().toString()
                                    if (isFareAvailable) {
                                        tv_trip_details_amount.text =
                                            "$${"0"}"
                                    } else {
                                        tv_trip_details_amount.text = "Cancelled"
                                        tv_trip_details_amount.setTextColor(resources.getColor(R.color.red))
                                    }
                                }



                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                        Log.e("RIDEDETAILEXCEPTION",e.toString())
                    }

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }



    private fun postRequest_HelpCategory(Url: String) {
       val dialog = Dialog(this)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------HELPCATEGORY Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        mRequest = ServiceRequest(this)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    dialog.dismiss()
                    println("-------------HELPCATEGORY Response----------------$response")

                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            //val response_object = `object`.getJSONObject("response")
                            if (`object`.length() > 0) {
                                val category_object = `object`.get("data")
                                if (category_object is JSONArray) {
                                    val category_data_array = `object`.getJSONArray("data")
                                    if (category_data_array.length() > 0) {
                                        for (i in 0 until category_data_array.length()) {
                                            val catdata_object = category_data_array.getJSONObject(i)
                                            Log.d("ride_object", "" + catdata_object)
                                            val pojo = HelpCategoryPojo()
                                            pojo.setId(catdata_object.getString("id"))
                                            pojo.setCategory(catdata_object.getString("category"))
                                            pojo.setStatus(catdata_object.getString("status"))
                                            itemlist_all.add(pojo)

                                        }
                                    }
                                }

                            }
                        }
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val layoutManager = LinearLayoutManager(
                                this@RideHistoryDetailsActivity,
                                RecyclerView.VERTICAL,
                                false
                            )
                            rv_helpcategory.layoutManager = layoutManager
                            val adapter = HelpCategoryAdapter(this@RideHistoryDetailsActivity, itemlist_all)
                            rv_helpcategory.adapter = adapter


                        } else {
                            val Sresponse = `object`.getString("response")
                            //alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                        dialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        dialog.dismiss()
                    }

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    companion object {
        // private var isInternetPresent: Boolean? = false
        lateinit internal var myrideDetail_class: RideHistoryDetailsActivity

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }

        //code to Check Email Validation
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }


    private fun getLocale(strCode: String): Locale? {
        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }

    //--------------alert Method-----------
    private fun alert(title: String, alert: String) {

        val mDialog = PkDialog(this)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slidedown, R.anim.slideup)
    }


    //----------Method for Invoice Email--------
    private fun mailInvoice() {


        completejob_dialog = Dialog(this@RideHistoryDetailsActivity)
        val view = LayoutInflater.from(this@RideHistoryDetailsActivity)
            .inflate(R.layout.mail_invoice_popup, null)
        completejob_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val Bt_Submit = view.findViewById(R.id.jsharetrip_popup_submit) as LinearLayout
        val Bt_Cancel = view.findViewById(R.id.sharetrip_popup_cancel) as LinearLayout
        val btn_close = view.findViewById(R.id.iv_share_close) as ImageView
        completejob_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        completejob_dialog!!.setContentView(view)
        completejob_dialog!!.show()
        //completejob_dialog!!.setView(view).show()
        Bt_Submit.setOnClickListener {
            if (!isValidEmail(Et_dialog_InvoiceEmail!!.text.toString())) {
                erroredit(
                    Et_dialog_InvoiceEmail!!,
                    resources.getString(R.string.register_label_alert_email)
                )
            } else {

                postRequest_EmailInvoice(
                    Iconstant.myride_details_inVoiceEmail_url,
                    Et_dialog_InvoiceEmail!!.text.toString(),
                    rideID
                )
                /* if (isInternetPresent!!) {
                     postRequest_EmailInvoice(Iconstant.myride_details_inVoiceEmail_url, Et_dialog_InvoiceEmail!!.text.toString(), rideID)
                 } else {
                     Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                 }*/
            }
        }
        Bt_Cancel.setOnClickListener { completejob_dialog!!.dismiss() }
        btn_close.setOnClickListener { completejob_dialog!!.dismiss() }

        Et_dialog_InvoiceEmail = view.findViewById(R.id.mail_invoice_email_edittext) as EditText
        Et_dialog_InvoiceEmail!!.addTextChangedListener(mailInvoice_EditorWatcher)

        Et_dialog_InvoiceEmail!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(
                    Et_dialog_InvoiceEmail!!.applicationWindowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
            false
        }
    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@RideHistoryDetailsActivity, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@RideHistoryDetailsActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
            mDialog.dismiss()

            if (completejob_dialog != null) {
                completejob_dialog!!.dismiss()
            }
        })
        mDialog.show()

    }


    //-----------------------MyRide Email Invoice Post Request-----------------
    private fun postRequest_EmailInvoice(Url: String, Semail: String, SrideId: String) {
        dialog = Dialog(this@RideHistoryDetailsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_sending_invoice)

        println("-------------MyRide Email Invoice Url----------------$Url" + "-----" + SrideId + "-----" + Semail)
        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = Semail
        jsonParams["ride_id"] = SrideId

        mRequest = ServiceRequest(this@RideHistoryDetailsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    var Sstatus = ""
                    var Sresponse = ""
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Sresponse = `object`.getString("response")
                        println("-------------MyRide Email Invoiceresponse----------------$Sstatus$Sresponse")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            completejob_dialog!!.dismiss()
                            Alert(resources.getString(R.string.action_success), Sresponse)
                        } else {
                            Sresponse = `object`.getString("response")
                            Alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }
                        completejob_dialog!!.dismiss()
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    private fun shareTrip() {
        completejob_dialog = Dialog(this@RideHistoryDetailsActivity)
        val view = LayoutInflater.from(this@RideHistoryDetailsActivity)
            .inflate(R.layout.share_trip_popup, null)
        completejob_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        Et_share_trip_mobileno = view.findViewById(R.id.sharetrip_mobilenoEt)
        val Bt_Submit = view.findViewById(R.id.jsharetrip_popup_submit) as LinearLayout
        val Bt_Cancel = view.findViewById(R.id.sharetrip_popup_cancel) as LinearLayout
        val btn_close = view.findViewById(R.id.iv_share_close) as ImageView
        completejob_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        completejob_dialog!!.setContentView(view)
        completejob_dialog!!.show()
        //completejob_dialog!!.setView(view).show()
        Bt_Submit.setOnClickListener {
            if (Et_share_trip_mobileno!!.text.toString().length == 0) {
                Alert(
                    resources.getString(R.string.alert_label_title),
                    resources.getString(R.string.profile_lable_error_mobile)
                )
            } else {

                share_trip_postRequest_MyRides(
                    this@RideHistoryDetailsActivity,
                    Iconstant.share_trip_url,
                    "jobcomplete"
                )
                /* if (isInternetPresent!!) {
                     share_trip_postRequest_MyRides(this@RideHistoryDetailsActivity, Iconstant.share_trip_url, "jobcomplete")
                 } else {
                     Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                 }*/
            }
        }

        Bt_Cancel.setOnClickListener { completejob_dialog!!.dismiss() }
        btn_close.setOnClickListener { completejob_dialog!!.dismiss() }

    }


    //----------------------------------Share Trip post reques------------------------
    private fun share_trip_postRequest_MyRides(mContext: Context, url: String, key: String) {
        dialog = Dialog(this@RideHistoryDetailsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("------------- ride_id----------------$rideID")
        println("------------- mobile_no----------------" + Et_share_trip_mobileno!!.text.toString())

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = rideID
        jsonParams["mobile_no"] = Et_share_trip_mobileno!!.text.toString()

        mRequest = ServiceRequest(this@RideHistoryDetailsActivity)
        mRequest!!.makeServiceRequest(
            url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    Log.e("share trip", response)

                    var Str_status = ""
                    var Str_response = ""
                    println("sharetrip response-------------$response")
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        Str_response = `object`.getString("response")

                        if (Str_status.equals("1", ignoreCase = true)) {

                            Alert(resources.getString(R.string.action_success), Str_response)

                        } else {
                            Alert(resources.getString(R.string.action_error), Str_response)

                        }

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                       // e.printStackTrace()
                    }

                    dialog.dismiss()
                }

                override fun onErrorListener() {

                    dialog.dismiss()

                }
            })

    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private fun postRequest_CancelRides_Reason(Url: String) {
        dialog = Dialog(this@RideHistoryDetailsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)


        println("-------------MyRide Cancel Reason Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID

        mRequest = ServiceRequest(this@RideHistoryDetailsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------MyRide Cancel Reason Response----------------$response")

                    var Sstatus = ""

                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            if (response_object.length() > 0) {

                                val check_reason_object = response_object.get("reason")
                                if (check_reason_object is JSONArray) {

                                    val reason_array = response_object.getJSONArray("reason")
                                    if (reason_array.length() > 0) {
                                        itemlist_reason.clear()
                                        for (i in 0 until reason_array.length()) {
                                            val reason_object = reason_array.getJSONObject(i)
                                            val pojo = CancelTripPojo()
                                            pojo.setReason(reason_object.getString("reason"))
                                            pojo.setReasonId(reason_object.getString("id"))

                                            itemlist_reason.add(pojo)
                                        }

                                        isReasonAvailable = true
                                    } else {
                                        isReasonAvailable = false
                                    }
                                }
                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            Alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }


                        if (Sstatus.equals("1", ignoreCase = true) && isReasonAvailable) {

                            val passIntent = Intent(
                                this@RideHistoryDetailsActivity,
                                MyRideCancelTrip::class.java
                            )
                            val bundleObject = Bundle()
                            bundleObject.putSerializable("Reason", itemlist_reason)
                            passIntent.putExtras(bundleObject)
                            passIntent.putExtra("RideID", rideID)
                            startActivity(passIntent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    //----------Method to Send Email--------
    protected fun sendEmail() {
        val TO = arrayOf("info@zoplay.com")
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "message/rfc822"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message")
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(
                this@RideHistoryDetailsActivity,
                "There is no email client installed.",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    //-----------------------Track Ride Post Request-----------------
    private fun postRequest_TrackRide(Url: String) {
        dialog = Dialog(this@RideHistoryDetailsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)

        println("-------------Track Ride Url----------------$Url")
        println("-------------Track Ride ride_id----------------$rideID")

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = rideID

        mRequest = ServiceRequest(this@RideHistoryDetailsActivity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------Track Ride Response----------------$response")
                var Sstatus = ""
                var driverID = ""
                var driverName = ""
                var driverImage = ""
                var driverRating = ""
                var driverLat = ""
                var driverLong = ""
                var driverTime = ""
                var rideID = ""
                var driverMobile = ""
                var driverCar_no = ""
                var driverCar_model = ""
                var userLat = ""
                var userLong = ""
                var sRideStatus = ""

                var sPickUpLocation = ""
                var sPickUpLatitude = ""
                var sPickUpLongitude = ""
                var sDropLocation = ""
                var sDropLatitude = ""
                var sDropLongitude = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val driver_profile_object = response_object.getJSONObject("driver_profile")
                            if (driver_profile_object.length() > 0) {
                                driverID = driver_profile_object.getString("driver_id")
                                driverName = driver_profile_object.getString("driver_name")
                                driverImage = driver_profile_object.getString("driver_image")
                                driverRating = driver_profile_object.getString("driver_review")
                                driverLat = driver_profile_object.getString("driver_lat")
                                driverLong = driver_profile_object.getString("driver_lon")
                                driverTime = driver_profile_object.getString("min_pickup_duration")
                                rideID = driver_profile_object.getString("ride_id")
                                driverMobile = driver_profile_object.getString("phone_number")
                                driverCar_no = driver_profile_object.getString("vehicle_number")
                                driverCar_model = driver_profile_object.getString("vehicle_model")
                                userLat = driver_profile_object.getString("rider_lat")
                                userLong = driver_profile_object.getString("rider_lon")
                                sRideStatus = driver_profile_object.getString("ride_status")

                                val check_pickUp_object = driver_profile_object.get("pickup")
                                if (check_pickUp_object is JSONObject) {
                                    val pickup_object = driver_profile_object.getJSONObject("pickup")
                                    if (pickup_object.length() > 0) {
                                        sPickUpLocation = pickup_object.getString("location")
                                        val latLong_object = pickup_object.getJSONObject("latlong")
                                        if (latLong_object.length() > 0) {
                                            sPickUpLatitude = latLong_object.getString("lat")
                                            sPickUpLongitude = latLong_object.getString("lon")

                                            isRidePickUpAvailable = true
                                        } else {
                                            isRidePickUpAvailable = false
                                        }
                                    } else {
                                        isRidePickUpAvailable = false
                                    }
                                } else {
                                    isRidePickUpAvailable = false
                                }




                                val check_drop_object = driver_profile_object.get("drop")
                                if (check_drop_object is JSONObject) {
                                    val drop_object = driver_profile_object.getJSONObject("drop")
                                    if (drop_object.length() > 0) {
                                        sDropLocation = drop_object.getString("location")
                                        val latLong_object = drop_object.getJSONObject("latlong")
                                        if (latLong_object.length() > 0) {
                                            sDropLatitude = latLong_object.getString("lat")
                                            sDropLongitude = latLong_object.getString("lon")

                                            isRideDropAvailable = true
                                        } else {
                                            isRideDropAvailable = false
                                        }
                                    } else {
                                        isRideDropAvailable = false
                                    }
                                } else {
                                    isRideDropAvailable = false
                                }

                                isTrackRideAvailable = true
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                    if (Sstatus.equals("1", ignoreCase = true) && isTrackRideAvailable) {

                        val i = Intent(this@RideHistoryDetailsActivity, MyRideDetailTrackRide::class.java)
                        i.putExtra("driverID", driverID)
                        i.putExtra("driverName", driverName)
                        i.putExtra("driverImage", driverImage)
                        i.putExtra("driverRating", driverRating)
                        i.putExtra("driverLat", driverLat)
                        i.putExtra("driverLong", driverLong)
                        i.putExtra("driverTime", driverTime)
                        i.putExtra("rideID", rideID)
                        i.putExtra("driverMobile", driverMobile)
                        i.putExtra("driverCar_no", driverCar_no)
                        i.putExtra("driverCar_model", driverCar_model)
                        i.putExtra("userLat", userLat)
                        i.putExtra("userLong", userLong)
                        i.putExtra("rideStatus", sRideStatus)

                        if (isRidePickUpAvailable) {
                            i.putExtra("PickUpLocation", sPickUpLocation)
                            i.putExtra("PickUpLatitude", sPickUpLatitude)
                            i.putExtra("PickUpLongitude", sPickUpLongitude)
                        }

                        if (isRideDropAvailable) {
                            i.putExtra("DropLocation", sDropLocation)
                            i.putExtra("DropLatitude", sDropLatitude)
                            i.putExtra("DropLongitude", sDropLongitude)
                        }

                        startActivity(i)
                        overridePendingTransition(R.anim.enter, R.anim.exit)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }




}