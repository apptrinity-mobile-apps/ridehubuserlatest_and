package com.project.app.ridehubuser.Adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.app.ridehubuser.Activities.HelpSubCategoryActivity
import com.project.app.ridehubuser.PojoResponse.HelpCategoryPojo
import com.project.app.ridehubuser.R
import java.util.*


@SuppressLint("SetTextI18n")
class HelpCategoryAdapter(private val context: Context, private val data: ArrayList<HelpCategoryPojo>) :
    RecyclerView.Adapter<HelpCategoryAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_help_category, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (data[position].getCategory().equals("null")) {
            holder.tv_help_trip_issues.text = ""
        } else {
            holder.tv_help_trip_issues.text = data[position].getCategory()
        }


        holder.ll_category.setOnClickListener {
            val intent = Intent(context, HelpSubCategoryActivity::class.java)
            intent.putExtra("id", data[position].getId())
            intent.putExtra("category_name", data[position].getCategory())
            context.startActivity(intent)
            (context as Activity).overridePendingTransition(R.anim.enter, R.anim.exit)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_help_trip_issues: TextView = view.findViewById(R.id.tv_help_trip_issues)
        var ll_category: LinearLayout = view.findViewById(R.id.ll_category)


    }
}
