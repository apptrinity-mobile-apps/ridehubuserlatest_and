package com.project.app.ridehubuser.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.project.app.mylibrary.customRatingBar.BaseRatingBar
import com.project.app.mylibrary.customRatingBar.RotationRatingBar
import com.project.app.ridehubuser.PojoResponse.RatingPojo
import com.project.app.ridehubuser.R
import java.util.*

class RatingAdapter(private val context: Context, private val data: ArrayList<RatingPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var title: TextView? = null
        internal var rating: RotationRatingBar? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item_myrating, parent, false)
            holder = ViewHolder()
            holder?.title = view.findViewById(R.id.myride_rating_single_title) as TextView
            holder.rating = view.findViewById(R.id.myride_rating_single_ratingbar) as RotationRatingBar
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.title!!.setText(data[position].getRatingName())



        holder.rating!!.setOnRatingChangeListener(object:BaseRatingBar.OnRatingChangeListener{
            override fun onRatingChange(
                ratingBar: BaseRatingBar,
                rating: Float,
                fromUser: Boolean
            ) {
                data[position].setRatingcount(rating.toString())
            }

        })


        //holder.rating!!.setOnRatingChangeListener() = RotationRatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser -> data[position].setRatingcount(rating.toString()) }

        return view
    }
}