package com.project.app.ridehubuser.Utils;

public interface ProgressListener {
    public void onProgressChanged(boolean state);
}
