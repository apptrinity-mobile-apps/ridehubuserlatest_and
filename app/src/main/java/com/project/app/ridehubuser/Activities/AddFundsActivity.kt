package com.project.app.ridehubuser.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.AddFundsAdapter
import com.project.app.ridehubuser.Adapters.MyRideStripeCardListAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.StripeCardListPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.stripe.android.PaymentConfiguration
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

@SuppressLint("SetTextI18n")
class AddFundsActivity : ActivityHockeyApp() {

    val PUBLISHABLE_KEY = "pk_test_0I3IGKx3mcL97rMJ0Dbt5pm9"

    lateinit var rv_funds: RecyclerView
    lateinit var tv_discounted_percent: TextView
    lateinit var tv_wallet_balance: TextView
    lateinit var tv_add_amount_id: TextView
    lateinit var iv_back_funds: ImageView
    lateinit var iv_close_funds: ImageView
    lateinit var rv_new_cards_list: RecyclerView
    lateinit var id_add_amount_id: TextView


    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    lateinit var userID: String
    lateinit var walletAmount: String
    private var dialog: Dialog? = null
    private var mRequest: ServiceRequest? = null

    lateinit var addFundsAdapter: AddFundsAdapter
    lateinit var itemStripeCardlist: ArrayList<StripeCardListPojo>
    private var card_id = ""
    private var amount_stg = ""

    lateinit var card:Card

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_funds)
        amount_stg=intent.getStringExtra("amount")
        initialize()



        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()
        val wallet = session!!.walletAmount
        walletAmount = wallet[SessionManager.KEY_WALLET_AMOUNT].toString()

        tv_wallet_balance.text ="$ "+ walletAmount
        tv_discounted_percent.text =
            "Add funds and get up to 5% off your purchase" // change after service call

        iv_back_funds.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
            finish()
        }

        iv_close_funds.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        addFundsAdapter = AddFundsAdapter(this/*,*/)
        rv_funds.adapter = addFundsAdapter
        addFundsAdapter.notifyDataSetChanged()
        postRequest_StripeCardList(Iconstant.makepayment_stripe_cardList)

        PaymentConfiguration.init(PUBLISHABLE_KEY)
    }

    private fun initialize() {
        session = SessionManager(this@AddFundsActivity)
        cd = ConnectionDetector(this@AddFundsActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        tv_add_amount_id = findViewById(R.id.tv_add_amount_id)
        rv_funds = findViewById(R.id.rv_funds)
        tv_discounted_percent = findViewById(R.id.tv_discounted_percent)
        tv_wallet_balance = findViewById(R.id.tv_wallet_balance)
        iv_back_funds = findViewById(R.id.iv_back_funds)
        iv_close_funds = findViewById(R.id.iv_close_funds)
        rv_new_cards_list = findViewById(R.id.rv_new_cards_list)
        id_add_amount_id = findViewById(R.id.id_add_amount_id)

        val manager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_funds.layoutManager = manager


        tv_add_amount_id.setOnClickListener{
           // amount_stg
            val intent = Intent(this@AddFundsActivity, StripePayment::class.java)
            intent.putExtra("amount",amount_stg)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)

        }

        id_add_amount_id.text="$ "+amount_stg
    }


    private fun postRequest_StripeCardList(Url: String) {
        itemStripeCardlist = ArrayList()
        itemStripeCardlist.clear()
        dialog = Dialog(this@AddFundsActivity)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        println("-------------StripeCardList Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        mRequest = ServiceRequest(this@AddFundsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------StripeCardList Response----------------$response")
                    var Sstatus = ""
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")


                            if (response_object.length() > 0) {
                                val card_array = response_object.getJSONArray("cardlist")
                                if (card_array.length() > 0) {
                                    itemStripeCardlist!!.clear()
                                    for (i in 0 until card_array.length()) {
                                        val reason_object = card_array.getJSONObject(i)
                                        val pojo = StripeCardListPojo()
                                        pojo.setLast4_digits(reason_object.getString("last4_digits"))
                                        pojo.setCredit_card_type(reason_object.getString("credit_card_type"))
                                        pojo.setExp_year(reason_object.getString("exp_year"))
                                        pojo.setExp_month(reason_object.getString("exp_month"))
                                        pojo.setCard_id(reason_object.getString("card_id"))

                                        itemStripeCardlist!!.add(pojo)
                                    }
                                    // adapter_stripe = MyRideStripeCardListAdapter(this@StripeCardsListFareBreakUp, itemStripeCardlist!!,"farebreakup")
                                    // lv_allcards!!.adapter = adapter_stripe
                                    // isPaymentAvailable = true

                                    val layoutManager = LinearLayoutManager(
                                        this@AddFundsActivity,
                                        RecyclerView.VERTICAL,
                                        false
                                    )
                                    rv_new_cards_list.layoutManager = layoutManager
                                    val adapter = AllCardDeatailsAdapter(
                                        this@AddFundsActivity,
                                        itemStripeCardlist
                                    )
                                    rv_new_cards_list.adapter = adapter
                                    adapter.notifyDataSetChanged()

                                } else {

                                    //tv_nocards!!.visibility = View.VISIBLE
                                    // isPaymentAvailable = false
                                }
                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            //Alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }


                    } catch (e: JSONException) {
                    }

                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    fun AddFunds(url:String,card_token:String){
        //cabily_add_money_url

        dialog = Dialog(this@AddFundsActivity)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["total_amount"] = amount_stg
        jsonParams["stripeToken"] = card_token
        mRequest = ServiceRequest(this@AddFundsActivity)
        mRequest!!.makeServiceRequest(
            url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------PayWallet_Response----------------$response")
                    Alert("Payment Status","Transaction successful")

                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })

    }

    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@AddFundsActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss()

            val intent = Intent(this@AddFundsActivity, NavigationDrawer::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        })
        mDialog.show()
    }

//card_id = itemStripeCardlist!![i].getCard_id().toString()


    inner class AllCardDeatailsAdapter(
        private val context: Context,
        private val data: ArrayList<StripeCardListPojo>
    ) :
        RecyclerView.Adapter<AllCardDeatailsAdapter.ViewHolder>() {
        private val mInflater: LayoutInflater

        init {
            mInflater = LayoutInflater.from(context)
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_payment_card_type, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return data.size
        }


        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {


            holder.reason!!.text = "Card ending with " + data[position].getLast4_digits()

            Log.e("CARDTYPE", data[position].getCredit_card_type())
            //holder.card_type!!.setText(data[position].getCredit_card_type())

            if (data[position].getCredit_card_type().equals("Visa")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.card_type!!.setImageDrawable(context.getDrawable(R.drawable.ic_visa_card))
                }
            } else if (data[position].getCredit_card_type().equals("MasterCard")) {
                holder.card_type!!.setImageDrawable(context.getDrawable(R.drawable.ic_master_card))
            } else {
                holder.card_type!!.setImageDrawable(context.getDrawable(R.drawable.ic_amex_card))
            }

            holder.card_expiry!!.setText(data[position].getExp_month() + " / " + data[position].getExp_year())


            holder.my_ride_stripe_card_check_box!!.visibility = View.GONE
            holder.cv_payment_card.setOnClickListener {

                card_id = data!![position].getCard_id().toString()

               // card=data!![position].getCard_id()

                Log.e("card_id_lll",card_id)

                AddFunds(Iconstant.cabily_add_money_url,card_id)
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            // var tv_help_trip_issues: TextView = view.findViewById(R.id.tv_help_trip_issues)

            internal var reason: TextView = view.findViewById(R.id.myride_payment_list_textview)
            internal var card_type: ImageView =
                view.findViewById(R.id.my_ride_stripe_card_type_image_view)
            internal var card_expiry: TextView =
                view.findViewById(R.id.myride_stripe_card_expiry_textview)
            internal var my_ride_stripe_card_check_box: CheckBox =
                view.findViewById(R.id.my_ride_stripe_card_check_box)
            internal var cv_payment_card: CardView = view.findViewById(R.id.cv_payment_card)


        }
    }


  /*  private fun buy() {
        val validation = card!!.validateCard()
        if (validation) {
            startProgress("Validating Credit Card")
            Stripe(this).createToken(
                card!!,
                PUBLISHABLE_KEY,
                object : TokenCallback {
                    override fun onError(error: Exception) {
                        Log.d("Stripe", error.toString())
                    }

                    override fun onSuccess(token: Token) {
                        finishProgress()
                        // charge(token)
                        Log.e("cardToken_lll", token.getId() + "--" + token.card.customerId + "---" + token.card.last4 + "---" + token.card.expMonth + "---" + token.card.expYear)
                        if (from_stg.equals("load_funds")) {
                            callPaymentAPI(token.getId()!!)
                        } else if (from_stg.equals("services")) {
                            callStripePayServiceAPI(token.getId()!!)
                        }


                    }
                })
        } else if (!card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }*/

}
