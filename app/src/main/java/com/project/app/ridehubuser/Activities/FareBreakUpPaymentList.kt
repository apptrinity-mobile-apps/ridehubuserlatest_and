package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.Request
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.dialog.PkDialog2
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.MyRidePaymentListAdapter
import com.project.app.ridehubuser.PojoResponse.PaymentListPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.project.app.ridehubuser.subclass.ActivitySubClass
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*


class FareBreakUpPaymentList : ActivitySubClass() {


    private var back: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    //private String UserID = "";
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var itemlist: ArrayList<PaymentListPojo>? = null
    private var adapter: MyRidePaymentListAdapter? = null
    private var listview: ExpandableHeightListView? = null
    private var SrideId_intent = ""
    private var TotalAmount = ""
    private var isPaymentAvailable = false
    private var SpaymentCode = ""
    internal lateinit var UserID: String
    internal lateinit var EmailID: String
    internal lateinit var mobilenum: String

    internal var email = ""
    internal var wallet_by_stg = ""
    internal var mobile_no = ""
    internal var mHandler = Handler()
    internal var mRunnable: Runnable = Runnable { MakePayment_Stripe(Iconstant.makepayment_autoDetect_url) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_payment_list)
        myride_paymentList_class = this@FareBreakUpPaymentList

        wallet_by_stg=intent.getStringExtra("wallet_by")

        initialize()
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()
        EmailID = user.get(SessionManager.KEY_EMAIL).toString()
        mobilenum = user.get(SessionManager.KEY_PHONENO).toString()
        back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        listview!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            cd = ConnectionDetector(this@FareBreakUpPaymentList)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                if (itemlist!![position].getPaymentCode().equals("cash")) {
                    Log.e("GATEWAY", "CASH")
                    MakePayment_Cash(Iconstant.makepayment_cash_url)
                } else if (itemlist!![position].getPaymentCode().equals("wallet")) {
                    Log.e("GATEWAY", "wallet")
                    MakePayment_Wallet(Iconstant.makepayment_wallet_url)
                } else if (itemlist!![position].getPaymentCode().equals("auto_detect")) {
                    Log.e("GATEWAY", "auto_detect")
                    MakePayment_Stripe(Iconstant.makepayment_autoDetect_url)
                }else if (itemlist!![position].getPaymentCode().equals("swipe")) {
                    Log.e("GATEWAY", "SWIPE")
                    //MakePayment_Swipe(Iconstant.makepayment_swipe_url)
                    MakePayment_Cash(Iconstant.makepayment_cash_url)
                } else {
                    Log.e("GATEWAY", "WebView_MobileID")
                    SpaymentCode = itemlist!![position].getPaymentCode().toString()
                    MakePayment_WebView_MobileID(Iconstant.makepayment_Get_webview_mobileId_url)
                }
            } else {
                Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
            }
        }
        // makeFareBreakUp(Iconstant.getfareBreakUpURL);
    }

    fun showLoadingDialog() {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    private fun makeFareBreakUp(url: String) {
        showLoadingDialog()
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
          override  fun onCompleteListener(response: String) {
                try {
                    val mainResponse = JSONObject(response)
                    val responseJSON = mainResponse.getJSONObject("response")
                    val driverInfoJSON = mainResponse.getJSONObject("driverinfo")
                    val fareJSON = mainResponse.getJSONObject("fare")
                    val status = mainResponse.getString("status")
                    if ("1".equals(status, ignoreCase = true)) {
                        val stripe_connected = fareJSON.getString("stripe_connected")
                        if ("yes".equals(stripe_connected, ignoreCase = true)) {
                            val payment_timeout = fareJSON.getString("payment_timeout")
                            // showLoadTimer(payment_timeout);
                        }
                    } else {
                        val errorResponse = mainResponse.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), errorResponse)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onErrorListener() {}
        })
    }


    private fun initialize() {
        session = SessionManager(this@FareBreakUpPaymentList)
        cd = ConnectionDetector(this@FareBreakUpPaymentList)
        isInternetPresent = cd!!.isConnectingToInternet
        itemlist = ArrayList<PaymentListPojo>()
        back = findViewById(R.id.my_rides_payment_header_back_layout) as ImageView
        listview = findViewById(R.id.my_rides_payment_listView) as ExpandableHeightListView
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()
        val intent = intent
        SrideId_intent = intent.getStringExtra("RideID")
        TotalAmount = intent.getStringExtra("TotalAmount")

        Log.e("TOTALAMOUNT",TotalAmount)

        if (isInternetPresent!!) {
            postRequest_PaymentList(Iconstant.paymentList_url)
        } else {
            Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@FareBreakUpPaymentList)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------PaymentList Post Request-----------------
    private fun postRequest_PaymentList(Url: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        println("-------------PaymentList Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------PaymentList Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val payment_array = response_object.getJSONArray("payment")
                            if (payment_array.length() > 0) {
                                itemlist!!.clear()
                                for (i in 0 until payment_array.length()) {
                                    val reason_object = payment_array.getJSONObject(i)
                                    val pojo = PaymentListPojo()
                                    if (reason_object.getString("name").equals("CCavenue", ignoreCase = true)) {
                                        pojo.setPaymentName("Pay by Paytm")
                                    } else {
                                        pojo.setPaymentName(reason_object.getString("name"))
                                    }
                                    pojo.setPaymentCode(reason_object.getString("code"))
                                    itemlist!!.add(pojo)
                                }
                                isPaymentAvailable = true
                            } else {
                                isPaymentAvailable = false
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                    if (Sstatus.equals("1", ignoreCase = true) && isPaymentAvailable) {
                        adapter = MyRidePaymentListAdapter(this@FareBreakUpPaymentList, itemlist!!)
                        listview!!.adapter = adapter
                        listview!!.isExpanded = true
                    }

                } catch (e: JSONException) {
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    //-----------------------MakePayment Cash Post Request-----------------
    private fun MakePayment_Cash(Url: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)


        println("-------------MakePayment Cash Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["wallet_check"] = wallet_by_stg
        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {
                println("-------------MakePayment Cash Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog2(this@FareBreakUpPaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            val intent = Intent(this@FareBreakUpPaymentList, NavigationDrawer::class.java)
                           // intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override  fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }



    //-----------------------MakePayment Cash Post Request-----------------
    private fun MakePayment_Swipe(Url: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)


        println("-------------MakePayment Swipe Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["wallet_check"] = wallet_by_stg
        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {
                println("-------------MakePayment Swipe Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog2(this@FareBreakUpPaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            val intent = Intent(this@FareBreakUpPaymentList, NavigationDrawer::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override  fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    //-----------------------MakePayment Wallet Post Request-----------------
    private fun MakePayment_Wallet(Url: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        println("-------------MakePayment Wallet Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent

        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------MakePayment Wallet Response----------------$response")
                var Sstatus = ""
                var Scurrency_code = ""
                var Scurrent_wallet_balance = ""
                var currencycode: Currency? = null
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("0", ignoreCase = true)) {
                        Alert(resources.getString(R.string.my_rides_payment_empty_wallet_sorry), resources.getString(R.string.my_rides_payment_empty_wallet))
                    } else if (Sstatus.equals("1", ignoreCase = true)) {
                        //Updating wallet amount on Navigation Drawer Slide
                        Scurrency_code = `object`.getString("currency")
                        currencycode = Currency.getInstance(getLocale(Scurrency_code))
                        Scurrent_wallet_balance = `object`.getString("wallet_amount")
                        session!!.createWalletAmount(currencycode!!.symbol + Scurrent_wallet_balance)
                        NavigationDrawer.navigationNotifyChange()
                        val mDialog = PkDialog(this@FareBreakUpPaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_wallet_success))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            FareBreakUp.farebreakup_class.finish()
                            val intent = Intent(this@FareBreakUpPaymentList, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else if (Sstatus.equals("2", ignoreCase = true)) {

                        FareBreakUp.invisibleTips()

                        //Updating wallet amount on Navigation Drawer Slide
                        Scurrency_code = `object`.getString("currency")
                        currencycode = Currency.getInstance(getLocale(Scurrency_code))
                        Scurrent_wallet_balance = `object`.getString("wallet_amount")
                        session!!.createWalletAmount(currencycode!!.symbol + Scurrent_wallet_balance)
                        NavigationDrawer.navigationNotifyChange()
                        val broadcastIntent = Intent()
                        broadcastIntent.action = "com.package.ACTION_CLASS_REFRESH"
                        sendBroadcast(broadcastIntent)
                        val mDialog = PkDialog(this@FareBreakUpPaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            postRequest_PaymentList(Iconstant.paymentList_url)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override  fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    //-----------------------MakePayment Auto-Detect Post Request-----------------
    private fun MakePayment_Stripe(Url: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)
        println("-------------MakePayment Auto-Detect Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["wallet_check"] = wallet_by_stg
        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------MakePayment Auto-Detect Response----------------$response")

                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val mDialog = PkDialog(this@FareBreakUpPaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            FareBreakUp.farebreakup_class.finish()
                            val intent = Intent(this@FareBreakUpPaymentList, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    //-----------------------MakePayment WebView-MobileID Post Request-----------------
    private fun MakePayment_WebView_MobileID(Url: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)
        println("-------------PAYTM MakePayment WebView-MobileID Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["gateway"] = SpaymentCode

        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {

                println("-------------PAYTM MakePayment WebView-MobileID Response----------------$response")

                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mobileId = `object`.getString("mobile_id")
                        val intent = Intent(this@FareBreakUpPaymentList, StripeCardsListFareBreakUp::class.java)
                        intent.putExtra("MobileID", mobileId)
                        intent.putExtra("RideID", SrideId_intent)
                        intent.putExtra("wallet_check", wallet_by_stg)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter, R.anim.exit)
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }



    //-----------------------Online Payment Post Request-----------------
    private fun MakePayment_Online(Url: String, response: String) {
        dialog = Dialog(this@FareBreakUpPaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)


        println("-------------Online Payment Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["amount"] = TotalAmount
        jsonParams["online_response"] = response

        mRequest = ServiceRequest(this@FareBreakUpPaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------Online Payment Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog(this@FareBreakUpPaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            FareBreakUp.farebreakup_class.finish()
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            return true
        }
        return false
    }

    companion object {
        lateinit var myride_paymentList_class: FareBreakUpPaymentList

        //method to convert currency code to currency symbol
        fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }
}

