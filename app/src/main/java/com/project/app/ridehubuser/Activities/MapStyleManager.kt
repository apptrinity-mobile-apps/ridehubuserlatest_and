package com.project.app.ridehubuser.Activities

import android.content.Context
import androidx.annotation.RawRes
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MapStyleOptions
import java.util.*

class MapStyleManager private constructor(private val context: Context, private val map: GoogleMap, private val onCameraMoveListener: GoogleMap.OnCameraMoveListener?) : GoogleMap.OnCameraMoveListener {
    private val styleMap = TreeMap<Float, Int>()

    @RawRes
    private var currentMapStyleRes = 0

    init {

        this.map.setOnCameraMoveListener(this)
    }

    override fun onCameraMove() {
        onCameraMoveListener?.onCameraMove()
        updateMapStyle()
    }

    fun addStyle(minZoomLevel: Float, @RawRes mapStyle: Int) {
        this.styleMap[minZoomLevel] = mapStyle
        updateMapStyle()
    }

    private fun updateMapStyle() {
        val cameraPosition = this.map.cameraPosition
        val currentZoomLevel = cameraPosition.zoom

        for (key in this.styleMap.descendingKeySet()) {
            if (currentZoomLevel >= key) {
                val styleId = this.styleMap[key]
                setMapStyle(styleId!!)
                return
            }
        }
    }

    private fun setMapStyle(@RawRes styleRes: Int) {
        if (this.currentMapStyleRes != styleRes) {
            val style = MapStyleOptions.loadRawResourceStyle(this.context, styleRes)
            this.map.setMapStyle(style)
            this.currentMapStyleRes = styleRes
        }
    }

    companion object {

        fun attachToMap(context: Context, map: GoogleMap, onCameraMoveListener: GoogleMap.OnCameraMoveListener?): MapStyleManager {
            return MapStyleManager(context, map, onCameraMoveListener)
        }

        fun attachToMap(context: Context, map: GoogleMap): MapStyleManager {
            return MapStyleManager(context, map, null)
        }
    }

}
