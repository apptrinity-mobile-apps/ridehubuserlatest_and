package com.project.app.ridehubuser.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R

class SingUpAndSignIn : ActivityHockeyApp() {
    /* private var signin: Button? = null
     private var register: Button? = null*/

    private var Str_Hash = ""

    lateinit var iv_logo: ImageView
    lateinit var iv_google: ImageView
    lateinit var iv_facebook: ImageView
    lateinit var iv_twitter: ImageView
    lateinit var tv_login: TextView
    lateinit var tv_register: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.signin_and_signup)
        setContentView(R.layout.activity_signup_register)
        activty = this
        initialize()
        /* signin!!.setOnClickListener {
             signin!!.setTextColor(resources.getColor(R.color.border_color))
             register!!.setTextColor(resources.getColor(R.color.white))
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                 signin!!.background = resources.getDrawable(R.drawable.signup_bg_curved)
             }
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                 register!!.background = resources.getDrawable(R.drawable.signup_bg_curved)
             }
             val i = Intent(this@SingUpAndSignIn, LoginPage::class.java)
             i.putExtra("HashKey", Str_Hash)
             startActivity(i)
             overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
         }*/

        /*register!!.setOnClickListener {
            register!!.setTextColor(getResources().getColor(R.color.border_color))
            signin!!.setTextColor(getResources().getColor(R.color.white))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                signin!!.background = getResources().getDrawable(R.drawable.button_curve_login_background_blue)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                register!!.background = getResources().getDrawable(R.drawable.button_curve_background_blue2)
            }
            val i = Intent(this@SingUpAndSignIn, RegisterPage::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }*/

        tv_register.setOnClickListener {
            val i = Intent(this@SingUpAndSignIn, RegisterPage::class.java)
            i.putExtra("HashKey", Str_Hash)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        tv_login.setOnClickListener {
            val i = Intent(this@SingUpAndSignIn, LoginPage::class.java)
            i.putExtra("HashKey", Str_Hash)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    private fun initialize() {
        /*signin = findViewById<Button>(R.id.signin_main_button)
        register = findViewById<Button>(R.id.register_main_button)*/

        iv_logo = findViewById(R.id.iv_logo)
        iv_google = findViewById(R.id.iv_google)
        iv_facebook = findViewById(R.id.iv_facebook)
        iv_twitter = findViewById(R.id.iv_twitter)
        tv_login = findViewById(R.id.tv_login)
        tv_register = findViewById(R.id.tv_register)

        val i = intent

        if (i.getStringExtra("HashKey") != null) {
            Str_Hash = i.getStringExtra("HashKey")!!
            println("Str_Hash--------------------$Str_Hash")
        }

        /*signin!!.typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf")
        register!!.typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf")*/
    }

    companion object {
        lateinit var activty: SingUpAndSignIn
    }

}
