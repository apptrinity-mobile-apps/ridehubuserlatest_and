package com.project.app.ridehubuser.PojoResponse

import java.text.SimpleDateFormat
import java.util.*

class MyRidesPojo {

    private var ride_id: String? = null
    private var ride_time: String? = null
    private var ride_date: String? = null
    private var pickup: String? = null
    private var drop: String? = null
    internal var ride_status: String? = null
    private var group: String? = null
    private var datetime: String? = null
    private var type: String? = null
    private var distance: String? = null
    private var duration: String? = null
    private var source_lat: String? = null
    private var source_lng: String? = null
    private var destination_lat: String? = null
    private var destination_lng: String? = null
    private var vehicle_model: String? = null
    private var total_fare: String? = null
    private var pickup_long: String? = null
    private var pickup_lat: String? = null
    private var drop_lon: String? = null
    private var drop_lat: String? = null
    private var cab_type: String? = null
    private var vehicle_no: String? = null
    private var driver_rating: String? = null
    private var user_rating: String? = null
    private var image: String? = null


    fun getImage(): String? {
        return image
    }

    fun setImage(image: String) {
        this.image = image
    }


    fun getRide_id(): String? {
        return ride_id
    }

    fun setRide_id(ride_id: String) {
        this.ride_id = ride_id
    }

    fun getRide_time(): String? {
        return ride_time
    }

    fun setRide_time(ride_time: String) {
        this.ride_time = ride_time
    }

    fun getRide_date(): String? {
        return ride_date
    }

    fun setRide_date(ride_date: String) {
        this.ride_date = ride_date
    }

    fun getPickUp(): String? {
        return pickup
    }

    fun setPickup(pickup: String) {
        this.pickup = pickup
    }

    fun getDrop(): String? {
        return drop
    }

    fun setDrop(drop: String) {
        this.drop = drop
    }

    fun getRide_status(): String? {
        return ride_status
    }

    fun setRide_status(ride_status: String) {
        this.ride_status = ride_status
    }

    fun getGroup(): String? {
        return group
    }

    fun setGroup(group: String) {
        this.group = group
    }

    fun getDatetime(): String? {
        return datetime
    }

    fun setDatetime(datetime: String) {
        this.datetime = datetime
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

    fun getDistance(): String? {
        return distance
    }

    fun setDistance(distance: String) {
        this.distance = distance
    }

    fun getDuration(): String? {
        return duration
    }

    fun setDuration(duration: String) {
        this.duration = duration
    }

    fun getSource_lat(): String? {
        return source_lat
    }

    fun setSource_lat(source_lat: String) {
        this.source_lat = source_lat
    }

    fun getSource_lng(): String? {
        return source_lng
    }

    fun setSource_lng(source_lng: String) {
        this.source_lng = source_lng
    }

    fun getDestination_lat(): String? {
        return destination_lat
    }

    fun setDestination_lat(destination_lat: String) {
        this.destination_lat = destination_lat
    }

    fun getDestination_lng(): String? {
        return destination_lng
    }

    fun setDestination_lng(destination_lng: String) {
        this.destination_lng = destination_lng
    }





    fun getUser_rating(): String? {
        return user_rating
    }

    fun setUser_rating(user_rating: String) {
        this.user_rating = user_rating
    }



    fun getDriver_rating(): String? {
        return driver_rating
    }

    fun setDriver_rating(driver_rating: String) {
        this.driver_rating = driver_rating
    }





    fun getVehicle_model(): String? {
        return vehicle_model
    }

    fun setVehicle_model(vehicle_model: String) {
        this.vehicle_model = vehicle_model
    }

    fun getTotal_fare(): String? {
        return total_fare
    }

    fun setTotal_fare(total_fare: String) {
        this.total_fare = total_fare
    }

    fun getPickup_long(): String? {
        return pickup_long
    }

    fun setPickup_long(pickup_long: String) {
        this.pickup_long = pickup_long
    }


    fun getPickup_lat(): String? {
        return pickup_lat
    }

    fun setPickup_lat(pickup_lat: String) {
        this.pickup_lat = pickup_lat
    }


    fun getDrop_lon(): String? {
        return drop_lon
    }

    fun setDrop_lon(drop_lon: String) {
        this.drop_lon = drop_lon
    }


    fun getDrop_lat(): String? {
        return drop_lat
    }

    fun setDrop_lat(drop_lat: String) {
        this.drop_lat = drop_lat
    }

    fun getCab_type(): String? {
        return cab_type
    }

    fun setCab_type(cab_type: String) {
        this.cab_type = cab_type
    }

    fun getVehicle_no(): String? {
        return vehicle_no
    }

    fun setVehicle_no(vehicle_no: String) {
        this.vehicle_no = vehicle_no
    }


    // comparing dates and arranging in HIGH TO LOW order
    var dateWiseComparator: Comparator<MyRidesPojo> = object : Comparator<MyRidesPojo> {
        override fun compare(s1: MyRidesPojo, s2: MyRidesPojo): Int {
            var Sdate1 = ""
            var Sdate2 = ""
            val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
            if (s1.getRide_date()!!.toString().contains("/")) {
                Sdate1 = s1.getRide_date()!!.toString().replace("/", "-")
            } else if (s1.getRide_date()!!.toString().contains("")) {
                // for canceled rides in lyft
                Sdate1 = "00-00-0000"
            } else {
                Sdate1 = s1.getRide_date()!!.toString()
            }

            if (s2.getRide_date()!!.toString().contains("/")) {
                Sdate2 = s2.getRide_date()!!.toString().replace("/", "-")
            } else if (s2.getRide_date()!!.toString().contains("")) {
                // for canceled rides in lyft
                Sdate2 = "00-00-0000"
            } else {
                Sdate2 = s2.getRide_date()!!.toString()
            }
            val date1 = sdf.parse(Sdate1)
            val date2 = sdf.parse(Sdate2)
            return date2!!.compareTo(date1!!)

            /* for LOW TO HIGH order */
            // return date1!!.compareTo(date2!!)

        }
    }

    override fun toString(): String {
        return ride_date!!
    }


}