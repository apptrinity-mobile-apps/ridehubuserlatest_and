package com.project.app.ridehubuser.PojoResponse

class RatingPojo {

    private var ratingName: String? = null
    private var ratingId:String? = null
    private var ratingcount:String? = null

    fun getRatingName(): String? {
        return ratingName
    }

    fun setRatingName(ratingName: String) {
        this.ratingName = ratingName
    }

    fun getRatingId(): String? {
        return ratingId
    }

    fun setRatingId(ratingId: String) {
        this.ratingId = ratingId
    }

    fun getRatingcount(): String? {
        return ratingcount
    }

    fun setRatingcount(ratingcount: String) {
        this.ratingcount = ratingcount
    }



}