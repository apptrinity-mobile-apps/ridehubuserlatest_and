package com.project.app.ridehubuser.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by indobytes21 on 3/13/2017.
 */

public class SprefManager {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context fcontext;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "flexdroid";

    private static final String IS_FIRST_TIME_LAUNCH_ONLY = "IsFirstTimeLaunchonly";

    public SprefManager(Context context) {
        this.fcontext = context;
        sharedPreferences = fcontext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH_ONLY, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunchonly() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH_ONLY, true);
    }
}
