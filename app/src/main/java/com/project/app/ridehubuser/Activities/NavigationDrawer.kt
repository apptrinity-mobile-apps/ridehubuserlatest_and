package com.project.app.ridehubuser.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ListView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.HomeMenuListAdapter
import com.project.app.ridehubuser.Fragments.Fragment_HomePage
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ActionBarActivityRidehubApp
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.view.get
import kotlinx.android.synthetic.main.drawer_list_item.view.*


class NavigationDrawer : ActionBarActivityRidehubApp() {
    internal lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private var mDrawerList: ListView? = null
    private var UserID = ""
    private var UserName = ""
    private var UserMobileno = ""
    private var UserCountyCode = ""
    private var UserEmail = ""
    private var UserorAgent = ""
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    private var title: Array<String>? = null
    private var icon: IntArray? = null
    private var session: SessionManager? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    //internal var bookmyride: Fragment = Fragment_HomePage()

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_navigationdrawer)
        context = applicationContext
        session = SessionManager(this@NavigationDrawer)
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID]!!
        UserName = user[SessionManager.KEY_USERNAME]!!
        UserMobileno = user[SessionManager.KEY_PHONENO]!!
        UserEmail = user[SessionManager.KEY_EMAIL]!!
        UserCountyCode = user[SessionManager.KEY_COUNTRYCODE]!!
        UserorAgent = user[SessionManager.KEY_AGENT]!!
        println("---------------UserorAgent-----------------$UserorAgent")
        drawerLayout = findViewById(
            R.id.navigation_drawer
        ) as DrawerLayout
        mDrawer = findViewById(
            R.id.drawer
        ) as RelativeLayout
        mDrawerList = findViewById(R.id.drawer_listview) as ListView

        if (savedInstanceState == null) {
            val tx = supportFragmentManager.beginTransaction()
            tx.replace(R.id.content_frame, Fragment_HomePage())
            tx.commit()
        }

        actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            R.string.app_name,
            R.string.app_name
        )
        drawerLayout.setDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        title = arrayOf(
            "username",
            resources.getString(R.string.navigation_label_bookaride),
            resources.getString(R.string.navigation_label_offline),
            resources.getString(R.string.navigation_label_ridehistory),
            resources.getString(R.string.navigation_label_freerides),
            resources.getString(R.string.navigation_label_payment),
            resources.getString(R.string.navigation_label_help),
            resources.getString(R.string.navigation_label_settings)
        )
        icon = intArrayOf(
            R.drawable.menu_get_a_ride, R.drawable.menu_get_a_ride,
            R.drawable.menu_offline_ride, R.drawable.menu_ridehistory,
            R.drawable.menu_freerides, R.drawable.menu_payment,
            R.drawable.menu_help, R.drawable.menu_settings
        )
        mMenuAdapter = HomeMenuListAdapter(
            context!!, title!!, icon!!
        )
        mDrawerList!!.adapter =
            mMenuAdapter
        mMenuAdapter!!.notifyDataSetChanged()

        mDrawerList!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                cd = ConnectionDetector(this@NavigationDrawer)
                isInternetPresent = cd!!.isConnectingToInternet
                val ft = supportFragmentManager.beginTransaction()
                 when (position) {
                     0 -> {
                         val intent = Intent(this@NavigationDrawer, ProfilePageActivity::class.java)
                         startActivity(intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     }
                     1 -> {
                         val tx = supportFragmentManager.beginTransaction()
                         tx.replace(R.id.content_frame, Fragment_HomePage())
                         tx.commit()
                     }
                     2 -> if (isInternetPresent!!) {
                         val intent = Intent(this@NavigationDrawer, OfflineActivity::class.java)
                         startActivity(intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     } else {
                         Alert(resources.getString(R.string.alert_label_title), resources.getString(
                             R.string.alert_nointernet))
                     }
                     3 -> if (isInternetPresent!!) {
                         val myRide_intent = Intent(this@NavigationDrawer, RideHistoryActivity::class.java)
                         startActivity(myRide_intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     } else {
                         Alert(resources.getString(R.string.alert_label_title), resources.getString(
                             R.string.alert_nointernet))
                     }
                     4 -> if (isInternetPresent!!) {
                         val invite_intent = Intent(this@NavigationDrawer, InviteAndEarnActivity::class.java)
                         startActivity(invite_intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     } else {
                         Alert(resources.getString(R.string.alert_label_title), resources.getString(
                             R.string.alert_nointernet))
                     }
                     5 -> if (isInternetPresent!!) {
                         val intent = Intent(this@NavigationDrawer, Payment::class.java)
                         startActivity(intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     } else {
                         Alert(resources.getString(R.string.alert_label_title), resources.getString(
                             R.string.alert_nointernet))
                     }
                     6 -> if (isInternetPresent!!) {
                         val intent = Intent(this@NavigationDrawer, HelpActivity::class.java)
                         startActivity(intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     } else {
                         Alert(resources.getString(R.string.alert_label_title), resources.getString(
                             R.string.alert_nointernet))
                     }
                     7 -> if (isInternetPresent!!){
                         val intent = Intent(this@NavigationDrawer, SettingsActivity::class.java)
                         startActivity(intent)
                         overridePendingTransition(R.anim.enter, R.anim.exit)
                     } else{
                         Alert(resources.getString(R.string.alert_label_title), resources.getString(
                             R.string.alert_nointernet))
                     }
                 }

                ft.commit()
                mDrawerList!!.setItemChecked(position, true)
                drawerLayout!!.closeDrawer(
                    mDrawer!!
                )
            }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

	        //noinspection SimplifiableIfStatement
	        if (id == R.id.action_settings) {
	            return true;
	        }*/
        return super.onOptionsItemSelected(item)
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@NavigationDrawer)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //-----------------------Logout Request-----------------
    private fun postRequest_Logout(Url: String) {
        showDialog(resources.getString(R.string.action_logging_out))
        println("---------------LogOut Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["device"] = "ANDROID"

        mRequest = ServiceRequest(this@NavigationDrawer)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------LogOut Response-----------------$response")
                    var Sstatus = ""
                    var Sresponse = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Sresponse = `object`.getString("response")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        logoutFromFacebook()
                        session!!.logoutUser()
                        val local = Intent()
                        local.action = "com.app.logout"
                        this@NavigationDrawer.sendBroadcast(local)

                        onBackPressed()
                        // overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    } else {
                        Alert(resources.getString(R.string.action_error), Sresponse)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    //--------------Show Dialog Method-----------
    private fun showDialog(data: String) {
        dialog = Dialog(this@NavigationDrawer)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = data
    }

    fun logoutFromFacebook() {
        // your sharedPrefrence
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }

    companion object {
        internal lateinit var drawerLayout: DrawerLayout
        internal var mDrawer: RelativeLayout? = null
        internal var mMenuAdapter: HomeMenuListAdapter? = null

        private var context: Context? = null

        fun openDrawer() {
            drawerLayout.openDrawer(
                mDrawer!!
            )
        }

        fun disableSwipeDrawer() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }

        fun enableSwipeDrawer() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }


        fun navigationNotifyChange() {
            mMenuAdapter!!.notifyDataSetChanged()
        }
    }


    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

}