package com.project.app.ridehubuser.Activities

import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager

class DriverDetailsActivity : ActivityHockeyApp() {

    private lateinit var ll_driver_details: LinearLayout
    private lateinit var iv_close_driver_profile: ImageView
    private lateinit var iv_setting_driver_profile: ImageView
    private lateinit var tv_driver_first_name: TextView
    private lateinit var tv_driver_last_name: TextView
    private lateinit var cv_driver_details: CardView
    private lateinit var tv_driver_loc: TextView
    private lateinit var tv_driver_lang: TextView
    private lateinit var tv_driver_status: TextView
    private lateinit var tv_driver_quote: TextView
    private lateinit var tv_driver_trips: TextView
    private lateinit var tv_driver_rating: TextView
    private lateinit var tv_driver_years: TextView
    private lateinit var ll_driver_profile: LinearLayout
    private lateinit var cv_driver_fun_fact: CardView
    private lateinit var iv_fun_fact_1: ImageView
    private lateinit var tv_fun_fact: TextView
    private lateinit var cv_driver_exp: CardView
    private lateinit var iv_exp_1: ImageView
    private lateinit var tv_exp: TextView
    private lateinit var cv_driver_drive: CardView
    private lateinit var iv_drive_1: ImageView
    private lateinit var tv_drive: TextView
    private lateinit var ll_driver_compliments: LinearLayout
    private lateinit var rv_achievements: RecyclerView
    private lateinit var rv_compliments: RecyclerView
    private lateinit var ll_driver_achievements: LinearLayout
    private lateinit var rv_compliments_1: RecyclerView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var userID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_profile)

        initialize()

        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID]!!

        iv_close_driver_profile.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

    }

    private fun initialize() {
        session = SessionManager(this@DriverDetailsActivity)
        cd = ConnectionDetector(this@DriverDetailsActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_close_driver_profile = findViewById(R.id.iv_close_driver_profile)
        iv_setting_driver_profile = findViewById(R.id.iv_setting_driver_profile)
        tv_driver_first_name = findViewById(R.id.tv_driver_first_name)
        tv_driver_last_name = findViewById(R.id.tv_driver_last_name)
        cv_driver_details = findViewById(R.id.cv_driver_details)
        tv_driver_loc = findViewById(R.id.tv_driver_loc)
        tv_driver_lang = findViewById(R.id.tv_driver_lang)
        tv_driver_status = findViewById(R.id.tv_driver_status)
        tv_driver_quote = findViewById(R.id.tv_driver_quote)
        tv_driver_trips = findViewById(R.id.tv_driver_trips)
        tv_driver_rating = findViewById(R.id.tv_driver_rating)
        tv_driver_years = findViewById(R.id.tv_driver_years)
        ll_driver_profile = findViewById(R.id.ll_driver_profile)
        ll_driver_details = findViewById(R.id.ll_driver_details)
        cv_driver_fun_fact = findViewById(R.id.cv_driver_fun_fact)
        iv_fun_fact_1 = findViewById(R.id.iv_fun_fact_1)
        tv_fun_fact = findViewById(R.id.tv_fun_fact)
        cv_driver_exp = findViewById(R.id.cv_driver_exp)
        iv_exp_1 = findViewById(R.id.iv_exp_1)
        tv_exp = findViewById(R.id.tv_exp)
        cv_driver_drive = findViewById(R.id.cv_driver_drive)
        iv_drive_1 = findViewById(R.id.iv_drive_1)
        tv_drive = findViewById(R.id.tv_drive)
        ll_driver_compliments = findViewById(R.id.ll_driver_compliments)
        ll_driver_achievements = findViewById(R.id.ll_driver_achievements)
        rv_achievements = findViewById(R.id.rv_achievements)
        rv_compliments = findViewById(R.id.rv_compliments)
        rv_compliments_1 = findViewById(R.id.rv_compliments_1)

        val manager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        val manager1 = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        val manager2 = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        rv_achievements.layoutManager = manager
        rv_compliments.layoutManager = manager1
        rv_compliments_1.layoutManager = manager2

    }

}
