package com.project.app.ridehubuser.PojoResponse

class HelpSubCategoryPojo {

    private var id: String? = null
    private var sub_category_id: String? = null
    private var category: String? = null
    private var sub_category: String? = null
    private var status: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String) {
        this.category = category
    }


    fun getSubcategoryId(): String? {
        return sub_category_id
    }

    fun setSubCategoryId(sub_category_id: String) {
        this.sub_category_id = sub_category_id
    }


    fun getSubCategory(): String? {
        return sub_category
    }

    fun setSubCategory(sub_category: String) {
        this.sub_category = sub_category
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }


}