package com.project.app.ridehubuser.Activities

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.CurrencySymbolConverter
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class InviteAndEarnActivity : ActivityHockeyApp(), View.OnClickListener {
    override fun onClick(v: View?) {

    }

    internal val PERMISSION_REQUEST_CODE = 111
    internal lateinit var Rl_whatsApp: LinearLayout
    internal lateinit var Rl_messenger: LinearLayout
    internal lateinit var Rl_sms: LinearLayout
    internal lateinit var Rl_email: LinearLayout
    internal lateinit var Rl_twitter: LinearLayout
    internal lateinit var Rl_facebook: LinearLayout
    internal lateinit var ll_invite_code: LinearLayout
    internal lateinit var ll_invite_works_detail: LinearLayout
    internal lateinit var ll_shareinvitecode: LinearLayout
    internal lateinit var tv_invite_code_works: TextView
    internal var sCurrencySymbol = ""
    internal lateinit var tv_invite_now: TextView
    internal lateinit var tv_shareride: TextView
    private var back: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var Tv_friends_earn: TextView? = null
    private var Tv_you_earn: TextView? = null
    private var Tv_referral_code: TextView? = null
    //private RelativeLayout Rl_whatsApp, Rl_messenger, Rl_sms, Rl_email, Rl_twitter, Rl_facebook;
    private var mRequest: ServiceRequest? = null
    private var UserID = ""
    private var isdataPresent = false
    private var Sstatus = ""
    private var friend_earn_amount = ""
    private var you_earn_amount = ""
    private var friends_rides = ""
    private var ScurrencyCode = ""
    private var referral_code = ""
    private var sShareLink = ""
    private val package_name = "com.project.app.ridehubuser"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_free_rides);

        initialize()

        //Start XMPP Chat Service
        try{
            ChatService.startUserAction(this@InviteAndEarnActivity)
        }catch (e:Exception){
            e.printStackTrace()
        }

        back!!.setOnClickListener {
            if (ll_invite_works_detail.isVisible) {
                ll_invite_code.visibility = View.VISIBLE
                ll_invite_works_detail.visibility = View.GONE
            } else {
                onBackPressed()
                finish()
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
        }

        tv_invite_code_works.setOnClickListener {
            ll_invite_code.visibility = View.GONE
            ll_invite_works_detail.visibility = View.VISIBLE
        }
    }

    private fun initialize() {
        session = SessionManager(this@InviteAndEarnActivity)
        cd = ConnectionDetector(this@InviteAndEarnActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById(R.id.iv_free_rides_close) as ImageView
        /*Tv_friends_earn = findViewById(R.id.invite_earn_friend_earn_textview) as TextView
        Tv_you_earn = findViewById(R.id.invite_earn_you_earn_textview) as TextView*/
        Tv_referral_code = findViewById(R.id.tv_invite_code) as TextView
        // tv_invite_now = findViewById(R.id.tv_invite_now) as TextView
        ll_shareinvitecode = findViewById(R.id.tv_invite_friends) as LinearLayout
        tv_shareride = findViewById(R.id.tv_share_invite_amount) as TextView
        tv_invite_code_works = findViewById(R.id.tv_invite_code_works) as TextView
        ll_invite_works_detail = findViewById(R.id.ll_invite_works_detail) as LinearLayout
        ll_invite_code = findViewById(R.id.ll_invite_code) as LinearLayout


        val relative =
            "Share the Ride Hub love and give friends free rides to try Ride, Worth up to $0"

        tv_shareride.text = relative


// get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()

        if (isInternetPresent!!) {
            displayInvite_Request(Iconstant.invite_earn_friends_url)
        } else {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.alert_nointernet)
            )
        }


        ll_shareinvitecode.setOnClickListener(this)

        ll_shareinvitecode.setOnClickListener {
            val text =
                getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(
                    R.string.invite_earn_label_share_messgae2
                ) + " " + referral_code + " to ride free.Enjoy! " + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"
            whatsApp_sendMsg(text)
        }


    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@InviteAndEarnActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            getResources().getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------Sending message on WhatsApp Method------
    private fun whatsApp_sendMsg(text: String) {
        val pm = this@InviteAndEarnActivity.getPackageManager()
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, text)
            sendIntent.type = "text/plain"
            Intent.createChooser(sendIntent, "Share via")
            startActivity(sendIntent)
        } catch (e: Exception) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_whatsApp_not_installed)
            )
        }


    }

    //--------Sending message on Facebook Messenger Method------
    private fun messenger_sendMsg(text: String) {
        val pm = this@InviteAndEarnActivity.getPackageManager()
        try {
            val waIntent = Intent(Intent.ACTION_SEND)
            waIntent.type = "text/plain"
            val info = pm.getPackageInfo(
                "com.facebook.orca",
                PackageManager.GET_META_DATA
            )  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.facebook.orca")
            waIntent.putExtra(Intent.EXTRA_TEXT, text)
            startActivity(Intent.createChooser(waIntent, "Share with"))
        } catch (e: PackageManager.NameNotFoundException) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_messenger_not_installed)
            )
        }

    }

    //--------Sending message on SMS Method------
    private fun sms_sendMsg(text: String) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkSmsPermission()) {
                requestPermission()
            } else {
                val defaultSmsPackageName =
                    Telephony.Sms.getDefaultSmsPackage(this@InviteAndEarnActivity) //Need to change the build to API 19
                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, text)
                if (defaultSmsPackageName != null) {
                    sendIntent.setPackage(defaultSmsPackageName)
                }
                startActivity(sendIntent)
            }
        } else {
            val sendIntent = Intent(Intent.ACTION_VIEW)
            sendIntent.putExtra("sms_body", text)
            sendIntent.type = "vnd.android-dir/mms-sms"
            startActivity(sendIntent)
        }
    }


    //----------Sending message on Email Method--------
    protected fun sendEmail(text: String) {
        val TO = arrayOf("")
        val CC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "message/rfc822"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
        emailIntent.putExtra(Intent.EXTRA_CC, CC)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Rideys app Invitation")
        emailIntent.putExtra(Intent.EXTRA_TEXT, text)
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_email_not_installed)
            )
        }

    }


    //----------Share Image and Text on Twitter Method--------
    protected fun shareTwitter(text: String, image: Uri?) {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_TEXT, text)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_STREAM, image)
        intent.type = "image/jpeg"
        intent.setPackage("com.twitter.android")

        try {
            startActivity(intent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_twitter_not_installed)
            )
        }

    }

    //----------Share Link on Method--------
    private fun shareFacebookLink(link: String) {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, link)
        val pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.orca")
        val pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana")
        val pacakage3 = getPackageManager().getLaunchIntentForPackage("com.example.facebook")
        val pacakage4 = getPackageManager().getLaunchIntentForPackage("com.facebook.android")
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.orca")
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana")
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook")
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android")
        } else {
            intent.setPackage("com.facebook.orca")
        }

        try {
            startActivity(intent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_facebook_not_installed)
            )
        }

    }

    //----------Share Image and Text on Facebook Method--------
    protected fun shareFacebook(text: String, image: Uri?) {

        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_STREAM, image)
        val pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.orca")
        val pacakage2 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana")
        val pacakage3 = getPackageManager().getLaunchIntentForPackage("com.example.facebook")
        val pacakage4 = getPackageManager().getLaunchIntentForPackage("com.facebook.android")
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.orca")
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.katana")
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.facebook")
        } else if (pacakage4 != null) {
            intent.setPackage("com.facebook.android")
        } else {
            intent.setPackage("com.facebook.orca")
        }

        try {
            startActivity(intent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_facebook_not_installed)
            )
        }

    }


    //-----------------------Display Invite Amount Post Request-----------------
    private fun displayInvite_Request(Url: String) {
        val dialog = Dialog(this@InviteAndEarnActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(this@InviteAndEarnActivity.getResources().getString(R.string.action_pleasewait))

        println("-------------displayInvite_Request Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID

        mRequest = ServiceRequest(this@InviteAndEarnActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------displayInvite_Request Response----------------$response")

                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (`object`.length() > 0) {
                            val response_Object = `object`.getJSONObject("response")
                            if (response_Object.length() > 0) {
                                val detail_object = response_Object.getJSONObject("details")
                                if (detail_object.length() > 0) {
                                    friend_earn_amount =
                                        detail_object.getString("friends_earn_amount")
                                    you_earn_amount = detail_object.getString("your_earn_amount")
                                    friends_rides = detail_object.getString("your_earn")
                                    referral_code = detail_object.getString("referral_code")
                                    ScurrencyCode = detail_object.getString("currency")
                                    sShareLink = detail_object.getString("url")

                                    isdataPresent = true
                                } else {
                                    isdataPresent = false
                                }
                            } else {
                                isdataPresent = false
                            }
                        } else {
                            isdataPresent = false
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    if (isdataPresent) {
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)
                        val relative: String
                        if (friends_rides == "") {
                            relative =
                                "Share the Ride Hub love and give friends free rides to try Ride, Worth up to $0"
                        } else {
                            relative =
                                "Share the Ride Hub love and give friends free rides to try Ride, Worth up to $$friends_rides"
                        }
                        tv_shareride.text = relative

                        Tv_referral_code!!.text = referral_code
                    }
                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    private fun checkSmsPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.SEND_SMS),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                val text =
                    getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(
                        R.string.invite_earn_label_share_messgae2
                    ) + " " + referral_code + " " + getResources().getString(R.string.invite_earn_label_share_messgae3)

                var defaultSmsPackageName: String? = null //Need to change the build to API 19
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this)
                }

                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, text)
                if (defaultSmsPackageName != null) {
                    sendIntent.setPackage(defaultSmsPackageName)
                }
                startActivity(sendIntent)
            } else {
            }
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            if (ll_invite_works_detail.isVisible) {
                ll_invite_code.visibility = View.VISIBLE
                ll_invite_works_detail.visibility = View.GONE
            } else {
                onBackPressed()
                finish()
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
            /*onBackPressed()
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)*/
            return true
        }
        return false
    }

}
