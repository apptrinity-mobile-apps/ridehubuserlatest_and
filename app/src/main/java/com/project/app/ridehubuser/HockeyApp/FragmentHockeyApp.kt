package com.project.app.ridehubuser.HockeyApp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.hockeyapp.android.CrashManager
import net.hockeyapp.android.UpdateManager

open class FragmentHockeyApp : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        checkForUpdates()
        return null
    }

    override fun onResume() {
        super.onResume()
        checkForCrashes()
    }

    override fun onPause() {
        super.onPause()
        unregisterManagers()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterManagers()
    }

    private fun checkForCrashes() {
        CrashManager.register(activity, APP_ID)
    }

    private fun checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(activity, APP_ID)
    }

    private fun unregisterManagers() {
        UpdateManager.unregister()
        // unregister other managers if necessary...
    }

    companion object {
        private val APP_ID = "9f8e1861d5cc413ba593e3367676bca3"
    }

}