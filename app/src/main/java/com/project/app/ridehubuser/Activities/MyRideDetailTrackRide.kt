package com.project.app.ridehubuser.Activities

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.card.MaterialCardView
import com.project.app.mylibrary.customRatingBar.RotationRatingBar
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.googlemapdrawpolyline.GMapV2GetRouteDirection
import com.project.app.mylibrary.gps.GPSTracker
import com.project.app.mylibrary.latlnginterpolation.LatLngInterpolator
import com.project.app.mylibrary.latlnginterpolation.MarkerAnimation
import com.project.app.mylibrary.widgets.RoundedImageView
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.PojoResponse.CancelTripPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.squareup.picasso.Picasso
import me.drakeet.materialdialog.MaterialDialog
import org.w3c.dom.Document
import java.util.*

class MyRideDetailTrackRide : FragmentActivity(), View.OnClickListener, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {
    private var tv_done: TextView? = null
    private var tv_drivername: TextView? = null
    private var tv_carModel: TextView? = null
    private var tv_carNo: TextView? = null
    private var tv_rating: TextView? = null
    private var driver_image: RoundedImageView? = null
    private var rl_callDriver: ImageView? = null
    private var rl_endTrip: TextView? = null
    private var mMap: GoogleMap? = null
    private val marker: MarkerOptions? = null
    private var gps: GPSTracker? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var driverID = ""
    private var driverName = ""
    private var driverImage = ""
    private var driverRating = ""
    private var driverLat: String? = ""
    private var driverLong: String? = ""
    private var driverTime = ""
    private var rideID = ""
    private var driverMobile: String? = ""
    private var driverCar_no = ""
    private var driverCar_model = ""
    private var userLat = ""
    private var userLong = ""
    private var sRideStatus = ""
    private var session: SessionManager? = null
    private var UserID = ""
    private var itemlist_reason: ArrayList<CancelTripPojo>? = null
    private var fromPosition: LatLng? = null
    private var toPosition: LatLng? = null
    private var markerOptions: MarkerOptions? = null
    internal lateinit var mLocationRequest: LocationRequest
    private var mGoogleApiClient: GoogleApiClient? = null
    private var currentLocation: Location? = null
    private var refreshReceiver: RefreshReceiver? = null
    private val realTimeHandler = Handler()


    var isFirstPosition = true
    var startPositionn: LatLng? = null
    var endPosition: LatLng? = null


    internal var sPickUpLocation = ""
    internal var sPickUpLatitude = ""
    internal var sPickUpLongitude = ""
    internal var sDropLocation = ""
    internal var sDropLatitude = ""
    internal var sDropLongitude = ""
    private var isRidePickUpAvailable = false
    private var isRideDropAvailable = false

    internal val PERMISSION_REQUEST_CODE = 111
    internal val PERMISSION_REQUEST_CODES = 222
    private var sSelectedPanicNumber = ""

    internal var mLatLngInterpolator: LatLngInterpolator? = null

    //Driver details
    private var iv_driver_image: ImageView? = null
    private var rl_back_button: RelativeLayout? = null
    private var tv_driver_name: TextView? = null
    private var tv_driver_found: TextView? = null
    private var ll_call: LinearLayout? = null
    private var iv_close_safetykit: ImageView? = null
    private var ll_main: MaterialCardView? = null
    private var ll_include_safettytoolkit: LinearLayout? = null
    private var ll_safety: LinearLayout? = null
    private var ll_message: LinearLayout? = null
    private var tv_cab_type: TextView? = null
    private var tv_car_number: TextView? = null
    private var tv_driver_estimated_time: TextView? = null
    private var tv_car_model: TextView? = null
    private var driver_rating: RotationRatingBar? = null

    private var currentLocationn: LatLng? = null
    private var lat: Double = 0.toDouble()
    private var lng: Double = 0.toDouble()
    private var dropLatLng: LatLng? = null
    private var pickUpLatLng: LatLng? = null
    private var polyline: Polyline? = null
    private var firsttimepolyline: Boolean? = true



    //Safety Kit
    private var cardview_safety_center: MaterialCardView? = null
    private var cardview_sharemytrip: MaterialCardView? = null
    private var cardview_911_assistance: MaterialCardView? = null


    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this)
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onLocationChanged(location: Location) {
        this.currentLocation = location
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()

    }

    override fun onResume() {
        super.onResume()

        try{
            ChatService.startUserAction(this@MyRideDetailTrackRide)
        }catch (e:Exception){
            e.printStackTrace()
        }

        startLocationUpdates()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //mMap.setMyLocationEnabled(true);
            val styleManager = MapStyleManager.attachToMap(this, mMap!!)
            styleManager.addStyle(0f, R.raw.map_silver_json)
            styleManager.addStyle(10f, R.raw.map_silver_json)
            styleManager.addStyle(12f, R.raw.map_silver_json)

        }
        if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {
            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()
            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude
            // Move the camera to last position with a zoom level
            val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(18f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        } else {
            Alert(resources.getString(R.string.action_error), resources.getString(R.string.alert_gpsEnable))
        }


    }

    inner class RefreshReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver") {
                println("triparrived----------------------")
                tv_driver_found!!.text =
                    resources.getString(R.string.action_driver_arrived)
            } else if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip") {
                println("tripbegin----------------------")
                tv_driver_found!!.text = resources.getString(R.string.action_enjy_your_ride)
            } else if (intent.action == "com.package.track.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver") {
            }
            println("check--------------")
            if (intent.extras != null && intent.extras!!.containsKey("drop_lat") && intent.extras!!.containsKey("drop_lng")) {
                val lat = intent.extras!!.get("drop_lat") as String
                val lng = intent.extras!!.get("drop_lng") as String
                val pickUp_lat = intent.extras!!.get("pickUp_lat") as String
                val pickUp_lng = intent.extras!!.get("pickUp_lng") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    val pick_lat_decimal = java.lang.Double.parseDouble(pickUp_lat)
                    val pick_lng_decimal = java.lang.Double.parseDouble(pickUp_lng)
                    updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal, "")
                    println("inside updategoogle1--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }

            }
            println("out else--------------")
            if (intent.extras != null && intent.extras!!.containsKey(Iconstant.isContinousRide)) {
                val lat = intent.extras!!.get("latitude") as String
                val lng = intent.extras!!.get("longitude") as String
                val ride_id = intent.extras!!.get("ride_id") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    updateDriverOnMap(lat_decimal, lng_decimal)
                } catch (e: Exception) {
                }

            }
        }
    }

    /*private fun updateDriverOnMap(lat_decimal: kotlin.Double, lng_decimal: kotlin.Double) {

        val latLng = LatLng(lat_decimal, lng_decimal)

        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = LatLngInterpolator.Linear()
        }

        if (curentDriverMarker != null) {
            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator)

            val zoom = mMap!!.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)

        } else {

            mLatLngInterpolator = LatLngInterpolator.Linear()
            curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    .anchor(0.5f, 0.5f)
                    .flat(true))

            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
        }
    }*/




    private fun updateDriverOnMap(lat_decimal: Double, lng_decimal: Double) {
        val latLng = LatLng(lat_decimal, lng_decimal)
        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = LatLngInterpolator.Linear()
        }
        Log.d("tess", "inside run ")
        if (curentDriverMarker != null) {
            Log.d("UPDATEDRIVERONMAP", "inside run IF")
            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator)
            val zoom = mMap!!.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
            if (isFirstPosition) {
                startPositionn = curentDriverMarker!!.getPosition();
                curentDriverMarker!!.setAnchor(0.5f, 0.5f);
                mMap!!.moveCamera(
                    CameraUpdateFactory
                        .newCameraPosition
                            (
                            CameraPosition.Builder()
                                .target(startPositionn)
                                .zoom(15.5f)
                                .build()
                        )
                );

                isFirstPosition = false;

            } else {
                endPosition = LatLng(latLng.latitude, latLng.longitude);
                if ((startPositionn!!.latitude != endPosition!!.latitude) || (startPositionn!!.longitude != endPosition!!.longitude)) {
                    Log.e("asdhashd", "NOT SAME");
                    startBikeAnimation(startPositionn!!, endPosition!!);
                } else {
                    Log.e("dfsdfsd", "SAMME");
                }
            }
        } else {
            Log.d("UPDATEDRIVERONMAP", "inside run ELSE")
            mLatLngInterpolator = LatLngInterpolator.Linear()
            curentDriverMarker = mMap!!.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    .anchor(0.5f, 0.5f)
                    .flat(true)
            )
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
        }

    }



    private fun startBikeAnimation(start: LatLng, end: LatLng) {
        val handler = Handler()
        Log.e("Startbikeanimation", "Entered to me")
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.duration = 3000 // duration 3 second
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.addUpdateListener { animation ->
            try {
                val v = animation.animatedFraction

                lng = v * end!!.longitude + (1 - v) * start!!.longitude
                lat = v * end!!.latitude + (1 - v) * start!!.latitude

                val newPos = LatLng(lat, lng)
                curentDriverMarker!!.setPosition(newPos)
               curentDriverMarker!!.setAnchor(0.5f, 0.5f)
              curentDriverMarker!!.rotation = getBearing(start, end)
                mMap!!.moveCamera(
                    CameraUpdateFactory
                        .newCameraPosition(
                            CameraPosition.Builder()
                                .target(newPos)
                                .zoom(15.5f)
                                .build()
                        )
                )
                startPositionn = curentDriverMarker!!.getPosition();
            } catch (ex: Exception) {
                //I don't care atm..
            }
        }
        valueAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)

            }
        })
        valueAnimator.start()
//        firsttimepolyline = false
        Log.e("currentLocationn", currentLocationn.toString())
        val runnable = Runnable {
            currentLocationn = start
            if (currentLocationn != null) {
                val getRoute = TrackRouteTask()
                try {
                    getRoute.setToAndFromLocationTrackride(dropLatLng!!, pickUpLatLng!!)
                    getRoute.execute()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        handler.postDelayed(runnable, 3000)
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private inner class TrackRouteTask : AsyncTask<String, Void, String>() {
        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        private var endLocation: LatLng? = null
        private var currentLocation: LatLng? = null
        fun setToAndFromLocationTrackride(currentLocation: LatLng, endLocation: LatLng) {
            Log.e("UPDATELATLNG_INSIDEDROP", "" + currentLocation + "-----" + endLocation)
            this.currentLocation = currentLocation
            this.endLocation = endLocation
        }

        override fun onPreExecute() {
            Log.e("GETDROPROUTE", "GetDropRouteTask")
            currentLocationn = currentLocation
        }

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(
                endLocation,
                currentLocation,
                GMapV2GetRouteDirection.MODE_DRIVING
            )
            response = "Success"
            return response
        }

        override fun onPostExecute(result: String) {
            if (polyline != null) {
                polyline!!.remove()
            }
            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine =
                        PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }

                    // Adding ride_detail_trackride on the map
                    if (firsttimepolyline!!) {
                        markerOptions!!.position(toPosition!!)
                        markerOptions!!.draggable(true)
                        //mMap.addMarker(markerOptions);
                        firsttimepolyline = false
                        val markerWidth = 350
                        val markerHeight = 250
                        // destination marker
                        val destn_marker_view =
                            (this@MyRideDetailTrackRide.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                                R.layout.custom_drop_marker,
                                null
                            )
                        val tv_marker_drop =
                            destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                        tv_marker_drop.text = sDropLocation!!
                        val destn_bitmap = createBitmapFromLayout(destn_marker_view)
                        val destn_small_bitmap =
                            Bitmap.createScaledBitmap(
                                destn_bitmap,
                                markerWidth,
                                markerHeight,
                                false
                            )

                        mMap!!.addMarker(
                            MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromBitmap(destn_small_bitmap))
                        )

                        curentDriverMarker = mMap!!.addMarker(
                            MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                        )
                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(toPosition!!)
                        builder.include(toPosition!!)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                    }
                    polyline = mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(toPosition!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }



    //Method for finding bearing between two points
    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return 1f
    }




    /*private fun updateGoogleMapTrackRide(lat_decimal: kotlin.Double, lng_decimal: kotlin.Double, pick_lat_decimal: kotlin.Double, pick_lng_decimal: kotlin.Double) {
        if (mMap != null) {
            mMap!!.clear()
        }
        val dropLatLng = LatLng(lat_decimal, lng_decimal)
        val pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)

        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng, pickUpLatLng)
        draw_route_asyncTask.execute()
    }*/



    private fun updateGoogleMapTrackRide(
        lat_decimal: Double,
        lng_decimal: Double,
        pick_lat_decimal: Double,
        pick_lng_decimal: Double,
        status: String
    ) {
        if (mMap != null) {
            mMap!!.clear()
        }
        val dropLatLng = LatLng(lat_decimal, lng_decimal)
        val pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)
        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng, pickUpLatLng, status)
        draw_route_asyncTask.execute()
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_detail_track_ride)
        trackyour_ride_class = this@MyRideDetailTrackRide
        initialize()
        try {
            setLocationRequest()
            buildGoogleApiClient()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initializeMap()
        //Start XMPP Chat Service
    }


    private fun initialize() {
        cd = ConnectionDetector(this@MyRideDetailTrackRide)
        isInternetPresent = cd!!.isConnectingToInternet
        gps = GPSTracker(this@MyRideDetailTrackRide)
        session = SessionManager(this@MyRideDetailTrackRide)
        itemlist_reason = ArrayList()
        markerOptions = MarkerOptions()


        //Driver data
        iv_driver_image = findViewById(R.id.iv_driver_image) as ImageView
        rl_back_button = findViewById(R.id.rl_back_button) as RelativeLayout
        tv_driver_name = findViewById(R.id.tv_driver_name) as TextView
        tv_cab_type = findViewById(R.id.tv_cab_type) as TextView
        tv_car_number = findViewById(R.id.tv_car_number) as TextView
        driver_rating = findViewById(R.id.driver_rating) as RotationRatingBar
        tv_driver_estimated_time = findViewById(R.id.tv_driver_estimated_time) as TextView
        tv_car_model = findViewById(R.id.tv_car_model) as TextView
        tv_driver_found = findViewById(R.id.tv_driver_found) as TextView
        tv_driver_found = findViewById(R.id.tv_driver_found) as TextView
        ll_call = findViewById(R.id.ll_call) as LinearLayout
        ll_main = findViewById(R.id.ll_main) as MaterialCardView
        iv_close_safetykit = findViewById(R.id.iv_close_safetykit) as ImageView
        ll_include_safettytoolkit =
            findViewById(R.id.ll_include_safettytoolkit) as LinearLayout
        ll_message = findViewById(R.id.ll_message) as LinearLayout
        ll_safety = findViewById(R.id.ll_safety) as LinearLayout

        //Safety Tools Kit
        cardview_safety_center =
            findViewById(R.id.cardview_safety_center) as MaterialCardView
        cardview_sharemytrip = findViewById(R.id.cardview_sharemytrip) as MaterialCardView
        cardview_911_assistance =
            findViewById(R.id.cardview_911_assistance) as MaterialCardView



        val intent = intent
        if (intent != null) {
            driverID = intent.getStringExtra("driverID")
            driverName = intent.getStringExtra("driverName")
            driverImage = intent.getStringExtra("driverImage")
            driverRating = intent.getStringExtra("driverRating")
            driverLat = intent.getStringExtra("driverLat")
            driverLong = intent.getStringExtra("driverLong")
            driverTime = intent.getStringExtra("driverTime")
            rideID = intent.getStringExtra("rideID")
            driverMobile = intent.getStringExtra("driverMobile")
            driverCar_no = intent.getStringExtra("driverCar_no")
            driverCar_model = intent.getStringExtra("driverCar_model")
            userLat = intent.getStringExtra("userLat")
            userLong = intent.getStringExtra("userLong")
            sRideStatus = intent.getStringExtra("rideStatus")

            if (intent.hasExtra("PickUpLocation")) {
                sPickUpLocation = intent.getStringExtra("PickUpLocation")
                sPickUpLatitude = intent.getStringExtra("PickUpLatitude")
                sPickUpLongitude = intent.getStringExtra("PickUpLongitude")

                isRidePickUpAvailable = true
            } else {
                isRidePickUpAvailable = false
            }

            if (intent.hasExtra("DropLocation")) {
                sDropLocation = intent.getStringExtra("DropLocation")
                sDropLatitude = intent.getStringExtra("DropLatitude")
                sDropLongitude = intent.getStringExtra("DropLongitude")

                isRideDropAvailable = true
            } else {
                isRideDropAvailable = false
            }



        }


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = RefreshReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver")
        registerReceiver(refreshReceiver, intentFilter)


        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        //Driver data Details
        Picasso.with(this!!)
            .load(driverImage)
            .into(iv_driver_image)
        tv_driver_name!!.setText(driverName)
        tv_cab_type!!.setText("Ridehub")
        tv_car_number!!.setText(driverCar_no)
        tv_car_model!!.setText(driverCar_model)
        if (driverRating.length > 0) {
            driver_rating!!.rating = java.lang.Float.parseFloat(driverRating)
        }
        tv_driver_estimated_time!!.setText("driver will pickup you in " + driverTime)


        rl_back_button!!.setOnClickListener {
            onBackPressed()
        }


        ll_call!!.setOnClickListener {
            if (driverMobile != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + driverMobile)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + driverMobile)
                    startActivity(callIntent)
                }
            }
        }

        ll_message!!.setOnClickListener {
            val i = Intent(this!!, ChatActivity::class.java)
            i.putExtra("driverID", driverID)
            i.putExtra("driverName", driverName)
            i.putExtra("driverImage", driverImage)
            startActivity(i)
        }

        ll_safety!!.setOnClickListener {
            ll_include_safettytoolkit!!.visibility = View.VISIBLE
            ll_main!!.visibility = View.GONE

        }

        iv_close_safetykit!!.setOnClickListener {
            ll_include_safettytoolkit!!.visibility = View.GONE
            ll_main!!.visibility = View.VISIBLE
        }


        cardview_safety_center!!.setOnClickListener {
            val i = Intent(this!!, SafetyCenterActivity::class.java)
            startActivity(i)
        }
        cardview_sharemytrip!!.setOnClickListener {
            val i = Intent(this!!, ShareTripActivity::class.java)
            startActivity(i)
        }
        cardview_911_assistance!!.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermission()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + 911)
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + 911)
                startActivity(callIntent)
            }
        }


    }

    private fun panic() {
        println("----------------panic method-----------------")
        val view = View.inflate(this@MyRideDetailTrackRide, R.layout.panic_page, null)
        val dialog = MaterialDialog(this@MyRideDetailTrackRide)
        dialog.setContentView(view).setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
        ) { dialog.dismiss() }.show()
        val call_police = view.findViewById(R.id.call_police_button) as Button
        val call_fire = view.findViewById(R.id.call_fireservice_button) as Button
        val call_ambulance = view.findViewById(R.id.call_ambulance_button) as Button
        call_police.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+100"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "+100")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "+100")
                startActivity(callIntent)
            }
        }

        call_fire.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+101"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "101")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "101")
                startActivity(callIntent)
            }
        }
        call_ambulance.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+108"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "108")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "108")
                startActivity(callIntent)
            }
        }

    }


    private fun initializeMap() {
        if (mMap == null) {
            val mapFragment = supportFragmentManager.findFragmentById(R.id.myride_detail_track_your_ride_mapview) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                Toast.makeText(this@MyRideDetailTrackRide, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show()
            }
        }


        if (sRideStatus.equals("Onride", ignoreCase = true)) {
            tv_driver_found!!.text = resources.getString(R.string.action_enjy_your_ride)
        } else if (sRideStatus.equals("arrived", ignoreCase = true)) {
            tv_driver_found!!.text = resources.getString(R.string.action_driver_arrived)
        } else if (sRideStatus.equals("Confirmed", ignoreCase = true)) {
            tv_driver_found!!.text = resources.getString(R.string.track_your_ride_label_track)
        }

        if (isRideDropAvailable && sRideStatus.equals("Onride", ignoreCase = true)) {
            println("--------------inside isRideDropAvailable if -----------------------")

            try {
                val lat_decimal = java.lang.Double.parseDouble(sDropLatitude)
                val lng_decimal = java.lang.Double.parseDouble(sDropLongitude)
                val pick_lat_decimal = java.lang.Double.parseDouble(sPickUpLatitude)
                val pick_lng_decimal = java.lang.Double.parseDouble(sPickUpLongitude)
                updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal,"")
            } catch (e: Exception) {
            }

        } else {
            //set marker for driver location.
            if (driverLat != null && driverLong != null) {
                fromPosition = LatLng(java.lang.Double.parseDouble(userLat), java.lang.Double.parseDouble(userLong))
                toPosition = LatLng(java.lang.Double.parseDouble(driverLat!!), java.lang.Double.parseDouble(driverLong!!))
                if (fromPosition != null && toPosition != null) {

                    println("--------------inside isRideDropAvailable else fromPosition-----------------------" + fromPosition!!)
                    println("--------------inside isRideDropAvailable else toPosition-----------------------" + toPosition!!)

                    val draw_route_asyncTask = GetRouteTask()
                    draw_route_asyncTask.execute()
                }
            }
        }
    }

    override fun onClick(v: View) {
        if (v === tv_done) {
            val finish_timerPage = Intent()
            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
            sendBroadcast(finish_timerPage)
            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        } else if (v === rl_callDriver) {
            if (driverMobile != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + driverMobile!!)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + driverMobile!!)
                    startActivity(callIntent)
                }
            } else {
                Alert(this@MyRideDetailTrackRide.resources.getString(R.string.alert_label_title), this@MyRideDetailTrackRide.resources.getString(R.string.track_your_ride_alert_content1))
            }
        } else if (v === rl_endTrip) {
            val mDialog = PkDialog(this@MyRideDetailTrackRide)
            mDialog.setDialogTitle(resources.getString(R.string.my_rides_detail_cancel_ride_alert_title))
            mDialog.setDialogMessage(resources.getString(R.string.my_rides_detail_cancel_ride_alert))
            mDialog.setPositiveButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_yes), View.OnClickListener {
                mDialog.dismiss()
                cd = ConnectionDetector(this@MyRideDetailTrackRide)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    //postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url);
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            })
            mDialog.setNegativeButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_no), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()

        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@MyRideDetailTrackRide)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values

            document = v2GetRouteDirection.getDocument(toPosition, fromPosition, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map
                    mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(fromPosition!!)
                    markerOptions!!.position(toPosition!!)
                    markerOptions!!.draggable(true)

                    println("-------------inside getRoute---------------------")

                    val pickup_marker_view =
                        (this@MyRideDetailTrackRide!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_pickup_marker,
                            null
                        )
                    val tv_marker_pickup =
                        pickup_marker_view.findViewById<TextView>(R.id.tv_marker_pickup)
                    tv_marker_pickup.text = sPickUpLocation!!
                    val pickup_bitmap = createBitmapFromLayout(pickup_marker_view)
                    // destination marker
                    val destn_marker_view =
                        (this@MyRideDetailTrackRide!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_drop_marker,
                            null
                        )
                    val tv_marker_drop =
                        destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                    tv_marker_drop.text = sDropLocation!!
                    val destn_bitmap = createBitmapFromLayout(destn_marker_view)

                    /*mMap!!.addMarker(
                        MarkerOptions()
                            .position(fromPosition!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_dot))
                            .icon(BitmapDescriptorFactory.fromBitmap(pickup_bitmap))
                    )*/

                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(toPosition!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.destilocation))
                            .icon(BitmapDescriptorFactory.fromBitmap(destn_bitmap))
                    )

                    curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                            .position(toPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car)))

                    //Show path in
                    val builder = LatLngBounds.Builder()
                    builder.include(fromPosition!!)
                    builder.include(toPosition!!)
                    val bounds = builder.build()
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            val finish_timerPage = Intent()
            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
            sendBroadcast(finish_timerPage)

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

            return true
        }
        return false
    }

    public override fun onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver)
        ChatService.disableChat()
        super.onDestroy()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private inner class GetDropRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        private var currentLocation: LatLng? = null
        private var endLocation: LatLng? = null
        internal var status = ""

        fun setToAndFromLocation(currentLocation: LatLng, endLocation: LatLng, status: String) {
            this.currentLocation = currentLocation
            this.endLocation = endLocation
            this.status = status
        }

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(endLocation, currentLocation, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(
                            resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding route on the map
                    mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(endLocation!!)
                    markerOptions!!.position(currentLocation!!)
                    markerOptions!!.draggable(true)


                    val pickup_marker_view =
                        (this@MyRideDetailTrackRide!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_pickup_marker,
                            null
                        )
                    val tv_marker_pickup =
                        pickup_marker_view.findViewById<TextView>(R.id.tv_marker_pickup)
                    tv_marker_pickup.text = sPickUpLocation!!
                    val pickup_bitmap = createBitmapFromLayout(pickup_marker_view)
                    // destination marker
                    val destn_marker_view =
                        (this@MyRideDetailTrackRide!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_drop_marker,
                            null
                        )
                    val tv_marker_drop =
                        destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                    tv_marker_drop.text = sDropLocation!!
                    val destn_bitmap = createBitmapFromLayout(destn_marker_view)

                    /*mMap!!.addMarker(
                        MarkerOptions()
                            .position(endLocation!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_dot))
                            .icon(BitmapDescriptorFactory.fromBitmap(pickup_bitmap))
                    )*/

                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(currentLocation!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.destilocation))
                            .icon(BitmapDescriptorFactory.fromBitmap(destn_bitmap))
                    )

                    curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                            .position(endLocation!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car)))

                    println("inside---------marker--------------")

                    //Show path in
                    val builder = LatLngBounds.Builder()
                    builder.include(endLocation!!)
                    builder.include(currentLocation!!)
                    val bounds = builder.build()
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }


    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODE)
    }


    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODES)
    }


    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + driverMobile!!)
                startActivity(callIntent)
            }


            PERMISSION_REQUEST_CODES ->

                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$sSelectedPanicNumber")
                    startActivity(callIntent)
                }
        }
    }

    companion object {
        lateinit var trackyour_ride_class: MyRideDetailTrackRide
        private var curentDriverMarker: Marker? = null
        private val movingMarker: Marker? = null
        internal val REQUEST_LOCATION = 199

        internal var latLngInterpolator: LatLngInterpolator = LatLngInterpolator.Spherical()

        fun updateMap(latLng: LatLng) {
            MarkerAnimation.animateMarkerToICS(movingMarker, latLng, latLngInterpolator)
        }
    }


    fun createBitmapFromLayout(view: View): Bitmap {

        val displayMetrics = DisplayMetrics()
        (this as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)

        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap =
            Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)

        return bitmap
    }

}
