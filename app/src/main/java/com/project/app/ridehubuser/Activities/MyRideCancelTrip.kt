package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.Request
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.MyRideCancelTripAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.CancelTripPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MyRideCancelTrip : ActivityHockeyApp() {
    private var back: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var UserID = ""

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    internal lateinit var itemlist: ArrayList<CancelTripPojo>
    internal lateinit var adapter: MyRideCancelTripAdapter
    private var listview: ExpandableHeightListView? = null
    private var SrideId_intent = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_cancel_trip)
        context = getApplicationContext()
        initialize()

        back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        listview!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            cd = ConnectionDetector(this@MyRideCancelTrip)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                cancel_MyRide(Iconstant.cancel_myride_url, itemlist[position].reasonId)
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
            }
        }

    }

    private fun initialize() {
        session = SessionManager(this@MyRideCancelTrip)
        cd = ConnectionDetector(this@MyRideCancelTrip)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById(R.id.my_rides_cancel_trip_header_back_layout) as ImageView
        listview = findViewById(R.id.my_rides_cancel_trip_listView) as ExpandableHeightListView

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        val intent = getIntent()
        SrideId_intent = intent.getStringExtra("RideID")
        try {
            val bundleObject = getIntent().getExtras()
            itemlist = bundleObject!!.getSerializable("Reason") as ArrayList<CancelTripPojo>
            adapter = MyRideCancelTripAdapter(this@MyRideCancelTrip, itemlist)
            listview!!.adapter = adapter
            listview!!.isExpanded = true
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@MyRideCancelTrip)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //-----------------------Cancel MyRide Post Request-----------------
    private fun cancel_MyRide(Url: String, reasonId: String) {
        dialog = Dialog(this@MyRideCancelTrip)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.my_rides_cancel_trip_action_cancel))


        println("-------------Cancel MyRide Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["reason"] = reasonId

        mRequest = ServiceRequest(this@MyRideCancelTrip)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
           override fun onCompleteListener(response: String) {

                println("-------------Cancel MyRide Response----------------$response")

                var Sstatus = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val message = response_object.getString("message")

                            val mDialog = PkDialog(this@MyRideCancelTrip)
                            mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                            mDialog.setDialogMessage(message)
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                                mDialog.dismiss()
                                finish()

                                val broadcastIntent = Intent()
                                broadcastIntent.action = "com.pushnotification.updateBottom_view"
                                sendBroadcast(broadcastIntent)

                                RideHistoryDetailsActivity.myrideDetail_class.finish()
                                RideHistoryActivity.myride_class.finish()
                                onBackPressed()
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            })
                            mDialog.show()
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

           override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
   override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            return true
        }
        return false
    }

    companion object {
        private var context: Context? = null
    }
}
