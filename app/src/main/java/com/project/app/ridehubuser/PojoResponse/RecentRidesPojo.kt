package com.project.app.ridehubuser.PojoResponse

class RecentRidesPojo(/*destination: String*/) {


    /* var destination = ""
     init {

         this.destination = destination
     }*/
    private var title: String? = null
    private var address: String? = null


    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun getAddress(): String? {
        return address
    }

    fun setAddress(address: String) {
        this.address = address
    }


}