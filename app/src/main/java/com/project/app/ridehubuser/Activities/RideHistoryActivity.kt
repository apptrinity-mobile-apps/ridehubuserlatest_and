package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.Adapters.MyRidesAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.MyRidesPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class RideHistoryActivity : ActivityHockeyApp() {



    lateinit var iv_close_ride_history: ImageView
    lateinit var tv_ride_history_cancelled: TextView
    lateinit var tv_ride_history_upcoming: TextView
    lateinit var tv_no_rides: TextView
    lateinit var tv_ride_history_past: TextView
    lateinit var ll_ride_history: LinearLayout
    lateinit var rv_past_rides: RecyclerView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var userID = ""

    private var isRideAvailable = false
    val type_ridehub = "RIDEHUB"
    lateinit var dialog: Dialog
    var itemlist_all: ArrayList<MyRidesPojo> = ArrayList()
    var itemlist_upcoming: ArrayList<MyRidesPojo> = ArrayList()
    var itemlist_cancelled: ArrayList<MyRidesPojo> = ArrayList()
    private var mRequest: ServiceRequest? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride_history)
        myride_class = this@RideHistoryActivity
        initialise()

        //Start XMPP Chat Service

        try{
            ChatService.startUserAction(this@RideHistoryActivity)
        }catch (e:Exception){
            e.printStackTrace()
        }

        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID]!!

        if (isInternetPresent!!) {
            postRequest_MyRides(Iconstant.myrides_url)
        } else {
            alert(
                resources.getString(R.string.alert_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

        iv_close_ride_history.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        tv_ride_history_past.setOnClickListener {
            tv_ride_history_past.setBackgroundResource(R.drawable.button_dark_blue_rounded_solid_bg)
            tv_ride_history_past.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_ride_history_upcoming.setBackgroundResource(R.drawable.button_gray_rounded_border_bg)
            tv_ride_history_upcoming.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.title_header
                )
            )
            tv_ride_history_cancelled.setBackgroundResource(R.drawable.button_gray_rounded_border_bg)
            tv_ride_history_cancelled.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.title_header
                )
            )

            if (itemlist_all.isEmpty()) {
                ll_ride_history.visibility = View.GONE
                tv_no_rides.visibility = View.VISIBLE
            } else {
                ll_ride_history.visibility = View.VISIBLE
                tv_no_rides.visibility = View.GONE
                ll_ride_history.removeAllViews()
                val recyclerView = RecyclerView(this)
                val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                recyclerView.layoutManager = layoutManager
                val adapter = MyRidesAdapter(this@RideHistoryActivity, itemlist_all)
                recyclerView.adapter = adapter
                ll_ride_history.addView(recyclerView)
            }

        }
        tv_ride_history_upcoming.setOnClickListener {
            tv_ride_history_past.setBackgroundResource(R.drawable.button_gray_rounded_border_bg)
            tv_ride_history_past.setTextColor(ContextCompat.getColor(this, R.color.title_header))
            tv_ride_history_upcoming.setBackgroundResource(R.drawable.button_dark_blue_rounded_solid_bg)
            tv_ride_history_upcoming.setTextColor(ContextCompat.getColor(this, R.color.white))
            tv_ride_history_cancelled.setBackgroundResource(R.drawable.button_gray_rounded_border_bg)
            tv_ride_history_cancelled.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.title_header
                )
            )

            if (itemlist_upcoming.isEmpty()) {
                ll_ride_history.visibility = View.GONE
                tv_no_rides.visibility = View.VISIBLE
            } else {
                ll_ride_history.visibility = View.VISIBLE
                tv_no_rides.visibility = View.GONE
                ll_ride_history.removeAllViews()
                val recyclerView = RecyclerView(this)
                val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                recyclerView.layoutManager = layoutManager
                val adapter = MyRidesAdapter(this@RideHistoryActivity, itemlist_upcoming)
                recyclerView.adapter = adapter
                ll_ride_history.addView(recyclerView)
            }

        }
        tv_ride_history_cancelled.setOnClickListener {
            tv_ride_history_past.setBackgroundResource(R.drawable.button_gray_rounded_border_bg)
            tv_ride_history_past.setTextColor(ContextCompat.getColor(this, R.color.title_header))
            tv_ride_history_upcoming.setBackgroundResource(R.drawable.button_gray_rounded_border_bg)
            tv_ride_history_upcoming.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.title_header
                )
            )
            tv_ride_history_cancelled.setBackgroundResource(R.drawable.button_dark_blue_rounded_solid_bg)
            tv_ride_history_cancelled.setTextColor(ContextCompat.getColor(this, R.color.white))

            if (itemlist_cancelled.isEmpty()) {
                ll_ride_history.visibility = View.GONE
                tv_no_rides.visibility = View.VISIBLE
            } else {
                ll_ride_history.visibility = View.VISIBLE
                tv_no_rides.visibility = View.GONE
                ll_ride_history.removeAllViews()
                val recyclerView = RecyclerView(this)
                val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                recyclerView.layoutManager = layoutManager
                val adapter = MyRidesAdapter(this@RideHistoryActivity, itemlist_cancelled)
                recyclerView.adapter = adapter
                ll_ride_history.addView(recyclerView)
            }

        }

    }


    private fun initialise() {
        session = SessionManager(this@RideHistoryActivity)
        cd = ConnectionDetector(this@RideHistoryActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_close_ride_history = findViewById(R.id.iv_close_ride_history)
        ll_ride_history = findViewById(R.id.ll_ride_history)
        tv_ride_history_past = findViewById(R.id.tv_ride_history_past)
        tv_ride_history_upcoming = findViewById(R.id.tv_ride_history_upcoming)
        tv_ride_history_cancelled = findViewById(R.id.tv_ride_history_cancelled)
        tv_no_rides = findViewById(R.id.tv_no_rides)

        rv_past_rides = findViewById(R.id.rv_past_rides)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_past_rides.layoutManager = layoutManager
    }

    private fun postRequest_MyRides(Url: String) {
        dialog = Dialog(this)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------MyRides Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["type"] = "all"

        mRequest = ServiceRequest(this)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    dialog.dismiss()
                    println("-------------MyRides Response----------------$response")

                    val Sstatus: String

                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            if (response_object.length() > 0) {
                                val check_rides_object = response_object.get("rides")
                                if (check_rides_object is JSONArray) {
                                    val ride_array = response_object.getJSONArray("rides")
                                    if (ride_array.length() > 0) {

                                        for (i in 0 until ride_array.length()) {
                                            val ride_object = ride_array.getJSONObject(i)
                                            Log.d("ride_object", "" + ride_object)
                                            val pojo = MyRidesPojo()
                                            pojo.setRide_id(ride_object.getString("ride_id"))
                                            pojo.setRide_time(ride_object.getString("ride_time"))
                                            if (ride_object.getString("ride_date").contains("/")) {
                                                val replaceDate =
                                                    ride_object.getString("ride_date").toString()
                                                        .replace("/", "-")
                                                pojo.setRide_date(replaceDate)
                                            } else {
                                                pojo.setRide_date(ride_object.getString("ride_date"))
                                            }
                                            pojo.setPickup(ride_object.getString("pickup"))
                                            pojo.setDrop(ride_object.getString("drop"))
                                            pojo.setRide_status(ride_object.getString("ride_status"))
                                            pojo.setGroup(ride_object.getString("group"))
                                            pojo.setDatetime(ride_object.getString("datetime"))
                                            pojo.setVehicle_model(ride_object.getString("vehicle_model"))
                                            pojo.setTotal_fare(ride_object.getString("total_fare"))
                                            pojo.setPickup_long(ride_object.getString("pickup_long"))
                                            pojo.setPickup_lat(ride_object.getString("pickup_lat"))
                                            pojo.setDrop_lon(ride_object.getString("drop_lon"))
                                            pojo.setDrop_lat(ride_object.getString("drop_lat"))
                                            pojo.setCab_type(ride_object.getString("cab_type"))
                                            pojo.setVehicle_no(ride_object.getString("vehicle_no"))
                                            pojo.setDriver_rating(ride_object.getString("driver_rating"))
                                            pojo.setUser_rating(ride_object.getString("user_rating"))
                                            pojo.setImage(ride_object.getString("image"))
                                            pojo.setType(type_ridehub)
                                            when {
                                                ride_object.getString("ride_status") == "Completed" -> itemlist_all.add(
                                                    pojo
                                                )
                                                ride_object.getString("ride_status") == "Arrived" -> itemlist_all.add(
                                                    pojo
                                                )
                                                ride_object.getString("ride_status") == "Confirmed" -> itemlist_all.add(
                                                    pojo
                                                )
                                                ride_object.getString("ride_status") == "Finished" -> itemlist_all.add(
                                                    pojo
                                                )
                                                ride_object.getString("ride_status") == "Onride" -> itemlist_all.add(
                                                    pojo
                                                )
                                                ride_object.getString("ride_status") == "Booked" -> itemlist_upcoming.add(
                                                    pojo
                                                )
                                                ride_object.getString("ride_status") == "Cancelled" -> itemlist_cancelled.add(
                                                    pojo
                                                )
                                            }
                                        }
                                        isRideAvailable = true
                                    } else {
                                        isRideAvailable = false
                                    }
                                } else {
                                    isRideAvailable = false
                                }
                            }

                        }

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            if (isRideAvailable) {

                                Collections.sort(itemlist_all, MyRidesPojo().dateWiseComparator)
                                Collections.sort(
                                    itemlist_upcoming,
                                    MyRidesPojo().dateWiseComparator
                                )
                                Collections.sort(
                                    itemlist_cancelled,
                                    MyRidesPojo().dateWiseComparator
                                )
                                for (str in itemlist_all) {
                                    System.out.println("----MYRIDESDATESRIDEHUB----$str")
                                }

                                ll_ride_history.removeAllViews()
                                val recyclerView = RecyclerView(this@RideHistoryActivity)
                                val layoutManager = LinearLayoutManager(
                                    this@RideHistoryActivity,
                                    RecyclerView.VERTICAL,
                                    false
                                )
                                recyclerView.layoutManager = layoutManager
                                val adapter = MyRidesAdapter(this@RideHistoryActivity, itemlist_all)
                                recyclerView.adapter = adapter
                                ll_ride_history.addView(recyclerView)
                                tv_no_rides.visibility = View.GONE

                            } else {
                                ll_ride_history.visibility = View.GONE
                                tv_no_rides.visibility = View.VISIBLE
                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                        dialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        dialog.dismiss()
                    }

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    //--------------alert Method-----------
    private fun alert(title: String, alert: String) {

        val mDialog = PkDialog(this)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    companion object {
        lateinit var myride_class: RideHistoryActivity
    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slidedown, R.anim.slideup)
    }
}
