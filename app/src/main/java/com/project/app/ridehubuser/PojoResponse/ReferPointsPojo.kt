package com.project.app.ridehubuser.PojoResponse

class ReferPointsPojo {

   private var refer_from: String? = null
    private var refer_to: String? = null
    private  var points: String? = null
    private var referrer_name: String? = null
    internal var type: String? = null
    private var ride_id: String? = null
    private var trans_type: String? = null
    private var level: String? = null
    private var date: String? = null



    fun getRefer_from(): String? {
        return refer_from
    }

    fun setRefer_from(refer_from: String) {
        this.refer_from = refer_from
    }

    fun getRefer_to(): String? {
        return refer_to
    }

    fun setRefer_to(refer_to: String) {
        this.refer_to = refer_to
    }

    fun getPoints(): String? {
        return points
    }

    fun setPoints(points: String) {
        this.points = points
    }

    fun getReferrer_name(): String? {
        return referrer_name
    }

    fun setReferrer_name(referrer_name: String) {
        this.referrer_name = referrer_name
    }


    fun getType(): String? {
        return type
    }

    fun setType(type: String) {
        this.type = type
    }

    fun getRide_id(): String? {
        return ride_id
    }

    fun setRide_id(ride_id: String) {
        this.ride_id = ride_id
    }

    fun getTrans_type(): String? {
        return trans_type
    }

    fun setTrans_type(trans_type: String) {
        this.trans_type = trans_type
    }



    fun getLevel(): String? {
        return level
    }

    fun setLevel(level: String) {
        this.level = level
    }

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String) {
        this.date = date
    }




}