package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.dialog.PkDialog2
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.MyRidePaymentListAdapter
import com.project.app.ridehubuser.PojoResponse.PaymentListPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.CurrencySymbolConverter
import com.project.app.ridehubuser.Utils.JSONParser
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.project.app.ridehubuser.subclass.ActivitySubClass
import com.paytm.pgsdk.PaytmOrder
import com.paytm.pgsdk.PaytmPGService
import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import com.stripe.android.PaymentConfiguration
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*


class MyRidePaymentList : ActivitySubClass() {
    private var back: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var UserID = ""
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var itemlist: ArrayList<PaymentListPojo>? = null
    private var adapter: MyRidePaymentListAdapter? = null
    private var listview: ExpandableHeightListView? = null
    private var SrideId_intent = ""
    private var isPaymentAvailable = false
    private var SpaymentCode = ""
    private var TotalAmount = ""
    internal lateinit var EmailID: String
    internal lateinit var mobilenum: String
    internal var mid = ""
    internal var order_id = ""
    internal var cust_id = ""
    internal var channel_id = ""
    internal var callback = ""
    internal var industry_type = ""
    internal var txn_amount = ""
    internal var checksum = ""
    internal var website = ""
    internal var email = ""
    internal var mobile_no = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_payment_list)
        PaymentConfiguration.init(PUBLISHABLE_KEY)

        myride_paymentList_class = this@MyRidePaymentList
        initialize()
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()
        EmailID = user[SessionManager.KEY_EMAIL].toString()
        mobilenum = user[SessionManager.KEY_PHONENO].toString()
        back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        listview!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            cd = ConnectionDetector(this@MyRidePaymentList)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                if (itemlist!![position].getPaymentCode().equals("cash")) {

                    Log.e("GATEWAY", "CASH")
                    MakePayment_Cash(Iconstant.makepayment_cash_url)
                } else if (itemlist!![position].getPaymentCode().equals("wallet")) {
                    Log.e("GATEWAY", "wallet")
                    MakePayment_Wallet(Iconstant.makepayment_wallet_url)
                } else if (itemlist!![position].getPaymentCode().equals("auto_detect")) {
                    Log.e("GATEWAY", "auto_detect")
                    MakePayment_Stripe(Iconstant.makepayment_autoDetect_url)

                }else if (itemlist!![position].getPaymentCode().equals("swipe")) {
                    Log.e("GATEWAY", "SWIPE")
                    MakePayment_Swipe(Iconstant.makepayment_swipe_url)
                } else {
                    SpaymentCode = itemlist!![position].getPaymentCode().toString()
                    Log.e("GATEWAY", SpaymentCode)
                    MakePayment_WebView_MobileID(Iconstant.makepayment_Get_webview_mobileId_url)
                    /*Intent intent = new Intent(MyRidePaymentList.this,PaymentMultilineActivity.class);
                startActivity(intent);*/
                }
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
            }
        }
        //makeFareBreakUp(Iconstant.getfareBreakUpURL);
    }

    private fun initialize() {
        session = SessionManager(this@MyRidePaymentList)
        cd = ConnectionDetector(this@MyRidePaymentList)
        isInternetPresent = cd!!.isConnectingToInternet
        itemlist = ArrayList<PaymentListPojo>()

        back = findViewById(R.id.my_rides_payment_header_back_layout) as ImageView
        listview = findViewById(R.id.my_rides_payment_listView) as ExpandableHeightListView

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        val intent = getIntent()
        SrideId_intent = intent.getStringExtra("RideID")
        TotalAmount = intent.getStringExtra("TotalAmount")
        Log.d("ridepayment", SrideId_intent)

        if (isInternetPresent!!) {
            postRequest_PaymentList(Iconstant.paymentList_url)
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@MyRidePaymentList)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------PaymentList Post Request-----------------
    private fun postRequest_PaymentList(Url: String) {
        dialog = Dialog(this@MyRidePaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))
        println("-------------PaymentList Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------PaymentList Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val payment_array = response_object.getJSONArray("payment")
                            if (payment_array.length() > 0) {
                                itemlist!!.clear()
                                for (i in 0 until payment_array.length()) {
                                    val reason_object = payment_array.getJSONObject(i)
                                    val pojo = PaymentListPojo()
                                    if (reason_object.getString("name").equals("CCavenue", ignoreCase = true)) {
                                        pojo.setPaymentName("Pay Online")
                                    } else {
                                        pojo.setPaymentName(reason_object.getString("name"))
                                    }

                                    pojo.setPaymentCode(reason_object.getString("code"))
                                    itemlist!!.add(pojo)
                                }
                                isPaymentAvailable = true
                            } else {
                                isPaymentAvailable = false
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }
                    if (Sstatus.equals("1", ignoreCase = true) && isPaymentAvailable) {
                        adapter = MyRidePaymentListAdapter(this@MyRidePaymentList, itemlist!!)
                        listview!!.adapter = adapter
                        listview!!.isExpanded = true
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    fun showLoadingDialog() {
        dialog = Dialog(this@MyRidePaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }


    //-----------------------MakePayment Cash Post Request-----------------
    private fun MakePayment_Cash(Url: String) {
        showLoadingDialog()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))
        println("-------------MakePayment Cash Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------MakePayment Cash Response----------------$response")

                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog(this@MyRidePaymentList)
                        mDialog.setDialogTitle(getResources().getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            RideHistoryDetailsActivity.myrideDetail_class.finish()
                            RideHistoryActivity.myride_class.finish()
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        })
                        mDialog.show()
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }
                } catch (e: JSONException) {
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }



    //-----------------------MakePayment Cash Post Request-----------------
    private fun MakePayment_Swipe(Url: String) {
        dialog = Dialog(this@MyRidePaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)


        println("-------------MakePayment Swipe Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent

        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {
                println("-------------MakePayment Swipe Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog2(this@MyRidePaymentList)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            /*mDialog.dismiss()
                            finish()
                            FareBreakUp.farebreakup_class.finish()
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)*/
                            //mDialog.dismiss()
                            val intent = Intent(this@MyRidePaymentList, NavigationDrawer::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override  fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    //-----------------------MakePayment Wallet Post Request-----------------

    private fun MakePayment_Wallet(Url: String) {
        showLoadingDialog()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))
        println("-------------MakePayment Wallet Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("-------------MakePayment Wallet Response----------------$response")

                var Sstatus = ""
                var Scurrency_code = ""
                var Scurrent_wallet_balance = ""
                var sCurrencySymbol = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("0", ignoreCase = true)) {
                        Alert(getResources().getString(R.string.my_rides_payment_empty_wallet_sorry), getResources().getString(R.string.my_rides_payment_empty_wallet))
                    } else if (Sstatus.equals("1", ignoreCase = true)) {
                        //Updating wallet amount on Navigation Drawer Slide
                        Scurrency_code = `object`.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(Scurrency_code)
                        Scurrent_wallet_balance = `object`.getString("wallet_amount")
                        session!!.createWalletAmount(sCurrencySymbol + Scurrent_wallet_balance)
                        NavigationDrawer.navigationNotifyChange()
                        val mDialog = PkDialog(this@MyRidePaymentList)
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_payment_wallet_success))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            RideHistoryDetailsActivity.myrideDetail_class.finish()
                            RideHistoryActivity.myride_class.finish()


                            val intent = Intent(this@MyRidePaymentList, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else if (Sstatus.equals("2", ignoreCase = true)) {
                        //Updating wallet amount on Navigation Drawer Slide
                        Scurrency_code = `object`.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(Scurrency_code)
                        Scurrent_wallet_balance = `object`.getString("wallet_amount")

                        session!!.createWalletAmount(sCurrencySymbol + Scurrent_wallet_balance)
                        NavigationDrawer.navigationNotifyChange()

                        val broadcastIntent = Intent()
                        broadcastIntent.action = "com.package.ACTION_CLASS_REFRESH"
                        sendBroadcast(broadcastIntent)

                        val mDialog = PkDialog(this@MyRidePaymentList)
                        mDialog.setDialogTitle(getResources().getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            postRequest_PaymentList(Iconstant.paymentList_url)
                        })
                        mDialog.show()
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    private fun getFareBreakUp() {}


    //-----------------------MakePayment Auto-Detect Post Request-----------------

    private fun MakePayment_Stripe(Url: String) {
        dialog = Dialog(this@MyRidePaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))
        println("-------------MakePayment Auto-Detect Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------MakePayment Auto-Detect Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog(this@MyRidePaymentList)
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_payment_cash_success))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            RideHistoryDetailsActivity.myrideDetail_class.finish()
                            RideHistoryActivity.myride_class.finish()

                            val intent = Intent(this@MyRidePaymentList, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }


    //-----------------------MakePayment WebView-MobileID Post Request-----------------

    private fun MakePayment_WebView_MobileID(Url: String) {
        dialog = Dialog(this@MyRidePaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))
        println("-------------MakePayment WebView-MobileID Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["gateway"] = SpaymentCode
        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------MakePayment WebView-MobileID Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mobileId = `object`.getString("mobile_id")
                        val intent = Intent(this@MyRidePaymentList, StripeCardsListFareBreakUp::class.java)
                        intent.putExtra("MobileID", mobileId)
                        intent.putExtra("RideID", SrideId_intent)
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter, R.anim.exit)
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }

                    /*if (Sstatus.equalsIgnoreCase("1")) {
                        new PaytmTask().execute();

                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }*/

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })

    }

    internal inner class PaytmTask : AsyncTask<String, String, String>() {


        override fun doInBackground(vararg params: String): String? {

            val url = Iconstant.paytm_generate_checksum
            val jsonParser = JSONParser()


            val r = Random(System.currentTimeMillis())
            val orderId = ("ID" + (1 + r.nextInt(2)) * 10000
                    + r.nextInt(10000))
            val paramMap = HashMap<String, String>()
            paramMap["ORDER_ID"] = orderId
            paramMap["CUST_ID"] = UserID
            paramMap["MID"] = getString(R.string.merchant_id_value)
            paramMap["CHANNEL_ID"] = getString(R.string.channel_id_value)
            paramMap["INDUSTRY_TYPE_ID"] = getString(R.string.industry_type_id_value)
            paramMap["WEBSITE"] = getString(R.string.website_value)
            paramMap["TXN_AMOUNT"] = TotalAmount
            //paramMap.put("EMAIL", EmailID);
            //paramMap.put("MOBILE_NO", mobilenum);
            paramMap["CALLBACK_URL"] = Iconstant.paytm_callback_url + orderId


            val jsonObject = jsonParser.makeHttpRequest(url, "GET", paramMap)
            Log.e("CheckSum result >>", jsonObject.toString())
            try {
                val jsonObject1 = jsonObject.getJSONObject("Result")
                mid = jsonObject1.getString("MID")
                order_id = jsonObject1.getString("ORDER_ID")
                cust_id = jsonObject1.getString("CUST_ID")
                channel_id = jsonObject1.getString("CHANNEL_ID")
                callback = jsonObject1.getString("CALLBACK_URL")
                industry_type = jsonObject1.getString("INDUSTRY_TYPE_ID")
                txn_amount = jsonObject1.getString("TXN_AMOUNT")
                website = jsonObject1.getString("WEBSITE")
                //email = jsonObject1.getString("EMAIL");
                //mobile_no = jsonObject1.getString("MOBILE_NO");
                checksum = jsonObject1.getString("CHECKSUMHASH")

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return null

        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            val Service = PaytmPGService.getProductionService()

            val paramMap = HashMap<String, String>()

            //these are mandatory parameters
            Log.e("TO PAYTM >>", "$mid=$checksum=$callback=$website")

            paramMap["ORDER_ID"] = order_id
            paramMap["CUST_ID"] = cust_id
            paramMap["MID"] = getString(R.string.merchant_id_value)
            paramMap["CHANNEL_ID"] = getString(R.string.channel_id_value)
            paramMap["INDUSTRY_TYPE_ID"] = getString(R.string.industry_type_id_value)
            paramMap["WEBSITE"] = getString(R.string.website_value)
            paramMap["TXN_AMOUNT"] = txn_amount
            // paramMap.put("EMAIL", email);
            // paramMap.put("MOBILE_NO", mobile_no);
            paramMap["CALLBACK_URL"] = Iconstant.paytm_callback_url + order_id
            paramMap["CHECKSUMHASH"] = checksum


            val Order = PaytmOrder(paramMap)



            Service.initialize(Order, null)
            Service.startPaymentTransaction(this@MyRidePaymentList, true, true, object :
                PaytmPaymentTransactionCallback {
                override fun someUIErrorOccurred(inErrorMessage: String) {
                    // Some UI Error Occurred in Payment Gateway Activity.
                    // // This may be due to initialization of views in
                    // Payment Gateway Activity or may be due to //
                    // initialization of webview. // Error Message details
                    // the error occurred.
                }

                override fun onTransactionResponse(inResponse: Bundle) {
                    Log.d("LOG", "Payment Transaction : $inResponse")
                    val response = inResponse.getString("RESPMSG")
                    if (response == "Txn Successful.") {
                        //new ConfirmMerchent().execute();
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show()
                        MakePayment_Online(Iconstant.receivedonline_amount_url, inResponse.toString())

                    } else {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show()
                    }

                    //Toast.makeText(getApplicationContext(), "Payment Transaction response "+inResponse.toString(), Toast.LENGTH_LONG).show();

                }


                override fun networkNotAvailable() {
                    // If network is not
                    // available, then this
                    // method gets called.
                }

                override fun clientAuthenticationFailed(inErrorMessage: String) {
                    // This method gets called if client authentication
                    // failed. // Failure may be due to following reasons //
                    // 1. Server error or downtime. // 2. Server unable to
                    // generate checksum or checksum response is not in
                    // proper format. // 3. Server failed to authenticate
                    // that client. That is value of payt_STATUS is 2. //
                    // Error Message describes the reason for failure.
                }

                override fun onErrorLoadingWebPage(iniErrorCode: Int,
                                                   inErrorMessage: String, inFailingUrl: String) {

                }

                // had to be added: NOTE
                override fun onBackPressedCancelTransaction() {
                    // TODO Auto-generated method stub
                }

                override fun onTransactionCancel(inErrorMessage: String, inResponse: Bundle) {
                    Log.d("LOG", "Payment Transaction Failed $inErrorMessage")
                    Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show()
                }
            })
        }

        //Log.d("Response", result);
    }

    //-----------------------Online Payment Post Request-----------------
    private fun MakePayment_Online(Url: String, response: String) {
        dialog = Dialog(this@MyRidePaymentList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))


        println("-------------Online Payment Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["amount"] = TotalAmount
        jsonParams["online_response"] = response

        mRequest = ServiceRequest(this@MyRidePaymentList)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------Online Payment Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val mDialog = PkDialog(this@MyRidePaymentList)
                        mDialog.setDialogTitle(getResources().getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()

                            TODO()//UNCOOMENT BELOW CODE
                            //FareBreakUp.farebreakup_class.finish()
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            return true
        }
        return false
    }

    companion object {

        lateinit var myride_paymentList_class: MyRidePaymentList


        private val PUBLISHABLE_KEY = "pk_test_0I3IGKx3mcL97rMJ0Dbt5pm9"

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }
}


