package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.HelpCategoryAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.HelpCategoryPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class HelpActivity : ActivityHockeyApp() {

    lateinit var iv_help_close: RelativeLayout
    lateinit var tv_last_trip_time: TextView
    lateinit var latest_ride_map_image: ImageView
    lateinit var tv_last_trip_vehicle: TextView
    lateinit var tv_last_trip_amount: TextView
    lateinit var tv_last_trip_edit_rating: TextView
    lateinit var tv_last_trip_edit_payment: TextView
    lateinit var ll_rideDetaillatest: LinearLayout
    lateinit var cv_edit_rating: CardView
    lateinit var cv_edit_payment: CardView
    lateinit var ll_last_trip: LinearLayout
    lateinit var rv_helpcategory: RecyclerView
    lateinit var dialog: Dialog
    private var mRequest: ServiceRequest? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    var itemlist_all: ArrayList<HelpCategoryPojo> = ArrayList()

    var latest_ride_id : String = ""
    var latest_ride_date : String = ""
    var latest_ride_status : String = ""
    var latest_vehicle_model : String = ""
    var latest_cab_type : String = ""
    var latest_vehicle_no : String = ""
    var latest_image : String = ""
    var latest_total_fare : String = ""
    var latest_ride_time : String = ""
    private var userID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)


        initialise()



        iv_help_close.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        cv_edit_rating.setOnClickListener {
            val intent = Intent(this@HelpActivity, HelpEditRatingPaymentActivity::class.java)
            intent.putExtra("screen", "rating")
            intent.putExtra("RideID", latest_ride_id)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }
        tv_last_trip_edit_rating.setOnClickListener {
            val intent = Intent(this@HelpActivity, HelpEditRatingPaymentActivity::class.java)
            intent.putExtra("screen", "rating")
            intent.putExtra("RideID", latest_ride_id)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }
        cv_edit_payment.setOnClickListener {
            val intent = Intent(this@HelpActivity, HelpEditRatingPaymentActivity::class.java)
            intent.putExtra("screen", "payment")
            intent.putExtra("RideID", latest_ride_id)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }
        tv_last_trip_edit_payment.setOnClickListener {
            val intent = Intent(this@HelpActivity, HelpEditRatingPaymentActivity::class.java)
            intent.putExtra("screen", "payment")
            intent.putExtra("RideID", latest_ride_id)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }

        ll_rideDetaillatest.setOnClickListener {
            val intent = Intent(this@HelpActivity, RideHistoryDetailsActivity::class.java)
            intent.putExtra("RideID", latest_ride_id)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }

        //TODO get last trip api

        ll_last_trip.visibility = View.VISIBLE
        // make visible when last trip is available
    }

    private fun initialise() {
        session = SessionManager(this@HelpActivity)
        cd = ConnectionDetector(this@HelpActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID]!!

        iv_help_close = findViewById(R.id.iv_help_close)
        tv_last_trip_time = findViewById(R.id.tv_last_trip_time)
        latest_ride_map_image = findViewById(R.id.latest_ride_map_image)
        tv_last_trip_vehicle = findViewById(R.id.tv_last_trip_vehicle)
        tv_last_trip_amount = findViewById(R.id.tv_last_trip_amount)
        tv_last_trip_edit_rating = findViewById(R.id.tv_last_trip_edit_rating)
        tv_last_trip_edit_payment = findViewById(R.id.tv_last_trip_edit_payment)
        ll_rideDetaillatest = findViewById(R.id.ll_rideDetaillatest)
        cv_edit_payment = findViewById(R.id.cv_edit_payment)
        cv_edit_rating = findViewById(R.id.cv_edit_rating)
        ll_last_trip = findViewById(R.id.ll_last_trip)
        rv_helpcategory = findViewById(R.id.rv_helpcategory)


        postRequest_MyRidesLatest(Iconstant.myrides_latestride_url)

        postRequest_HelpCategory(Iconstant.help_category_url)

    }



    private fun postRequest_HelpCategory(Url: String) {
        dialog = Dialog(this)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------HELPCATEGORY Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        mRequest = ServiceRequest(this)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    dialog.dismiss()
                    println("-------------HELPCATEGORY Response----------------$response")

                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            //val response_object = `object`.getJSONObject("response")
                            if (`object`.length() > 0) {
                                val category_object = `object`.get("data")
                                if (category_object is JSONArray) {
                                    val category_data_array = `object`.getJSONArray("data")
                                    if (category_data_array.length() > 0) {
                                        for (i in 0 until category_data_array.length()) {
                                            val catdata_object = category_data_array.getJSONObject(i)
                                            Log.d("ride_object", "" + catdata_object)
                                            val pojo = HelpCategoryPojo()
                                            pojo.setId(catdata_object.getString("id"))
                                            pojo.setCategory(catdata_object.getString("category"))
                                            pojo.setStatus(catdata_object.getString("status"))
                                            itemlist_all.add(pojo)

                                        }
                                    }
                                }

                            }
                        }
                        if (Sstatus.equals("1", ignoreCase = true)) {
                                val layoutManager = LinearLayoutManager(
                                    this@HelpActivity,
                                    RecyclerView.VERTICAL,
                                    false
                                )
                            rv_helpcategory.layoutManager = layoutManager
                                val adapter = HelpCategoryAdapter(this@HelpActivity, itemlist_all)
                            rv_helpcategory.adapter = adapter


                        } else {
                            val Sresponse = `object`.getString("response")
                            //alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                        dialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        dialog.dismiss()
                    }

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }




    private fun postRequest_MyRidesLatest(Url: String) {
       val dialog = Dialog(this)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------MyRidesLatest Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        mRequest = ServiceRequest(this)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    dialog.dismiss()
                    println("-------------MyRides Response----------------$response")
                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            if (response_object.length() > 0) {
                                val check_rides_object = response_object.get("rides")
                                if (check_rides_object is JSONArray) {
                                    val ride_array = response_object.getJSONArray("rides")
                                    if (ride_array.length() > 0) {
                                        for (i in 0 until ride_array.length()) {
                                            val ride_object = ride_array.getJSONObject(i)
                                            Log.d("ride_object", "" + ride_object)
                                            latest_ride_id = ride_object.getString("ride_id")
                                            latest_ride_time = ride_object.getString("ride_time")
                                            if (ride_object.getString("ride_date").contains("/")) {
                                                val replaceDate =
                                                    ride_object.getString("ride_date").toString()
                                                        .replace("/", "-")
                                                latest_ride_date = replaceDate
                                            } else {
                                                latest_ride_date = ride_object.getString("ride_date")
                                            }
                                            latest_ride_status = ride_object.getString("ride_status")
                                            latest_vehicle_model = ride_object.getString("vehicle_model")
                                            latest_total_fare = ride_object.getString("total_fare")
                                            latest_cab_type = ride_object.getString("cab_type")
                                            latest_vehicle_no = ride_object.getString("vehicle_no")
                                            latest_image = ride_object.getString("image")
                                        }

                                        tv_last_trip_time.setText(latest_ride_date +" "+latest_ride_time)
                                        tv_last_trip_vehicle.setText(latest_cab_type)
                                        tv_last_trip_amount.setText("$ "+latest_total_fare)
                                        Picasso.with(this@HelpActivity).load("https://ridehub.co/"+latest_image).into(latest_ride_map_image)

                                    } else {
                                    }
                                } else {
                                }
                            }

                        }

                        dialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        dialog.dismiss()
                    }

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }




    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.slidedown, R.anim.slideup)
    }

}
