package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.MyRideStripeCardListAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.StripeCardListPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class Payment : ActivityHockeyApp() {

    lateinit var rv_available_cards: ListView
    lateinit var tv_add_payment_methods: TextView
    lateinit var tv_wallet_amount: TextView
    lateinit var tv_make_payment: TextView
    lateinit var back: ImageView
    lateinit var cv_wallet: CardView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null

    lateinit var userID: String
    lateinit var emailID: String
    lateinit var mobileNum: String
    lateinit var walletAmount: String

    private var itemStripeCardList: ArrayList<StripeCardListPojo>? = null
    private var isPaymentAvailable = false
    private var adapter: MyRideStripeCardListAdapter? = null

    private var dialog: Dialog? = null
    private var mRequest: ServiceRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        initialize()

        val user = session!!.getUserDetails()
        val wallet = session!!.walletAmount
        userID = user[SessionManager.KEY_USERID].toString()
        emailID = user[SessionManager.KEY_EMAIL].toString()
        mobileNum = user[SessionManager.KEY_PHONENO].toString()
        walletAmount = wallet[SessionManager.KEY_WALLET_AMOUNT].toString()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rv_available_cards!!.isNestedScrollingEnabled = true
        }
        back.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }
        tv_wallet_amount.requestFocus()
        tv_wallet_amount.text = walletAmount

        tv_add_payment_methods.setOnClickListener {
            val intent = Intent(this@Payment, AddPaymentActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }

        cv_wallet.setOnClickListener {
            val intent = Intent(this@Payment, AddCashToWalletActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        if (isInternetPresent!!) {
            //postRequest_StripeCardList(Iconstant.makepayment_stripe_cardList)
            getWalletAmount(Iconstant.cabily_money_url)
        } else {
            alert(
                resources.getString(R.string.alert_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

    }

    private fun initialize() {
        session = SessionManager(this@Payment)
        cd = ConnectionDetector(this@Payment)
        isInternetPresent = cd!!.isConnectingToInternet

        rv_available_cards = findViewById(R.id.rv_available_cards)
        tv_add_payment_methods = findViewById(R.id.tv_add_payment_methods)
        tv_make_payment = findViewById(R.id.tv_make_payment)
        back = findViewById(R.id.iv_close_payment)
        tv_wallet_amount = findViewById(R.id.tv_wallet_amount)
        cv_wallet = findViewById(R.id.cv_wallet)

        val manager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        //rv_available_cards.layoutManager = manager
        itemStripeCardList = ArrayList()
    }

    private fun postRequest_StripeCardList(Url: String) {
        dialog = Dialog(this@Payment)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(resources.getString(R.string.action_pleasewait))

        println("-------------StripeCardList Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        mRequest = ServiceRequest(this@Payment)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------StripeCardList Response----------------$response")
                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")

                            if (response_object.length() > 0) {
                                val card_array = response_object.getJSONArray("cardlist")
                                if (card_array.length() > 0) {
                                    itemStripeCardList!!.clear()
                                    for (i in 0 until card_array.length()) {
                                        val reason_object = card_array.getJSONObject(i)
                                        val pojo = StripeCardListPojo()
                                        pojo.setLast4_digits(reason_object.getString("last4_digits"))
                                        pojo.setCredit_card_type(reason_object.getString("credit_card_type"))
                                        pojo.setExp_year(reason_object.getString("exp_year"))
                                        pojo.setExp_month(reason_object.getString("exp_month"))
                                        pojo.setCard_id(reason_object.getString("card_id"))

                                        itemStripeCardList!!.add(pojo)
                                    }
                                    adapter = MyRideStripeCardListAdapter(
                                        this@Payment, itemStripeCardList!!,"payment"
                                    )
                                    rv_available_cards.adapter = adapter
                                    isPaymentAvailable = true
                                } else {
                                    isPaymentAvailable = false
                                }
                            }
                        } else {
                            val Sresponse = `object`.getString("response")
                            alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                        if (Sstatus.equals("1", ignoreCase = true) && isPaymentAvailable) {

                        }

                    } catch (e: JSONException) {
                        Log.e("payment", "" + e)
                    }

                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    fun getWalletAmount(url:String){
        dialog = Dialog(this@Payment)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
      //  dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        mRequest = ServiceRequest(this@Payment)
        mRequest!!.makeServiceRequest(
            url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------amountWallet_Response----------------$response")

                    postRequest_StripeCardList(Iconstant.makepayment_stripe_cardList)
                  //  dialog!!.dismiss()

                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")

                            if (response_object.length() > 0) {

                                val currency_stg = response_object.getString("currency")
                                val current_balance_stg = response_object.getString("current_balance")

                                tv_wallet_amount.text="$ "+current_balance_stg

                                session!!.createWalletAmount(current_balance_stg)
Log.e("lalallal",current_balance_stg)
                            }
                        }
                    }catch (e:Exception){

                    }

                    /*"status": "1",
   "response": {
      "currency": "USD",
       "current_balance": "140.00",
        "recharge_boundary": {
            "min_amount": 1,
         "middle_amount": 50.5,
            "max_amount": 100
         }
   },
     "auto_charge_status": "0"
 }*/

                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    private fun alert(title: String, alert: String) {
        val mDialog = PkDialog(this@Payment)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

}
