package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.RatingAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.RatingPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class HelpEditRatingPaymentActivity : ActivityHockeyApp() {


    lateinit var ll_help_trip_rating: LinearLayout
    lateinit var ll_help_trip_payment: LinearLayout
    lateinit var iv_close_help_trip: ImageView
    lateinit var ll_submit_rating: LinearLayout

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var listview: ExpandableHeightListView? = null
    private var RideID: String? = null
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    internal var itemlist: ArrayList<RatingPojo>? = null
    internal lateinit var adapter: RatingAdapter
    private var isDataAvailable = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_trip)

        initialise()

        val screen = intent.getStringExtra("screen")

        if (screen == "rating") {
            ll_help_trip_payment.visibility = View.GONE
            ll_help_trip_rating.visibility = View.VISIBLE
        } else if (screen == "payment") {
            ll_help_trip_payment.visibility = View.VISIBLE
            ll_help_trip_rating.visibility = View.GONE
        }

        iv_close_help_trip.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        ll_submit_rating.setOnClickListener {

            var isRatingEmpty = false

            if (itemlist != null) {
                for (i in itemlist!!.indices) {
                    if (itemlist!![i].getRatingcount()?.length == 0 || itemlist!![i].getRatingcount().equals("0.0")) {
                        isRatingEmpty = true
                    }
                }
                if (!isRatingEmpty) {
                    if (isInternetPresent!!) {
                        println("------------ride_id-------------$RideID")
                        /*if (Et_comment!!.text.toString().length > 0) {*/
                        val jsonParams = HashMap<String, String>()
                        jsonParams["comments"] = ""
                        jsonParams["ratingsFor"] = "driver"
                        jsonParams["ride_id"] = RideID!!
                        for (i in itemlist!!.indices) {
                            jsonParams["ratings[$i][option_id]"] = itemlist!![i].getRatingId().toString()
                            jsonParams["ratings[$i][option_title]"] = itemlist!![i].getRatingName().toString()
                            jsonParams["ratings[$i][rating]"] = itemlist!![i].getRatingcount().toString()
                        }
                        println("------------jsonParams-------------$jsonParams")
                        postRequest_SubmitRating(Iconstant.myride_rating_submit_url, jsonParams)
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                    }
                } else {
                    Alert(getResources().getString(R.string.my_rides_rating_header_sorry_textview), getResources().getString(R.string.my_rides_rating_header_enter_all))
                }
            }


        }

    }

    private fun initialise() {

        session = SessionManager(this@HelpEditRatingPaymentActivity)
        cd = ConnectionDetector(this@HelpEditRatingPaymentActivity)
        isInternetPresent = cd!!.isConnectingToInternet
        RideID = intent.getStringExtra("RideID")
        itemlist = ArrayList<RatingPojo>()

        ll_help_trip_rating = findViewById(R.id.ll_help_trip_rating)
        ll_help_trip_payment = findViewById(R.id.ll_help_trip_payment)
        iv_close_help_trip = findViewById(R.id.iv_close_help_trip)
        ll_submit_rating = findViewById(R.id.ll_submit_rating)
        listview = findViewById(R.id.my_rides_rating_listView) as ExpandableHeightListView



        if (isInternetPresent!!) {
            postRequest_RatingList(Iconstant.myride_rating_url)
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slidedown, R.anim.slideup)
    }



    //-----------------------Rating List Post Request-----------------
    private fun postRequest_RatingList(Url: String) {
        dialog = Dialog(this@HelpEditRatingPaymentActivity)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))


        println("-------------Rating List Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["optionsFor"] = "driver"
        jsonParams["ride_id"] = RideID!!
        println("rideid-----------$RideID")

        mRequest = ServiceRequest(this@HelpEditRatingPaymentActivity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override  fun onCompleteListener(response: String) {

                Log.e("rateing", response)

                println("-------------Rating List Response----------------$response")

                var Sstatus = ""
                var SRating_status = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    SRating_status = `object`.getString("ride_ratting_status")

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val payment_array = `object`.getJSONArray("review_options")
                        if (payment_array.length() > 0) {
                            itemlist!!.clear()
                            for (i in 0 until payment_array.length()) {
                                val reason_object = payment_array.getJSONObject(i)
                                val pojo = RatingPojo()
                                pojo.setRatingId(reason_object.getString("option_id"))
                                pojo.setRatingName(reason_object.getString("option_title"))
                                pojo.setRatingcount("")
                                itemlist!!.add(pojo)
                            }
                            isDataAvailable = true
                        } else {
                            isDataAvailable = false
                        }
                    }

                    if (Sstatus.equals("1", ignoreCase = true) && isDataAvailable) {
                        if (SRating_status.equals("1", ignoreCase = true)) {
                            Toast.makeText(getApplicationContext(), "Already submitted your rating", Toast.LENGTH_SHORT).show()
                        } else {
                            adapter = RatingAdapter(this@HelpEditRatingPaymentActivity, itemlist!!)
                            listview!!.adapter = adapter
                            listview!!.isExpanded = true
                        }
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------------Submit Rating Post Request-----------------
    private fun postRequest_SubmitRating(Url: String, jsonParams: HashMap<String, String>) {
        dialog = Dialog(this@HelpEditRatingPaymentActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))


        println("-------------Submit Rating Url----------------$Url")

        mRequest = ServiceRequest(this@HelpEditRatingPaymentActivity)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                println("------------Submit Rating Response----------------$response")

                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val broadcastIntent = Intent()
                        broadcastIntent.action = "com.pushnotification.updateBottom_view"
                        sendBroadcast(broadcastIntent)

                        val mDialog = PkDialog(this@HelpEditRatingPaymentActivity)
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                        mDialog.setDialogMessage(getResources().getString(R.string.my_rides_rating_submit_successfully))
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            val broadcastIntent = Intent()
                            broadcastIntent.action = "com.pushnotification.updateBottom_view"
                            println("------------Submit Rating Response----------------OKDONE"+broadcastIntent.action)
                            sendBroadcast(broadcastIntent)
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                            finish()
                            val intent = Intent(this@HelpEditRatingPaymentActivity, HelpActivity::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.enter, R.anim.exit)

                        })
                        mDialog.show()
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse)
                    }
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override  fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@HelpEditRatingPaymentActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

}