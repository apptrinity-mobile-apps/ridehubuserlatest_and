package com.project.app.ridehubuser.PojoResponse

import java.io.Serializable
import java.util.*

class EstimateDetailPojo : Serializable {
    var rate_cartype: String? = null
    var rate_note: String? = null
    var minfare_amt: String? = null
    var minfare_km: String? = null
    var afterfare_amt: String? = null
    var afterfare_km: String? = null
    var otherfare_amt: String? = null
    var otherfare_km: String? = null
    var currencyCode: String? = null

    var estimatePojo: ArrayList<EstimateDetailPojo>? = null

    constructor() {}

    constructor(dummy: String, data: ArrayList<EstimateDetailPojo>) {
        this.estimatePojo = data
    }
}