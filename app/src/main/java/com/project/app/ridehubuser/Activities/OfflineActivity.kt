package com.project.app.ridehubuser.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.project.app.mylibrary.gps.GPSTracker
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.Adapters.PlaceSearchAdapter
import com.project.app.ridehubuser.Fragments.Fragment_HomePage
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import indo.com.ridehub_lyft_uber.HockeyApp.FragmentActivityHockeyApp
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class OfflineActivity : FragmentActivityHockeyApp(),
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null
    private var session: SessionManager? = null
    lateinit var back: ImageView
    var userID = ""
    lateinit var et_chauffl_id: EditText
    lateinit var ll_source: LinearLayout
    lateinit var ll_destination: LinearLayout
    lateinit var ll_submit: LinearLayout
    lateinit var tv_source: TextView
    lateinit var tv_destination: TextView
    var source = ""
    var destination = ""
    var destination_lat = ""
    var destination_lon = ""
    var source_lat = ""
    var source_lon = ""
    lateinit var gps: GPSTracker
    internal lateinit var mLocationRequest: LocationRequest

    private var mGoogleApiClient: GoogleApiClient? = null
    internal lateinit var result: PendingResult<LocationSettingsResult>
    internal val REQUEST_LOCATION = 299

    lateinit var listview: ListView
    lateinit var et_source: EditText
    private var mRequest: ServiceRequest? = null
    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()

    internal var adapter: PlaceSearchAdapter? = null
    private var isdataAvailable = false
    internal var dialog: Dialog? = null
    lateinit var location_search_dialog: Dialog
    var location_stg = ""
    var lat_stg = ""
    var lon_stg = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offline)
        context = applicationContext
        initialize()

        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()

        //Start XMPP Chat Service
        try {
            ChatService.startUserAction(this@OfflineActivity)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            source_lat = gps.getLatitude().toString()
            source_lon = gps.getLongitude().toString()
            source = getCompleteAddressString(gps.getLatitude(), gps.getLongitude())
            tv_source.text = source
        } else {
            enableGpsService()
        }
        if (ContextCompat.checkSelfPermission(
                this@OfflineActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            buildGoogleApiClient()
        }
        back.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
        ll_source.setOnClickListener {
            showLocationSearchDialog(tv_source, "source")
        }
        ll_destination.setOnClickListener {
            showLocationSearchDialog(tv_destination, "destination")
        }
        tv_source.setOnClickListener {
            showLocationSearchDialog(tv_source, "source")
        }
        tv_destination.setOnClickListener {
            showLocationSearchDialog(tv_destination, "destination")
        }
        et_chauffl_id.imeOptions = EditorInfo.IME_ACTION_DONE
        ll_submit.setOnClickListener {
            when {
                tv_source.text.toString() == "" -> {
                    Toast.makeText(
                        this@OfflineActivity,
                        "Pick up location must not be empty!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                tv_destination.text.toString() == "" -> {
                    Toast.makeText(
                        this@OfflineActivity,
                        "Drop location must not be empty!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                et_chauffl_id.text.toString() == "" -> {
                    Toast.makeText(
                        this@OfflineActivity,
                        "Chauffl ID must not be empty!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                else -> {
                    book_offline_ride(
                        Iconstant.book_offline_ride_url,
                        tv_source.text.toString(),
                        tv_destination.text.toString(),
                        et_chauffl_id.text.toString(),
                        source_lon,
                        source_lat,
                        destination_lon,
                        destination_lat,
                        userID
                    )
                }
            }
        }
    }

    private fun initialize() {
        session = SessionManager(this@OfflineActivity)
        cd = ConnectionDetector(this@OfflineActivity)
        isInternetPresent = cd!!.isConnectingToInternet
        back = findViewById(R.id.iv_offline_close)

        ll_destination = findViewById(R.id.ll_destination)
        ll_source = findViewById(R.id.ll_source)
        tv_destination = findViewById(R.id.tv_destination)
        tv_source = findViewById(R.id.tv_source)
        et_chauffl_id = findViewById(R.id.et_chauffl_id)
        ll_submit = findViewById(R.id.ll_submit)

        gps = GPSTracker(this@OfflineActivity)
    }

    private fun showLocationSearchDialog(textView: TextView, source_or_destination: String) {
        location_search_dialog = Dialog(this@OfflineActivity)
        location_search_dialog.window
        location_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        location_search_dialog.setContentView(R.layout.fav_place_search_dialog)
        location_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        location_search_dialog.setCanceledOnTouchOutside(false)
        location_search_dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        location_search_dialog.show()

        val iv_close = location_search_dialog.findViewById(R.id.iv_close) as ImageView
        et_source = location_search_dialog.findViewById(R.id.tv_source) as EditText
        listview = location_search_dialog.findViewById(R.id.location_search_listView) as ListView

        iv_close.setOnClickListener { location_search_dialog.dismiss() }
        listview.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                location_stg = itemList_location[position]
                textView.text = location_stg
                location_search_dialog.dismiss()
                cd = ConnectionDetector(this@OfflineActivity)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    latLongRequest(
                        Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[position],
                        source_or_destination
                    )
                } else {
                    Toast.makeText(
                        this@OfflineActivity,
                        resources.getString(R.string.alert_nointernet),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }

        et_source.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

                cd = ConnectionDetector(this@OfflineActivity)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    val data = et_source.text.toString().toLowerCase(Locale.getDefault())
                        .replace(" ", "%20")
                    citySearchRequest(Iconstant.place_search_url + data)
                } else {
                    Toast.makeText(
                        this@OfflineActivity,
                        resources.getString(R.string.alert_nointernet),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            }
        })

        et_source.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event!!.keyCode === KeyEvent.KEYCODE_ENTER) {
                closeKeyboard(et_source)
            }
            false
        }

    }

    /*-------------Method to get Complete Address------------*/
    private fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {
                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                } else {
                    try {
                        strReturnedAddress.append(returnedAddress.getAddressLine(0))
                    } catch (ignored: Exception) {
                    }

                }
                strAdd = strReturnedAddress.toString()
            } else {
                Log.e("Current loction address", "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Current loction address", "Canont get Address!")
        }

        return strAdd
    }

    //Enabling Gps Service
    private fun enableGpsService() {
        mGoogleApiClient = GoogleApiClient.Builder(this@OfflineActivity)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        mGoogleApiClient!!.connect()
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    try {
                        status.startResolutionForResult(this@OfflineActivity, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this@OfflineActivity)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient!!.connect()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slidedown, R.anim.slideup)
    }

    override fun onConnected(p0: Bundle?) {}

    override fun onConnectionSuspended(p0: Int) {}

    override fun onConnectionFailed(p0: ConnectionResult) {}

    private fun closeKeyboard(editText: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            editText.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    private fun citySearchRequest(Url: String) {

        println("--------------Search city url-------------------$Url")

        mRequest = ServiceRequest(this@OfflineActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Search city  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_array = `object`.getJSONArray("predictions")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_array.length() > 0) {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    for (i in 0 until place_array.length()) {
                                        val place_object = place_array.getJSONObject(i)
                                        itemList_location.add(place_object.getString("description"))
                                        itemList_placeId.add(place_object.getString("place_id"))
                                    }
                                    isdataAvailable = true
                                } else {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    isdataAvailable = false
                                }
                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    /*if (!isdataAvailable) {
                        ll_location_search.visibility = View.GONE
                    } else {
                        ll_location_search.visibility = View.VISIBLE
                    }*/
                    adapter = PlaceSearchAdapter(this@OfflineActivity, itemList_location)
                    listview.adapter = adapter!!
                    adapter!!.notifyDataSetChanged()
                }

                override fun onErrorListener() {
                    // close keyboard
                    closeKeyboard(et_source)
                }
            })
    }

    private fun latLongRequest(Url: String, source_or_destination: String) {

        dialog = Dialog(this@OfflineActivity)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        println("--------------LatLong url-------------------$Url")

        mRequest = ServiceRequest(this@OfflineActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------LatLong  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_object = `object`.getJSONObject("result")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_object.length() > 0) {
                                    val geometry_object = place_object.getJSONObject("geometry")
                                    if (geometry_object.length() > 0) {
                                        val location_object =
                                            geometry_object.getJSONObject("location")
                                        if (location_object.length() > 0) {
                                            lat_stg = location_object.getString("lat")
                                            lon_stg = location_object.getString("lng")
                                            isdataAvailable = true
                                        } else {
                                            isdataAvailable = false
                                        }
                                    } else {
                                        isdataAvailable = false
                                    }
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    if (isdataAvailable) {
                        dialog!!.dismiss()
                        if (source_or_destination == "source") {
                            source_lat = lat_stg
                            source_lon = lon_stg
                        } else if (source_or_destination == "destination") {
                            destination_lat = lat_stg
                            destination_lon = lon_stg
                        }
                    } else {
                        dialog!!.dismiss()
                        Toast.makeText(this@OfflineActivity, status, Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    private fun book_offline_ride(
        Url: String,
        pickup_location: String,
        drop_location: String,
        chauffl_no: String,
        pickup_lon: String,
        pickup_lat: String,
        drop_lon: String,
        drop_lat: String,
        user_id: String
    ) {

        dialog = Dialog(this@OfflineActivity)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        val jsonParams = HashMap<String, String>()
        jsonParams["pickup_location"] = pickup_location
        jsonParams["drop_location"] = drop_location
        jsonParams["chauffl_no"] = chauffl_no
        jsonParams["pickup_lon"] = pickup_lon
        jsonParams["pickup_lat"] = pickup_lat
        jsonParams["drop_lon"] = drop_lon
        jsonParams["drop_lat"] = drop_lat
        jsonParams["user_id"] = user_id

        println("--------------book offline-------------------$Url-------$jsonParams")

        mRequest = ServiceRequest(this@OfflineActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("--------------offline ride response-------------------$response")

                    try {
                        val `object` = JSONObject(response)
                        Log.d("response_test", response)
                        if (`object`.length() > 0) {
                            val status = `object`.getString("status")
                            if (status.equals("1", ignoreCase = true)) {
                                Toast.makeText(this@OfflineActivity, "Call sent successfully ", Toast.LENGTH_SHORT)
                                    .show()
                                val i = Intent(this@OfflineActivity,NavigationDrawer::class.java)
                                startActivity(i)
                                finish()

                            } else {
                                val response_stg = `object`.getString("response")
                                Toast.makeText(this@OfflineActivity, response_stg, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

}