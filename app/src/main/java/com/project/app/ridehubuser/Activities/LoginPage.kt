package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.AppInfoSessionManager
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.CurrencySymbolConverter
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.NumberFormat
import java.util.*

class LoginPage : ActivityHockeyApp() {
    private var back: RelativeLayout? = null
    private var forgotPwd: RelativeLayout? = null
    private var username: EditText? = null
    private var password: EditText? = null
    private var tv_signup: TextView? = null
    private lateinit var iv_show_password: ImageView
    private var submit: TextView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var session: SessionManager? = null

    private var appinfo_session: AppInfoSessionManager? = null

    private var Str_Hash = ""

    private var mHandler: Handler? = null
    private var sCurrencySymbol = ""
    private var android_id: String? = null

    private var GCM_Id = ""

    private var isPasswordShown = false

    //----------------------Code for TextWatcher-------------------------
    private val loginEditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (username!!.text.isNotEmpty()) {
                username!!.error = null
            }
            if (password!!.text.isNotEmpty()) {
                password!!.error = null
            }
        }
    }

    //--------Handler Method------------
    internal var dialogRunnable: Runnable = Runnable {
        dialog = Dialog(this@LoginPage)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    //--------Handler Method------------
    internal var dialogFacebookRunnable: Runnable = Runnable {
        dialog = Dialog(this@LoginPage)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        setContentView(R.layout.loginpage)
        setContentView(R.layout.activity_login)
        context = applicationContext
        //mAsyncRunner = AsyncFacebookRunner(facebook)

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this@LoginPage, object:
            OnSuccessListener<InstanceIdResult> {
           override fun onSuccess(instanceIdResult:InstanceIdResult) {
                val newToken = instanceIdResult.getToken()
               GCM_Id = newToken
                Log.e("newToken", GCM_Id)
            }
        })

        android_id = Settings.Secure.getString(
            context!!.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        initialize()

        try {
            val info = packageManager.getPackageInfo(
                "your.package",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }
         forgotPwd!!.setOnClickListener {
           val i = Intent(this@LoginPage, ForgotPassword::class.java)
             startActivity(i)
             overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
         }

        tv_signup!!.setOnClickListener {
            val i = Intent(this@LoginPage, RegisterPage::class.java)
            i.putExtra("HashKey", Str_Hash)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        submit!!.setOnClickListener {
            if (username!!.text.toString().isEmpty()) {
                erroredit(username!!, resources.getString(R.string.login_label_alert_username))
            } else if (!isValidEmail(username!!.text.toString())) {
                erroredit(username!!, resources.getString(R.string.login_label_alert_email_invalid))
            } else if (password!!.text.toString().isEmpty()) {
                erroredit(password!!, resources.getString(R.string.login_label_alert_password))
            } else {
                cd = ConnectionDetector(this@LoginPage)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    mHandler!!.post(dialogRunnable)
                    //---------Getting GCM Id----------=
                    PostRequest(Iconstant.loginurl)
                } else {
                    Alert(
                        resources.getString(R.string.alert_nointernet),
                        resources.getString(R.string.alert_nointernet_message)
                    )
                }
            }
        }


        username!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(username!!)
            }
            false
        }


        password!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(password!!)
            }
            false
        }

        iv_show_password.setOnClickListener {
            if (isPasswordShown){
                iv_show_password.setImageResource(R.drawable.ic_hide)
                password!!.transformationMethod = PasswordTransformationMethod()
                isPasswordShown = false
            }else{
                iv_show_password.setImageResource(R.drawable.ic_visible)
                password!!.transformationMethod = HideReturnsTransformationMethod()
                isPasswordShown = true
            }
        }

    }

    private fun initialize() {
        session = SessionManager(this@LoginPage)
        appinfo_session = AppInfoSessionManager(this@LoginPage)
        cd = ConnectionDetector(this@LoginPage)
        isInternetPresent = cd!!.isConnectingToInternet
        mHandler = Handler()


        val i = intent
        Str_Hash = i.getStringExtra("HashKey")!!

        println("Str_Hash------------$Str_Hash")

       forgotPwd = findViewById<RelativeLayout>(R.id.login_forgotpwd_layout)
        username = findViewById<EditText>(R.id.et_user_name)
       tv_signup = findViewById<TextView>(R.id.tv_signup)


        password = findViewById<EditText>(R.id.et_password)
        submit = findViewById<TextView>(R.id.tv_login)
        iv_show_password = findViewById(R.id.iv_show_password)

//        submit!!.typeface = Typeface.createFromAsset(assets, "fonts/Poppins-Medium.ttf")

        //code to make password editText as dot
        password!!.transformationMethod = PasswordTransformationMethod()

        username!!.addTextChangedListener(loginEditorWatcher)
        password!!.addTextChangedListener(loginEditorWatcher)
    }


    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@LoginPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //--------------------Code to set error for EditText-----------------------
    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@LoginPage, R.anim.shake)
        editname.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }


    // -------------------------code for Login Post Request----------------------------------

    private fun PostRequest(Url: String) {

        println("-------GCM_Id-------$GCM_Id")

        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = username!!.text.toString() /*"manibabu@indobytes.com"*/
        jsonParams["password"] = password!!.text.toString()/*"123456"*/
        jsonParams["gcm_id"] = GCM_Id

        mRequest = ServiceRequest(this@LoginPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    Log.e("login", response)

                    println("--------------Login reponse-------------------$response")
                    var Sstatus = ""
                    var Smessage = ""
                    var Suser_image = ""
                    var Suser_id = ""
                    var Suser_name = ""
                    var Semail = ""
                    var Scountry_code = ""
                    var SphoneNo = ""
                    var Sreferal_code = ""
                    var Scategory = ""
                    val Subcategory = ""
                    var SsecretKey = ""
                    var SwalletAmount = ""
                    var ScurrencyCode = ""

                    var is_alive_other = ""

                    var gcmId = ""


                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("message")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            Suser_image = `object`.getString("user_image")
                            Suser_id = `object`.getString("user_id")
                            Suser_name = `object`.getString("user_name")
                            Semail = `object`.getString("email")
                            Scountry_code = `object`.getString("country_code")
                            SphoneNo = `object`.getString("phone_number")
                            Sreferal_code = `object`.getString("referal_code")
                            Scategory = `object`.getString("category")
                            //Subcategory = object.getString("subcategory");
                            SsecretKey = `object`.getString("sec_key")
                            SwalletAmount = `object`.getString("wallet_amount")
                            gcmId = `object`.getString("key")
                            ScurrencyCode = `object`.getString("currency")
                            is_alive_other = `object`.getString("is_alive_other")
                            sCurrencySymbol =
                                CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        session!!.createLoginSession(
                            Semail,
                            Suser_id,
                            Suser_name,
                            Suser_image,
                            Scountry_code,
                            SphoneNo,
                            Sreferal_code,
                            Scategory,
                            Subcategory,
                            gcmId
                        )
                        session!!.createWalletAmount(sCurrencySymbol + SwalletAmount)

                        Log.e("login_response", "$Suser_id==$SsecretKey")
                        session!!.setXmppKey(Suser_id, SsecretKey)

                        println("insidesession gcm--------------$gcmId")

                        if (is_alive_other.equals("Yes", ignoreCase = true)) {

                            val mDialog = PkDialog(this@LoginPage)
                            mDialog.setDialogTitle(resources.getString(R.string.app_name))
                            mDialog.setDialogMessage(resources.getString(R.string.alert_multiple_login))
                            mDialog.setPositiveButton(
                                resources.getString(R.string.action_ok),
                                View.OnClickListener {
                                    mDialog.dismiss()
                                    try{
                                        ChatService.startUserAction(this@LoginPage)
                                    }catch (e:Exception){
                                        e.printStackTrace()
                                    }
                                    SingUpAndSignIn.activty.finish()
                                    val intent = Intent(context, UpdateUserLocation::class.java)
                                    //val intent = Intent(context, ChatActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                })
                            mDialog.show()

                        } else {

                            try{
                                ChatService.startUserAction(this@LoginPage)
                            }catch (e:Exception){
                                e.printStackTrace()
                            }

                            SingUpAndSignIn.activty.finish()
                            val intent = Intent(context, UpdateUserLocation::class.java)
                            //val intent = Intent(context, ChatActivity::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

                        }

                    } else {
                        Alert(resources.getString(R.string.login_label_alert_signIn_failed), Smessage)
                    }
                    // close keyboard
                    val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mgr.hideSoftInputFromWindow(username!!.windowToken, 0)

                    if (dialog != null) {
                        dialog!!.dismiss()
                    }
                }

                override fun onErrorListener() {
                    if (dialog != null) {
                        dialog!!.dismiss()
                    }
                }
            })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            this@LoginPage.finish()
            this@LoginPage.overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            return true
        }
        return false
    }

    companion object {
        // Your FaceBook APP ID
        //private static String APP_ID = "468945646630814";
        private val APP_ID = "335037517017419"


        //-------------------------code to Check Email Validation-----------------------
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }


}
