package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.PlaceSearchAdapter
import com.project.app.ridehubuser.Fragments.Fragment_HomePage
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.CustomProgress
import com.project.app.ridehubuser.Utils.ProgressListener
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LocationSearch : ActivityHockeyApp(), ProgressListener {

    private var back: RelativeLayout? = null
    private var et_desti_search: EditText? = null
    private var et_source_search: EditText? = null
    private var listview: ListView? = null
    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var tv_emptyText: TextView? = null
    private var progresswheel: ProgressBar? = null
    private var iv_source_clear: ImageView? = null
    private var iv_destination_clear: ImageView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var context: Context
    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()

    internal lateinit var adapter: PlaceSearchAdapter
    private var isdataAvailable = false

    private var Slatitude = ""
    private var Dlatitude = ""
    private var Slongitude = ""
    private var Dlongitude = ""
    private var Sselected_location = ""

    internal var dialog: Dialog? = null
    internal lateinit var source_address: String
    internal lateinit var source_address2: String
    internal lateinit var setpickuploc: String
    internal var identi = 0
    internal lateinit var session: SessionManager
    private var UserID: String? = null
    internal lateinit var ET_source_status: String
    internal var ET_destination_status = ""
    private var tv_fav_workplace: TextView? = null
    private var tv_fav_homeplace: TextView? = null
    private var tv_fav_headworkplace: TextView? = null
    private var tv_fav_headhomeplace: TextView? = null
    private var fav_alert_layout: RelativeLayout? = null
    private var fav_tv_emptyText: TextView? = null
    private var fav_alert_textview: TextView? = null
    private var ll_mainsearch: LinearLayout? = null
    internal lateinit var fragment_homePage: Fragment_HomePage
    internal lateinit var reccent_rides_listview: RecyclerView

    lateinit var avd: AnimatedVectorDrawableCompat
    lateinit var iv_line: ImageView

    lateinit var ll_pickuponmap: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.place_search)
        context = applicationContext
        initialize()

        fragment_homePage = Fragment_HomePage()
        session = SessionManager(applicationContext)
        Slatitude = intent.getStringExtra("Slatitude")!!
        Slongitude = intent.getStringExtra("Slongitude")!!
        source_address = intent.getStringExtra("source_address")!!
        setpickuploc = intent.getStringExtra("setpickuploc")!!


        if (!source_address.equals("", ignoreCase = true)) {
            et_source_search!!.setText(source_address)
            listview!!.clearTextFilter()
            listview!!.visibility = View.GONE
        }

        et_desti_search!!.requestFocus()
        Log.e("SOURCEADDRESS", source_address)

        listview!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->

                Toast.makeText(context, "" + identi, Toast.LENGTH_SHORT).show()
                Log.e("listitem", "$ET_source_status---$identi---$ET_destination_status")
                Sselected_location = itemList_location[position]

                if (ET_source_status.equals("active", ignoreCase = true)) {
                    source_address = Sselected_location
                }

                if (identi == 0) {
                    et_source_search!!.setText(Sselected_location)
                }
                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {

                    Log.e("LISTVIEW", itemList_placeId[position])
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[position])
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = getResources().getString(R.string.alert_nointernet)
                }
            }

        et_desti_search!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                ET_source_status = "inactive"
                et_source_search!!.setText(source_address)

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                // et_source_search.setText(source_address);
                ET_destination_status = "active"
                listview!!.visibility = View.VISIBLE
            }

            override fun afterTextChanged(s: Editable) {
                identi = 1
                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    listview!!.visibility = View.VISIBLE
                    val data = et_desti_search!!.text.toString().toLowerCase().replace(" ", "%20")
                    CitySearchRequest(Iconstant.place_search_url + data)

                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = getResources().getString(R.string.alert_nointernet)
                }

            }
        })

        et_desti_search!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(et_desti_search!!)
            }
            false
        }

        back!!.setOnClickListener {
            // close keyboard


            /*val i = Intent(this@LocationSearch, Fragment_HomePage::class.java)
            startActivity(i)*/
            onBackPressed()

            //fragment_homePage.startTimer()
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        et_desti_search!!.postDelayed({
            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(et_desti_search, 0)
        }, 200)


        et_source_search!!.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                // Log.e("ETSEARCH_CITY", "Cesr destination");
                if (ET_destination_status.equals("active", ignoreCase = true)) {
                    et_desti_search!!.setText("")
                    et_desti_search!!.hint = "Where to ?"
                }
                val et_quantity = v as EditText
                et_quantity.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                        ET_source_status = "active"
                        ET_destination_status = "inactive"

                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        identi = 0
                        cd = ConnectionDetector(this@LocationSearch)
                        isInternetPresent = cd!!.isConnectingToInternet

                        if (isInternetPresent!!) {
                            if (mRequest != null) {
                                mRequest!!.cancelRequest()
                            }
                            listview!!.visibility = View.VISIBLE
                            val data =
                                et_source_search!!.text.toString().toLowerCase().replace(" ", "%20")
                            Log.e("ETSEARCH_CITY11", data)
                            CitySearchRequest(Iconstant.place_search_url + data)
                        } else {
                            alert_layout!!.visibility = View.VISIBLE
                            alert_textview!!.text = getResources().getString(R.string.alert_nointernet)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {}
                })
            } else {

            }
        }



        et_desti_search!!.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {

            } else {
            }
        }

        et_source_search!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(et_source_search!!)

            }
            false
        }


        et_source_search!!.postDelayed({
            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(et_source_search, 0)
        }, 200)


        iv_source_clear!!.setOnClickListener {
            et_source_search!!.text.clear()
            et_desti_search!!.text.clear()
        }

        iv_destination_clear!!.setOnClickListener {
            et_desti_search!!.text.clear()
        }


        CustomProgress.getInstance().setListener(this)
    }

    private fun initialize() {
        alert_layout = findViewById(R.id.location_search_alert_layout) as RelativeLayout
        alert_textview = findViewById(R.id.location_search_alert_textView) as TextView
        back = findViewById(R.id.location_search_back_layout) as RelativeLayout
        et_desti_search = findViewById(R.id.location_destinat_search_editText) as EditText
        listview = findViewById(R.id.location_search_listView) as ListView
        progresswheel = findViewById(R.id.location_search_progressBar) as ProgressBar
        tv_emptyText = findViewById(R.id.location_search_empty_textview) as TextView

        et_source_search = findViewById(R.id.location_source_search_editText) as EditText
        iv_source_clear = findViewById(R.id.iv_source_clear) as ImageView
        iv_destination_clear = findViewById(R.id.iv_destination_clear) as ImageView
        ll_mainsearch = findViewById(R.id.ll_mainsearch) as LinearLayout
        session = SessionManager(this@LocationSearch)
        val user = session.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID)

        reccent_rides_listview = findViewById(R.id.reccent_rides_listview) as RecyclerView
        reccent_rides_listview.isNestedScrollingEnabled = false

        // recent_rides_array = ArrayList<RecentRidesPojo>()

        //postRequest_recentRides(home_recent_reides_url)

        iv_line = findViewById(R.id.iv_line)

        ll_pickuponmap = findViewById(R.id.ll_pickuponmap)

        avd = AnimatedVectorDrawableCompat.create(this, R.drawable.avd_line)!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            iv_line.background = avd
        }
        iv_line.visibility = View.GONE

    }


    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@LocationSearch)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            getResources().getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //-------------------Search Place Request----------------
    private fun CitySearchRequest(Url: String) {

        progresswheel!!.visibility = View.INVISIBLE
        CustomProgress.getInstance().changeState(true)
        println("--------------Search city url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Search city  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_array = `object`.getJSONArray("predictions")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_array.length() > 0) {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    for (i in 0 until place_array.length()) {
                                        val place_object = place_array.getJSONObject(i)
                                        itemList_location.add(place_object.getString("description"))
                                        itemList_placeId.add(place_object.getString("place_id"))
                                    }
                                    isdataAvailable = true
                                } else {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    isdataAvailable = false
                                }
                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                    CustomProgress.getInstance().changeState(false)
                    progresswheel!!.visibility = View.INVISIBLE
                    alert_layout!!.visibility = View.GONE
                    if (isdataAvailable) {
                        tv_emptyText!!.visibility = View.GONE
                    } else {
                        tv_emptyText!!.visibility = View.VISIBLE
                    }
                    adapter = PlaceSearchAdapter(this@LocationSearch, itemList_location)
                    adapter.setProgress(iv_line)
                    listview!!.adapter = adapter
                    adapter.notifyDataSetChanged()
                }

                override fun onErrorListener() {
                    CustomProgress.getInstance().changeState(false)
                    progresswheel!!.visibility = View.INVISIBLE
                    alert_layout!!.visibility = View.GONE

                    // close keyboard
                    CloseKeyboard(et_desti_search!!)
                }
            })
    }




    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private fun LatLongRequest(Url: String) {

        dialog = Dialog(this@LocationSearch)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_processing))

        println("--------------LatLong url-------------------$Url-----$identi")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------LatLong  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_object = `object`.getJSONObject("result")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_object.length() > 0) {
                                    val geometry_object = place_object.getJSONObject("geometry")
                                    if (geometry_object.length() > 0) {
                                        val location_object =
                                            geometry_object.getJSONObject("location")
                                        if (location_object.length() > 0) {

                                            if (identi == 0) {
                                                Slatitude = location_object.getString("lat")
                                                Slongitude = location_object.getString("lng")
                                                Log.e(
                                                    "LOCATION_CLASS_0",
                                                    "$Slatitude--------$Slongitude"
                                                )

                                            } else {
                                                Dlatitude = location_object.getString("lat")
                                                Dlongitude = location_object.getString("lng")
                                                Log.e(
                                                    "LOCATION_CLASS_1",
                                                    "$Dlatitude--------$Dlongitude"
                                                )
                                            }

                                            isdataAvailable = true
                                        } else {
                                            isdataAvailable = false
                                        }
                                    } else {
                                        isdataAvailable = false
                                    }
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (isdataAvailable) {
                        dialog!!.dismiss()


                        if (!et_desti_search!!.text.toString().equals("", ignoreCase = true)) {
                            Log.e(
                                "Intant",
                                Slatitude + "--" + Slongitude + "--" + Dlatitude + "--------" + Dlongitude + "---------" + Sselected_location + "--------" + et_source_search!!.text.toString() + "" + setpickuploc
                            )
                            val returnIntent = Intent()
                            returnIntent.putExtra("Selected_Latitude", Slatitude)
                            returnIntent.putExtra("Selected_Longitude", Slongitude)
                            returnIntent.putExtra("Selected_DLatitude", Dlatitude)
                            returnIntent.putExtra("Selected_DLongitude", Dlongitude)
                            returnIntent.putExtra("Selected_Location", Sselected_location)
                            returnIntent.putExtra(
                                "Selected_SourceLocation",
                                et_source_search!!.text.toString()
                            )
                            returnIntent.putExtra("setpickuploc", setpickuploc)
                            returnIntent.putExtra("searchlocation", "yes")

                            val searchValue = "yes"
                            session.setLocationSearchValue(searchValue)
                            setResult(RESULT_OK, returnIntent)
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            finish()
                        }

                    } else {
                        dialog!!.dismiss()
                        Alert(getResources().getString(R.string.alert_label_title), status)
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }


    override fun onDestroy() {
        super.onDestroy()
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            return true
        }
        return false
    }

    private fun startAnimation(isStart: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isStart) {
                repeatAnimation()
            } else {
                handler.postDelayed({
                    iv_line.removeCallbacks(action)
                    avd.stop()
                    iv_line.visibility = View.GONE
                    iv_line.invalidate()
                }, 800)

            }
        }
    }

    private val action = Runnable { repeatAnimation() }
    internal var handler = Handler()
    private fun repeatAnimation() {
        if (avd != null) {
            Thread(Runnable {

                handler.post {
                    iv_line.visibility = View.VISIBLE
                    avd.start()
                    iv_line.postDelayed(action, 1000)
                    iv_line.invalidate()
                }
            }).start()

        }
    }

    override fun onProgressChanged(state: Boolean) {
        startAnimation(state)
    }
}
