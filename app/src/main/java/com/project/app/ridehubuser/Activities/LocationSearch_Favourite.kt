package com.project.app.ridehubuser.Activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import org.json.JSONException
import org.json.JSONObject
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.iconstant.Iconstant.profile_AddFavouriteaddress_url
import com.project.app.ridehubuser.Adapters.PlaceSearchAdapter
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.project.app.ridehubuser.Utils.ConnectionDetector
import android.text.Editable
import android.text.TextWatcher
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.project.app.ridehubuser.PojoResponse.EstimateDetailPojo
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R


class LocationSearch_Favourite : ActivityHockeyApp() {

    private var back: RelativeLayout? = null
    private var et_search: EditText? = null
    private var listview: ListView? = null
    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var tv_emptyText: TextView? = null
    private var progresswheel: ProgressBar? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mRequest: ServiceRequest? = null
    internal var context: Context? = null
    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()

    internal var adapter: PlaceSearchAdapter? = null
    private var isdataAvailable = false
    private val isEstimateAvailable = false

    private var Slatitude = ""
    private var Slongitude = ""
    private var Sselected_location = ""

    internal var dialog: Dialog? = null
    internal var ratecard_list = ArrayList<EstimateDetailPojo>()
    internal var fav_hom_work: String? = null
    internal var address_type: String? = null
    private var session: SessionManager? = null
    private var UserID: String? = null

    private var tv_title_saved_place_id: TextView? = null
    internal var btn_save_location: Button? = null
    internal var tv_get_from_list_id: TextView? = null
    internal var et_title_id: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fav_place_search)
        context = applicationContext
        initialize()


        address_type = intent.getStringExtra("address_type")
        Log.e("", address_type!!)


        //fav_hom_work = getIntent().getStringExtra("home");
        listview!!.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Sselected_location = itemList_location[position]

                cd = ConnectionDetector(this@LocationSearch_Favourite)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {

                    /*if (address_type!!.equals("additional", ignoreCase = true)) {
                        et_title_id!!.visibility = View.VISIBLE
                        tv_get_from_list_id!!.visibility = View.VISIBLE
                        tv_get_from_list_id!!.text = Sselected_location
                        listview!!.visibility = View.GONE
                        et_search!!.visibility = View.GONE
                        tv_title_saved_place_id!!.visibility = View.VISIBLE
                        tv_title_saved_place_id!!.text = "Saved Places"
                        btn_save_location!!.setVisibility(View.VISIBLE)
                        btn_save_location!!.setOnClickListener(object : View.OnClickListener {
                           override fun onClick(v: View) {
                                LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[position])
                                Log.e("data_main", itemList_placeId[position])
                            }
                        })
                    } else {
                        LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[position])
                    }*/
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId[position])
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = resources.getString(R.string.alert_nointernet)
                }

            }
        })

        et_search!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

                cd = ConnectionDetector(this@LocationSearch_Favourite)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    val data = et_search!!.text.toString().toLowerCase().replace(" ", "%20")
                    CitySearchRequest(Iconstant.place_search_url + data)
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = resources.getString(R.string.alert_nointernet)
                }

            }
        })

        et_search!!.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER) {
                    CloseKeyboard(et_search!!)
                }
                return false
            }
        })

        back!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

                this@LocationSearch_Favourite.finish()
                this@LocationSearch_Favourite.overridePendingTransition(
                    android.R.anim.fade_in,
                    android.R.anim.fade_out
                )
            }
        })

        et_search!!.postDelayed({
            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(et_search, 0)
        }, 200)
    }

    private fun initialize() {
        alert_layout = findViewById(R.id.location_search_alert_layout) as RelativeLayout
        alert_textview = findViewById(R.id.location_search_alert_textView) as TextView
        back = findViewById(R.id.location_search_back_layout) as RelativeLayout
        et_search = findViewById(R.id.location_destinat_search_editText) as EditText
        listview = findViewById(R.id.location_search_listView) as ListView
        progresswheel = findViewById(R.id.location_search_progressBar) as ProgressBar
        tv_emptyText = findViewById(R.id.location_search_empty_textview) as TextView

        /*et_title_id = findViewById(R.id.et_title_id) as EditText
        tv_title_saved_place_id = findViewById(R.id.tv_title_saved_place_id) as TextView
        btn_save_location = findViewById(R.id.btn_save_location) as Button
        tv_get_from_list_id = findViewById(R.id.tv_get_from_list_id) as TextView*/

        session = SessionManager(this@LocationSearch_Favourite)
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID]

    }

    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@LocationSearch_Favourite)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            object : View.OnClickListener {
                override fun onClick(v: View) {
                    mDialog.dismiss()
                }
            })
        mDialog.show()

    }

    //-------------------Search Place Request----------------
    private fun CitySearchRequest(Url: String) {

        progresswheel!!.visibility = View.VISIBLE
        println("--------------Search city url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch_Favourite)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Search city  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_array = `object`.getJSONArray("predictions")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_array.length() > 0) {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    for (i in 0 until place_array.length()) {
                                        val place_object = place_array.getJSONObject(i)
                                        itemList_location.add(place_object.getString("description"))
                                        itemList_placeId.add(place_object.getString("place_id"))
                                    }
                                    isdataAvailable = true
                                } else {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    isdataAvailable = false
                                }
                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    progresswheel!!.visibility = View.INVISIBLE
                    alert_layout!!.visibility = View.GONE
                    if (isdataAvailable) {
                        tv_emptyText!!.visibility = View.GONE
                    } else {
                        tv_emptyText!!.visibility = View.VISIBLE
                    }
                    adapter = PlaceSearchAdapter(this@LocationSearch_Favourite, itemList_location)
                    listview!!.adapter = adapter!!
                    adapter!!.notifyDataSetChanged()
                }

                override fun onErrorListener() {
                    progresswheel!!.visibility = View.INVISIBLE
                    alert_layout!!.visibility = View.GONE

                    // close keyboard
                    CloseKeyboard(et_search!!)
                }
            })
    }


    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private fun LatLongRequest(Url: String) {

        dialog = Dialog(this@LocationSearch_Favourite)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        println("--------------LatLong url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch_Favourite)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------LatLong  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_object = `object`.getJSONObject("result")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_object.length() > 0) {
                                    val geometry_object = place_object.getJSONObject("geometry")
                                    if (geometry_object.length() > 0) {
                                        val location_object =
                                            geometry_object.getJSONObject("location")
                                        if (location_object.length() > 0) {
                                            Slatitude = location_object.getString("lat")
                                            Slongitude = location_object.getString("lng")
                                            isdataAvailable = true
                                        } else {
                                            isdataAvailable = false
                                        }
                                    } else {
                                        isdataAvailable = false
                                    }
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (isdataAvailable) {

                        if (address_type!!.equals("ridelater_source", ignoreCase = true)) {
                            val returnIntent = Intent()
                            returnIntent.putExtra("ridelater", "ridelater_source")
                            returnIntent.putExtra("Selected_Latitude_ridelater", Slatitude)
                            returnIntent.putExtra("Selected_Longitude_ridelater", Slongitude)
                            returnIntent.putExtra("selected_Location_ridelater", Sselected_location)

                            setResult(Activity.RESULT_OK, returnIntent)
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            finish()

                        } else if (address_type!!.equals("ridelater_destination", ignoreCase = true)) {
                            val returnIntent = Intent()
                            returnIntent.putExtra("ridelater", "ridelater_destination")
                            returnIntent.putExtra("Selected_Latitude_ridelater", Slatitude)
                            returnIntent.putExtra("Selected_Longitude_ridelater", Slongitude)
                            returnIntent.putExtra("selected_Location_ridelater", Sselected_location)

                            setResult(Activity.RESULT_OK, returnIntent)
                            onBackPressed()
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            finish()
                        } else{
                            postRequest_addFavourite(profile_AddFavouriteaddress_url)
                        }

                    } else {
                        dialog!!.dismiss()
                        Alert(resources.getString(R.string.alert_label_title), status)
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }


    override fun onDestroy() {
        super.onDestroy()
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() === 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch_Favourite.finish()
            this@LocationSearch_Favourite.overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            return true
        }
        return false
    }


    private fun postRequest_addFavourite(Url: String) {
        println("---------------ADD Favourite Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID!!

        if (address_type!!.equals("additional", ignoreCase = true)) {
            // jsonParams["title"] = et_title_id!!.text.toString()
        } else {
            jsonParams["title"] = address_type!!
        }
        //        jsonParams.put("title", address_type);
        jsonParams["address"] = Sselected_location
        jsonParams["latitude"] = Slatitude
        jsonParams["longitude"] = Slongitude


        mRequest = ServiceRequest(this@LocationSearch_Favourite)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------ADD Favourite Response-----------------$response")
                    var Sstatus = ""
                    val Smessage = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog!!.dismiss()

                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val returnIntent =
                            Intent(this@LocationSearch_Favourite, SettingsActivity::class.java)
                        startActivity(returnIntent)
                        overridePendingTransition(R.anim.slideup, R.anim.slidedown)
                        //Alert(getResources().getString(R.string.action_success), getResources().getString(R.string.profile_lable_username_success));
                    } else {
                        //Toast.makeText(context, "Failed status 0", Toast.LENGTH_SHORT).show();
                        val returnIntent =
                            Intent(this@LocationSearch_Favourite, SettingsActivity::class.java)
                        startActivity(returnIntent)
                        overridePendingTransition(R.anim.slideup, R.anim.slidedown)
                        //Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }


}