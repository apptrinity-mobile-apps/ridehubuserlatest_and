package com.project.app.ridehubuser.Utils

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.project.app.ridehubuser.Activities.SingUpAndSignIn
import java.util.*

class SessionManager(// Context
        internal var _context: Context) {
    internal var pref: SharedPreferences
    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor
    // Shared pref mode
    internal var PRIVATE_MODE = 0
    /**
     * Get stored session data
     */
    fun getUserDetails(): HashMap<String, String> {
        val user = HashMap<String, String>()
        user[KEY_EMAIL] = pref.getString(KEY_EMAIL, "")!!
        user[KEY_USERID] = pref.getString(KEY_USERID, "")!!
        user[KEY_USERNAME] = pref.getString(KEY_USERNAME, "")!!
        user[KEY_USERIMAGE] = pref.getString(KEY_USERIMAGE, "")!!
        user[KEY_COUNTRYCODE] = pref.getString(KEY_COUNTRYCODE, "")!!
        user[KEY_PHONENO] = pref.getString(KEY_PHONENO, "")!!
        user[KEY_REFERAL_CODE] = pref.getString(KEY_REFERAL_CODE, "")!!
        user[KEY_CATEGORY] = pref.getString(KEY_CATEGORY, "")!!
        user[KEY_SUBCATEGORY] = pref.getString(KEY_SUBCATEGORY, "")!!
        user[KEY_GCM_ID] = pref.getString(KEY_GCM_ID, "")!!
        user[KEY_AGENT] = pref.getString(KEY_AGENT, "")!!


        return user
    }


    //-----------Get user coupon code-----
    val couponCode: HashMap<String, String>
        get() {
            val code = HashMap<String, String>()
            code[KEY_COUPON_CODE] = pref.getString(KEY_COUPON_CODE, "")!!

            return code
        }


    //-----------Get CategoryId code-----
    val categoryID: HashMap<String, String>
        get() {
            val catID = HashMap<String, String>()
            catID[KEY_CATEGORY_ID] = pref.getString(KEY_CATEGORY_ID, "")!!
            return catID
        }


    val locationSearchValue: HashMap<String, String>
        get() {
            val locationValue = HashMap<String, String>()
            locationValue[KEY_LOCATION_SEARCH_VALUE] = pref.getString(KEY_LOCATION_SEARCH_VALUE, "")!!
            return locationValue
        }


    //-----------Get XMPP Secret Key-----
    fun getXmppKey(): HashMap<String, String> {
        val code = HashMap<String, String>()
        code[KEY_XMPP_USERID] = pref.getString(KEY_XMPP_USERID, "")!!
        code[KEY_XMPP_SEC_KEY] = pref.getString(KEY_XMPP_SEC_KEY, "")!!
        return code
    }


    //-----------Get Wallet Amount-----
    val walletAmount: HashMap<String, String>
        get() {
            val code = HashMap<String, String>()
            code[KEY_WALLET_AMOUNT] = pref.getString(KEY_WALLET_AMOUNT, "")!!
            return code
        }

    /**
     * Quick check for login
     */
    // Get Login State
    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)




    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    /**
     * Create login session
     */
    fun createLoginSession(email: String, userid: String, username: String, userimage: String, countrycode: String, phoneno: String, referalcode: String, category: String, subcategory: String, gcmID: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(KEY_EMAIL, email)
        editor.putString(KEY_USERID, userid)
        editor.putString(KEY_USERNAME, username)
        editor.putString(KEY_USERIMAGE, userimage)
        editor.putString(KEY_COUNTRYCODE, countrycode)
        editor.putString(KEY_PHONENO, phoneno)
        editor.putString(KEY_REFERAL_CODE, referalcode)
        editor.putString(KEY_CATEGORY, category)
        editor.putString(KEY_SUBCATEGORY, subcategory)
        editor.putString(KEY_GCM_ID, gcmID)

        // commit changes
        editor.commit()
    }
    fun Profile_editSession(user_name:String,user_image:String,user_phone:String){
        editor.putString(KEY_USERNAME, user_name)
        editor.putString(KEY_USERIMAGE, user_image)
        editor.putString(KEY_PHONENO, user_phone)


        // commit changes
        editor.commit()
    }


    fun setCategoryID(categoryID: String) {
        editor.putString(KEY_CATEGORY_ID, categoryID)
        editor.commit()
    }

    fun setLocationSearchValue(searchValue: String) {
        editor.putString(KEY_LOCATION_SEARCH_VALUE, searchValue)
        editor.commit()
    }


    fun createWalletAmount(amount: String) {
        editor.putString(KEY_WALLET_AMOUNT, amount)
        // commit changes
        editor.commit()
    }

    fun createFavouriteLocation(home_addr:String,work_addr:String){
        editor.putString(KEY_HOME_FAV,home_addr)
        editor.putString(KEY_WORK_FAV,work_addr)
        editor.commit()
    }

    //------username update code-----
    fun setUserNameUpdate(name: String) {
        editor.putString(KEY_USERNAME, name)
        editor.commit()
    }


    //------RIDEHUBSETTINGS  code-----
    fun setRidehubService(ridehub: Boolean) {
        editor.putBoolean(IS_RIDEHUB, ridehub)
        editor.commit()
    }
    fun getRidehubService(): Boolean {
        return pref.getBoolean(IS_RIDEHUB,true)
    }

    //------UBERSETTINGS  code-----
    fun setUberService(uber: Boolean) {
        editor.putBoolean(IS_UBER, uber)
        editor.commit()
    }
    fun getUberService(): Boolean {
        return pref.getBoolean(IS_UBER,true)
    }


    //------LYFTSETTINGS  code-----
    fun setLyftService(lyft: Boolean) {
        editor.putBoolean(IS_LYFT, lyft)
        editor.commit()
    }
    fun getLyftService(): Boolean {
        return pref.getBoolean(IS_LYFT,true)
    }


    //------PUBLICTRANSITSETTINGS  code-----
    fun setPublicTransitService(publictransit: Boolean) {
        editor.putBoolean(IS_PUBLICTRANSIT, publictransit)
        editor.commit()
    }
    fun getPublicTransitService(): Boolean {
        return pref.getBoolean(IS_PUBLICTRANSIT,true)
    }



    //------MobileNumber update code-----
    fun setMobileNumberUpdate(code: String, mobile: String) {
        editor.putString(KEY_COUNTRYCODE, code)
        editor.putString(KEY_PHONENO, mobile)
        editor.commit()
    }

    //------string user coupon code-----
    fun setCouponCode(code: String) {
        editor.putString(KEY_COUPON_CODE, code)
        editor.commit()
    }

    //------string user agent-----
    fun setAgent(agent: String) {
        editor.putString(KEY_AGENT, agent)
        editor.commit()
    }

    //------ Xmpp Connect Secrect Code-----
    fun setXmppKey(userId: String, secretKey: String) {
        editor.putString(KEY_XMPP_USERID, userId)
        editor.putString(KEY_XMPP_SEC_KEY, secretKey)
        editor.commit()
    }


    //UBER
    fun uber_tokens(access_token: String, refresh_token: String, rxp_time: String) {
        editor.putString(UBER_AUTHORIZATION_TOKEN, access_token)
        editor.putString(UBER_AUTHORIZATION_REFRESH, refresh_token)
        editor.putString(UBER_AUTHORIZATION_TIME, rxp_time)
        editor.commit()
    }

    fun uber_code(code: String) {
        editor.putString(UBER_AUTHORIZATION_CODE, code)
        editor.commit()
    }

    fun getUberData() : String {
        /*val uber_user = HashMap<String, String>()
        uber_user[UBER_AUTHORIZATION_CODE] = */
        return pref.getString(UBER_AUTHORIZATION_CODE, "")!!
    }

    fun getUberTokens(): HashMap<String, String> {
        val uber_user = HashMap<String, String>()
        uber_user[UBER_AUTHORIZATION_TOKEN] = pref.getString(UBER_AUTHORIZATION_TOKEN, "")!!
        uber_user[UBER_AUTHORIZATION_REFRESH] = pref.getString(UBER_AUTHORIZATION_REFRESH, "")!!
        uber_user[UBER_AUTHORIZATION_TIME] = pref.getString(UBER_AUTHORIZATION_TIME, "")!!
        return uber_user
    }


    //lyft///

    fun refreshToken(refresh_token: String) {
        editor.putString(KEY_USER_REFRESH_TOKEN_LYFT, refresh_token)
        editor.commit()
    }


    fun userAccessToken(user_access_token: String) {
        editor.putString(KEY_USER_ACCESS_TOKEN_LYFT, user_access_token)
        editor.commit()
    }

    fun getRefreshToken(): String {
        return pref.getString(KEY_USER_REFRESH_TOKEN_LYFT, "")!!
    }

    fun getUserAccessToken(): String {
        return pref.getString(KEY_USER_ACCESS_TOKEN_LYFT, "")!!
    }

    fun setlyft_ride_id(id: String) {
        editor.putString(KEY_LYFT_RIDE_ID_LYFT, id)
        editor.commit()
    }

    fun getlyft_ride_id(): String {
        return pref.getString(KEY_LYFT_RIDE_ID_LYFT, "")!!
    }


    /**
     * Clear session details
     */
    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()

         val i = Intent(_context, SingUpAndSignIn::class.java)
         i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
         i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
         _context.startActivity(i)

    }

    companion object {

        // Sharedpref file name
        private val PREF_NAME = "PremKumar"

        // All Shared Preferences Keys
        private val IS_LOGIN = "IsLoggedIn"

        private val IS_RIDEHUB = "IsRideHUB"
        private val IS_UBER = "IsUber"
        private val IS_LYFT = "IsLyft"
        private val IS_PUBLICTRANSIT = "IsPublictransit"

        val KEY_EMAIL = "email"
        val KEY_USERID = "userid"
        val KEY_USERNAME = "username"
        val KEY_USERIMAGE = "userimage"
        val KEY_GCM_ID = "gcmId"


        val KEY_COUNTRYCODE = "countrycode"
        val KEY_PHONENO = "phoneno"
        val KEY_REFERAL_CODE = "referalcode"
        val KEY_CATEGORY = "category"
        val KEY_SUBCATEGORY = "subcategory"
        val KEY_CATEGORY_ID = "categoryId"
        val KEY_LOCATION_SEARCH_VALUE = "searchValue"

        val KEY_COUPON_CODE = "coupon"
        val KEY_WALLET_AMOUNT = "walletAmount"
        val KEY_AGENT = "agent"

        val KEY_XMPP_USERID = "xmppUserId"
        val KEY_XMPP_SEC_KEY = "xmppSecKey"


        //UBER
        val UBER_AUTHORIZATION_CODE = "ar_code"
        val UBER_AUTHORIZATION_TOKEN = "ar_token"
        val UBER_AUTHORIZATION_REFRESH = "ar_refresh_token"
        val UBER_AUTHORIZATION_TIME = "ar_refresh_time"


        //LYFT
        val KEY_USER_ACCESS_TOKEN_LYFT = "user_access_token"
        val KEY_USER_REFRESH_TOKEN_LYFT = "refresh_token"
        val KEY_LYFT_RIDE_ID_LYFT = "ride_id_lyft"

        val KEY_HOME_FAV = "home_fav"
        val KEY_WORK_FAV = "work_fav"

    }

}
