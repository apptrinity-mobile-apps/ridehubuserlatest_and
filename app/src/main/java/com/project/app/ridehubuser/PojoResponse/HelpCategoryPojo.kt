package com.project.app.ridehubuser.PojoResponse

class HelpCategoryPojo {

    private var id: String? = null
    private var category: String? = null
    private var status: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String) {
        this.category = category
    }


    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }






}