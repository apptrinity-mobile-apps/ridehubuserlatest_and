package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.facebook.Util
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.project.app.ridehubuser.iconstant.Iconstant.profile_getFavouriteaddress_url
import com.squareup.picasso.Picasso
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class SettingsActivity : ActivityHockeyApp() {
    private var ll_home_location: LinearLayout? =
        null
    private var ll_work_location: LinearLayout? = null
    private var ll_safetytools: LinearLayout? = null
    private var ll_privacypolicy: LinearLayout? = null
    private var ll_sign_out: LinearLayout? = null
    private var ll_privacy: LinearLayout? = null
    private var tv_home_loc_head: TextView? =
        null
    private var tv_home_loc_place: TextView? = null
    private var tv_work_loc_head: TextView? = null
    private var tv_work_loc_place: TextView? = null
    private var session: SessionManager? = null
    private var UserID: String? = null
    private var mRequest: ServiceRequest? = null
    private var iv_settings_close: ImageView? = null
    private var context: Context? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    internal lateinit var dialog: Dialog
    internal lateinit var Location_keys: HashMap<String, String>
    internal lateinit var tv_work_loc_delete: ImageView
    internal lateinit var tv_home_loc_delete: ImageView
    internal lateinit var profile_icon: ImageView
    internal lateinit var ll_profile_settings: LinearLayout
    internal lateinit var tv_add_place_new: TextView
    internal lateinit var profile_name: TextView
    internal lateinit var profile_mobile_number: TextView
    private lateinit var account_email: TextView
    private var UserName = ""
    private var UserMobileno = ""
    private var UserEmail = ""
    private var UserprofileImage: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        context = this@SettingsActivity
        initialize()
        try{
            ChatService.startUserAction(this@SettingsActivity)
        }catch (e:Exception){
            e.printStackTrace()
        }



        cd = ConnectionDetector(this@SettingsActivity)
        isInternetPresent = cd!!.isConnectingToInternet


        postRequest_getFavourite(profile_getFavouriteaddress_url)

        ll_home_location!!.setOnClickListener {
            val intent = Intent(this@SettingsActivity, LocationSearch_Favourite::class.java)
            intent.putExtra("address_type", "home")
            startActivity(intent)
        }

        ll_profile_settings!!.setOnClickListener {
            val intent = Intent(this@SettingsActivity, ProfilePageActivity::class.java)
            startActivity(intent)
        }

        ll_privacypolicy!!.setOnClickListener {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://ridehub.co/policy")
            )
            startActivity(browserIntent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        ll_safetytools!!.setOnClickListener {
            val intent = Intent(this@SettingsActivity, SafetyCenterActivity::class.java)
            startActivity(intent)
        }

        ll_work_location!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@SettingsActivity, LocationSearch_Favourite::class.java)
            intent.putExtra("address_type", "work")
            startActivity(intent)
        })

        iv_settings_close!!.setOnClickListener {
            val intent = Intent(this@SettingsActivity, NavigationDrawer::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        ll_sign_out!!.setOnClickListener {
            showBackPressedDialog(false)
        }





        /*tv_home_loc_delete.setOnClickListener(View.OnClickListener {
            postRequest_Delete(
                Iconstant.favoritelist_delete_url,
                Location_keys["home"]!!
            )
        })*/

        /* tv_work_loc_delete.setOnClickListener {
             postRequest_Delete(
                 Iconstant.favoritelist_delete_url,
                 Location_keys["work"]!!
             )
         }*/

        /* ll_privacy!!.setOnClickListener(View.OnClickListener {
             Toast.makeText(this, "Privacy Settings", Toast.LENGTH_SHORT).show()
             //postRequest_Delete(Iconstant.favoritelist_delete_url,Location_keys.get("work"));
         })
 */
        /*tv_add_place_new.setOnClickListener {
            val intent = Intent(this@SettingsActivity, SavedPlaces::class.java)
            //intent.putExtra("address_type", "additional");
            startActivity(intent)
        }*/


    }

    private fun initialize() {


        tv_home_loc_head = findViewById<TextView>(R.id.tv_header_home)
        tv_work_loc_head = findViewById<TextView>(R.id.tv_header_work)
        profile_name = findViewById<TextView>(R.id.tv_account_name)
        profile_mobile_number = findViewById<TextView>(R.id.tv_account_mobile)
        account_email = findViewById<TextView>(R.id.tv_account_email)
        /*tv_header_home = findViewById(R.id.tv_header_home) as TextView
        tv_header_work = findViewById(R.id.tv_header_work) as TextView*/
        tv_home_loc_place = findViewById<TextView>(R.id.tv_fav_home)
        tv_work_loc_place = findViewById<TextView>(R.id.tv_fav_work)
        profile_icon = findViewById<ImageView>(R.id.iv_account_profile)
        ll_profile_settings = findViewById<LinearLayout>(R.id.ll_profile_settings)
        iv_settings_close = findViewById<ImageView>(R.id.iv_settings_close)

        ll_sign_out = findViewById<LinearLayout>(R.id.ll_sign_out)
        ll_home_location = findViewById<LinearLayout>(R.id.ll_home_location)
        ll_work_location = findViewById<LinearLayout>(R.id.ll_work_location)
        ll_safetytools = findViewById<LinearLayout>(R.id.ll_safetytools)
        ll_privacypolicy = findViewById<LinearLayout>(R.id.ll_privacypolicy)

        tv_home_loc_head!!.visibility = View.VISIBLE
        tv_home_loc_place!!.visibility = View.GONE
        tv_home_loc_head!!.text = "Add Home"
        tv_work_loc_head!!.visibility = View.VISIBLE
        tv_work_loc_place!!.visibility = View.GONE
        tv_work_loc_head!!.text = "Add Work"

        session = SessionManager(this@SettingsActivity)
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID]

        UserName = user[SessionManager.KEY_USERNAME]!!
        UserMobileno = user[SessionManager.KEY_PHONENO]!!
        UserprofileImage = user[SessionManager.KEY_USERIMAGE]
        UserEmail = user[SessionManager.KEY_EMAIL]!!

        profile_name.text = UserName
        profile_mobile_number.text = UserMobileno
        account_email.text = UserEmail
        Picasso.with(this).load(UserprofileImage.toString())
            .placeholder(R.drawable.user_icon_default).into(profile_icon)


    }


    fun showBackPressedDialog(isLogout: Boolean) {
        //System.gc()

        val dialogView: View
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater

        val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            dialogView = inflater.inflate(R.layout.logout_alert, null)
        } else {
            dialogView = inflater.inflate(R.layout.logout_alert, null)
        }

        dialogBuilder.setView(dialogView)

        val rl_yes = dialogView.findViewById(R.id.layout_logout_yes) as TextView

        val rl_no = dialogView.findViewById(R.id.layout_logout_no) as TextView

        val b = dialogBuilder.create()
        b.show()
        rl_yes.setOnClickListener(View.OnClickListener {
            b.dismiss()
            Log.e("LOGOUT", "" + isLogout)
            if (!isLogout) {
                Log.e("LOGOUTffff", "" + isLogout)
                postRequest_Logout(Iconstant.logout_url)
            }
        })
        rl_no.setOnClickListener(View.OnClickListener { b.dismiss() })


    }


    private fun postRequest_getFavourite(Url: String) {
        println("---------------GET Favourite Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID!!
        mRequest = ServiceRequest(this@SettingsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------GET Favourite Response-----------------$response")
                    var Sstatus = ""
                    val Smessage = ""
                    Location_keys = HashMap()
                    Location_keys.clear()
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val Location_aray = `object`.getJSONArray("locations")
                            for (i in 0 until Location_aray.length()) {
                                val address_object = Location_aray.getJSONObject(i)

                                val address = address_object.getString("address")
                                val title = address_object.getString("title")
                                val location_key = address_object.getString("location_key")
                                Location_keys[title] = location_key

                                if (title.equals("home", ignoreCase = true)) {
                                    tv_home_loc_head!!.visibility = View.VISIBLE
                                    tv_home_loc_head!!.text = "Home"
                                    tv_home_loc_place!!.visibility = View.VISIBLE
                                    tv_home_loc_place!!.text = address
                                   // tv_home_loc_delete.visibility = View.VISIBLE
                                } else if (title.equals("work", ignoreCase = true)) {
                                    tv_work_loc_head!!.visibility = View.VISIBLE
                                    tv_work_loc_head!!.text = "Work"
                                    tv_work_loc_place!!.visibility = View.VISIBLE
                                    tv_work_loc_place!!.text = address
                                    tv_work_loc_delete.visibility = View.VISIBLE

                                }
                                Log.e("ADDRESS_RES", "$address------$title")

                            }

                            // Alert(getResources().getString(R.string.action_success), getResources().getString(R.string.profile_lable_username_success));
                        } else {
                            //Alert(getResources().getString(R.string.action_error), Smessage);
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@SettingsActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
            val intent = Intent(this@SettingsActivity, SettingsActivity::class.java)
            startActivity(intent)
            mDialog.dismiss()
        })
        mDialog.show()

    }

    private fun postRequest_Logout(Url: String) {
        showDialog(resources.getString(R.string.action_logging_out))
        println("---------------LogOut Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID!!
        jsonParams["device"] = "ANDROID"

        mRequest = ServiceRequest(this@SettingsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------LogOut Response-----------------$response")
                    var Sstatus = ""
                    var Sresponse = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Sresponse = `object`.getString("response")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        logoutFromFacebook()
                        session!!.logoutUser()
                        val local = Intent()
                        local.action = "com.app.logout"
                        this@SettingsActivity.sendBroadcast(local)
                        onBackPressed()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    } else {
                        Alert(resources.getString(R.string.action_error), Sresponse)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    fun logoutFromFacebook() {
        Util.clearCookies(this@SettingsActivity)
        // your sharedPrefrence
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }

    private fun showDialog(data: String) {
        dialog = Dialog(this@SettingsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = data
    }


    //-----------------------Favourite List Delete Post Request-----------------
    private fun postRequest_Delete(Url: String, locationKey: String) {
        dialog = Dialog(this@SettingsActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_deleting)

        println("$locationKey-------------Favourite Delete Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID!!
        jsonParams["location_key"] = locationKey

        mRequest = ServiceRequest(this@SettingsActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------Favourite Delete Response----------------$response")

                    var Sstatus = ""
                    var Smessage = ""

                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("message")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            //removing the deleted position from listView

                            if (locationKey.equals("home", ignoreCase = true)) {
                                tv_home_loc_head!!.visibility = View.VISIBLE
                                tv_home_loc_head!!.text = "Add Home"
                                tv_home_loc_place!!.setVisibility(View.GONE)
                               // tv_home_loc_delete.setVisibility(View.GONE)
                            } else if (locationKey.equals("work", ignoreCase = true)) {
                                tv_work_loc_head!!.setVisibility(View.VISIBLE)
                                tv_work_loc_head!!.setText("Add Work")
                                tv_work_loc_place!!.setVisibility(View.GONE)
                                tv_work_loc_delete.visibility = View.GONE
                            }



                            Alert(resources.getString(R.string.action_success), Smessage)

                        } else {
                            Alert(resources.getString(R.string.alert_label_title), Smessage)
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog.dismiss()

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

}
