package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.Request
import com.project.app.mylibrary.customRatingBar.RotationRatingBar
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.project.app.ridehubuser.subclass.ActivitySubClass
import com.squareup.picasso.Picasso
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*


class FareBreakUp : ActivitySubClass() {
    private var Tv_baseFare: TextView? = null
    private var Tv_discount: TextView? = null
    private var Tv_duration: TextView? = null
    private var Tv_waiting: TextView? = null
    private var Tv_timeTravel: TextView? = null
    private var Tv_serviceTax_Title: TextView? = null
    private var Rl_payment: TextView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var SrideId_intent = ""
    private var ScurrencyCode = ""
    private var StotalAmount = ""
    private var total_and_refer = ""
    private var points_conversion = ""
    private var fianl_conversion = ""
    private var Sduation = ""
    private var SwaitingTime = ""
    private var StravelDistance = ""
    internal var currencycode: Currency? = null
    private var Im_DriverImage: ImageView? = null
    private var Tv_DriverName: TextView? = null
    private var Tv_SubTotal: TextView? = null
    private var Tv_TripTotal: TextView? = null

    //------Tip Declaration-----
    private var Et_tip_Amount: EditText? = null
    private var Tv_Referpoints_Amount: TextView? = null
    private var tv_note_referpoints: TextView? = null
    private var Bt_tip_Apply: TextView? = null
    private var Tv_tip: TextView? = null
    private var Ll_referralpoints: LinearLayout? = null
    private var ll_main_referpoints: LinearLayout? = null
    private var ll_wallet_id: LinearLayout? = null
    private var cb_wallet_id: CheckBox? = null
    private var tv_wallet_amount_id: TextView? = null
    private var Ll_TipAmount: LinearLayout? = null
    private var sSelectedTipAmount = ""
    private var Rb_driver: RotationRatingBar? = null
    private var Tv_serviceTax: TextView? = null
    private var Tv_referralpoints: TextView? = null
    private var tv_vehicle_type: TextView? = null
    private var tv_vehicle_model: TextView? = null
    private var tv_vehicle_no: TextView? = null

    private var sDriverName = ""
    private var sDriverImage = ""
    private var sDriverRating = ""
    private var sDriverLat = ""
    private var sDriverLong = ""
    private var sUserLat = ""
    private var sUserLong = ""
    private var sSubTotal = ""
    private var sServiceTax = ""
    private var sServiceTax_percentage = ""
    private var sTotalPayment = ""
    private var CouponDiscount = ""
    private var ReferPoints_Total = ""
    private var number_of_points = ""
    private var amount_per_point = ""
    private var vehicle_no = ""
    private var vehicle_model = ""
    private var cabtype = ""
    private var walletAmount_stg = ""

    private var mRequest: ServiceRequest? = null
    private var Cb_referpoints: CheckBox? = null
    internal lateinit var dialog: Dialog
    private var session: SessionManager? = null
    internal lateinit var UserID: String

    private var is_Checked_points = "No"
    private var is_wallet_Checked = "No"

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fare_break_up)
        farebreakup_class = this@FareBreakUp
        initialize()

        Rl_payment!!.setOnClickListener {
            cd = ConnectionDetector(this@FareBreakUp)
            isInternetPresent = cd!!.isConnectingToInternet


            if(is_wallet_Checked.equals("Yes")){
                //
                if((walletAmount_stg).toDouble() >= sTotalPayment.toDouble()){
                    //wallet service
                    Log.e("is_wallet_Checked","if")
                    MakePayment_Wallet(Iconstant.makepayment_wallet_url)
                }else{

                    Log.e("is_wallet_Checked","else")
                    //
                    val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                    passIntent.putExtra("RideID", SrideId_intent)
                    passIntent.putExtra("TotalAmount", total_and_refer)
                    passIntent.putExtra("wallet_by", "Yes")
                    startActivity(passIntent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }


            }else{
                if (isInternetPresent!!) {

                    Log.e("MAKEPAYMENT_REFERRAL",ReferPoints_Total+"-----"+is_Checked_points)
                    if (ReferPoints_Total.toFloat() >= 0) {
                        if (is_Checked_points.equals("No")) {

                            val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                            passIntent.putExtra("RideID", SrideId_intent)
                            passIntent.putExtra("TotalAmount", total_and_refer)
                            passIntent.putExtra("wallet_by", "No")
                            startActivity(passIntent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        } else {
                            Log.e("CHECKEDVALUE",is_Checked_points)
                            postRequest_PayByREFERPoints(Iconstant.pay_by_referpoints)
                        }

                    } else {
                        Log.e("MAKEPAYMENT_REFERRAL",ReferPoints_Total+"-----"+is_Checked_points)

                        val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                        passIntent.putExtra("RideID", SrideId_intent)
                        passIntent.putExtra("TotalAmount", total_and_refer)
                        passIntent.putExtra("wallet_by", "No")
                        startActivity(passIntent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    }


                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            }



        }


        Bt_tip_Apply!!.setOnClickListener {
            cd = ConnectionDetector(this@FareBreakUp)
            isInternetPresent = cd!!.isConnectingToInternet

            if (Et_tip_Amount!!.text.toString().length > 0 && java.lang.Double.parseDouble(Et_tip_Amount!!.text.toString()) > 0.0) {
                if (isInternetPresent!!) {
                    postRequest_Tip(Iconstant.tip_add_url, "Apply")
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
                }
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.my_rides_detail_tip_empty_label))
            }
        }


        Ll_RemoveTip!!.setOnClickListener {
            cd = ConnectionDetector(this@FareBreakUp)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                postRequest_Tip(Iconstant.tip_remove_url, "Remove")
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet))
            }
        }


        Cb_referpoints!!.setOnClickListener { v ->

            if ((v as CheckBox).isChecked) {
                is_Checked_points = "Yes"
                total_and_refer = (StotalAmount.toFloat() - ReferPoints_Total.toFloat()).toString()
                Log.e("REAMAINGBALNCE", total_and_refer+"----"+sTotalPayment.toFloat()+"-------"+points_conversion.toFloat())
                Ll_referralpoints!!.visibility = View.VISIBLE


                val fianl_conversion = sTotalPayment.toFloat() - points_conversion.toFloat()

                if(sTotalPayment.toFloat() > points_conversion.toFloat()){
                    Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
                }else{
                    Tv_SubTotal!!.text = currencycode!!.symbol + "0"
                }
                Log.e("FINALCONVERSION", fianl_conversion.toString())

            } else {
                is_Checked_points = "No"
                Ll_referralpoints!!.visibility = View.GONE
                Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
            }
        }


        cb_wallet_id!!.setOnClickListener { wallet ->

            if ((wallet as CheckBox).isChecked) {
                is_wallet_Checked = "Yes"

            } else {
                is_wallet_Checked = "No"

            }
        }


    }

    private fun initialize() {

        session = SessionManager(this@FareBreakUp)
        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()


        val wallet = session!!.walletAmount
        walletAmount_stg = wallet[SessionManager.KEY_WALLET_AMOUNT].toString()

        Tv_baseFare = findViewById(R.id.fare_breakup_total_amount_textview) as TextView
        Tv_discount = findViewById(R.id.fare_breakup_discount_amount_textview) as TextView
        Tv_duration = findViewById(R.id.fare_breakup_duration_textview) as TextView
        Tv_waiting = findViewById(R.id.fare_breakup_waiting_textview) as TextView
        Tv_timeTravel = findViewById(R.id.fare_breakup_timetravel_textview) as TextView
        Rl_payment = findViewById(R.id.fare_breakup_payment_layout) as TextView

        Im_DriverImage = findViewById(R.id.fare_breakup_imageview) as ImageView
        Tv_DriverName = findViewById(R.id.fare_breakup_driver_name_textView) as TextView
        Tv_SubTotal = findViewById(R.id.fare_breakup_subtotal_textView) as TextView
        Tv_TripTotal = findViewById(R.id.fare_breakup_trip_total_textView) as TextView

        Et_tip_Amount = findViewById(R.id.fare_breakup_tip_editText) as EditText
        Tv_Referpoints_Amount = findViewById(R.id.fare_breakup_referpoints_TextView) as TextView
        tv_note_referpoints = findViewById(R.id.tv_note_referpoints) as TextView
        Bt_tip_Apply = findViewById(R.id.fare_breakup_tip_apply_button) as TextView
        Cb_referpoints = findViewById(R.id.fare_breakup_referpoints_checkBox) as CheckBox

        Tv_tip = findViewById(R.id.fare_breakup_tip_amount_textView) as TextView
        Ll_RemoveTip = findViewById(R.id.fare_breakup_tip_amount_remove_layout) as ImageView
        Rb_driver = findViewById(R.id.fare_breakup_driver_ratingBar) as RotationRatingBar
        Tv_serviceTax = findViewById(R.id.fare_breakup_serviceTax_textView) as TextView

        Ll_TipAmount = findViewById(R.id.fare_breakup_tip_amount_layout) as LinearLayout


        Ll_referralpoints = findViewById(R.id.fare_breakup_referralPoints_layout) as LinearLayout
        ll_main_referpoints = findViewById(R.id.ll_main_referpoints) as LinearLayout
        ll_wallet_id = findViewById(R.id.ll_wallet_id) as LinearLayout
        cb_wallet_id = findViewById(R.id.cb_wallet_id)
        tv_wallet_amount_id = findViewById(R.id.tv_wallet_amount_id)
        Tv_referralpoints = findViewById(R.id.fare_breakup_referralPoints_textView) as TextView
        tv_vehicle_no = findViewById(R.id.tv_vehicle_no) as TextView
        tv_vehicle_model = findViewById(R.id.tv_vehicle_model) as TextView
        tv_vehicle_type = findViewById(R.id.tv_vehicle_type) as TextView

        val intent = getIntent()
        SrideId_intent = intent.getStringExtra("RideID")
        ScurrencyCode = intent.getStringExtra("CurrencyCode")
        StotalAmount = intent.getStringExtra("TotalAmount")
        StravelDistance = intent.getStringExtra("TravelDistance")
        Sduation = intent.getStringExtra("Duration")
        SwaitingTime = intent.getStringExtra("WaitingTime")
        sDriverName = intent.getStringExtra("DriverName")
        sDriverImage = intent.getStringExtra("DriverImage")
        sDriverRating = intent.getStringExtra("DriverRating")
        sDriverLat = intent.getStringExtra("DriverLatitude")
        sDriverLong = intent.getStringExtra("DriverLongitude")
        sUserLat = intent.getStringExtra("UserLatitude")
        sUserLong = intent.getStringExtra("UserLongitude")
        sSubTotal = intent.getStringExtra("SubTotal")
        sServiceTax = intent.getStringExtra("ServiceTax")
        sTotalPayment = intent.getStringExtra("TotalPayment")
        CouponDiscount = intent.getStringExtra("CouponDiscount")
        sServiceTax_percentage = intent.getStringExtra("ServiceTax_Percent")
        ReferPoints_Total = intent.getStringExtra("ReferPoints_Total")
        number_of_points = intent.getStringExtra("number_of_points")
        amount_per_point = intent.getStringExtra("amount_per_point")
        vehicle_no = intent.getStringExtra("vehicle_no")
        vehicle_model = intent.getStringExtra("vehicle_model")
        cabtype = intent.getStringExtra("cabtype")

        Log.e("TOTALREFERPOINTS", ReferPoints_Total + "------" + number_of_points + "------" + amount_per_point +"------"+sTotalPayment )



        tv_vehicle_no!!.setText(vehicle_no)
        tv_vehicle_model!!.setText(vehicle_model)
        tv_vehicle_type!!.setText(cabtype)


        currencycode = Currency.getInstance(getLocale(ScurrencyCode))

        Picasso.with(this@FareBreakUp).invalidate(sDriverImage)
        Picasso.with(this@FareBreakUp).load(sDriverImage).into(Im_DriverImage)
        Tv_DriverName!!.text = sDriverName
        if (sDriverRating.length > 0) {
            Rb_driver!!.rating = java.lang.Float.parseFloat(sDriverRating)
        }
        //StotalAmount
        Tv_baseFare!!.text = currencycode!!.symbol + sSubTotal
        Tv_discount!!.text = CouponDiscount
        Tv_duration!!.text = Sduation
        Tv_waiting!!.text = SwaitingTime
        Tv_timeTravel!!.text = StravelDistance
        //sSubTotal

        if (is_Checked_points.equals("Yes")) {
            fianl_conversion = (sTotalPayment.toFloat() - points_conversion.toFloat()).toString()
            Log.e("FINALCONVERSION", fianl_conversion.toString())
            Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
        } else {
            Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
        }


        Tv_serviceTax!!.text = currencycode!!.symbol + sServiceTax
        Tv_TripTotal!!.text = currencycode!!.symbol + (java.lang.Double.parseDouble(sTotalPayment) + java.lang.Double.parseDouble(CouponDiscount))


        if (walletAmount_stg.equals("") || walletAmount_stg.equals("0")|| walletAmount_stg.equals("$0")) {
            ll_wallet_id!!.visibility = View.GONE
        }else{
            ll_wallet_id!!.visibility = View.VISIBLE
            tv_wallet_amount_id!!.setText("$ "+walletAmount_stg)
        }


        if (ReferPoints_Total.equals("") || ReferPoints_Total.equals("0")) {
            Ll_referralpoints!!.visibility = View.GONE
            ll_main_referpoints!!.visibility = View.GONE
        } else {
            ll_main_referpoints!!.visibility = View.VISIBLE
            Tv_Referpoints_Amount!!.text = ReferPoints_Total
            // Ll_referralpoints!!.visibility = View.VISIBLE
            points_conversion = (ReferPoints_Total.toFloat() / number_of_points.toFloat()).toString()
            val used_points = (sTotalPayment.toFloat() * amount_per_point.toFloat()).toString()
            if(used_points.toFloat() >= ReferPoints_Total.toFloat()){
                Tv_referralpoints!!.setText(ReferPoints_Total)
            }else{
                Tv_referralpoints!!.setText(used_points)
            }

           // Tv_referralpoints!!.setText("US $" + ReferPoints_Total.toFloat())

        }

        if (number_of_points.equals("")) {
        } else {
            tv_note_referpoints!!.text = "Note : " + number_of_points + " points equals to " + " $ " + amount_per_point
        }


    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@FareBreakUp)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Tip Post Request-----------------
    private fun postRequest_Tip(Url: String, tipStatus: String) {
        dialog = Dialog(this@FareBreakUp)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))


        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = SrideId_intent
        if (tipStatus.equals("Apply", ignoreCase = true)) {
            jsonParams["tips_amount"] = Et_tip_Amount!!.text.toString()
        }

        mRequest = ServiceRequest(this@FareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {

                var sStatus = ""
                var sResponse = ""
                var sTipAmount = ""
                try {

                    val `object` = JSONObject(response)
                    sStatus = `object`.getString("status")
                    if (sStatus.equals("1", ignoreCase = true)) {

                        val response_Object = `object`.getJSONObject("response")
                        sTipAmount = response_Object.getString("tips_amount")
                        sTotalPayment = response_Object.getString("total")
                        if (tipStatus.equals("Apply", ignoreCase = true)) {
                            sSelectedTipAmount = sTipAmount
                            Tv_tip!!.text = currencycode!!.symbol + sTipAmount
                            Tv_TripTotal!!.text = currencycode!!.symbol + sTotalPayment

                            if (is_Checked_points.equals("Yes")) {
                                fianl_conversion = (sTotalPayment.toFloat() - points_conversion.toFloat()).toString()
                                Log.e("FINALCONVERSION", fianl_conversion.toString())
                                Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
                            } else {
                                Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            }
                            Ll_TipAmount!!.visibility = View.VISIBLE
                        } else {
                            Tv_TripTotal!!.text = currencycode!!.symbol + sTotalPayment
                            // Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            if (is_Checked_points.equals("Yes")) {
                                fianl_conversion = (sTotalPayment.toFloat() - points_conversion.toFloat()).toString()
                                Log.e("FINALCONVERSION", fianl_conversion.toString())
                                Tv_SubTotal!!.text = currencycode!!.symbol + fianl_conversion
                            } else {
                                Tv_SubTotal!!.text = currencycode!!.symbol + sTotalPayment
                            }

                            Ll_TipAmount!!.visibility = View.GONE
                            Et_tip_Amount!!.setText("")
                        }

                    } else {
                        sResponse = `object`.getString("response")
                        Alert(getResources().getString(R.string.alert_label_title), sResponse)
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.fare_breakup_label_complete_payment))
            return true
        }
        return false
    }

    companion object {

        lateinit var farebreakup_class: FareBreakUp
        private var Ll_RemoveTip: ImageView? = null

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {
            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }


        fun invisibleTips() {
            Ll_RemoveTip!!.visibility = View.INVISIBLE
        }
    }


    //-----------------------Tip Post Request-----------------
    private fun postRequest_PayByREFERPoints(Url: String) {
        dialog = Dialog(this@FareBreakUp)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_pleasewait))

        System.out.println("PayByREFERPointsPARAMS " + UserID + "----" + SrideId_intent + "----" + total_and_refer + "----" + is_Checked_points)

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent
        jsonParams["pay_by_points"] = is_Checked_points


        mRequest = ServiceRequest(this@FareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {


                System.out.println("PayByREFERPointsResponse " + response)

                var Sstatus = ""

                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("0", ignoreCase = true)) {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    } else if (Sstatus.equals("1", ignoreCase = true)) {
                        //Updating wallet amount on Navigation Drawer Slide

                        NavigationDrawer.navigationNotifyChange()
                        val mDialog = PkDialog(this@FareBreakUp)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_wallet_success))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            finish()
                            farebreakup_class.finish()
                            val intent = Intent(this@FareBreakUp, MyRideRating::class.java)
                            intent.putExtra("RideID", SrideId_intent)
                            startActivity(intent)
                            overridePendingTransition(R.anim.enter, R.anim.exit)
                        })
                        mDialog.show()

                    } else if (Sstatus.equals("2", ignoreCase = true)) {

                        FareBreakUp.invisibleTips()

                        val unbill_amount = `object`.getString("unbill_amount")
                        //Updating wallet amount on Navigation Drawer Slide

                        val mDialog = PkDialog(this@FareBreakUp)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            val passIntent = Intent(this@FareBreakUp, FareBreakUpPaymentList::class.java)
                            passIntent.putExtra("RideID", SrideId_intent)
                            passIntent.putExtra("TotalAmount", unbill_amount)
                            startActivity(passIntent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

                        })
                        mDialog.show()

                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }


                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog.dismiss()
            }

            override fun onErrorListener() {
                dialog.dismiss()
            }
        })
    }
    private fun MakePayment_Wallet(Url: String) {
        dialog = Dialog(this@FareBreakUp)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        println("-------------MakePayment Wallet Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = SrideId_intent

        mRequest = ServiceRequest(this@FareBreakUp)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------MakePayment Wallet Response----------------$response")
                var Sstatus = ""
                var Scurrency_code = ""
                var Scurrent_wallet_balance = ""
                var currencycode: Currency? = null
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("0", ignoreCase = true)) {
                        Alert(resources.getString(R.string.my_rides_payment_empty_wallet_sorry), resources.getString(R.string.my_rides_payment_empty_wallet))
                    } else if (Sstatus.equals("1", ignoreCase = true)) {
                        val intent = Intent(this@FareBreakUp, MyRideRating::class.java)
                        intent.putExtra("RideID", SrideId_intent)
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter, R.anim.exit)

                    }/* else if (Sstatus.equals("2", ignoreCase = true)) {

                        FareBreakUp.invisibleTips()

                        //Updating wallet amount on Navigation Drawer Slide
                        Scurrency_code = `object`.getString("currency")
                        currencycode = Currency.getInstance(getLocale(Scurrency_code))
                        Scurrent_wallet_balance = `object`.getString("wallet_amount")
                        session!!.createWalletAmount(currencycode!!.symbol + Scurrent_wallet_balance)
                        NavigationDrawer.navigationNotifyChange()
                        val broadcastIntent = Intent()
                        broadcastIntent.action = "com.package.ACTION_CLASS_REFRESH"
                        sendBroadcast(broadcastIntent)
                        val mDialog = PkDialog(this@FareBreakUp)
                        mDialog.setDialogTitle(resources.getString(R.string.my_rides_payment_cash_success))
                        mDialog.setDialogMessage(resources.getString(R.string.my_rides_payment_cash_driver_confirm_label))
                        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
                            mDialog.dismiss()
                            postRequest_PaymentList(Iconstant.paymentList_url)
                        })
                        mDialog.show()

                    }*/ else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override  fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

}
