package com.project.app.ridehubuser.Adapters

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.project.app.ridehubuser.R

class AddFundsAdapter(context: Context/*, list: ArrayList<HomePojo>*/) :
    RecyclerView.Adapter<AddFundsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    /*private var mList: ArrayList<HomePojo> = ArrayList()*/

    init {
        this.mContext = context
        /*this.mList = list*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycle_item_add_funds, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 3
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_total_amount: TextView = view.findViewById(R.id.tv_total_amount)
        var tv_discount_price: TextView = view.findViewById(R.id.tv_discount_price)
        var tv_discount_percent: TextView = view.findViewById(R.id.tv_discount_percent)
        var tv_after_discount_amount: TextView = view.findViewById(R.id.tv_after_discount_amount)

    }

}




