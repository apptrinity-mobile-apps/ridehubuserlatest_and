package com.project.app.ridehubuser.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.app.ridehubuser.PojoResponse.HelpSubCategoryAnswersPojo
import com.project.app.ridehubuser.R
import java.util.*


@SuppressLint("SetTextI18n")
class HelpSubCategoryAnswersAdapter(private val context: Context, private val data: ArrayList<HelpSubCategoryAnswersPojo>) :
    RecyclerView.Adapter<HelpSubCategoryAnswersAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_help_category_qa, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (data[position].getQuestion().equals("null")) {
            holder.tv_question.text = ""
        } else {
            holder.tv_question.text = data[position].getQuestion()
        }

        if (data[position].getAnswer().equals("null")) {
            holder.tv_answer.text = ""
        } else {
            holder.tv_answer.text = data[position].getAnswer()
        }


        /*holder.ll_category.setOnClickListener {
            val intent = Intent(context, HelpSubCategoryAnswersActivity::class.java)
            intent.putExtra("id", data[position].getId())
            intent.putExtra("category_id", data[position].getCategory())
            intent.putExtra("subcategory_id", data[position].getSubcategoryId())
            intent.putExtra("subcategoryname", data[position].getSubCategory())
            context.startActivity(intent)
            (context as Activity).overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }*/

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_question: TextView = view.findViewById(R.id.tv_question)
        var tv_answer: TextView = view.findViewById(R.id.tv_answer)

    }
}
