package com.project.app.ridehubuser.Activities

import android.os.Bundle
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager

class AddPaymentActivity : ActivityHockeyApp() {

    lateinit var iv_back_add_payment: ImageView
    lateinit var iv_close_add_payment: ImageView
    lateinit var cv_credit_card: CardView
    lateinit var cv_gift_card: CardView
    lateinit var cv_paypal: CardView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    lateinit var userID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_payment_method)

        initialize()
        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()

        iv_back_add_payment.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            finish()
        }

        iv_close_add_payment.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.slidedown, R.anim.slideup)
            finish()
        }

    }

    private fun initialize() {
        session = SessionManager(this@AddPaymentActivity)
        cd = ConnectionDetector(this@AddPaymentActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_back_add_payment = findViewById(R.id.iv_back_add_payment)
        iv_close_add_payment = findViewById(R.id.iv_close_add_payment)
        cv_credit_card = findViewById(R.id.cv_credit_card)
        cv_gift_card = findViewById(R.id.cv_gift_card)
        cv_paypal = findViewById(R.id.cv_paypal)

    }

}
