package com.project.app.ridehubuser.Utils

interface SmsListener {

    fun messageReceived(sender: String, messageText: String)

}