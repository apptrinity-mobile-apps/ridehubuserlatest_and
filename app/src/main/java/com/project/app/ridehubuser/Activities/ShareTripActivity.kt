package com.project.app.ridehubuser.Activities

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager

class ShareTripActivity : ActivityHockeyApp() {

    lateinit var iv_close_share_trip: ImageView
    lateinit var tv_privacy_policy: TextView
    lateinit var tv_later: TextView
    lateinit var tv_next: TextView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    lateinit var userID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_my_trip)

        initialize()
        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()

        iv_close_share_trip.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.slidedown, R.anim.slideup)
            finish()
        }
        tv_later.setOnClickListener {  }
        tv_next.setOnClickListener {  }
        tv_privacy_policy.setOnClickListener {  }
    }

    private fun initialize() {
        session = SessionManager(this@ShareTripActivity)
        cd = ConnectionDetector(this@ShareTripActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_close_share_trip = findViewById(R.id.iv_close_share_trip)
        tv_privacy_policy = findViewById(R.id.tv_privacy_policy)
        tv_later = findViewById(R.id.tv_later)
        tv_next = findViewById(R.id.tv_next)
    }

}


