package com.project.app.ridehubuser.PojoResponse

class HelpSubCategoryAnswersPojo {



    private var id: String? = null
    private var sub_category_id: String? = null
    private var category: String? = null
    private var sub_category: String? = null
    private var status: String? = null
    private var question: String? = null
    private var answer: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getCategory(): String? {
        return category
    }

    fun setCategory(category: String) {
        this.category = category
    }


    fun getSubcategoryId(): String? {
        return sub_category_id
    }

    fun setSubCategoryId(sub_category_id: String) {
        this.sub_category_id = sub_category_id
    }


    fun getSubCategory(): String? {
        return sub_category
    }

    fun setSubCategory(sub_category: String) {
        this.sub_category = sub_category
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }

    fun getQuestion(): String? {
        return question
    }

    fun setQuestion(question: String) {
        this.question = question
    }

    fun getAnswer(): String? {
        return answer
    }

    fun setAnswer(answer: String) {
        this.answer = answer
    }


}