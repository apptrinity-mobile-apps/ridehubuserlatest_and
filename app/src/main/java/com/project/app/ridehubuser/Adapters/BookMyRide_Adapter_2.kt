package com.project.app.ridehubuser.Adapters

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.project.app.ridehubuser.PojoResponse.HomePojo
import com.project.app.ridehubuser.R
import java.util.*

class BookMyRide_Adapter_2(context: Context, list: ArrayList<HomePojo>) :
    RecyclerView.Adapter<BookMyRide_Adapter_2.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<HomePojo> = ArrayList()
    private var setselected = 0

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.bookmyride_single, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.tv_car_ame.text = mList[position].getCat_name()
        holder.tv_car_price.text = "$${mList[position].getMin_amount()}"
        holder.tv_car_time.text = mList[position].getCat_time()
        holder.bookmyride_cab_qty.text = mList[position].getCar_capacity_max()
        holder.image.setImageDrawable(ContextCompat.getDrawable(mContext!!,R.drawable.dash_car))

        if(setselected == position){
            holder.ll_lvridetype.background =ContextCompat.getDrawable(mContext!!,R.drawable.blue_gradient_bg)
            holder.image.setImageDrawable(ContextCompat.getDrawable(mContext!!,R.drawable.cab_user_select))
            holder.iv_cab_type.setImageDrawable(ContextCompat.getDrawable(mContext!!,R.drawable.cab_selected))
            holder.tv_car_ame.setTextColor(ContextCompat.getColor(mContext!!,R.color.white))
            holder.tv_car_price.setTextColor(ContextCompat.getColor(mContext!!,R.color.green_tv))
            holder.tv_car_time.setTextColor(ContextCompat.getColor(mContext!!,R.color.white))
            holder.bookmyride_cab_qty.setTextColor(ContextCompat.getColor(mContext!!,R.color.white))
        }else{
            holder.ll_lvridetype.background = ContextCompat.getDrawable(mContext!!,R.drawable.white_gradient_bg)
            holder.image.setImageDrawable(ContextCompat.getDrawable(mContext!!,R.drawable.cab_user_deselect))
            holder.iv_cab_type.setImageDrawable(ContextCompat.getDrawable(mContext!!,R.drawable.cab_unselected))
            holder.tv_car_ame.setTextColor(ContextCompat.getColor(mContext!!,R.color.black))
            holder.tv_car_price.setTextColor(ContextCompat.getColor(mContext!!,R.color.ride_total))
            holder.tv_car_time.setTextColor(ContextCompat.getColor(mContext!!,R.color.black))
            holder.bookmyride_cab_qty.setTextColor(ContextCompat.getColor(mContext!!,R.color.black))
        }


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_car_ame: TextView
         var image: ImageView
         var iv_cab_type: ImageView
         var tv_car_price: TextView
         var tv_car_time: TextView
         var bookmyride_cab_qty: TextView
         var ll_lvridetype: LinearLayout

        init {
            tv_car_ame = view.findViewById(R.id.bookmyride_single_carname) as TextView
            image = view.findViewById(R.id.bookmyride_single_car_image) as ImageView
            iv_cab_type = view.findViewById(R.id.iv_cab_type) as ImageView
            tv_car_price = view.findViewById(R.id.bookmyride_single_carprice) as TextView
            tv_car_time = view.findViewById<View>(R.id.bookmyride_single_time) as TextView
            bookmyride_cab_qty = view.findViewById<View>(R.id.bookmyride_cab_qty) as TextView
            ll_lvridetype = view.findViewById<View>(R.id.ll_lvridetype) as LinearLayout


        }
    }

    fun setSelection(position: Int){
        this.setselected = position
        notifyDataSetChanged()
    }

    fun getBookRideData(position: Int):HomePojo{
        return mList[position]
    }

    fun updateList(updateList: ArrayList<HomePojo>) {
        mList = updateList
        notifyDataSetChanged()
    }

}




