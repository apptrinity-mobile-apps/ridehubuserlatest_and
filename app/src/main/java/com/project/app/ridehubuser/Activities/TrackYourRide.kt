package com.project.app.ridehubuser.Activities

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.googlemapdrawpolyline.GMapV2GetRouteDirection
import com.project.app.mylibrary.gps.GPSTracker
import com.project.app.mylibrary.latlnginterpolation.LatLngInterpolator
import com.project.app.mylibrary.latlnginterpolation.MarkerAnimation
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.widgets.RoundedImageView
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.PojoResponse.CancelTripPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.squareup.picasso.Picasso
import com.widget.CustomTextView
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.util.*

class TrackYourRide : FragmentActivity(), OnMapReadyCallback, View.OnClickListener, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private var tv_done: TextView? = null
    private var tv_drivername: TextView? = null
    private var tv_carModel: TextView? = null
    private var tv_driver_estTime: CustomTextView? = null
    private var tv_carNo: TextView? = null
    private var tv_rating: TextView? = null
    private var tv_track_cab_type: ImageView? = null
    private var driver_image: RoundedImageView? = null
    private var rl_callDriver: ImageView? = null
    private var track_your_ride_carimage: ImageView? = null
    private var iv_userchat: ImageView? = null
    private var rl_endTrip: CustomTextView? = null
    //private GoogleMap googleMap;
    private val marker: MarkerOptions? = null
    private var gps: GPSTracker? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var driverID = ""
    private var desti_location = ""
    private var source_Location = ""
    private var driverName = ""
    private var driverImage = ""
    private var driverRating = ""
    private var driverLat: String? = ""
    private var driverLong: String? = ""
    private var driverTime = ""
    private var rideID = ""
    private var driverMobile: String? = ""
    private var driverCar_no = ""
    private var driverCar_model = ""
    private var driverEst_time = ""
    private var userLat = ""
    private var userLong = ""
    private var cat_type = ""
    private var cab_type_img = ""
    private var isReasonAvailable = false
    private var mRequest: ServiceRequest? = null
    private var dialog: Dialog? = null
    private var session: SessionManager? = null
    private var UserID = ""
    private var itemlist_reason: ArrayList<CancelTripPojo>? = null
    private var Tv_headerTitle: TextView? = null
    private var track_your_ride_view1: View? = null
    private var fromPosition: LatLng? = null
    private var toPosition: LatLng? = null
    private var markerOptions: MarkerOptions? = null
    internal lateinit var mLocationRequest: LocationRequest
    private var mGoogleApiClient: GoogleApiClient? = null
    private var currentLocation: Location? = null
    private var currentLocationn: LatLng? = null
    private var  dropLatLng: LatLng? = null
    private var pickUpLatLng: LatLng? = null
    private var refreshReceiver: RefreshReceiver? = null
    private val realTimeTrackYourRideHandler = Handler()

    internal val PERMISSION_REQUEST_CODE = 111

    internal val PERMISSION_REQUEST_CODES = 222
    private var sSelectedPanicNumber = ""
    private var panic_btn: CardView? = null
    private var current_location_track: CardView? = null
    private var tv_trackride_source_loc: CustomTextView? = null
    private var tv_trackride_desti_loc: CustomTextView? = null
    private var ll_confirm_call_or_sms: LinearLayout? = null
    private var ll_source_destination: LinearLayout? = null
    private var mMap: GoogleMap? = null
    private var lat: Double = 0.toDouble()
    private var lng: Double = 0.toDouble()


    internal var mLatLngInterpolator: LatLngInterpolator? = null

    private  var firsttimepolyline: Boolean? = true
    var isFirstPosition = true
    var startPositionn: LatLng? = null
    var endPosition: LatLng? = null
    private  var polyline: Polyline? = null


    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this)
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onLocationChanged(location: Location) {
        this.currentLocation = location
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()

    }

    override fun onResume() {
        super.onResume()
        ChatService.startUserAction(this@TrackYourRide)
        startLocationUpdates()
    }


    override fun onMapReady(googleMap: GoogleMap) {
        //enableGpsService();
        mMap = googleMap

        if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {
            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()

            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude

            mMap!!.projection
            mMap!!.cameraPosition
            // Move the camera to last position with a zoom level
            val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(17f).build()
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            mMap!!.isMyLocationEnabled = false

            mMap!!.uiSettings.isTiltGesturesEnabled
            mMap!!.uiSettings.isMyLocationButtonEnabled
            mMap!!.uiSettings.isMapToolbarEnabled
            mMap!!.uiSettings.isCompassEnabled
            mMap!!.mapType

            mMap!!.uiSettings.isZoomControlsEnabled
            mMap!!.focusedBuilding
            mMap!!.uiSettings.isRotateGesturesEnabled
            mMap!!.uiSettings.isIndoorLevelPickerEnabled = true
            mMap!!.uiSettings.isZoomGesturesEnabled
            mMap!!.uiSettings.isIndoorLevelPickerEnabled
            mMap!!.setMaxZoomPreference(50f)
            mMap!!.uiSettings.isZoomGesturesEnabled = true
            mMap!!.projection.visibleRegion


        } else {

            // enableGpsService();
        }

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
            //mMap.setMyLocationEnabled(true);

            val AM_PM: String
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val min = c.get(Calendar.MINUTE)
            val ds = c.get(Calendar.AM_PM)
            Log.e("date_string", "" + ds)
            if (ds == 0)
                AM_PM = "am"
            else
                AM_PM = "pm"


            val styleManager = MapStyleManager.attachToMap(this, mMap!!)
            styleManager.addStyle(0f, R.raw.map_silver_json)
            styleManager.addStyle(10f, R.raw.map_silver_json)
            styleManager.addStyle(12f, R.raw.map_silver_json)

        }

    }


    inner class RefreshReceiver : BroadcastReceiver() {
        @SuppressLint("NewApi")
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver") {
                println("triparrived----------------------")
                //Tv_headerTitle!!.text = resources.getString(R.string.action_driver_arrived)
                tv_driver_estTime!!.text = resources.getString(R.string.action_driver_arrived)

                rl_endTrip!!.visibility = View.GONE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    // rl_callDriver!!.background = resources.getDrawable(R.drawable.curve_background)
                }
                // track_your_ride_view1!!.visibility = View.GONE
            } else if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip") {
                println("tripbegin----------------------")
                //  Tv_headerTitle!!.text = resources.getString(R.string.action_enjy_your_ride)
                rl_endTrip!!.visibility = View.GONE
                tv_driver_estTime!!.text = resources.getString(R.string.action_enjy_your_ride)

                ll_source_destination!!.visibility = View.VISIBLE
                tv_trackride_source_loc!!.text = source_Location
                tv_trackride_desti_loc!!.text = desti_location
                ll_confirm_call_or_sms!!.visibility = View.GONE

                //rl_callDriver!!.background = resources.getDrawable(R.drawable.curve_background)
                // track_your_ride_view1!!.visibility = View.GONE
                panic_btn!!.visibility = View.VISIBLE
            } else if (intent.action == "com.package.track.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver") {

                //System.out.println("check--------------REFRESH_UpdateDriver");

            }

            //System.out.println("check--------------");
            if (intent.extras != null && intent.extras!!.containsKey("drop_lat") && intent.extras!!.containsKey("drop_lng")) {
                val lat = intent.extras!!.get("drop_lat") as String
                val lng = intent.extras!!.get("drop_lng") as String
                val pickUp_lat = intent.extras!!.get("pickUp_lat") as String
                val pickUp_lng = intent.extras!!.get("pickUp_lng") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    val pick_lat_decimal = java.lang.Double.parseDouble(pickUp_lat)
                    val pick_lng_decimal = java.lang.Double.parseDouble(pickUp_lng)

                    Log.e("UPDATELATLNG_BEFORE",""+lat_decimal+"-----"+lng_decimal+"-----"+pick_lat_decimal+"-----"+pick_lng_decimal)

                    updateGoogleMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal)
                    println("updateGoogleMapTrackRide--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }

            }
            println("out else--------------")
            if (intent.extras != null && intent.extras!!.containsKey(Iconstant.isContinousRide)) {
                val lat = intent.extras!!.get("latitude") as String
                val lng = intent.extras!!.get("longitude") as String

                val ride_id = intent.extras!!.get("ride_id") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)

                    println("updateDriverOnMap-----------updateing location---------")
                    updateDriverOnMap(lat_decimal, lng_decimal)
                } catch (e: Exception) {
                }

            }
        }
    }

    private fun updateDriverOnMap(lat_decimal: Double, lng_decimal: Double) {

        val latLng = LatLng(lat_decimal, lng_decimal)

        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = LatLngInterpolator.Linear()
        }

        Log.d("tess", "inside run ")
        // animateMarkerNew(latLng, curentDriverMarker);

        if (curentDriverMarker != null) {

            Log.d("UPDATEDRIVERONMAP", "inside run IF")

            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator)

            val zoom = mMap!!.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
            // animateMarkerNew(latLng, curentDriverMarker)

            if (isFirstPosition) {
                startPositionn = curentDriverMarker!!.getPosition();


                curentDriverMarker!!.setAnchor(0.5f, 0.5f);

                mMap!!.moveCamera(CameraUpdateFactory
                        .newCameraPosition
                        (CameraPosition.Builder()
                                .target(startPositionn)
                                .zoom(15.5f)
                                .build()));

                isFirstPosition = false;

            } else {
                endPosition = LatLng(latLng.latitude, latLng.longitude);

               // Log.d("ahsdgashd", startPosition!!.latitude + "--" + endPosition!!.latitude + "--Check --" + startPosition!!.longitude + "--" + endPosition!!.longitude);

                if ((startPositionn!!.latitude != endPosition!!.latitude) || (startPositionn!!.longitude != endPosition!!.longitude)) {

                    Log.e("asdhashd", "NOT SAME");
                    startBikeAnimation(startPositionn!!, endPosition!!);

                } else {

                    Log.e("dfsdfsd", "SAMME");
                }
            }


        } else {

            Log.d("UPDATEDRIVERONMAP", "inside run ELSE")

            mLatLngInterpolator = LatLngInterpolator.Linear()
            curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    .anchor(0.5f, 0.5f)
                    .flat(true))

            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)

            // animateMarkerNew(latLng, curentDriverMarker)

        }


    }

    private fun updateGoogleMapTrackRide(lat_decimal: Double, lng_decimal: Double, pick_lat_decimal: Double, pick_lng_decimal: Double) {
        if (mMap != null) {
            mMap!!.clear()
        }

        Log.e("UPDATELATLNG_INSIDE",""+lat_decimal+"-----"+lng_decimal+"-----"+pick_lat_decimal+"-----"+pick_lng_decimal)
         dropLatLng = LatLng(lat_decimal, lng_decimal)
         pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)

        Log.e("UPDATELATLNG_INSIDELATLNG",""+dropLatLng+"-----"+pickUpLatLng)

        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng!!, pickUpLatLng!!)
        draw_route_asyncTask.execute()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.track_your_ride)
        trackyour_ride_class = this@TrackYourRide
        initialize()
        try {
            setLocationRequest()
            buildGoogleApiClient()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initializeMap()

        panic_btn!!.setOnClickListener {
            println("----------------panic onclick method-----------------")
            panic()
        }


        current_location_track!!.setOnClickListener {

            gps = GPSTracker(this)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return@setOnClickListener
            }
            mMap!!.setMyLocationEnabled(false)
            if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {

                MyCurrent_lat = gps!!.getLatitude()
                MyCurrent_long = gps!!.getLongitude()

                // Move the camera to last position with a zoom level
                val cameraPosition = CameraPosition.Builder().target(LatLng(MyCurrent_lat, MyCurrent_long)).zoom(17f).build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                current_location_track!!.setVisibility(View.VISIBLE)

            } else {
                //enableGpsService()
                //Toast.makeText(getActivity(), "GPS not Enabled !!!", Toast.LENGTH_LONG).show();
            }

        }


        //Start XMPP Chat Service
    }


    private fun initialize() {
        cd = ConnectionDetector(this@TrackYourRide)
        isInternetPresent = cd!!.isConnectingToInternet
        gps = GPSTracker(this@TrackYourRide)
        session = SessionManager(this@TrackYourRide)
        itemlist_reason = ArrayList<CancelTripPojo>()
        markerOptions = MarkerOptions()
        tv_done = findViewById(R.id.track_your_ride_done_textview) as TextView
        tv_drivername = findViewById(R.id.track_your_ride_driver_name) as TextView
        tv_carModel = findViewById(R.id.track_your_ride_driver_carmodel) as TextView
        tv_driver_estTime = findViewById(R.id.tv_confride_driver_esti_time) as CustomTextView
        tv_carNo = findViewById(R.id.track_your_ride_driver_carNo) as TextView
        tv_rating = findViewById(R.id.track_your_ride_driver_rating) as TextView
        tv_track_cab_type = findViewById(R.id.tv_track_cab_type) as ImageView
        driver_image = findViewById(R.id.track_your_ride_driverimage) as RoundedImageView
        rl_callDriver = findViewById(R.id.track_your_ride_calldriver_layout) as ImageView
        track_your_ride_carimage = findViewById(R.id.track_your_ride_carimage) as ImageView
        iv_userchat = findViewById(R.id.iv_userchat) as ImageView
        rl_endTrip = findViewById(R.id.track_your_ride_endtrip_layout) as CustomTextView
        Tv_headerTitle = findViewById(R.id.track_your_ride_track_label) as TextView
        //track_your_ride_view1 = findViewById(R.id.track_your_ride_view1) as View
        panic_btn = findViewById(R.id.track_your_ride_panic_cardview_layout) as CardView
        current_location_track = findViewById(R.id.track_your_ride_current_loc_cardview_layout) as CardView
        ll_source_destination = findViewById(R.id.ll_source_destination) as LinearLayout
        tv_trackride_source_loc = findViewById(R.id.tv_trackride_source_loc) as CustomTextView
        tv_trackride_desti_loc = findViewById(R.id.tv_trackride_desti_loc) as CustomTextView
        ll_confirm_call_or_sms = findViewById(R.id.ll_confirm_call_or_sms) as LinearLayout

        val intent = intent
        if (intent != null) {


            driverID = intent.getStringExtra("driverID")
            driverName = intent.getStringExtra("driverName")
            driverImage = intent.getStringExtra("driverImage")
            driverRating = intent.getStringExtra("driverRating")
            driverLat = intent.getStringExtra("driverLat")
            driverLong = intent.getStringExtra("driverLong")
            driverTime = intent.getStringExtra("driverTime")
            rideID = intent.getStringExtra("rideID")
            driverMobile = intent.getStringExtra("driverMobile")
            driverCar_no = intent.getStringExtra("driverCar_no")
            driverCar_model = intent.getStringExtra("driverCar_model")
            source_Location = intent.getStringExtra("source_Location")
            driverEst_time = intent.getStringExtra("driverTime")
            userLat = intent.getStringExtra("userLat")
            userLong = intent.getStringExtra("userLong")
            cat_type = intent.getStringExtra("cat_type")
            desti_location = intent.getStringExtra("destination_Location")


            // cab_type_img = intent.getStringExtra("cab_type_image")
        }


        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = RefreshReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")
        intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver")
        registerReceiver(refreshReceiver, intentFilter)


        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user.get(SessionManager.KEY_USERID).toString()

        tv_drivername!!.text = driverName
        tv_carNo!!.text = driverCar_no
        tv_carModel!!.text = driverCar_model
        tv_driver_estTime!!.text = "Driver Arrives in " + driverEst_time
        tv_rating!!.text = driverRating + "*"

        Picasso.with(this)
                .load(driverImage)
                .into(driver_image)
        tv_done!!.setOnClickListener(this)
        rl_callDriver!!.setOnClickListener(this)
        rl_endTrip!!.setOnClickListener(this)


       /* iv_userchat!!.setOnClickListener {
            val i = Intent(this@TrackYourRide, ChatActivity::class.java)
            i.putExtra("driverID", driverID)
            i.putExtra("driverName", driverName)
            i.putExtra("driverImage", driverImage)
            startActivity(i)

        }*/


    }

    private fun panic() {
        println("----------------panic method-----------------")
        val view = View.inflate(this@TrackYourRide, R.layout.panic_page, null)
        val dialog = MaterialDialog(this@TrackYourRide)
        dialog.setContentView(view).setNegativeButton(resources.getString(R.string.my_rides_detail_cancel)
        ) { dialog.dismiss() }.show()
        val call_police = view.findViewById(R.id.call_police_button) as Button
        val call_fire = view.findViewById(R.id.call_fireservice_button) as Button
        val call_ambulance = view.findViewById(R.id.call_ambulance_button) as Button
        call_police.setOnClickListener {
            dialog.dismiss()

            sSelectedPanicNumber = "+100"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "+100")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "+100")
                startActivity(callIntent)
            }
        }

        call_fire.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+101"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "101")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "101")
                startActivity(callIntent)
            }
        }
        call_ambulance.setOnClickListener {
            dialog.dismiss()
            sSelectedPanicNumber = "+108"
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermissions()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + "108")
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + "108")
                startActivity(callIntent)
            }
        }

    }

    private fun initializeMap() {

        if (mMap == null) {
            //mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.book_my_ride_mapview));
            val mapFragment = supportFragmentManager.findFragmentById(R.id.track_your_ride_mapview) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                //Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }

            if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {
                val Dlatitude = gps!!.getLatitude()
                val Dlongitude = gps!!.getLongitude()
                MyCurrent_lat = Dlatitude
                MyCurrent_long = Dlongitude
                // Move the camera to last position with a zoom level
                val cameraPosition = CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(15f).build()
                //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else {
                Alert(resources.getString(R.string.action_error), resources.getString(R.string.alert_gpsEnable))
            }
            //set marker for driver location.

                if (driverLat != null && driverLong != null) {
                    fromPosition = LatLng(java.lang.Double.parseDouble(userLat), java.lang.Double.parseDouble(userLong))
                    toPosition = LatLng(java.lang.Double.parseDouble(driverLat!!), java.lang.Double.parseDouble(driverLong!!))
                    if (fromPosition != null && toPosition != null) {
                        val draw_route_asyncTask = GetRouteTask()
                        draw_route_asyncTask.execute()
                    }
                }



        }
    }


    override fun onClick(v: View) {

        if (v === tv_done) {
            /* Intent finish_timerPage = new Intent();
                finish_timerPage.setAction("com.pushnotification.finish.TimerPage");
                sendBroadcast(finish_timerPage);*/

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

        } else if (v === rl_callDriver) {
            if (driverMobile != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + driverMobile!!)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + driverMobile!!)
                    startActivity(callIntent)
                }
            } else {
                Alert(this@TrackYourRide.resources.getString(R.string.alert_label_title), this@TrackYourRide.resources.getString(R.string.track_your_ride_alert_content1))
            }
        } else if (v === rl_endTrip) {
            val mDialog = PkDialog(this@TrackYourRide)
            mDialog.setDialogTitle(resources.getString(R.string.my_rides_detail_cancel_ride_alert_title))
            mDialog.setDialogMessage(resources.getString(R.string.my_rides_detail_cancel_ride_alert))
            mDialog.setPositiveButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_yes), View.OnClickListener {
                mDialog.dismiss()
                cd = ConnectionDetector(this@TrackYourRide)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                        postRequest_CancelRides_Reason(Iconstant.cancel_myride_reason_url)
                } else {
                    Alert(resources.getString(R.string.alert_label_title), resources.getString(R.string.alert_nointernet))
                }
            })
            mDialog.setNegativeButton(resources.getString(R.string.my_rides_detail_cancel_ride_alert_no), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()

        }

    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@TrackYourRide)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        override fun onPreExecute() {

            Log.e("GETDROPROUTE","GetRouteTask")

        }

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(toPosition, fromPosition, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {

            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding ride_detail_trackride on the map

                    val markerWidth = 350
                    val markerHeight = 250

                    val destn_marker_view =
                        (this@TrackYourRide!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_drop_marker,
                            null
                        )
                    val pickup_marker_view =
                        (this@TrackYourRide!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_pickup_marker,
                            null
                        )

                    val tv_marker_pickup =
                        pickup_marker_view.findViewById<TextView>(R.id.tv_marker_pickup)
                   // tv_marker_pickup.text = Selected_SourceLocation!!
                    val pickup_bitmap = createBitmapFromLayout(pickup_marker_view)
                    val pickup_small_bitmap =
                        Bitmap.createScaledBitmap(pickup_bitmap, markerWidth, markerHeight, false)


                   // tv_marker_drop.text = Sselected_DestinationLocation!!
                    val destn_bitmap = createBitmapFromLayout(destn_marker_view)
                    val destn_small_bitmap =
                        Bitmap.createScaledBitmap(destn_bitmap, markerWidth, markerHeight, false)

                        markerOptions!!.position(fromPosition!!)

                        markerOptions!!.draggable(true)
                        //mMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(fromPosition!!)
                            .icon(BitmapDescriptorFactory.fromBitmap(pickup_small_bitmap)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                               .icon(BitmapDescriptorFactory.fromBitmap(destn_small_bitmap)))
                        curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car)))

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(fromPosition!!)
                        builder.include(toPosition!!)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))


                    markerOptions!!.position(toPosition!!)
                    polyline = mMap!!.addPolyline(rectLine)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }


    //-----------------------MyRide Cancel Reason Post Request-----------------
    private fun postRequest_CancelRides_Reason(Url: String) {
        dialog = Dialog(this@TrackYourRide)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        println("-------------MyRide Cancel Reason Url----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(this@TrackYourRide)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------MyRide Cancel Reason Response----------------$response")
                var Sstatus = ""
                try {
                    val `object` = JSONObject(response)
                    Sstatus = `object`.getString("status")
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        val response_object = `object`.getJSONObject("response")
                        if (response_object.length() > 0) {
                            val reason_array = response_object.getJSONArray("reason")
                            if (reason_array.length() > 0) {
                                itemlist_reason!!.clear()
                                for (i in 0 until reason_array.length()) {
                                    val reason_object = reason_array.getJSONObject(i)
                                    val pojo = CancelTripPojo()
                                    pojo.setReason(reason_object.getString("reason"))
                                    pojo.setReasonId(reason_object.getString("id"))
                                    itemlist_reason!!.add(pojo)
                                }
                                isReasonAvailable = true
                            } else {
                                isReasonAvailable = false
                            }
                        }
                    } else {
                        val Sresponse = `object`.getString("response")
                        Alert(resources.getString(R.string.alert_label_title), Sresponse)
                    }

                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                dialog!!.dismiss()
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            val finish_timerPage = Intent()
            finish_timerPage.action = "com.pushnotification.finish.TimerPage"
            sendBroadcast(finish_timerPage)

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.pushnotification.updateBottom_view"
            sendBroadcast(broadcastIntent)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()

            return true
        }
        return false
    }

    public override fun onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver)
        ChatService.disableChat()
        super.onDestroy()
    }


    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private inner class GetDropRouteTask : AsyncTask<String, Void, String>() {
        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        private var endLocation: LatLng? = null
        private var currentLocation: LatLng? = null
        fun setToAndFromLocation(currentLocation: LatLng, endLocation: LatLng) {
            Log.e("UPDATELATLNG_INSIDEDROP",""+currentLocation+"-----"+endLocation)
           this.currentLocation = currentLocation
            this.endLocation = endLocation
        }

        override fun onPreExecute() {
            Log.e("GETDROPROUTE","GetDropRouteTask")
            currentLocationn = currentLocation
        }
        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(endLocation, currentLocation, GMapV2GetRouteDirection.MODE_DRIVING)
            response = "Success"
            return response

        }

        override fun onPostExecute(result: String) {
            if (polyline != null) {
                polyline!!.remove()
            }
            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine = PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding ride_detail_trackride on the map
                    if(firsttimepolyline!!){
                        markerOptions!!.position(fromPosition!!)

                        markerOptions!!.draggable(true)
                        //mMap.addMarker(markerOptions);
                        mMap!!.addMarker(MarkerOptions()
                                .position(fromPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_sourceloc)))
                        mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.destilocation)))
                        curentDriverMarker = mMap!!.addMarker(MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car)))

                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(fromPosition!!)
                        builder.include(toPosition!!)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                    }
                    polyline = mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(toPosition!!)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }


        }
    }

    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODE)
    }


    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE), PERMISSION_REQUEST_CODES)
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + driverMobile!!)
                startActivity(callIntent)
            }

            PERMISSION_REQUEST_CODES ->

                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$sSelectedPanicNumber")
                    startActivity(callIntent)
                }
        }
    }

    //Method for finding bearing between two points
    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return 1f
    }

    companion object {
        lateinit var trackyour_ride_class: TrackYourRide
        private var curentDriverMarker: Marker? = null
        private val movingMarker: Marker? = null
        internal val REQUEST_LOCATION = 199

        internal var latLngInterpolator: LatLngInterpolator = LatLngInterpolator.Spherical()

        fun updateMap(latLng: LatLng) {
            MarkerAnimation.animateMarkerToICS(movingMarker, latLng, latLngInterpolator)
        }
    }

    private fun startBikeAnimation(start: LatLng, end: LatLng) {
        val handler = Handler()
        Log.e("Startbikeanimation","Entered to me")

            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 3000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction

                    lng = v * end!!.longitude + (1 - v) * start!!.longitude
                    lat = v * end!!.latitude + (1 - v) * start!!.latitude

                    val newPos = LatLng(lat, lng)

                    curentDriverMarker!!.setPosition(newPos)
                    curentDriverMarker!!.setAnchor(0.5f, 0.5f)
                    curentDriverMarker!!.rotation = getBearing(start, end)
                    mMap!!.moveCamera(CameraUpdateFactory
                            .newCameraPosition(CameraPosition.Builder()
                                    .target(newPos)
                                    .zoom(15.5f)
                                    .build()))
                    startPositionn = curentDriverMarker!!.getPosition();

                    /*val line = mMap!!.addPolyline(PolylineOptions().add(start, end)
                            .width(10f).color(resources.getColor(R.color.trackride_polyline)))*/
                } catch (ex: Exception) {
                    //I don't care atm..
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)

                }
            })
            valueAnimator.start()
        firsttimepolyline = false

        Log.e("currentLocationn", currentLocationn.toString())
        //to add new polyline
        val runnable = Runnable {
            currentLocationn = start
            if (currentLocationn != null) {
                val getRoute = GetDropRouteTask()
                getRoute.setToAndFromLocation(dropLatLng!!, pickUpLatLng!!)
                getRoute.execute()
            }
        }
        handler.postDelayed(runnable, 3000)


    }


    fun createBitmapFromLayout(view: View): Bitmap {

        val displayMetrics = DisplayMetrics()
        (this as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)

        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap =
            Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)

        return bitmap
    }



}