package com.project.app.ridehubuser.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant


class FareBreakUpPaymentWebView : ActivityHockeyApp() {
    private var back: ImageView? = null
    private var context: Context? = null
    private var webview: WebView? = null
    private var progressBar: ProgressBar? = null

    private var Str_mobileID = ""
    private var session: SessionManager? = null
    private var UserID = ""
    private var SrideId_intent = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.myride_payment_webview)
        context = getApplicationContext()
        initialize()
        println("-------------Testing payment webview----------------")
        back!!.setOnClickListener {
            val mDialog = PkDialog(this@FareBreakUpPaymentWebView)
            mDialog.setDialogTitle(getResources().getString(R.string.cabily_webview_lable_cancel_transaction))
            mDialog.setDialogMessage(getResources().getString(R.string.cabily_webview_lable_cancel_transaction_proceed))
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                mDialog.dismiss()

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

                onBackPressed()
                overridePendingTransition(R.anim.enter, R.anim.exit)
                finish()
            })
            mDialog.setNegativeButton(getResources().getString(R.string.action_cancel_alert), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()
        }

        // Show the progress bar
        webview!!.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress < 100 && progressBar!!.visibility == ProgressBar.GONE) {
                    progressBar!!.visibility = ProgressBar.VISIBLE
                }
                progressBar!!.progress = progress

                if (progress == 100) {
                    progressBar!!.visibility = ProgressBar.GONE
                }
            }
        }

    }

    private fun initialize() {
        session = SessionManager(this@FareBreakUpPaymentWebView)

        back = findViewById(R.id.myride_payment_webview_header_back_layout) as ImageView
        webview = findViewById(R.id.myride_payment_webview) as WebView
        progressBar = findViewById(R.id.myride_payment_webview_progressbar) as ProgressBar

        // Enable Javascript to run in WebView
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.domStorageEnabled = true

        // Allow Zoom in/out controls
        webview!!.settings.builtInZoomControls = true

        // Zoom out the best fit your screen
        webview!!.settings.loadWithOverviewMode = true
        webview!!.settings.useWideViewPort = true


        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        val i = getIntent()
        Str_mobileID = i.getStringExtra("MobileID")
        SrideId_intent = i.getStringExtra("RideID")

        startWebView(Iconstant.makepayment_webview_url + Str_mobileID)
    }


    private fun startWebView(url: String) {
        webview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {}

            override fun onPageFinished(view: WebView, url: String) {
                try {
                    if (url.contains("failed")) {
                        AlertPay(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.cabily_webview_lable_transaction_failed))
                    } else if (url.contains("pay-completed")) {
                        AlertPaySuccess(getResources().getString(R.string.action_success), getResources().getString(R.string.cabily_webview_lable_transaction_success))
                    } else if (url.contains("pay-cancel")) {
                        AlertPay(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.cabily_webview_lable_transaction_cancel))
                    }
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }

            }
        }

        //Load url in webView
        webview!!.loadUrl(url)
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@FareBreakUpPaymentWebView)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------------Alert Pay Transaction Method-----------
    private fun AlertPay(title: String, alert: String) {

        val mDialog = PkDialog(this@FareBreakUpPaymentWebView)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
            mDialog.dismiss()
            finish()
            onBackPressed()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        })
        mDialog.show()

    }

    //--------------Alert Pay Transaction Success Method-----------
    private fun AlertPaySuccess(title: String, alert: String) {


        val mDialog = PkDialog(this@FareBreakUpPaymentWebView)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
            mDialog.dismiss()

            val intent = Intent(this@FareBreakUpPaymentWebView, MyRideRating::class.java)
            intent.putExtra("RideID", SrideId_intent)
            startActivity(intent)
            overridePendingTransition(R.anim.enter, R.anim.exit)

            finish()
            FareBreakUp.farebreakup_class.finish()
            FareBreakUpPaymentList.myride_paymentList_class.finish()
        })
        mDialog.show()
    }

    override fun onBackPressed() {
        if (webview!!.canGoBack()) {
            webview!!.goBack()
        } else {
            // Let the system handle the back button
            super.onBackPressed()
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            val mDialog = PkDialog(this@FareBreakUpPaymentWebView)
            mDialog.setDialogTitle(getResources().getString(R.string.cabily_webview_lable_cancel_transaction))
            mDialog.setDialogMessage(getResources().getString(R.string.cabily_webview_lable_cancel_transaction_proceed))
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener {
                mDialog.dismiss()

                // close keyboard
                val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

                onBackPressed()
                finish()
                overridePendingTransition(R.anim.enter, R.anim.exit)
            })
            mDialog.setNegativeButton(getResources().getString(R.string.action_cancel_alert), View.OnClickListener { mDialog.dismiss() })
            mDialog.show()

            return true
        }
        return false
    }

}


