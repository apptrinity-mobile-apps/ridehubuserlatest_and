package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class ProfileOtpPage : ActivityHockeyApp() {
    private var context: Context? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null

    private var back: RelativeLayout? = null
    private var Eotp: EditText? = null
    private var send: TextView? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog

    private var Suserid = ""
    private var Sphone = ""
    private var ScountryCode = ""
    private var Sotp_Status = ""
    private var Sotp = ""

    //----------------------Code for TextWatcher-------------------------
    private val EditorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            //clear error symbol after entering text
            if (Eotp!!.text.isNotEmpty()) {
                Eotp!!.error = null
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_page)
        context = applicationContext
        initialize()

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }

        send!!.setOnClickListener {
            if (Eotp!!.text.toString().isEmpty()) {
                errorEdit(Eotp!!, resources.getString(R.string.otp_label_alert_otp))
            } else if (Sotp != Eotp!!.text.toString()) {
                errorEdit(Eotp!!, resources.getString(R.string.otp_label_alert_invalid))
            } else {
                if (isInternetPresent!!) {
                    PostRequest(Iconstant.profile_edit_mobileNo_url)
                } else {
                    alert(
                        resources.getString(R.string.alert_label_title),
                        resources.getString(R.string.alert_nointernet)
                    )
                }
            }
        }


        Eotp!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(
                    Eotp!!.applicationWindowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
            false
        }
    }

    private fun initialize() {
        session = SessionManager(this@ProfileOtpPage)
        cd = ConnectionDetector(this@ProfileOtpPage)
        isInternetPresent = cd!!.isConnectingToInternet

        back = findViewById<RelativeLayout>(R.id.otp_header_back_layout)
        Eotp = findViewById<EditText>(R.id.otp_password_editText)
        send = findViewById<TextView>(R.id.otp_submit_button)

        Eotp!!.addTextChangedListener(EditorWatcher)

        val intent = intent
        Suserid = intent.getStringExtra("UserID")!!
        Sphone = intent.getStringExtra("Phone")!!
        ScountryCode = intent.getStringExtra("CountryCode")!!
        Sotp_Status = intent.getStringExtra("Otp_Status")!!
        Sotp = intent.getStringExtra("Otp")!!

        if (Sotp_Status.equals("development", ignoreCase = true)) {
            Eotp!!.setText(Sotp)
        } else {
            Eotp!!.setText("")
        }
    }

    //--------------Alert Method-----------
    private fun alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ProfileOtpPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //--------------------Code to set error for EditText-----------------------
    private fun errorEdit(editText: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@ProfileOtpPage, R.anim.shake)
        editText.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editText.error = ssbuilder
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }


    // -----------------code for OTP Verification Post Request----------------
    private fun PostRequest(Url: String) {

        dialog = Dialog(this@ProfileOtpPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_otp)

        println("--------------Otp url-------------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = Suserid
        jsonParams["country_code"] = ScountryCode
        jsonParams["phone_number"] = Sphone
        jsonParams["otp"] = Sotp

        mRequest = ServiceRequest(this@ProfileOtpPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Otp reponse-------------------$response")
                    var Sstatus = ""
                    var Smessage = ""
                    var Scountry_code = ""
                    var Sphone_number = ""
                    try {

                        val `object` = JSONObject(response)

                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("response")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            Scountry_code = `object`.getString("country_code")
                            Sphone_number = `object`.getString("phone_number")
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        ProfilePageActivity.updateMobileDialog(Scountry_code, Sphone_number)
                        session!!.setMobileNumberUpdate(Scountry_code, Sphone_number)

                        val mDialog = PkDialog(this@ProfileOtpPage)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.profile_lable_mobile_success))
                        mDialog.setPositiveButton(
                            resources.getString(R.string.action_ok),
                            View.OnClickListener {
                                mDialog.dismiss()
                                finish()
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            })
                        mDialog.show()

                    } else {

                        val mDialog = PkDialog(this@ProfileOtpPage)
                        mDialog.setDialogTitle(resources.getString(R.string.action_error))
                        mDialog.setDialogMessage(Smessage)
                        mDialog.setPositiveButton(
                            resources.getString(R.string.action_ok),
                            View.OnClickListener { mDialog.dismiss() })
                        mDialog.show()
                    }

                    // close keyboard
                    val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mgr.hideSoftInputFromWindow(Eotp!!.windowToken, 0)

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

}