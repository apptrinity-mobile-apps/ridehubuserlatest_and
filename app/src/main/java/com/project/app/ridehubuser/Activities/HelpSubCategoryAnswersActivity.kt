package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.Adapters.HelpSubCategoryAnswersAdapter
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.PojoResponse.HelpSubCategoryAnswersPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class HelpSubCategoryAnswersActivity : ActivityHockeyApp() {

    lateinit var iv_help_close: RelativeLayout
    lateinit var tv_sub_category: TextView
    lateinit var rv_helpsubcategoryanswers: RecyclerView
    lateinit var dialog: Dialog
    private var mRequest: ServiceRequest? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    var itemlist_all: ArrayList<HelpSubCategoryAnswersPojo> = ArrayList()

    var id : String =""
    var category_id : String =""
    var subcategoryname : String =""
    var subcategory_id : String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_subcategory_answers)



        //id =  intent.getStringExtra("id")!!.toString()
        category_id =  intent.getStringExtra("category_id")!!.toString()
        subcategory_id =  intent.getStringExtra("subcategory_id")!!.toString()
        subcategoryname =  intent.getStringExtra("subcategoryname")!!.toString()


        Log.e("SUBCATEGORYINTENT",""+category_id+"----"+subcategory_id+"------"+subcategoryname)

        initialise()

        tv_sub_category!!.setText(subcategoryname)



        iv_help_close.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }



        //TODO get last trip api

        // make visible when last trip is available
    }

    private fun initialise() {
        session = SessionManager(this@HelpSubCategoryAnswersActivity)
        cd = ConnectionDetector(this@HelpSubCategoryAnswersActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_help_close = findViewById(R.id.iv_help_close)
        tv_sub_category = findViewById(R.id.tv_sub_category)

        rv_helpsubcategoryanswers = findViewById(R.id.rv_helpsubcategoryanswers)

        postRequest_HelpSubCategoryAnswers(Iconstant.help_sub_category_question_url)

    }



    private fun postRequest_HelpSubCategoryAnswers(Url: String) {
        dialog = Dialog(this)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------HELPSUBCATEGORYANSWERS Url----------------$Url----$category_id")

        val jsonParams = HashMap<String, String>()
        jsonParams["category_id"] = category_id
        jsonParams["sub_category_id"] = subcategory_id

        mRequest = ServiceRequest(this)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    dialog.dismiss()
                    println("-------------HELPSUBCATEGORYANSWERS Response----------------$response")

                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                           // val response_object = `object`.getJSONObject("response")
                            if (`object`.length() > 0) {
                                val category_object = `object`.get("data")
                                if (category_object is JSONArray) {
                                    val category_data_array = `object`.getJSONArray("data")
                                    if (category_data_array.length() > 0) {
                                        for (i in 0 until category_data_array.length()) {
                                            val catdata_object = category_data_array.getJSONObject(i)
                                            Log.d("ride_object", "" + catdata_object)
                                            val pojo = HelpSubCategoryAnswersPojo()
                                            pojo.setCategory(catdata_object.getString("category"))
                                            pojo.setStatus(catdata_object.getString("status"))
                                            pojo.setSubCategoryId(catdata_object.getString("sub_category_id"))
                                            //pojo.setSubCategory(catdata_object.getString("sub_category"))
                                            pojo.setAnswer(catdata_object.getString("answer"))
                                            pojo.setQuestion(catdata_object.getString("question"))
                                            itemlist_all.add(pojo)
                                        }
                                    }
                                }

                            }
                        }
                        if (Sstatus.equals("1", ignoreCase = true)) {
                                val layoutManager = LinearLayoutManager(
                                    this@HelpSubCategoryAnswersActivity,
                                    RecyclerView.VERTICAL,
                                    false
                                )
                            rv_helpsubcategoryanswers.layoutManager = layoutManager
                                val adapter = HelpSubCategoryAnswersAdapter(this@HelpSubCategoryAnswersActivity, itemlist_all)
                            rv_helpsubcategoryanswers.adapter = adapter


                        } else {
                            val Sresponse = `object`.getString("response")
                            //alert(resources.getString(R.string.alert_label_title), Sresponse)
                        }

                        dialog.dismiss()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        dialog.dismiss()
                    }

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }




    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.slidedown, R.anim.slideup)
    }

}
