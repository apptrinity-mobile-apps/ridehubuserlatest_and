package com.project.app.ridehubuser.Utils

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FirebaseMessageService : FirebaseMessagingService() {
    private val TAG = "FirebaseMessageService"


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)


        // Check whether the remoteMessage contains a notification payload.
        if (remoteMessage!!.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification()!!.getBody()!!)
           // sendNotification(remoteMessage.getNotification()!!.getBody(),"","")
        }


        //Check whether the remoteMessage contains a data payload.
        if (remoteMessage.getData().size > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString())

            if(remoteMessage.getData().get("type").equals("authentication")){

            }

          //  sendNotification( remoteMessage.getData().get("message"),remoteMessage.getData().get("type"),remoteMessage.getData().get("id"))
            try {
                val map = remoteMessage.getData()
                handleDataMessage(map)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }


        }
    }


    private fun handleDataMessage(map: Map<String, String>) {
        Log.e(TAG, "push json: " + map.toString())

        try {

            val type = map["type"]
            val id = map["id"]
            val message = map["message"]
            val title = map["title"]

            Log.e(TAG, "type: " + type)
            Log.e(TAG, "message: " + message)
            Log.e(TAG, "id: " + id)

        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }


    }



    /*fun sendNotification(messageBody: String?,type: String?, id: String?) {

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1
        val channelId = "channel-01"
        val channelName = "Channel Name"
        val importance = NotificationManager.IMPORTANCE_HIGH

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                    channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val mBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("BILL")
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setAutoCancel(true)

        if(type.equals("authentication")){
            val resultIntent = Intent(this, AuthenticatorActivity::class.java)
            resultIntent.putExtra("id",id)
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())

        }else{

            val resultIntent = Intent(this, ReminderActivity::class.java)
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addNextIntent(resultIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            mBuilder.setContentIntent(resultPendingIntent)

            notificationManager.notify(notificationId, mBuilder.build())
        }

    }*/



}