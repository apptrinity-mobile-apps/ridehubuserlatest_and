package com.project.app.ridehubuser.Adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.app.ridehubuser.Activities.HelpSubCategoryAnswersActivity
import com.project.app.ridehubuser.PojoResponse.HelpSubCategoryPojo
import com.project.app.ridehubuser.R
import java.util.*


@SuppressLint("SetTextI18n")
class HelpSubCategoryAdapter(private val context: Context, private val data: ArrayList<HelpSubCategoryPojo>) :
    RecyclerView.Adapter<HelpSubCategoryAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_help_category, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (data[position].getCategory().equals("null")) {
            holder.tv_help_trip_issues.text = ""
        } else {
            holder.tv_help_trip_issues.text = data[position].getSubCategory()
        }


        holder.ll_category.setOnClickListener {

            Log.e("SUBCATEGORYINTENT","------"+data[position].getCategory()+"----"+data[position].getSubcategoryId()+"------"+data[position].getSubCategory())
            val intent = Intent(context, HelpSubCategoryAnswersActivity::class.java)
            intent.putExtra("category_id", data[position].getCategory())
            intent.putExtra("subcategory_id", data[position].getSubcategoryId())
            intent.putExtra("subcategoryname", data[position].getSubCategory())
            context.startActivity(intent)
            (context as Activity).overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_help_trip_issues: TextView = view.findViewById(R.id.tv_help_trip_issues)
        var ll_category: LinearLayout = view.findViewById(R.id.ll_category)

    }
}
