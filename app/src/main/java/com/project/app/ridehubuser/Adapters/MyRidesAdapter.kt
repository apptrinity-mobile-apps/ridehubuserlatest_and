package com.project.app.ridehubuser.Adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.project.app.mylibrary.customRatingBar.RotationRatingBar
import com.project.app.ridehubuser.Activities.RideHistoryDetailsActivity
import com.project.app.ridehubuser.PojoResponse.MyRidesPojo
import com.project.app.ridehubuser.R
import com.squareup.picasso.Picasso
import java.util.*


@SuppressLint("SetTextI18n")
class MyRidesAdapter(private val context: Context, private val data: ArrayList<MyRidesPojo>) :
    RecyclerView.Adapter<MyRidesAdapter.ViewHolder>() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycle_item_ride_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (data[position].getTotal_fare().equals("null")) {
            holder.tv_ride_amount.text = "$-"
        } else {
            holder.tv_ride_amount.text = "$" + data[position].getTotal_fare()
        }
        if (data[position].getVehicle_model().equals("null") &&
            data[position].getVehicle_no().equals("null")
        ) {
            holder.tv_ride_vehicle_no.text = "- -"
        } else if (data[position].getVehicle_model().equals("null")) {
        } else if (data[position].getVehicle_no().equals("null")) {
        } else {
            holder.tv_ride_vehicle_no.text =
                data[position].getVehicle_model() + " " + data[position].getVehicle_no()
        }
        holder.ride_rating.rating = 4F


        if(data[position].getUser_rating() !="null"){
            val user_rating = data[position].getUser_rating()
            if (user_rating!!.length > 0) {
                holder.ride_rating.rating = java.lang.Float.parseFloat(
                    user_rating!!
                )
            }
        }else{
            println("-------------Driverratingnull----------------" + "Driverratingnull")
           // holder.ride_rating.visibility = View.GONE
            holder.ride_rating.rating= java.lang.Float.parseFloat(
                0.toString()
            )
        }


        if (data[position].getCab_type().equals("null")) {
            holder.tv_ride_car_type.text = "- -"
        } else {
            holder.tv_ride_car_type.text = data[position].getCab_type()
        }

        holder.tv_ride_time.text =
            data[position].getRide_date() + ", " + data[position].getRide_time()

        if (data[position].getRide_status().equals("Booked")) {

            //  holder.btn_ridestatus!!.text = "Upcoming"

        } else if (data[position].getRide_status().equals("Completed") || data[position].getRide_status().equals(
                "completed"
            )
        ) {

            // holder.btn_ridestatus!!.text = "Completed"
            holder.tv_ride_amount.setTextColor(ContextCompat.getColor(context, R.color.ride_total))

            Log.e("MAPIMAGE",data[position]!!.getImage())

            //Picasso.with(context).load("https://ridehub.co/"+data[position]!!.getImage()).placeholder(R.drawable.map_demo).into(holder.iv_ride_history_map)
            if(data[position]!!.getImage()!= null){
                Picasso.with(context).load("https://ridehub.co/"+data[position]!!.getImage()).placeholder(R.drawable.map_demo).into(holder.iv_ride_history_map)
            }


        } else if (data[position].getRide_status().equals("Cancelled") || data[position].getRide_status().equals(
                "canceled"
            )
        ) {

            // holder.btn_ridestatus!!.text = "Canceled"
            holder.tv_ride_amount.setTextColor(ContextCompat.getColor(context, R.color.red))

        } else if (data[position].getRide_status().equals("Onride")) {

            holder.tv_price_label!!.visibility = View.GONE
            holder.tv_ride_amount.setTextColor(ContextCompat.getColor(context, R.color.green_alert_dialog_text))
             holder.tv_ride_amount!!.text = "Onride"


        } else if (data[position].getRide_status().equals("Finished")) {

            // holder.btn_ridestatus!!.text = "Finished"
            holder.tv_price_label!!.visibility = View.VISIBLE
            holder.iv_ride_history_map!!.visibility = View.VISIBLE


            Log.e("MAPIMAGE",data[position]!!.getImage())

            Picasso.with(context).load("https://ridehub.co/"+data[position]!!.getImage()).placeholder(R.drawable.map_demo).into(holder.iv_ride_history_map)
            if(data[position]!!.getImage()!= null){
                Picasso.with(context).load("https://ridehub.co/"+data[position]!!.getImage()).placeholder(R.drawable.map_demo).into(holder.iv_ride_history_map)
            }




        } else if (data[position].getRide_status().equals("Confirmed")) {

            holder.tv_price_label!!.visibility = View.GONE
            holder.tv_ride_amount.setTextColor(ContextCompat.getColor(context, R.color.green_alert_dialog_text))
             holder.tv_ride_amount!!.text = "Confirmed"


        } else if (data[position].getRide_status().equals("Arrived")) {

            holder.tv_price_label!!.visibility = View.GONE
            holder.tv_ride_amount.setTextColor(ContextCompat.getColor(context, R.color.green_alert_dialog_text))
              holder.tv_ride_amount!!.text = "Arrived"


        }

        holder.cv_ride_card.setOnClickListener {
            val intent = Intent(context, RideHistoryDetailsActivity::class.java)
            intent.putExtra("RideID", data[position].getRide_id())
            context.startActivity(intent)
            (context as Activity).overridePendingTransition(R.anim.slideup, R.anim.slidedown)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_ride_amount: TextView = view.findViewById(R.id.tv_ride_amount)
        var ride_rating: RotationRatingBar = view.findViewById(R.id.ride_rating)
        var tv_ride_vehicle_no: TextView = view.findViewById(R.id.tv_ride_vehicle_no)
        var tv_ride_car_type: TextView = view.findViewById(R.id.tv_ride_car_type)
        var tv_ride_time: TextView = view.findViewById(R.id.tv_ride_time)
        var cv_ride_card: CardView = view.findViewById(R.id.cv_ride_card)
        var tv_price_label: TextView = view.findViewById(R.id.tv_price_label)
        var iv_ride_history_map: ImageView = view.findViewById(R.id.iv_ride_history_map)

    }
}
