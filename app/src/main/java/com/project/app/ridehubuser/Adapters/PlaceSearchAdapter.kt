package com.project.app.ridehubuser.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ImageLoader
import java.util.*

class PlaceSearchAdapter(private val context: Context, private val data: ArrayList<String>) : BaseAdapter() {
    private val imageLoader: ImageLoader
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
        imageLoader = ImageLoader(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var name: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.place_search_single, parent, false)
            holder = ViewHolder()
            holder?.name = view.findViewById<View>(R.id.place_search_single_textview) as TextView

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.name!!.text = data[position]


        return view
    }

    private var iv_line: ImageView? = null
    fun setProgress(iv_line: ImageView) {
        this.iv_line = iv_line
    }
}