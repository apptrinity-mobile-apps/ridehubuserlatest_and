package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle

import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import com.stripe.android.view.CardInputWidget

import retrofit2.Call
import retrofit2.Callback
import java.util.HashMap


class StripePayment : AppCompatActivity() {
    val PUBLISHABLE_KEY = "pk_test_0I3IGKx3mcL97rMJ0Dbt5pm9"
    private var dialog: Dialog? = null
    var amount_stg = ""
    var from_stg = ""
    var userID = ""
    var after_amount = ""
    var id = ""
    private var mRequest: ServiceRequest? = null
    lateinit var sessionManager: SessionManager
    lateinit var card_input_widget: CardInputWidget
    lateinit var et_card_name_id: EditText
    lateinit var tv_pay_amount_id: TextView
    private var card: Card? = null
    private var progress: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.stripe_card_details_layout)

        sessionManager = SessionManager(this)
        val user = sessionManager!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        et_card_name_id = findViewById(R.id.et_card_name_id)
        tv_pay_amount_id = findViewById(R.id.tv_pay_amount_id)
        card_input_widget = findViewById(R.id.card_input_widget)

        amount_stg = intent.getStringExtra("amount")

        tv_pay_amount_id.setText("ADD " + "$ " + amount_stg)

        /* Parse.initialize(Parse.Configuration.Builder(this)
                 .applicationId(APPLICATION_ID)
                 .clientKey(CLIENT_KEY)
                 .server(ARMINFO).build())
         Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE)*/

        // Create a demo test credit Card
        // You can pass the payment form data to create a Real Credit card
        // But you need to implement youself.


        progress = ProgressDialog(this)
        val purchase = findViewById(R.id.ll_pay_amount_id) as LinearLayout
        purchase.setOnClickListener {

            val cardToSave = card_input_widget.getCard()
            if (cardToSave == null) {
                Toast.makeText(this@StripePayment, "Invalid Card Data", Toast.LENGTH_SHORT).show()
            } else {
                card = card_input_widget.card
                buy()
            }

        }

    }

    private fun buy() {
        val validation = card!!.validateCard()
        if (validation) {
            startProgress("Validating Credit Card")
            Stripe(this).createToken(
                card!!,
                PUBLISHABLE_KEY,
                object : TokenCallback {
                    override fun onError(error: Exception) {
                        Log.d("Stripe", error.toString())
                    }

                    override fun onSuccess(token: Token) {
                        finishProgress()
                        // charge(token)
                        Log.e(
                            "cardToken_lll",
                            token.getId() + "--" + token.card.customerId + "---" + token.card.last4 + "---" + token.card.expMonth + "---" + token.card.expYear
                        )

                        callPaymentAPI(Iconstant.cabily_add_money_url, token.getId()!!)

                    }
                })
        } else if (!card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }


    private fun startProgress(title: String) {
        progress!!.setTitle(title)
        progress!!.setMessage("Please Wait")
        progress!!.show()
    }

    private fun finishProgress() {
        progress!!.dismiss()
    }

    private fun callPaymentAPI(url: String, token: String) {
        dialog = Dialog(this@StripePayment)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = userID
        jsonParams["total_amount"] = amount_stg
        jsonParams["stripeToken"] = token
        mRequest = ServiceRequest(this@StripePayment)
        mRequest!!.makeServiceRequest(
            url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------PayWallet_Response----------------$response")
                    Alert("Payment Status","Transaction successful")
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@StripePayment)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), View.OnClickListener { mDialog.dismiss()

            val intent = Intent(this@StripePayment, NavigationDrawer::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        })
        mDialog.show()
    }
}
