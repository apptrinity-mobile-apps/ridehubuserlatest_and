package com.project.app.ridehubuser.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.project.app.ridehubuser.R;

import org.jitsi.meet.sdk.JitsiMeet;
import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.jitsi.meet.sdk.JitsiMeetUserInfo;

import java.net.MalformedURLException;
import java.net.URL;

public class VideocallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videocall);

        // Initialize default options for Jitsi Meet conferences.
        URL serverURL;
        try {
            serverURL = new URL("https://meet.jit.si");
            //serverURL = new URL("https://inforlinx.co");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Invalid server URL!");
        }
        //JitsiMeetUserInfo userInfo = new JitsiMeetUserInfo();
       // userInfo.setDisplayName( "Chaitanya" );

        JitsiMeetConferenceOptions defaultOptions
            = new JitsiMeetConferenceOptions.Builder()
                .setServerURL(serverURL)
                .setWelcomePageEnabled(false)
                .setFeatureFlag("live-streaming.enabled",false)
                //.setUserInfo( userInfo )
                .build();
        JitsiMeet.setDefaultConferenceOptions(defaultOptions);

    }

    public void onButtonClick(View v) {
        EditText editTextTextPersonName = findViewById(R.id.editTextTextPersonName);
        EditText editText = findViewById(R.id.conferenceName);
        String text = editText.getText().toString();
        String dname = editTextTextPersonName.getText().toString();

        if (text.length() > 0 && dname.length()>0) {
            JitsiMeetUserInfo userInfo = new JitsiMeetUserInfo();
            userInfo.setDisplayName( dname );
            // Build options object for joining the conference. The SDK will merge the default
            // one we set earlier and this one when joining.
            JitsiMeetConferenceOptions options
                = new JitsiMeetConferenceOptions.Builder()
                    .setRoom(text)
                    .setUserInfo( userInfo )
                    .setFeatureFlag("live-streaming.enabled",false)
                    .build();
            // Launch the new activity with the given options. The launch() method takes care
            // of creating the required Intent and passing the options.
            JitsiMeetActivity.launch(this, options);

        }else{
            Toast.makeText(getApplicationContext(),"Enter Name and Conference Name",Toast.LENGTH_SHORT).show();
        }
    }
}



