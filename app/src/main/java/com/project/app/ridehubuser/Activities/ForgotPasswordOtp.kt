package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import com.android.volley.toolbox.StringRequest
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector

class ForgotPasswordOtp : ActivityHockeyApp() {
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var Rl_back: ImageView? = null
    private var Et_otp: EditText? = null
    private var Bt_send: LinearLayout? = null

    internal var postrequest: StringRequest? = null
    internal var dialog: Dialog? = null

    private var Semail = ""
    private var Sotp_Status = ""
    private var Sotp = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgotpassword_otp)
        initialize()

        Rl_back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(Rl_back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }

        Et_otp!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(
                    Et_otp!!.applicationWindowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
            false
        }

        Bt_send!!.setOnClickListener {
            if (Et_otp!!.text.toString().length == 0) {
                Alert(
                    resources.getString(R.string.alert_label_title),
                    resources.getString(R.string.otp_label_alert_otp)
                )
            } else if (Sotp != Et_otp!!.text.toString()) {
                Alert(
                    resources.getString(R.string.alert_label_title),
                    resources.getString(R.string.otp_label_alert_invalid)
                )
            } else {
                val i = Intent(this@ForgotPasswordOtp, ResetPassword::class.java)
                i.putExtra("Intent_email", Semail)
                startActivity(i)
                finish()
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }


    }

    private fun initialize() {
        cd = ConnectionDetector(this@ForgotPasswordOtp)
        isInternetPresent = cd!!.isConnectingToInternet

        Rl_back = findViewById(R.id.forgot_password_otp_header_back_layout) as ImageView
        Et_otp = findViewById(R.id.forgot_password_otp_password_editText) as EditText
        Bt_send = findViewById(R.id.forgot_password_otp_submit_button) as LinearLayout

        val intent = intent
        Semail = intent.getStringExtra("Intent_email")
        Sotp_Status = intent.getStringExtra("Intent_Otp_Status")
        Sotp = intent.getStringExtra("Intent_verificationCode")

        if (Sotp_Status.equals("development", ignoreCase = true)) {
            Et_otp!!.setText(Sotp)
        } else {
            Et_otp!!.setText("")
        }

        Et_otp!!.setSelection(Et_otp!!.text.length)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ForgotPasswordOtp)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(Rl_back!!.windowToken, 0)

            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }

}
