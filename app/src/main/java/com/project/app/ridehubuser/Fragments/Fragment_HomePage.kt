package com.project.app.ridehubuser.Fragments

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.*
import android.app.Activity.RESULT_OK
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.speech.tts.TextToSpeech
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.card.MaterialCardView
import com.ncorti.slidetoact.SlideToActView
import com.project.app.mylibrary.customRatingBar.RotationRatingBar
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.googlemapdrawpolyline.GMapV2GetRouteDirection
import com.project.app.mylibrary.gps.GPSTracker
import com.project.app.mylibrary.latlnginterpolation.LatLngInterpolator
import com.project.app.mylibrary.latlnginterpolation.MarkerAnimation
import com.project.app.mylibrary.materialprogresswheel.ProgressWheel
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.Activities.*
import com.project.app.ridehubuser.Adapters.BookMyRide_Adapter_2
import com.project.app.ridehubuser.Fragments.Fragment_HomePage.RecentRidesAdapter.MyViewHolder
import com.project.app.ridehubuser.HockeyApp.FragmentHockeyApp
import com.project.app.ridehubuser.PojoResponse.HomePojo
import com.project.app.ridehubuser.PojoResponse.RecentRidesPojo
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.RecyclerItemClickListener
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.squareup.picasso.Picasso
import com.wang.avi.AVLoadingIndicatorView
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class Fragment_HomePage : FragmentHockeyApp(), OnMapReadyCallback,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, TextToSpeech.OnInitListener {


    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnected(p0: Bundle?) {
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            val Dlatitude = gps.getLatitude()
            val Dlongitude = gps.getLongitude()
            Log.e("GPSSSS", gps.getLatitude().toString() + "--" + gps.getLongitude())
            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude

            Recent_lat = Dlatitude
            Recent_long = Dlongitude
            val currLatLng = LatLng(gps.getLatitude(), gps.getLongitude())
            cd = ConnectionDetector(activity!!)
            isInternetPresent = cd!!.isConnectingToInternet

            Log.e("CURRENTLOCATIONLATLNG", currLatLng.toString())

            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                return
            }
            mMap!!.uiSettings.setAllGesturesEnabled(true)
            mMap!!.uiSettings.isMapToolbarEnabled = false
            mMap!!.isIndoorEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = true
            mMap!!.isIndoorEnabled = true
            mMap!!.isMyLocationEnabled = false
            mMap!!.uiSettings.isMyLocationButtonEnabled = false
            mMap!!.isBuildingsEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = false
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(LatLng(Recent_lat, Recent_long)))
            mMap!!.moveCamera(
                CameraUpdateFactory.newCameraPosition(
                    CameraPosition.Builder()
                        .target(mMap!!.cameraPosition.target)
                        .zoom(15f)
                        .bearing(30f)
                        .tilt(45f)
                        .build()
                )
            )

            address = getCompleteAddressString(MyCurrent_lat, MyCurrent_long)
            // Move the camera to last position with a zoom level
            val cameraPosition =
                CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(15f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            val pickup_marker_view =
                (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                    R.layout.custom_pickup_marker,
                    null
                )
            val tv_marker_pickup =
                pickup_marker_view.findViewById<TextView>(R.id.tv_marker_pickup)
            tv_marker_pickup.text = address!!
            val pickup_bitmap = createBitmapFromLayout(pickup_marker_view)
            mMap!!.addMarker(
                MarkerOptions()
                    .position(currLatLng)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_dot))
                    .icon(BitmapDescriptorFactory.fromBitmap(pickup_bitmap))
            )

        } else {
            enableGpsService()
        }
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            buildGoogleApiClient()
            val styleManager = MapStyleManager.attachToMap(activity!!, mMap!!)
            styleManager.addStyle(0F, R.raw.map_silver_json)
            styleManager.addStyle(10F, R.raw.map_silver_json)
            styleManager.addStyle(12F, R.raw.map_silver_json)

        }



        mMap!!.setOnCameraIdleListener {
            cameraIdle = true
        }


        mMap!!.setOnCameraMoveStartedListener {
            currentLocation_image!!.startAnimation(fade_out_anim)
        }

        mMap!!.setOnCameraMoveListener {
            if (cameraIdle == true) {
                //currentLocation_image!!.startAnimation(fade_out_anim)
                currentLocation_image!!.visibility = View.VISIBLE
            }


        }


        //tv_destination_search_address!!.text = address
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient!!.connect()
    }

    private var rootview: View? = null
    private val REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001
    private var mMap: GoogleMap? = null
    private var isInternetPresent: Boolean = false
    private var cd: ConnectionDetector? = null
    internal lateinit var gps: GPSTracker
    private var session: SessionManager? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var Recent_lat = 0.0
    private var Recent_long = 0.0
    internal lateinit var recent_source: String
    internal lateinit var recent_destination: String
    private var drawer_layout: ImageView? = null

    //-----Declaration For Enabling Gps-------
    internal lateinit var mLocationRequest: LocationRequest
    private var mGoogleApiClient: GoogleApiClient? = null
    internal lateinit var result: PendingResult<LocationSettingsResult>
    internal val REQUEST_LOCATION = 299

    private var currentLocation_image: ImageView? = null
    private var mRequest: ServiceRequest? = null

    private var UserID = ""
    private var UserName = ""

    lateinit var tv_no_cabs: TextView

    private var lv_rydetypes: RecyclerView? = null
    private var tv_cab_type_text: TextView? = null
    private var ll_source_address: LinearLayout? = null
    private var rv_recent_rides: RecyclerView? = null
    private var rl_ridetypes: RelativeLayout? = null

    private var tv_destination_search_address: TextView? = null
    private var rl_destination_search_address: RelativeLayout? = null

    private var progressWheel: ProgressWheel? = null
    internal lateinit var dialog: Dialog

    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null
    internal lateinit var logoutReciver: BroadcastReceiver
    private var ridehubservice: Boolean = false
    internal  var address: String = ""
    private val placeSearch_request_code = 200

    internal lateinit var recent_rides_array: java.util.ArrayList<RecentRidesPojo>

    private var SselectedLatitude = ""
    private var SselectedLongitude = ""
    private var DselectedLatitude = ""
    private var DselectedLongitude = ""
    private var search_status = 0
    private var setpickuploc: String? = null
    private var locatiosearch = "no"
    private var SdestinationLatitude = ""
    private var SsourceLatitude = ""
    private var SsourceLongitude = ""
    private var SdestinationLongitude = ""
    private var SdestinationLocation = ""
    private var Sselected_DestinationLocation: String? = null
    private var Selected_SourceLocation: String? = null
    internal var Sselected_latitude = ""
    internal var Sselected_longitude = ""
    internal var driver_list = ArrayList<HomePojo>()
    internal var category_list = ArrayList<HomePojo>()
    private var endLoc: LatLng? = null
    private var curLoc: LatLng? = null
    private var markerOptions: MarkerOptions? = null
    private var driver_status = false
    private var category_status = false
    private var main_response_status = false
    private var ride_adapter: BookMyRide_Adapter_2? = null

    private var CategoryID = ""
    private var SubCategoryID = ""
    private var list_item_selected = false
    private var CarAvailable = ""
    private var ScarType = ""
    private var selectedType = ""
    private var riderId = ""
    private var response_time = ""
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null

    lateinit var tv_welcome_name: TextView

    lateinit var ll_fav_and_ridetypes: LinearLayout
    lateinit var ll_main_notefordriver: LinearLayout
    lateinit var ll_note_for_driver: LinearLayout
    lateinit var tv_confirm_pickup: TextView
    lateinit var tv_btn_go: TextView
    lateinit var rl_back_button: RelativeLayout
    lateinit var ll_slidetocancel: LinearLayout

    lateinit var recent_rides_list: ArrayList<RecentRidesPojo>
    lateinit var recent_rides_list_final: ArrayList<RecentRidesPojo>

    var position_selected = -1
    internal lateinit var updateReciver: BroadcastReceiver
    internal var mHandler = Handler()
    private var retry: String? = ""
    private var toast: Toast? = null
    private var slidetocancel: SlideToActView? = null
    private var ll_driver_on_way_main: LinearLayout? = null
    private var rl_driver_on_way_header: RelativeLayout? = null
    private var ll_driver_on_theway: LinearLayout? = null
    private var close_driveronway: ImageView? = null
    private var ll_driveronway_fulldetail: LinearLayout? = null
    private var ll_edit_ride: LinearLayout? = null
    private var edit_cancel_ride: MaterialCardView? = null
    private var ll_include_editride: LinearLayout? = null
    private var ll_cancel_ride: LinearLayout? = null
    private var tv_submit_notefordriver: TextView? = null
    private var tv_notedriver_enterd: TextView? = null
    private var ll_notefordriver: LinearLayout? = null
    private var iv_close_notefordriver: ImageView? = null
    private var et_notefordriver: EditText? = null
    private var close_editride: ImageView? = null
    private var ll_safety: LinearLayout? = null
    private var iv_close_safetykit: ImageView? = null
    private var ll_include_safettytoolkit: LinearLayout? = null
    private var ll_call: LinearLayout? = null
    private var ll_message: LinearLayout? = null
    private var tv_include_driver_found: TextView? = null
    private var tv_include_pick_up_addr: TextView? = null
    private var tv_include_drop_addr: TextView? = null
    private var notefordriver_stg = ""
    var cameraIdle: Boolean? = false


    // Start Trackride parameters

    private var estimated_time = ""
    private var pickup_date_time = ""
    private var driverID = ""
    private var driverName = ""
    private var driverImage = ""
    private var driverRating = ""
    private var driverLat = ""
    private var driverLong = ""
    private var driverTime = ""
    private var rideID = ""
    private var driverMobile = ""
    private var driverCar_no = ""
    private var driverCar_model = ""
    private var source_Location = ""
    private var userLatitude = ""
    private var userLongitude = ""
    private var taxi_type = ""
    private var cab_type = ""
    private var destination_Location = ""
    internal var mLatLngInterpolator: LatLngInterpolator? = null
    private var firsttimepolyline: Boolean? = true
    var isFirstPosition = true
    var startPositionn: LatLng? = null
    var endPosition: LatLng? = null
    private var polyline: Polyline? = null
    private var currentLocation: Location? = null
    private var currentLocationn: LatLng? = null
    private var lat: Double = 0.toDouble()
    private var lng: Double = 0.toDouble()
    private var fromPosition: LatLng? = null
    private var toPosition: LatLng? = null
    private var dropLatLng: LatLng? = null
    private var pickUpLatLng: LatLng? = null
    private var ride_confirmed: Boolean? = false
    private var refreshReceiver: RefreshReceiver? = null

    //Driver details
    private var iv_driver_image: ImageView? = null
    private var tv_driver_name: TextView? = null
    private var tv_cab_type: TextView? = null
    private var tv_pick_up_time: TextView? = null
    private var tv_drop_time: TextView? = null
    private var tv_car_number: TextView? = null
    private var tv_driver_estimated_time: TextView? = null
    private var tv_car_model: TextView? = null
    private var tv_fare_estimate: TextView? = null
    private var tv_searching_cabs: TextView? = null
    private var driver_rating: RotationRatingBar? = null

    //Safety Kit
    private var cardview_safety_center: MaterialCardView? = null
    private var cardview_sharemytrip: MaterialCardView? = null
    private var cardview_911_assistance: MaterialCardView? = null

    //Call Permissions
    internal val PERMISSION_REQUEST_CODE = 111
    internal val PERMISSION_REQUEST_CODES = 222
    private var sSelectedPanicNumber = ""
    private val timer_request_code = 100

    lateinit var fade_in_anim: Animation
    lateinit var fade_out_anim: Animation

    var voice_stg = ""

    lateinit var address_new: List<Address>
    lateinit var tts: TextToSpeech

    //Bottomsheet
    lateinit var bottomSheetReview: BottomSheetBehavior<LinearLayout>
    var currentState = -1
    private var ll_driveronway_top_detail: LinearLayout? = null

    // code for generating app level notifications without fireBase
    var BUNDLE_NOTIFICATION_ID = 100
    val BUNDLE_CHANNEL_ID = "RIDEHUB_USER_APP"
    val BUNDLE_CHANNEL_NAME = "RIDEHUB_USER"


    internal var count = 0
    internal var seconds = 0

    var source_edit = false

    internal var mRunnable: Runnable = object : Runnable {
        override fun run() {

            if (count < seconds) {
                count++
                mHandler.postDelayed(this, 1000)
            } else {
                mHandler.removeCallbacks(this)
                if (mRequest != null) {
                    mRequest!!.cancelRequest()
                }
                val mDialog = PkDialog(activity!!)
                mDialog.setDialogTitle(getResources().getString(R.string.action_error))
                mDialog.setDialogMessage("No Drivers Available")
                mDialog.setPositiveButton(
                    getResources().getString(R.string.action_ok),
                    View.OnClickListener {
                        mDialog.dismiss()
                        riderId = ""
                        mMap!!.clear()
                        onMapReady(mMap)
                        drawer_layout!!.setVisibility(View.VISIBLE)
                        drawer_layout!!.setClickable(true)
                        currentLocation_image!!.setClickable(true)
                        currentLocation_image!!.setVisibility(View.VISIBLE)
                        drawer_layout!!.setEnabled(true)
                        NavigationDrawer.enableSwipeDrawer()
                        ll_source_address!!.visibility = View.VISIBLE
                        ll_fav_and_ridetypes.visibility = View.VISIBLE
                        ll_driver_on_way_main!!.visibility = View.GONE
                        ll_source_address!!.visibility = View.VISIBLE
                        tv_destination_search_address!!.text = ""
                        slidetocancel!!.resetSlider()
                        et_notefordriver!!.text.clear()
                        et_notefordriver!!.setText("")
                        tv_notedriver_enterd!!.text = ""
                        notefordriver_stg = ""
                        tv_welcome_name!!.text = timeOfDay() + ", " + UserName
                        ll_slidetocancel.visibility = View.GONE

                    })
                mDialog.show()

            }

        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootview = inflater.inflate(R.layout.homepage, container, false)
        initialize(rootview)
        initializeMap()
        // dialog = Dialog(activity!!)
        //  dialog = Dialog(activity!!)


//Start XMPP Chat Service
        ChatService.startUserAction(activity!!)
        /*try{
            ChatService.startUserAction(activity!!)
        }catch (e:Exception){
            e.printStackTrace()
        }*/

        // Finishing the activity using broadcast
        val filter = IntentFilter()
        filter.addAction("com.app.pushnotification.RideAccept")
        filter.addAction("com.pushnotification.updateBottom_view")


        // -----code to refresh drawer using broadcast receiver-----
        updateReciver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                try {

                    refreshReceiver = RefreshReceiver()
                    val intentFilter = IntentFilter()
                    intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver")
                    intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip")
                    intentFilter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver")
                    activity!!.registerReceiver(refreshReceiver, intentFilter)

                } catch (e: java.lang.Exception) {
                    Log.e("FRAGNMENTEXCEPTION", e.toString())
                }

                println("----------FRAGMENTHOMERIDEACCEPT--------------" + intent.getStringExtra("message"))

                if (intent.getStringExtra("Action").equals("ride_confirmed", ignoreCase = true)) {

                    ride_confirmed = true



                    driverID = intent.getStringExtra("driverID")!!
                    driverName = intent.getStringExtra("driverName")!!
                    driverImage = intent.getStringExtra("driverImage")!!
                    driverRating = intent.getStringExtra("driverRating")!!
                    driverLat = intent.getStringExtra("driverLat")!!
                    driverLong = intent.getStringExtra("driverLong")!!
                    driverTime = intent.getStringExtra("driverTime")!!
                    rideID = intent.getStringExtra("rideID")!!
                    driverMobile = intent.getStringExtra("driverMobile")!!
                    driverCar_no = intent.getStringExtra("driverCar_no")!!
                    driverCar_model = intent.getStringExtra("driverCar_model")!!
                    source_Location = intent.getStringExtra("source_Location")!!
                    userLatitude = intent.getStringExtra("userLatitude")!!
                    userLongitude = intent.getStringExtra("userLongitude")!!
                    cab_type = intent.getStringExtra("cab_type")!!
                    destination_Location = intent.getStringExtra("destination_Location")!!
                    taxi_type = intent.getStringExtra("taxi_type")!!

                    Log.e(
                        "TRACKRIDE",
                        "" + driverID + "---" + driverName + "---" + driverImage + "---" + driverRating + "---" + driverLat + "---" + driverLong + "---" + driverTime + "---" + rideID + "---" + driverMobile + "---" + driverCar_no + "---" + driverCar_model + "---" + source_Location + "---" + userLatitude + "---" + userLongitude + "---" + cab_type + "---" + destination_Location + "-------" + taxi_type
                    )

                    //Driver data Details
                    Picasso.with(activity!!)
                        .load(driverImage)
                        .into(iv_driver_image)
                    tv_driver_name!!.setText(driverName)
                    tv_cab_type!!.setText(taxi_type)
                    tv_pick_up_time!!.setText(driverTime)
                    tv_car_number!!.setText(driverCar_no)
                    tv_car_model!!.setText(driverCar_model)
                    if (driverRating.length > 0) {
                        driver_rating!!.rating = java.lang.Float.parseFloat(driverRating)
                    }
                    tv_driver_estimated_time!!.setText("driver will pickup you in " + driverTime)

                    tv_include_pick_up_addr!!.text = source_Location
                    tv_include_drop_addr!!.text = destination_Location


                    val drivenot =
                        driverName + " is arriving soon in a " + driverCar_model + " " + driverCar_no

                    showNotification(drivenot, BUNDLE_NOTIFICATION_ID.toString())


                    ll_driver_on_way_main!!.visibility = View.VISIBLE
                    ll_driver_on_way_main!!.background = null
                    ll_slidetocancel.visibility = View.GONE
                    rl_driver_on_way_header!!.visibility = View.GONE
                    mHandler.removeCallbacks(mRunnable)


                    mMap!!.clear()
                    if (driverLat != null && driverLong != null) {
                        fromPosition = LatLng(
                            java.lang.Double.parseDouble(userLatitude),
                            java.lang.Double.parseDouble(userLongitude)
                        )
                        toPosition = LatLng(
                            java.lang.Double.parseDouble(driverLat),
                            java.lang.Double.parseDouble(driverLong)
                        )
                        if (fromPosition != null && toPosition != null) {
                            val draw_route_asyncTask = GetRouteTask()
                            draw_route_asyncTask.execute()
                        }
                    }


                }

            }
        }
        activity!!.registerReceiver(updateReciver, filter)

        filter.addAction("com.app.logout")
        logoutReciver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.app.logout") {
                    activity!!.finish()
                } else if (intent.action == "com.pushnotification.updateBottom_view") {
                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet
                    // startTimer();

                    mMap!!.clear()
                    onMapReady(mMap)
                    drawer_layout!!.setVisibility(View.VISIBLE)
                    drawer_layout!!.setClickable(true)
                    currentLocation_image!!.setClickable(true)
                    currentLocation_image!!.setVisibility(View.VISIBLE)
                    drawer_layout!!.setEnabled(true)
                    NavigationDrawer.enableSwipeDrawer()
                    ll_source_address!!.visibility = View.VISIBLE
                    ll_fav_and_ridetypes.visibility = View.VISIBLE
                    ll_driver_on_way_main!!.visibility = View.GONE
                    ll_source_address!!.visibility = View.VISIBLE
                    tv_destination_search_address!!.text = ""
                    slidetocancel!!.resetSlider()
                    et_notefordriver!!.text.clear()
                    et_notefordriver!!.setText("")
                    tv_notedriver_enterd!!.text = ""
                    notefordriver_stg = ""
                    tv_welcome_name!!.text = timeOfDay() + ", " + UserName

                    val Dlatitude = gps.getLatitude()
                    val Dlongitude = gps.getLongitude()
                    // Move the camera to last position with a zoom level
                    val cameraPosition =
                        CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(17f)
                            .build()
                    mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))


                    // PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                }

            }
        }
        activity!!.registerReceiver(logoutReciver, filter)



        return rootview

        //  return super.onCreateView(inflater, container, savedInstanceState)
    }


    private fun initialize(rootview: View?) {

        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        session = SessionManager(activity!!)
        gps = GPSTracker(activity!!)

        currentLocation_image =
            rootview!!.findViewById(R.id.book_current_location_imageview) as ImageView
        //progressWheel = rootview.findViewById(R.id.book_my_ride_progress_wheel) as ProgressWheel

        currentLocation_image!!.visibility = View.GONE



        currentLocation_image!!.setOnClickListener {

            cameraIdle = false
            gps = GPSTracker(activity!!)
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                return@setOnClickListener
            }
            mMap!!.isMyLocationEnabled = false
            if (gps.canGetLocation() && gps.isgpsenabled()) {

                MyCurrent_lat = gps.getLatitude()
                MyCurrent_long = gps.getLongitude()

                val cameraPosition =
                    CameraPosition.Builder().target(LatLng(MyCurrent_lat, MyCurrent_long)).zoom(17f)
                        .build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))


            } else {
                enableGpsService()
            }

            currentLocation_image!!.startAnimation(fade_in_anim)
            if (cameraIdle == false) {
                currentLocation_image!!.visibility = View.GONE
            }


        }


        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID]!!
        UserName = user[SessionManager.KEY_USERNAME]!!
        CategoryID = user[SessionManager.KEY_CATEGORY]!!
        drawer_layout = rootview.findViewById(R.id.bg_hamberg) as ImageView
        drawer_layout!!.setOnClickListener { NavigationDrawer.openDrawer() }
        lv_rydetypes = rootview.findViewById(R.id.lv_rydetypes) as RecyclerView
        ll_source_address = rootview.findViewById(R.id.ll_source_address) as LinearLayout
        rl_ridetypes = rootview.findViewById(R.id.rl_ridetypes) as RelativeLayout
        rv_recent_rides = rootview.findViewById(R.id.rv_recent_rides) as RecyclerView
        tv_cab_type_text = rootview.findViewById(R.id.tv_cab_type_text) as TextView
        tv_searching_cabs = rootview.findViewById(R.id.tv_searching_cabs) as TextView

        tv_btn_go = rootview.findViewById(R.id.tv_btn_go) as TextView
        ll_fav_and_ridetypes = rootview.findViewById(R.id.ll_fav_and_ridetypes) as LinearLayout
        ll_main_notefordriver = rootview.findViewById(R.id.ll_main_notefordriver) as LinearLayout
        ll_note_for_driver = rootview.findViewById(R.id.ll_note_for_driver) as LinearLayout
        tv_confirm_pickup = rootview.findViewById(R.id.tv_confirm_pickup) as TextView
        rl_back_button = rootview.findViewById(R.id.rl_back_button) as RelativeLayout
        ll_slidetocancel = rootview.findViewById(R.id.ll_slidetocancel) as LinearLayout
        slidetocancel = rootview.findViewById(R.id.slidetocancel) as SlideToActView
        ll_driver_on_way_main = rootview.findViewById(R.id.ll_driver_on_way_main) as LinearLayout
        rl_driver_on_way_header =
            rootview.findViewById(R.id.rl_driver_on_way_header) as RelativeLayout
        ll_driver_on_theway = rootview.findViewById(R.id.ll_driver_on_theway) as LinearLayout
        close_driveronway = rootview.findViewById(R.id.close_driveronway) as ImageView
        ll_driveronway_fulldetail =
            rootview.findViewById(R.id.ll_driveronway_fulldetail) as LinearLayout
        ll_edit_ride = rootview.findViewById(R.id.ll_edit_ride) as LinearLayout
        ll_include_editride = rootview.findViewById(R.id.ll_include_editride) as LinearLayout
        edit_cancel_ride = rootview.findViewById(R.id.edit_cancel_ride) as MaterialCardView
        close_editride = rootview.findViewById(R.id.close_editride) as ImageView

        ll_safety = rootview.findViewById(R.id.ll_safety) as LinearLayout
        iv_close_safetykit = rootview.findViewById(R.id.iv_close_safetykit) as ImageView
        ll_include_safettytoolkit =
            rootview.findViewById(R.id.ll_include_safettytoolkit) as LinearLayout
        ll_call = rootview.findViewById(R.id.ll_call) as LinearLayout
        ll_message = rootview.findViewById(R.id.ll_message) as LinearLayout
        tv_include_driver_found = rootview.findViewById(R.id.tv_driver_found) as TextView
        tv_include_pick_up_addr = rootview.findViewById(R.id.tv_include_pick_up_addr) as TextView
        tv_include_drop_addr = rootview.findViewById(R.id.tv_include_drop_addr) as TextView

        //Driver data

        iv_driver_image = rootview.findViewById(R.id.iv_driver_image) as ImageView
        tv_driver_name = rootview.findViewById(R.id.tv_driver_name) as TextView
        tv_cab_type = rootview.findViewById(R.id.tv_cab_type) as TextView
        tv_pick_up_time = rootview.findViewById(R.id.tv_pick_up_time) as TextView
        tv_drop_time = rootview.findViewById(R.id.tv_drop_time) as TextView
        tv_car_number = rootview.findViewById(R.id.tv_car_number) as TextView
        driver_rating = rootview.findViewById(R.id.driver_rating) as RotationRatingBar
        tv_driver_estimated_time = rootview.findViewById(R.id.tv_driver_estimated_time) as TextView
        tv_car_model = rootview.findViewById(R.id.tv_car_model) as TextView
        tv_fare_estimate = rootview.findViewById(R.id.tv_fare_estimate) as TextView
        ll_cancel_ride = rootview.findViewById(R.id.ll_cancel_ride) as LinearLayout


        //Safety Tools Kit
        cardview_safety_center =
            rootview.findViewById(R.id.cardview_safety_center) as MaterialCardView
        cardview_sharemytrip = rootview.findViewById(R.id.cardview_sharemytrip) as MaterialCardView
        cardview_911_assistance =
            rootview.findViewById(R.id.cardview_911_assistance) as MaterialCardView


        tv_submit_notefordriver = rootview.findViewById(R.id.tv_submit_notefordriver) as TextView
        tv_notedriver_enterd = rootview.findViewById(R.id.tv_notedriver_enterd) as TextView
        iv_close_notefordriver = rootview.findViewById(R.id.iv_close_notefordriver) as ImageView
        et_notefordriver = rootview.findViewById(R.id.et_notefordriver) as EditText
        ll_notefordriver = rootview.findViewById(R.id.ll_notefordriver) as LinearLayout


        fade_in_anim = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        fade_out_anim = AnimationUtils.loadAnimation(context, R.anim.fade_out)


        getWalletAmount(Iconstant.cabily_money_url)

        setLocationRequest()
        toast = Toast(activity!!)
        markerOptions = MarkerOptions()

        tv_no_cabs = rootview.findViewById(R.id.tv_no_cabs)
        tv_welcome_name = rootview.findViewById(R.id.tv_welcome_name)

        ridehubservice = session!!.getRidehubService()
        Log.e("ALLSERVICES", "" + ridehubservice)
        tv_destination_search_address =
            rootview.findViewById(R.id.tv_destination_search_address) as TextView

        rl_destination_search_address =
            rootview.findViewById(R.id.rl_destination_search_address) as RelativeLayout
        // book_cardview_confirmride_layout = rootview.findViewById(R.id.book_cardview_confirmride_layout) as CardView
        avLoadingIndicatorView =
            rootview.findViewById(R.id.splash_avLoadingIndicatorView) as AVLoadingIndicatorView

        rl_destination_search_address!!.setOnClickListener {
            if (!source_edit) {
                val intent = Intent(activity, LocationSearch::class.java)
                intent.putExtra("source_address", address)
                intent.putExtra("Slatitude", MyCurrent_lat.toString())
                intent.putExtra("Slongitude", MyCurrent_long.toString())
                intent.putExtra("Fav_selected_Location", "")
                intent.putExtra("setpickuploc", "setpickuploc")
                startActivityForResult(intent, placeSearch_request_code)
                activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                source_edit = false

                val intent = Intent(activity, LocationSearch::class.java)
                intent.putExtra("source_address", address)
                intent.putExtra("Slatitude", Recent_lat.toString())
                intent.putExtra("Slongitude", Recent_long.toString())
                intent.putExtra("Fav_selected_Location", "")
                intent.putExtra("setpickuploc", "setpickuploc")
                startActivityForResult(intent, placeSearch_request_code)
                activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }

        recent_rides_list = ArrayList()
        recent_rides_list_final = ArrayList()

        postRequest_getFavourite(Iconstant.profile_getFavouriteaddress_url)

        tv_welcome_name.text = timeOfDay() + ", " + UserName

        lv_rydetypes!!.addOnItemTouchListener(
            RecyclerItemClickListener(activity!!, lv_rydetypes!!,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        Log.e(
                            "RIDETYPEs",
                            "" + position + "-----" + ride_adapter!!.getBookRideData(position)
                        )
                        tv_searching_cabs!!.visibility = View.VISIBLE
                        position_selected = position
                        mMap!!.uiSettings.setAllGesturesEnabled(true)
                        mMap!!.uiSettings.isScrollGesturesEnabled

                        val catname = category_list[position].getCat_name()
                        tv_cab_type_text!!.text = catname
                        ride_adapter!!.setSelection(position)

                        selectedType = "0"
                        CategoryID = category_list[position].getCat_id().toString()
                        CarAvailable = category_list[position].getCat_time().toString()
                        ScarType = category_list[position].getCat_name().toString()
                        tv_fare_estimate!!.setText("$ " + category_list[position].getMin_amount().toString())
                        Log.e(
                            "RIDETYPEs",
                            "" + position + "-----" + CategoryID
                        )
                        //PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                        PostRequest(
                            Iconstant.BookMyRide_url,
                            java.lang.Double.parseDouble(SselectedLatitude),
                            java.lang.Double.parseDouble(SselectedLongitude),
                            java.lang.Double.parseDouble(DselectedLatitude),
                            java.lang.Double.parseDouble(DselectedLongitude)
                        )
                    }
                })
        )

        tv_btn_go.setOnClickListener {
            Log.e("DRIVERSTATUS", "" + driver_status)
            if (driver_status) {
                source_edit = true
                ll_fav_and_ridetypes.visibility = View.GONE
                rl_ridetypes!!.visibility = View.GONE
                ll_source_address!!.visibility = View.VISIBLE
                ll_main_notefordriver.visibility = View.VISIBLE
                rl_back_button.visibility = View.VISIBLE
                tv_destination_search_address!!.text = address
                tv_welcome_name.setText("Confirm your Pickup Slot")
            }
        }

        rl_back_button.setOnClickListener {
            ll_fav_and_ridetypes.visibility = View.VISIBLE
            rl_ridetypes!!.visibility = View.VISIBLE
            ll_source_address!!.visibility = View.GONE
            ll_main_notefordriver.visibility = View.GONE
            rl_back_button.visibility = View.GONE
            tv_welcome_name.text = timeOfDay() + ", " + UserName
        }

        tv_confirm_pickup.setOnClickListener {
            ll_slidetocancel.visibility = View.VISIBLE
            ll_fav_and_ridetypes.visibility = View.GONE
            rl_ridetypes!!.visibility = View.GONE
            ll_source_address!!.visibility = View.GONE
            ll_main_notefordriver.visibility = View.GONE
            rl_back_button.visibility = View.GONE

            ConfirmRideRequest(
                Iconstant.confirm_ride_url,
                "",
                "",
                "",
                selectedType,
                CategoryID,
                /*tv_destination_search_address!!.text.toString()*/address,
                Recent_lat.toString(),
                Recent_long.toString(),
                "",
                Sselected_DestinationLocation.toString(),
                DselectedLatitude,
                DselectedLongitude,
                notefordriver_stg
            )
        }


        val slide = object : SlideToActView.OnSlideCompleteListener {
            @SuppressLint("MissingPermission")
            override fun onSlideComplete(view: SlideToActView) {
                // vibrate the device
                /*val vibrator = context!!.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibrator.vibrate(100)*/
                if (isInternetPresent) {
                    toast!!.cancel()
                    DeleteRideRequest(Iconstant.delete_ride_url)
                } else {
                    toast!!.setText(getResources().getString(R.string.alert_nointernet_message))
                    toast!!.show()
                }
            }
        }
        slidetocancel!!.onSlideCompleteListener = slide

        /*ll_driver_on_theway!!.setOnClickListener {
            rl_driver_on_way_header!!.visibility = View.VISIBLE
            ll_driveronway_fulldetail!!.visibility = View.VISIBLE
            drawer_layout!!.visibility = View.GONE
            ll_driver_on_way_main!!.setBackgroundColor(activity!!.resources.getColor(R.color.rating_bg))
        }

        close_driveronway!!.setOnClickListener {
            ll_driver_on_way_main!!.visibility = View.VISIBLE
            drawer_layout!!.visibility = View.VISIBLE
            rl_driver_on_way_header!!.visibility = View.GONE
            ll_driveronway_fulldetail!!.visibility = View.GONE
            ll_driver_on_way_main!!.background = null

        }*/
        ll_edit_ride!!.setOnClickListener {
            ll_driver_on_way_main!!.visibility = View.GONE
            ll_include_editride!!.visibility = View.VISIBLE
        }
        close_editride!!.setOnClickListener {
            ll_include_editride!!.visibility = View.GONE
            ll_driver_on_way_main!!.visibility = View.VISIBLE
        }
        ll_safety!!.setOnClickListener {
            ll_include_safettytoolkit!!.visibility = View.VISIBLE
            ll_driver_on_way_main!!.visibility = View.GONE
        }

        iv_close_safetykit!!.setOnClickListener {
            ll_include_safettytoolkit!!.visibility = View.GONE
            ll_driver_on_way_main!!.visibility = View.VISIBLE
        }
        ll_message!!.setOnClickListener {
            val i = Intent(activity!!, ChatActivity::class.java)
            i.putExtra("driverID", driverID)
            i.putExtra("driverName", driverName)
            i.putExtra("driverImage", driverImage)
            startActivity(i)
        }
        ll_call!!.setOnClickListener {
            if (driverMobile != null) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + driverMobile)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + driverMobile)
                    startActivity(callIntent)
                }
            }
        }


        cardview_safety_center!!.setOnClickListener {
            val i = Intent(activity!!, SafetyCenterActivity::class.java)
            startActivity(i)
        }
        cardview_sharemytrip!!.setOnClickListener {
            val i = Intent(activity!!, ShareTripActivity::class.java)
            startActivity(i)
        }
        cardview_911_assistance!!.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermission()
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + 911)
                    startActivity(callIntent)
                }
            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + 911)
                startActivity(callIntent)
            }
        }

        edit_cancel_ride!!.setOnClickListener {
            if (isInternetPresent) {
                toast!!.cancel()
                DeleteRideRequest(Iconstant.delete_ride_url)
            } else {
                toast!!.setText(getResources().getString(R.string.alert_nointernet_message))
                toast!!.show()
            }
        }

        ll_cancel_ride!!.setOnClickListener {
            if (isInternetPresent) {
                toast!!.cancel()
                DeleteRideRequest(Iconstant.delete_ride_url)
            } else {
                toast!!.setText(getResources().getString(R.string.alert_nointernet_message))
                toast!!.show()
            }
        }

        // -----Bottom Sheet-----
        ll_driveronway_top_detail =
            rootview.findViewById(R.id.ll_driveronway_top_detail) as LinearLayout

        // Bottom sheet
        ll_driveronway_top_detail!!.measure(0, 0)
        Log.d(
            "ll_driveronway_top_detail height",
            ll_driveronway_top_detail!!.measuredHeight.toString() + "-----" + ll_driveronway_top_detail!!.height.toString()
        )

        ll_driver_on_way_main!!.visibility = View.GONE
        bottomSheetReview = BottomSheetBehavior.from(ll_driver_on_way_main)
        bottomSheetReview.isHideable = false
//        bottomSheetReview.peekHeight = ll_driveronway_top_detail!!.measuredHeight
        bottomSheetReview.state = BottomSheetBehavior.STATE_COLLAPSED
        currentState = BottomSheetBehavior.STATE_COLLAPSED

        bottomSheetReview.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                currentState = newState
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                Log.d("onSlide", "onSlide value: $slideOffset")

            }
        })



        tv_submit_notefordriver!!.setOnClickListener {

            if (et_notefordriver!!.equals("")) {
                Toast.makeText(activity, "Please enter some text", Toast.LENGTH_LONG).show()
            } else {
                notefordriver_stg = et_notefordriver!!.text.toString()
                tv_notedriver_enterd!!.visibility = View.VISIBLE
                tv_notedriver_enterd!!.setText(notefordriver_stg)
                CloseKeyboard(et_notefordriver!!)
                ll_notefordriver!!.visibility = View.GONE
            }


        }

        iv_close_notefordriver!!.setOnClickListener {
            ll_notefordriver!!.visibility = View.GONE
            if (driver_status) {
                ll_fav_and_ridetypes.visibility = View.GONE
                rl_ridetypes!!.visibility = View.GONE
                ll_source_address!!.visibility = View.VISIBLE
                ll_main_notefordriver.visibility = View.VISIBLE
                rl_back_button.visibility = View.VISIBLE
                tv_destination_search_address!!.text = address
                tv_welcome_name.setText("Confirm your Pickup Slot")
            }

        }

        ll_note_for_driver.setOnClickListener {
            ll_notefordriver!!.visibility = View.VISIBLE
            et_notefordriver!!.text = null

        }


    }

    private fun initializeMap() {
        if (mMap == null) {
            val mapFragment =
                childFragmentManager.findFragmentById(R.id.book_my_ride_mapview) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                //Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private fun postRequest_getFavourite(Url: String) {
        println("---------------Favourite Url-----------------$Url")
        dialog = Dialog(activity!!)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {


                    var Sstatus = ""
                    val Smessage = ""
                    try {
                        println("---------------Favourite Response-----------------$response")
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val Location_aray = `object`.getJSONArray("locations")
                            for (i in 0 until Location_aray.length()) {
                                val address_object = Location_aray.getJSONObject(i)

                                val address = address_object.getString("address")
                                val title = address_object.getString("title")
                                val location_key = address_object.getString("location_key")

                                val recentRidesPojo = RecentRidesPojo()
                                recentRidesPojo.setTitle(location_key)
                                recentRidesPojo.setAddress(address)
                                Log.d("ADDRESS_RES", "$address------$title")
                                recent_rides_list.add(recentRidesPojo)
                            }
                        } else {
                        }
                        if (isInternetPresent) {
                            postRequest_recentRides(Iconstant.home_recent_reides_url)
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()

                    if (isInternetPresent) {
                        postRequest_recentRides(Iconstant.home_recent_reides_url)
                    }
                }
            })
    }


    //*****************Call Permissions*************

    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE),
            PERMISSION_REQUEST_CODE
        )
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE),
            PERMISSION_REQUEST_CODES
        )
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + driverMobile)
                startActivity(callIntent)
            }

            PERMISSION_REQUEST_CODES ->

                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:$sSelectedPanicNumber")
                    startActivity(callIntent)
                }
        }
    }
    // **************End Of Call Permissions*************


    //Recent Rides List API
    private fun postRequest_recentRides(Url: String) {
        println("---------------Recent Rides Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("---------------Recent Rides Response-----------------$response")
                    var Sstatus = ""
                    val Smessage = ""
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val jsonResponse = `object`.getJSONObject("response")
                            val listArray = jsonResponse.getJSONArray("list")
                            recent_rides_array = ArrayList<RecentRidesPojo>()
                            recent_rides_array.clear()
                            for (i in 0 until listArray.length()) {
                                val place_object = listArray.getJSONObject(i)
                                recent_source = place_object.getString("source")
                                recent_destination = place_object.getString("destination")
                                Log.e("RECENTSOU&DEST", "$recent_source--------$recent_destination")

                                val recentRidesPojo = RecentRidesPojo()
                                recentRidesPojo.setTitle("recent")
                                recentRidesPojo.setAddress(recent_destination)
                                recent_rides_array.add(recentRidesPojo)
                                recent_rides_list.add(recentRidesPojo)
                            }

                            val layoutManager = LinearLayoutManager(
                                (context)
                                , LinearLayoutManager.VERTICAL, false
                            )
                            rv_recent_rides!!.layoutManager = layoutManager
                            rv_recent_rides!!.itemAnimator = DefaultItemAnimator()

                            for (i in 0 until recent_rides_list.size) {
                                if (recent_rides_list[i].getTitle().equals("work") ||
                                    recent_rides_list[i].getTitle().equals("Work")
                                ) {
                                    // do nothing
                                } else {
                                    recent_rides_list_final.add(recent_rides_list[i])
                                }
                            }

                            val recentRidesAdapter =
                                RecentRidesAdapter(activity!!, recent_rides_list_final)
                            rv_recent_rides!!.adapter = recentRidesAdapter

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()

                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    internal inner class RecentRidesAdapter(
        private val mContext: Context,
        var recent_rides_array: java.util.ArrayList<RecentRidesPojo>
    ) : RecyclerView.Adapter<MyViewHolder>() {
        override fun getItemCount(): Int {
            return 2
        }

        internal inner class MyViewHolder(rowView: View) : RecyclerView.ViewHolder(rowView) {
            var tv_recent_destination: TextView
            var iv_home: ImageView
            var tv_home: TextView
            lateinit var ll_recent_ride_listitem: LinearLayout

            init {
                tv_recent_destination = rowView.findViewById(R.id.tv_home_address) as TextView
                iv_home = rowView.findViewById(R.id.iv_home) as ImageView
                ll_recent_ride_listitem =
                    rowView.findViewById(R.id.ll_recent_ride_listitem) as LinearLayout
                tv_home = rowView.findViewById(R.id.tv_home) as TextView

            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_recent_rides, parent, false)

            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            try {
                val recentRidesPojo = recent_rides_array[position]
                if (recent_rides_array.size > 0) {
                    holder.tv_recent_destination.text = recentRidesPojo.getAddress()
                    holder.tv_home.text = recentRidesPojo.getTitle()
                    holder.iv_home.setImageResource(R.drawable.dash_home)
                    if (recentRidesPojo.getTitle().equals("recent")) {
                        holder.tv_recent_destination.text = recentRidesPojo.getAddress()
                        holder.tv_home.text = "Recent"
                        holder.iv_home.setImageResource(R.drawable.dash_fav)
                    } else if (recentRidesPojo.getTitle().equals("work") ||
                        recentRidesPojo.getTitle().equals("Work")
                    ) {
                        holder.tv_recent_destination.text = recentRidesPojo.getAddress()
                        holder.tv_home.text = recentRidesPojo.getTitle()
                        holder.iv_home.setImageResource(R.drawable.dash_work)
                    } else if (recentRidesPojo.getTitle().equals("home") ||
                        recentRidesPojo.getTitle().equals("Home")
                    ) {
                        holder.tv_recent_destination.text = recentRidesPojo.getAddress()
                        holder.tv_home.text = "Home"
                        holder.iv_home.setImageResource(R.drawable.dash_home)
                    }

                }
                holder.ll_recent_ride_listitem.setOnClickListener {
                    val recent_ride_dest = recentRidesPojo.getAddress().toString()
                    if (isInternetPresent) {
                        val data = recent_ride_dest.toLowerCase().replace(" ", "%20")
                        Log.e("ETSEARCH_CITY222222", holder.tv_recent_destination.text.toString())
                        Sselected_DestinationLocation = recentRidesPojo.getAddress().toString()
                        getLocationFromAddress(
                            context!!,
                            recentRidesPojo.getAddress().toString()
                        )


                    } else {

                    }
                }
            } catch (e: Exception) {
                //Log.e("AdapterError", e.toString());
            }
        }
    }

    //-------------Method to get Complete Address------------
    private fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {
                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                } else {
                    try {
                        strReturnedAddress.append(returnedAddress.getAddressLine(0))
                    } catch (ignored: Exception) {
                    }

                }
                strAdd = strReturnedAddress.toString()
            } else {
                Log.e("Current loction address", "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Current loction address", "Canont get Address!")
        }

        return strAdd
    }

    //Enabling Gps Service
    private fun enableGpsService() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        mGoogleApiClient!!.connect()
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback(ResultCallback<LocationSettingsResult> { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    try {
                        status.startResolutionForResult(activity, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        })
    }


    fun startTimer() {
        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent) {
            timer = Timer()
            //initialize the TimerTask's job
            initializeTimerTask()
            //schedule the timer, to wake up every 1 second
            timer!!.schedule(timerTask, 3000, 10000)
        }//
    }


    fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                try {
                    Log.e("TIMER", "MANIBABU")
                    cd = ConnectionDetector(activity!!)
                    isInternetPresent = cd!!.isConnectingToInternet
                    if (isInternetPresent) {
                        PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                    }
                } catch (e: Exception) {
                    Log.e("TIMER_EXCEPTION", e.toString())
                }

            }
        }
    }

    fun stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("--------------onActivityResult requestCode----------------$requestCode")
        if (requestCode == timer_request_code && resultCode == RESULT_OK && data != null) {
            val ride_accepted = data.getStringExtra("Accepted_or_Not")
            val retry_count = data.getStringExtra("Retry_Count")
            if (retry_count.equals("2", ignoreCase = true) && ride_accepted.equals(
                    "not",
                    ignoreCase = true
                )
            ) run { DeleteRideRequest(Iconstant.delete_ride_url) } else
                if ((retry_count.equals("1", ignoreCase = true) || retry_count.equals(
                        "2",
                        ignoreCase = true
                    )) && ride_accepted.equals("Cancelled", ignoreCase = true)
                ) {
                    riderId = ""
                    mMap!!.clear()
                    onMapReady(mMap)
                    drawer_layout!!.setVisibility(View.VISIBLE)
                    drawer_layout!!.setClickable(true)
                    currentLocation_image!!.setClickable(true)
                    currentLocation_image!!.setVisibility(View.VISIBLE)
                    drawer_layout!!.setEnabled(true)
                    NavigationDrawer.enableSwipeDrawer()
                    ll_source_address!!.visibility = View.VISIBLE
                    ll_fav_and_ridetypes.visibility = View.VISIBLE
                    ll_driver_on_way_main!!.visibility = View.GONE
                    ll_source_address!!.visibility = View.VISIBLE
                    tv_destination_search_address!!.text = ""
                    slidetocancel!!.resetSlider()
                    et_notefordriver!!.text.clear()
                    et_notefordriver!!.setText("")
                    tv_notedriver_enterd!!.text = ""
                    notefordriver_stg = ""
                    tv_welcome_name!!.text = timeOfDay() + ", " + UserName

                }


        } else if (requestCode == placeSearch_request_code && resultCode == Activity.RESULT_OK && data != null) {

            if (search_status == 0) {
                setpickuploc = data.getStringExtra("setpickuploc")
                Log.e("setpickuploc", setpickuploc!!)
                if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                    lv_rydetypes!!.visibility = View.GONE
                    rl_ridetypes!!.visibility = View.GONE
                }
                SselectedLatitude = data.getStringExtra("Selected_Latitude")!!
                SselectedLongitude = data.getStringExtra("Selected_Longitude")!!
                DselectedLatitude = data.getStringExtra("Selected_DLatitude")!!
                DselectedLongitude = data.getStringExtra("Selected_DLongitude")!!
                locatiosearch = data.getStringExtra("searchlocation")!!
                Log.e(
                    "OnACTIVTY_IF",
                    "SOURCE-->" + SselectedLatitude + "" + SselectedLongitude + "DESTINATION--->" + DselectedLatitude + "-------" + DselectedLongitude + "---------" + locatiosearch
                )

                try {
                    val lat_decimal = java.lang.Double.parseDouble(SselectedLatitude)
                    val lng_decimal = java.lang.Double.parseDouble(SselectedLongitude)
                    val pick_lat_decimal = java.lang.Double.parseDouble(DselectedLatitude)
                    val pick_lng_decimal = java.lang.Double.parseDouble(DselectedLongitude)
                    //stoptimertask()
                    Recent_lat = lat_decimal
                    Recent_long = lng_decimal
                    updateGoogleMapTrackRide(
                        lat_decimal,
                        lng_decimal,
                        pick_lat_decimal,
                        pick_lng_decimal,
                        ""
                    )
                    PostRequest(
                        Iconstant.BookMyRide_url,
                        java.lang.Double.parseDouble(SselectedLatitude),
                        java.lang.Double.parseDouble(SselectedLongitude),
                        java.lang.Double.parseDouble(DselectedLatitude),
                        java.lang.Double.parseDouble(DselectedLongitude)
                    )


                    println("inside updategoogle1--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }

                Sselected_DestinationLocation = data.getStringExtra("Selected_Location")
                Selected_SourceLocation = data.getStringExtra("Selected_SourceLocation")
                Log.e("SELECTED_SOURCE", Selected_SourceLocation!!)
                address = Selected_SourceLocation!!
                tv_destination_search_address!!.text = /*Selected_SourceLocation*/address

                if (!SselectedLatitude.equals(
                        "",
                        ignoreCase = true
                    ) && SselectedLatitude.isNotEmpty() && !SselectedLongitude.equals(
                        "",
                        ignoreCase = true
                    ) && SselectedLongitude.isNotEmpty()
                ) {
                    // Move the camera to last position with a zoom level
                    val cameraPosition = CameraPosition.Builder().target(
                        LatLng(
                            java.lang.Double.parseDouble(SselectedLatitude),
                            java.lang.Double.parseDouble(SselectedLongitude)
                        )
                    ).zoom(17f).build()
                    mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                }

            } else {
                SdestinationLatitude = data.getStringExtra("Selected_Latitude")!!
                SdestinationLongitude = data.getStringExtra("Selected_Longitude")!!
                SsourceLatitude = data.getStringExtra("Selected_DLatitude")!!
                SsourceLongitude = data.getStringExtra("Selected_DLongitude")!!
                SdestinationLocation = data.getStringExtra("Selected_Location")!!

                Log.e(
                    "OnACTIVTY_ELSE",
                    SsourceLatitude + "-------" + SsourceLongitude + "----" + SdestinationLatitude + "-----" + SdestinationLongitude + "------------" + SdestinationLocation
                )

                println("-----------SdestinationLatitude----------------$SdestinationLatitude")
                println("-----------SdestinationLongitude----------------$SdestinationLongitude")
                println("-----------SsourceLatitude----------------$SsourceLatitude")
                println("-----------SsourceLongitude----------------$SsourceLongitude")
                println("-----------SdestinationLocation----------------$SdestinationLocation")
            }
        } else if (requestCode == REQUEST_LOCATION) {
            println("----------inside request location------------------")

            when (resultCode) {
                Activity.RESULT_OK -> {
                    Toast.makeText(activity, "Location enabled!", Toast.LENGTH_LONG).show()
                }
                Activity.RESULT_CANCELED -> {
                    enableGpsService()
                }
                else -> {
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun updateGoogleMapTrackRide(
        lat_decimal: Double,
        lng_decimal: Double,
        pick_lat_decimal: Double,
        pick_lng_decimal: Double,
        status: String
    ) {
        if (mMap != null) {
            mMap!!.clear()
        }
        val dropLatLng = LatLng(lat_decimal, lng_decimal)
        val pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)
        val draw_route_asyncTask = GetDropRouteTask()
        draw_route_asyncTask.setToAndFromLocation(dropLatLng, pickUpLatLng, status)
        draw_route_asyncTask.execute()
    }

    @SuppressLint("StaticFieldLeak")
    private inner class GetDropRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        private var currentLocation: LatLng? = null
        private var endLocation: LatLng? = null
        internal var status = ""

        fun setToAndFromLocation(currentLocation: LatLng, endLocation: LatLng, status: String) {
            this.currentLocation = currentLocation
            this.endLocation = endLocation
            this.status = status
        }

        override fun onPreExecute() {}
        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(
                currentLocation,
                endLocation,
                GMapV2GetRouteDirection.MODE_DRIVING
            )
            response = "Success"

            if (isInternetPresent) {
                //  println("--------------PostRequest longitude-------------------$SselectedLatitude----------$SselectedLongitude-------------$DselectedLatitude----$DselectedLongitude")

                /*PostRequest(
                    Iconstant.BookMyRide_url,
                    java.lang.Double.parseDouble(SselectedLatitude),
                    java.lang.Double.parseDouble(SselectedLongitude),
                    java.lang.Double.parseDouble(DselectedLatitude),
                    java.lang.Double.parseDouble(DselectedLongitude)
                )*/
                if (list_item_selected.equals(false)) {


                    //PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                }
            }
            return response

        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                mMap!!.clear()

                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)

                    // custom polyline
                    /* val green = 150.0f
                     val blue = 150.0f
                     val greenSteps = (green / directionPoint.size)
                     val blueSteps = (blue / directionPoint.size)
                     val builder1 = LatLngBounds.Builder()
                     builder1.include(directionPoint[0])
                     for (i in 1 until directionPoint.size) {
                         builder1.include(directionPoint[i])
                         val greenColor = (green - (greenSteps * i)).toInt()
                         val blueColor = (blue - (blueSteps * i)).toInt()
                         Log.e("Color", "" + blueColor)
                         val color = Color.rgb(0, greenColor, blueColor)
                         val options = PolylineOptions().width(20f).color(color).geodesic(true)
                         options.add(directionPoint[i - 1])
                         options.add(directionPoint[i])
                         val line = mMap!!.addPolyline(options)
                         line.endCap = RoundCap()
                     }*/

                    val rectLine = PolylineOptions().width(10f).color(
                        resources.getColor(R.color.blue_text_bg)
                    ).geodesic(true)
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint[i])
                    }
                    mMap!!.addPolyline(rectLine)

                    mMap!!.isMyLocationEnabled = false
                    mMap!!.setMaxZoomPreference(100f)

                    val markerWidth = 250
                    val markerHeight = 250

                    // pick up marker
                    val pickup_marker_view =
                        (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_pickup_marker,
                            null
                        )
                    val tv_marker_pickup =
                        pickup_marker_view.findViewById<TextView>(R.id.tv_marker_pickup)
                    tv_marker_pickup.text = Selected_SourceLocation!!
                    val pickup_bitmap = createBitmapFromLayout(pickup_marker_view)
                    val pickup_small_bitmap =
                        Bitmap.createScaledBitmap(pickup_bitmap, markerWidth, markerHeight, false)


                    // destination marker
                    val destn_marker_view =
                        (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_drop_marker,
                            null
                        )
                    val tv_marker_drop =
                        destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                    tv_marker_drop.text = Sselected_DestinationLocation!!
                    val destn_bitmap = createBitmapFromLayout(destn_marker_view)
                    val destn_small_bitmap =
                        Bitmap.createScaledBitmap(destn_bitmap, markerWidth, markerHeight, false)

                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(currentLocation!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination_dot))
                            .icon(BitmapDescriptorFactory.fromBitmap(pickup_bitmap))
                    )

                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(endLocation!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.destilocation))
                            .icon(BitmapDescriptorFactory.fromBitmap(destn_bitmap))
                    )

                    val builder = LatLngBounds.Builder()
                    for (i in directionPoint.indices) {
                        builder.include(directionPoint[i])
                    }
                    val bounds = builder.build()
                    val routePadding = 120
                    mMap!!.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(bounds, routePadding), 600, null
                    )
                    curLoc = currentLocation
                    endLoc = endLocation

                    //markerOptions!!.draggable(true)
                    if (!status.equals("nocars", ignoreCase = true)) {
                    }

                    if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {

                        ll_source_address!!.visibility = View.GONE
                        rl_ridetypes!!.visibility = View.VISIBLE
                        lv_rydetypes!!.visibility = View.VISIBLE
                    } else {
                        //listview!!.setVisibility(View.VISIBLE)
                        currentLocation_image!!.visibility = View.GONE
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

    //Get AVAILABLE DRIVERS ON MAP//START SERVICE
    private fun PostRequestDrivers(Url: String, latitude: Double, longitude: Double) {
        println("--------------AvailableDriver url-------------------$Url")
        Sselected_latitude = latitude.toString()
        Sselected_longitude = longitude.toString()
        println("--------------AvailableDriver UserID-------------------$UserID")
        println("--------------AvailableDriver latitude-------------------$latitude")
        println("--------------AvailableDriver longitude-------------------$longitude")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["lat"] = latitude.toString()
        jsonParams["lon"] = longitude.toString()

        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                @SuppressLint("LongLogTag")
                override fun onCompleteListener(response: String) {
                    println("--------------Get Drivers Response-------------------$response")
                    var fail_response = ""

                    rl_ridetypes!!.visibility = View.VISIBLE
                    lv_rydetypes!!.visibility = View.VISIBLE
                    ll_source_address!!.visibility = View.GONE
                    avLoadingIndicatorView!!.visibility = View.GONE
                    //mMap!!.getUiSettings().setAllGesturesEnabled(false)

                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {
                            if (`object`.getString("status").equals("1", ignoreCase = true)) {

                                val jobject = `object`.getJSONObject("response")
                                Log.e("AVAIALABLEDRIVER_RESPONSE", response)
                                if (jobject.length() > 0) {
                                    for (i in 0 until jobject.length()) {

                                        val check_driver_object = jobject.get("drivers")
                                        if (check_driver_object is JSONArray) {

                                            val driver_array = jobject.getJSONArray("drivers")
                                            if (driver_array.length() > 0) {
                                                driver_list.clear()

                                                for (j in 0 until driver_array.length()) {
                                                    val driver_object =
                                                        driver_array.getJSONObject(j)

                                                    val pojo = HomePojo()
                                                    pojo.setDriver_lat(driver_object.getString("lat"))
                                                    pojo.setDriver_long(driver_object.getString("lon"))

                                                    driver_list.add(pojo)
                                                }
                                                driver_status = true
                                            } else {
                                                driver_list.clear()
                                                driver_status = false
                                            }
                                        } else {
                                            driver_status = false
                                        }

                                        val check_category_object = jobject.get("category")
                                        if (check_category_object is JSONArray) {
                                            val cat_array = jobject.getJSONArray("category")
                                            if (cat_array.length() > 0) {
                                                category_list.clear()
                                                for (k in 0 until cat_array.length()) {
                                                    val cat_object = cat_array.getJSONObject(k)
                                                    val pojo = HomePojo()
                                                    pojo.setCat_name(cat_object.getString("name"))
                                                    //pojo.setCat_time(cat_object.getString("eta"));
                                                    pojo.setCat_id(cat_object.getString("id"))
                                                    pojo.setCat_time(cat_object.getString("eta"))

                                                    pojo.setCat_image(cat_object.getString("icon_normal"))
                                                    //pojo.setMin_amount(cat_object.getString("eta"))
                                                    pojo.setMain_type("Ridehub")
                                                    category_list.add(pojo)
                                                }
                                                category_status = true
                                                Log.e("CategorySTATUSIF", "" + category_status)

                                            } else {
                                                Log.e("CategorySTATUSELSE", "" + category_status)
                                                category_list.clear()
                                                category_status = false
                                            }
                                        } else {
                                            category_status = false
                                        }
                                    }
                                }
                                main_response_status = true

                                val layoutManager = LinearLayoutManager(
                                    (context)
                                    , LinearLayoutManager.HORIZONTAL, false
                                )
                                lv_rydetypes!!.layoutManager = layoutManager
                                lv_rydetypes!!.itemAnimator = DefaultItemAnimator()
                                ride_adapter = BookMyRide_Adapter_2(activity!!, category_list)
                                lv_rydetypes!!.adapter = ride_adapter

                            } else {
                                fail_response = `object`.getString("response")
                                main_response_status = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (main_response_status) {
                        Log.e(
                            "DRIVERSTAT",
                            "$driver_status----------$main_response_status---------$category_status-------"
                        )
                        if (driver_status) {
                            for (i in driver_list.indices) {

                                val Dlatitude =
                                    java.lang.Double.parseDouble(driver_list.get(i).getDriver_lat()!!)
                                val Dlongitude =
                                    java.lang.Double.parseDouble(driver_list.get(i).getDriver_long()!!)
                                // create marker double Dlatitude = gps.getLatitude();
                                val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
                                val height = 150
                                val width = 150
                                val bitmapdraw =
                                    resources.getDrawable(R.drawable.dash_car) as BitmapDrawable
                                val b = bitmapdraw.bitmap
                                val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
                                marker.icon(BitmapDescriptorFactory.fromBitmap(b))
                                mMap!!.addMarker(marker)

                            }
                        } else {

                        }
                    } else {

                    }

                }

                override fun onErrorListener() {
                }
            })


    }


    //-------------------AsynTask To get the current Address----------------
    private fun PostRequest(
        Url: String,
        latitude: Double,
        longitude: Double,
        Dlatitude: Double,
        Dlongitude: Double
    ) {
        //progressWheel!!.visibility = View.VISIBLE
        dialog = Dialog(activity!!)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        println("--------------Book My ride url-------------------$Url")
        Sselected_latitude = latitude.toString()
        Sselected_longitude = longitude.toString()
        category_list.clear()

        println("--------------Book My ride UserID-------------------$UserID")
        println("--------------Book My ride latitude-------------------$latitude")
        println("--------------Book My ride longitude-------------------$longitude")
        println("--------------Book My ride CategoryID-------------------$CategoryID")
        println("--------------Book My ride DLatitude-------------------$Dlatitude")
        println("--------------Book My ride DLongitude-------------------$Dlongitude")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["pickup_lat"] = latitude.toString()
        jsonParams["pickup_lon"] = longitude.toString()
        jsonParams["drop_lat"] = Dlatitude.toString()
        jsonParams["drop_lon"] = Dlongitude.toString()
        jsonParams["category"] = CategoryID
        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                @SuppressLint("LongLogTag")
                override fun onCompleteListener(response: String) {

                    println("--------------Book My ride reponse-------------------$response")
                    var fail_response = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {
                            if (`object`.getString("status").equals("1", ignoreCase = true)) {

                                rl_ridetypes!!.visibility = View.VISIBLE
                                lv_rydetypes!!.visibility = View.VISIBLE
                                ll_source_address!!.visibility = View.GONE
                                avLoadingIndicatorView!!.visibility = View.GONE

                                val jobject = `object`.getJSONObject("response")
                                if (jobject.length() > 0) {
                                    for (i in 0 until jobject.length()) {

                                        val check_driver_object = jobject.get("drivers")
                                        if (check_driver_object is JSONArray) {

                                            val driver_array = jobject.getJSONArray("drivers")
                                            if (driver_array.length() > 0) {
                                                driver_list.clear()

                                                for (j in 0 until driver_array.length()) {
                                                    val driver_object =
                                                        driver_array.getJSONObject(j)

                                                    val pojo = HomePojo()
                                                    pojo.setDriver_lat(driver_object.getString("lat"))
                                                    pojo.setDriver_long(driver_object.getString("lon"))

                                                    driver_list.add(pojo)
                                                }
                                                driver_status = true
                                            } else {
                                                driver_list.clear()
                                                driver_status = false
                                            }
                                        } else {
                                            driver_status = false
                                        }

                                        val check_ratecard_object = jobject.get("ratecard")
                                        if (check_ratecard_object is JSONObject) {

                                            val ratecard_object = jobject.getJSONObject("ratecard")
                                            if (ratecard_object.length() > 0) {
                                                val pojo = HomePojo()

                                                pojo.setRate_cartype(ratecard_object.getString("category"))
                                                pojo.setRate_note(ratecard_object.getString("note"))
                                                pojo.setCurrencyCode(jobject.getString("currency"))

                                                val farebreakup_object =
                                                    ratecard_object.getJSONObject("farebreakup")
                                                if (farebreakup_object.length() > 0) {
                                                    val minfare_object =
                                                        farebreakup_object.getJSONObject("min_fare")
                                                    if (minfare_object.length() > 0) {
                                                        pojo.setMinfare_amt(
                                                            minfare_object.getString(
                                                                "amount"
                                                            )
                                                        )
                                                        pojo.setMinfare_km(
                                                            minfare_object.getString(
                                                                "text"
                                                            )
                                                        )
                                                    }

                                                    val afterfare_object =
                                                        farebreakup_object.getJSONObject("after_fare")
                                                    if (afterfare_object.length() > 0) {
                                                        pojo.setAfterfare_amt(
                                                            afterfare_object.getString(
                                                                "amount"
                                                            )
                                                        )
                                                        pojo.setAfterfare_km(
                                                            afterfare_object.getString(
                                                                "text"
                                                            )
                                                        )
                                                    }

                                                    val otherfare_object =
                                                        farebreakup_object.getJSONObject("other_fare")
                                                    if (otherfare_object.length() > 0) {
                                                        pojo.setOtherfare_amt(
                                                            otherfare_object.getString(
                                                                "amount"
                                                            )
                                                        )
                                                        pojo.setOtherfare_km(
                                                            otherfare_object.getString(
                                                                "text"
                                                            )
                                                        )
                                                    }
                                                }

                                            } else {

                                            }
                                        } else {
                                        }
                                        //PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)
                                        val check_category_object = jobject.get("category")
                                        if (check_category_object is JSONArray) {
                                            val cat_array = jobject.getJSONArray("category")
                                            if (cat_array.length() > 0) {
                                                category_list.clear()
                                                for (k in 0 until cat_array.length()) {
                                                    val cat_object = cat_array.getJSONObject(k)
                                                    val pojo = HomePojo()
                                                    pojo.setCat_name(cat_object.getString("name"))
                                                    pojo.setCat_time(cat_object.getString("eta"));
                                                    pojo.setCat_id(cat_object.getString("id"))
                                                    pojo.setMin_amount(cat_object.getString("min_amount"))
                                                    pojo.setMax_amount(cat_object.getString("max_amount"))
                                                    pojo.setCar_capacity_min(cat_object.getString("seatcapacity_min"))
                                                    pojo.setCar_capacity_max(cat_object.getString("seatcapacity_max"))
                                                    pojo.setIcon_normal(cat_object.getString("icon_normal"))
                                                    pojo.setIcon_active(cat_object.getString("icon_active"))
                                                    pojo.setSelected_Cat(jobject.getString("selected_category"))

                                                    //Log.e("selected_subcategory",jobject.getString("selected_category")+"--"+jobject.getString("selected_subcategory"));
                                                    if (cat_object.getString("id") == jobject.getString(
                                                            "selected_category"
                                                        )
                                                    ) {
                                                        CarAvailable = cat_object.getString("eta")
                                                        ScarType = cat_object.getString("name")
                                                        Log.e(
                                                            "CatCARAVAILABLE",
                                                            "" + CarAvailable + "" + ScarType + "----" + cat_object.getString(
                                                                "max_amount"
                                                            )
                                                        )

                                                    }
                                                    category_list.add(pojo)
                                                }




                                                category_status = true
                                                Log.e("CategorySTATUSIF", "" + category_status)
                                            } else {
                                                Log.e("CategorySTATUSELSE", "" + category_status)
                                                category_list.clear()
                                                category_status = false
                                            }


                                        } else {
                                            category_status = false
                                        }
                                    }
                                }

                                main_response_status = true

                            } else {
                                fail_response = `object`.getString("response")
                                main_response_status = false
                            }

                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    // UberRideEstimateRequest()
                    //  PostRequestDrivers(Iconstant.Available_drivers_url, Recent_lat, Recent_long)


                    if (main_response_status) {
                        Log.e(
                            "DRIVERSTAT",
                            "$driver_status----------$main_response_status---------$category_status"
                        )
                        if (category_status) {
                            Log.e("category_list_RideHub", category_list.size.toString() + "")
                            dialog.dismiss()

                            //category_list.clear()
                            val layoutManager = LinearLayoutManager(
                                (context)
                                , LinearLayoutManager.HORIZONTAL, false
                            )

                            tv_searching_cabs!!.visibility = View.GONE
                            if (position_selected != -1) {
                                ride_adapter!!.setSelection(position_selected)
                                ride_adapter!!.updateList(category_list)
                            } else {
                                lv_rydetypes!!.layoutManager = layoutManager
                                lv_rydetypes!!.itemAnimator = DefaultItemAnimator()
                                ride_adapter = BookMyRide_Adapter_2(activity!!, category_list)
                                lv_rydetypes!!.adapter = ride_adapter
                            }

                            if (!driver_status) {
                                Log.e("DRIVER_FALSE", "MANIBABU")
                                currentLocation_image!!.visibility = View.GONE
                                if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                                    Log.e("SETPICKUP", setpickuploc!!)
                                } else {
                                    Log.e("SETPICKUP_no", setpickuploc!!)
                                    //listview.setVisibility(View.VISIBLE)
                                    currentLocation_image!!.visibility = View.GONE
                                }

                            } else {
                                Log.e("DRIVER_TRUE", "MANIBABU")

                                Log.e("SETPICKUP_lll", setpickuploc!!)
                                if (setpickuploc.equals("setpickuploc", ignoreCase = true)) {
                                    Log.e("SETPICKUP_lll", setpickuploc!!)
                                } else {
                                    Log.e("SETPICKUP_lll_no", setpickuploc!!)
                                    //listview.setVisibility(View.VISIBLE)
                                    currentLocation_image!!.visibility = View.GONE
                                }
                            }

                        } else {
                            //listview.setVisibility(GONE)
                        }

                        if (driver_status) {

                            currentLocation_image!!.setVisibility(View.GONE)
                            /***********Design Visibility Views********/
                            for (i in driver_list.indices) {
                                val Dlatitude =
                                    java.lang.Double.parseDouble(driver_list[i].getDriver_lat()!!)
                                val Dlongitude =
                                    java.lang.Double.parseDouble(driver_list[i].getDriver_long()!!)
                                // create marker double Dlatitude = gps.getLatitude();
                                val marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
                                val height = 150
                                val width = 150
                                val bitmapdraw =
                                    resources.getDrawable(R.drawable.dash_car) as BitmapDrawable
                                val b = bitmapdraw.bitmap
                                val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
                                marker.icon(BitmapDescriptorFactory.fromBitmap(b))
                                // marker.rotation(100f)
                                // adding marker
                                mMap!!.addMarker(marker)
                            }
                            //To Draw the Polyline between latlng

                            Log.e(
                                "LOCATION_SOURCE_CHANGE",
                                "$SselectedLatitude--$SselectedLongitude---$DselectedLatitude----$DselectedLongitude"
                            )
                            if (SselectedLatitude.equals("", ignoreCase = true)) {
                                SselectedLatitude = Sselected_latitude
                            }
                            if (SselectedLongitude.equals("", ignoreCase = true)) {
                                SselectedLongitude = Sselected_longitude
                            }
                            Log.e(
                                "LOCATION_SOURCE_CHANGE_ll",
                                "$SselectedLatitude--$SselectedLongitude---$DselectedLatitude----$DselectedLongitude"
                            )
                            // drawRoutePolyline(SselectedLatitude, SselectedLongitude, DselectedLatitude, DselectedLongitude, "")

                        } else {

                            Log.e(
                                "DRIVERSTATELSE",
                                "$driver_status----------$main_response_status---------$category_status"
                            )
                            Log.e(
                                "LOCATION_LAT",
                                "$SselectedLatitude----$Sselected_longitude--$DselectedLatitude-----$DselectedLongitude"
                            )
                            mMap!!.clear()
                            if (SselectedLatitude.equals("", ignoreCase = true)) {
                                SselectedLatitude = Sselected_latitude
                            }
                            if (SselectedLongitude.equals("", ignoreCase = true)) {
                                SselectedLongitude = Sselected_longitude
                            }
                            try {
                                val lat_decimal = java.lang.Double.parseDouble(SselectedLatitude)
                                val lng_decimal = java.lang.Double.parseDouble(SselectedLongitude)
                                val pick_lat_decimal =
                                    java.lang.Double.parseDouble(DselectedLatitude)
                                val pick_lng_decimal =
                                    java.lang.Double.parseDouble(DselectedLongitude)
                                updateGoogleMapTrackRide(
                                    lat_decimal,
                                    lng_decimal,
                                    pick_lat_decimal,
                                    pick_lng_decimal,
                                    "nocars"
                                )
                                println("inside updategoogle1--------------updateGoogleMapTrackRide")
                            } catch (e: Exception) {
                                println("try--------------$e")
                            }
                        }
                    } else {

                    }

                    // SselectedAddress = address
                    dialog.dismiss()

                }

                override fun onErrorListener() {
                    dialog.dismiss()

                }
            })
    }


    private fun ConfirmRideRequest(
        Url: String,
        code: String,
        pickUpDate: String,
        pickup_time: String,
        type: String,
        category: String,
        pickup_location: String,
        pickup_lat: String,
        pickup_lon: String,
        try_value: String,
        destination_location: String,
        destination_lat: String,
        destination_lon: String,
        note_for_driver: String
    ) {

        dialog = Dialog(activity!!)
        dialog.getWindow()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()

        val loading = dialog.findViewById(R.id.custom_loading_textview) as TextView
        loading.text = resources.getString(R.string.action_pleasewait)

        Log.e("DISTANCE_CALCU_CONFIRMRIDE", destination_lat + "" + destination_lon)
        println("--------------Confirm Ride url-------------------$Url")


        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["code"] = code
        jsonParams["pickup_date"] = pickUpDate
        jsonParams["pickup_time"] = pickup_time
        jsonParams["type"] = type
        jsonParams["category"] = category
        jsonParams["subcategory"] = SubCategoryID
        jsonParams["pickup"] = pickup_location
        jsonParams["pickup_lat"] = pickup_lat
        jsonParams["pickup_lon"] = pickup_lon
        jsonParams["ride_id"] = riderId
        jsonParams["goods_type"] = ""
        jsonParams["goods_type_quanity"] = ""
        jsonParams["drop_loc"] = destination_location
        jsonParams["drop_lat"] = destination_lat
        jsonParams["drop_lon"] = destination_lon
        jsonParams["note"] = note_for_driver

        println("---------------destination_location----------$destination_location")
        println("---------------destination_lat----------$destination_lat")
        println("---------------destination_lon----------$destination_lon")
        //}


        println("---------------user_id----------$UserID")
        println("---------------code----------$code")
        println("---------------pickpudate----------$pickUpDate")
        println("---------------pickup_time----------$pickup_time")
        println("---------------type----------$type")
        println("---------------category----------$category")
        println("---------------pickup----------$pickup_location")
        println("---------------pickup_lat----------$pickup_lat")
        println("---------------pickup_lon----------$pickup_lon")
        println("---------------try----------$try_value")
        println("---------------riderId----------$riderId")
        println("---------------note_for_driver----------$note_for_driver")


        mRequest = ServiceRequest(activity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("--------------Confirm Ride reponse-------------------$response")
                    Log.e("response_test", response)
                    var selected_type = ""
                    var Sacceptance = ""
                    var Str_driver_id = ""
                    var Str_driver_name = ""
                    var Str_driver_email = ""
                    var Str_driver_image = ""
                    var Str_driver_review = ""
                    var Str_driver_lat = ""
                    var Str_driver_lon = ""
                    var Str_min_pickup_duration = ""
                    var Str_ride_id = ""
                    var Str_phone_number = ""
                    var Str_vehicle_number = ""
                    var Str_vehicle_model = ""
                    try {
                        val `object` = JSONObject(response)

                        Log.e("response_test_trycahhe", response)
                        if (`object`.length() > 0) {
                            val status = `object`.getString("status")
                            if (status.equals("1", ignoreCase = true)) {
                                val response_object = `object`.getJSONObject("response")
                                Log.e("hemanth_test", response_object.toString())
                                selected_type = response_object.getString("type")
                                Sacceptance = `object`.getString("acceptance")
                                Log.e("Sacceptance", Sacceptance)
                                if (Sacceptance.equals("No", ignoreCase = true)) {
                                    response_time = response_object.getString("response_time")
                                    seconds = response_time.toInt()

                                    println("Seconds $seconds")

                                    mHandler = Handler()
                                    mHandler.post(mRunnable)

                                }
                                riderId = response_object.getString("ride_id")

                                if (Sacceptance.equals("Yes", ignoreCase = true)) {
                                    val driverObject =
                                        response_object.getJSONObject("driver_profile")

                                    Str_driver_id = driverObject.getString("driver_id")
                                    Str_driver_name = driverObject.getString("driver_name")
                                    Str_driver_email = driverObject.getString("driver_email")
                                    Str_driver_image = driverObject.getString("driver_image")
                                    Str_driver_review = driverObject.getString("driver_review")
                                    Str_driver_lat = driverObject.getString("driver_lat")
                                    Str_driver_lon = driverObject.getString("driver_lon")
                                    Str_min_pickup_duration =
                                        driverObject.getString("min_pickup_duration")

                                    Log.e("ride_id_test", riderId)
                                    Str_ride_id = driverObject.getString("ride_id")
                                    Str_phone_number = driverObject.getString("phone_number")
                                    Str_vehicle_number = driverObject.getString("vehicle_number")
                                    Str_vehicle_model = driverObject.getString("vehicle_model")
                                }
                                Log.e("selected_type", selected_type)
                                if (selected_type.equals("1", ignoreCase = true)) {
                                    val mDialog = PkDialog(activity!!)
                                    mDialog.setDialogTitle(activity!!.getResources().getString(R.string.action_success))
                                    mDialog.setDialogMessage(activity!!.getResources().getString(R.string.ridenow_label_confirm_success))
                                    mDialog.setPositiveButton(
                                        resources.getString(R.string.action_ok),
                                        View.OnClickListener {
                                            mDialog.dismiss()
                                            //destination_address!!.setText("")
                                            startTimer()
                                            mMap!!.clear()
                                            cd = ConnectionDetector(activity!!)
                                            isInternetPresent = cd!!.isConnectingToInternet
                                            val animFadeOut = AnimationUtils.loadAnimation(
                                                activity,
                                                R.anim.fade_out
                                            )
                                            currentLocation_image!!.isClickable = true
                                            currentLocation_image!!.visibility = View.GONE
                                            drawer_layout!!.isEnabled = true
                                            NavigationDrawer.enableSwipeDrawer()
                                        })
                                    mDialog.show()

                                } else if (selected_type.equals("0", ignoreCase = true)) {
                                    Log.e("selected_type 1", selected_type)
                                    if (Sacceptance.equals("Yes", ignoreCase = true)) {


                                    } else {

                                    }
                                }
                            } else {
                                val Sresponse = `object`.getString("response")
                                Alert(
                                    activity!!.getResources().getString(R.string.alert_label_title),
                                    Sresponse
                                )
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.action_ok), View.OnClickListener {
            mDialog.dismiss()
            startTimer()
            cd = ConnectionDetector(activity!!)
            isInternetPresent = cd!!.isConnectingToInternet
            /* if (isInternetPresent) {
                 postRequest_recentRides(home_recent_reides_url)
             }*/
            mMap!!.clear()
            onMapReady(mMap)
            drawer_layout!!.visibility = View.VISIBLE
            drawer_layout!!.isClickable = true
            drawer_layout!!.isClickable = true
            //book_cardview_source_address_layout!!.setVisibility(View.GONE)
        })
        mDialog.show()
    }


    fun createBitmapFromLayout(view: View): Bitmap {

        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)

        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap =
            Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)

        return bitmap
    }

    fun timeOfDay(): String {
        var s = ""
        val c = Calendar.getInstance()
        val timeOfDay = c.get(Calendar.HOUR_OF_DAY)
        if (timeOfDay >= 0 && timeOfDay < 12) {
            s = "Good Morning"
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            s = "Good Afternoon"
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            s = "Good Evening"
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            s = "Good Night"
        }
        return s
    }


    //-------------------Delete Ride Post Request----------------

    private fun DeleteRideRequest(Url: String) {

        println("--------------Timer Delete Ride url-------------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["ride_id"] = riderId

        dialog = Dialog(activity!!)
        dialog.getWindow()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        val loading = dialog.findViewById(R.id.custom_loading_textview) as TextView
        loading.text = resources.getString(R.string.action_pleasewait)

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Timer Delete Ride reponse-------------------$response")
                    dialog.dismiss()
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {
                            val status = `object`.getString("status")
                            val response_value = `object`.getString("response")
                            if (status.equals("1", ignoreCase = true)) {

                                ll_slidetocancel!!.visibility = View.GONE
                                mHandler.removeCallbacks(mRunnable)
                                val mDialog = PkDialog(activity!!)
                                mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                                mDialog.setDialogMessage(response_value)
                                mDialog.setPositiveButton(
                                    getResources().getString(R.string.action_ok),
                                    View.OnClickListener {
                                        mDialog.dismiss()

                                        mMap!!.clear()
                                        onMapReady(mMap)
                                        drawer_layout!!.setVisibility(View.VISIBLE)
                                        drawer_layout!!.setClickable(true)
                                        currentLocation_image!!.setClickable(true)
                                        currentLocation_image!!.setVisibility(View.VISIBLE)
                                        drawer_layout!!.setEnabled(true)
                                        NavigationDrawer.enableSwipeDrawer()
                                        ll_source_address!!.visibility = View.VISIBLE
                                        ll_fav_and_ridetypes.visibility = View.VISIBLE
                                        ll_driver_on_way_main!!.visibility = View.GONE
                                        ll_source_address!!.visibility = View.VISIBLE
                                        tv_destination_search_address!!.text = ""
                                        slidetocancel!!.resetSlider()
                                        et_notefordriver!!.text.clear()
                                        et_notefordriver!!.setText("")
                                        tv_notedriver_enterd!!.text = ""
                                        notefordriver_stg = ""
                                        tv_welcome_name!!.text = timeOfDay() + ", " + UserName

                                    })
                                mDialog.show()

                            } else {
                                Toast.makeText(activity!!, response_value, Toast.LENGTH_SHORT)
                                    .show()
                                val mDialog = PkDialog(activity!!)
                                mDialog.setDialogTitle(getResources().getString(R.string.action_error))
                                mDialog.setDialogMessage(response_value)
                                mDialog.setPositiveButton(
                                    getResources().getString(R.string.action_ok),
                                    View.OnClickListener {
                                        mDialog.dismiss()
                                        ll_driver_on_way_main!!.visibility = View.VISIBLE
                                        ll_include_editride!!.visibility = View.GONE

                                    })
                                mDialog.show()

                                //toast!!.setText(response_value)
                                //toast!!.show()
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                    ll_slidetocancel!!.visibility = View.VISIBLE
                    slidetocancel!!.visibility = View.VISIBLE
                }
            })
    }


    override fun onDestroy() {
        activity!!.unregisterReceiver(updateReciver)
        activity!!.unregisterReceiver(logoutReciver)
        mHandler.removeCallbacks(mRunnable)
        stoptimertask()
        super.onDestroy()
    }


    inner class RefreshReceiver : BroadcastReceiver() {
        @SuppressLint("NewApi")
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_Arrived_Driver") {
                println("triparrived----------------------")
                try {
                    mMap!!.clear()

                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(toPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_sourceloc))
                    )
                    curentDriverMarker = mMap!!.addMarker(
                        MarkerOptions()
                            .position(toPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    )

                    showNotification(
                        resources.getString(R.string.action_driver_arrived),
                        BUNDLE_NOTIFICATION_ID.toString()
                    )
                    tv_include_driver_found!!.text =
                        resources.getString(R.string.action_driver_arrived)
                    tv_driver_estimated_time!!.text = "Please be there in 5 Minutes"
                    tv_include_pick_up_addr!!.text = source_Location
                    tv_include_drop_addr!!.text = destination_Location

                    /*tts = TextToSpeech(context, this@Fragment_HomePage)
                    voice_stg = "arrived"*/

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else if (intent.action == "com.package.ACTION_CLASS_TrackYourRide_REFRESH_BeginTrip") {
                println("tripbegin----------------------")

                try {

                    estimated_time = intent.getStringExtra("estimated_time")!!
                    pickup_date_time = intent.getStringExtra("pickup_date_time")!!

                    val unit_price = String.format("%.2f", estimated_time.toFloat())
                    tv_pick_up_time!!.setText(editDateFormat(pickup_date_time))
                    tv_drop_time!!.setText(unit_price + " min")
                    tv_include_driver_found!!.text =
                        resources.getString(R.string.action_enjy_your_ride)
                    if (estimated_time != null) {
                        tv_driver_estimated_time!!.text = "Estimated  Time " + unit_price + " Mins"
                    }

                    tv_include_pick_up_addr!!.text = source_Location
                    tv_include_drop_addr!!.text = destination_Location

                    ll_cancel_ride!!.visibility = View.GONE

                    /*WelCome Speech */
                    tts = TextToSpeech(context, this@Fragment_HomePage)
                    voice_stg = "begin_trip"
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else if (intent.action == "com.package.track.ACTION_CLASS_TrackYourRide_REFRESH_UpdateDriver") {


                /*  println("updateDriverOnMap-----------Track Ride location---------")
                  val lat = intent.extras!!.get("latitude") as String
                  val lng = intent.extras!!.get("longitude") as String

                  val ride_id = intent.extras!!.get("ride_id") as String
                  try {
                      val lat_decimal = java.lang.Double.parseDouble(lat)
                      val lng_decimal = java.lang.Double.parseDouble(lng)

                      println("updateDriverOnMap-----------updateing location---------")
                      updateDriverOnMap(lat_decimal, lng_decimal)
                  } catch (e: Exception) {
                  }*/

            }

            if (intent.extras != null && intent.extras!!.containsKey("drop_lat") && intent.extras!!.containsKey(
                    "drop_lng"
                )
            ) {
                val lat = intent.extras!!.get("drop_lat") as String
                val lng = intent.extras!!.get("drop_lng") as String
                val pickUp_lat = intent.extras!!.get("pickUp_lat") as String
                val pickUp_lng = intent.extras!!.get("pickUp_lng") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)
                    val pick_lat_decimal = java.lang.Double.parseDouble(pickUp_lat)
                    val pick_lng_decimal = java.lang.Double.parseDouble(pickUp_lng)

                    Log.e(
                        "UPDATELATLNG_BEFORE",
                        "" + lat_decimal + "-----" + lng_decimal + "-----" + pick_lat_decimal + "-----" + pick_lng_decimal
                    )

                    updateMapTrackRide(lat_decimal, lng_decimal, pick_lat_decimal, pick_lng_decimal)
                    println("updateMapTrackRide--------------")
                } catch (e: Exception) {
                    println("try--------------$e")
                }

            }
            println("out else--------------")
            if (intent.extras != null && intent.extras!!.containsKey(Iconstant.isContinousRide)) {
                val lat = intent.extras!!.get("latitude") as String
                val lng = intent.extras!!.get("longitude") as String

                val ride_id = intent.extras!!.get("ride_id") as String
                try {
                    val lat_decimal = java.lang.Double.parseDouble(lat)
                    val lng_decimal = java.lang.Double.parseDouble(lng)

                    println("updateDriverOnMap-----------updateing location---------")
                    updateDriverOnMap(lat_decimal, lng_decimal)
                } catch (e: Exception) {
                }

            }
        }
    }


    private fun updateDriverOnMap(lat_decimal: Double, lng_decimal: Double) {
        val latLng = LatLng(lat_decimal, lng_decimal)
        if (mLatLngInterpolator == null) {
            mLatLngInterpolator = LatLngInterpolator.Linear()
        }
        Log.d("tess", "inside run ")
        if (curentDriverMarker != null) {
            Log.d("UPDATEDRIVERONMAP", "inside run IF")
            MarkerAnimation.animateMarkerToGB(curentDriverMarker, latLng, mLatLngInterpolator)
            val zoom = mMap!!.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
            if (isFirstPosition) {
                startPositionn = curentDriverMarker!!.getPosition();
                curentDriverMarker!!.setAnchor(0.5f, 0.5f);
                mMap!!.moveCamera(
                    CameraUpdateFactory
                        .newCameraPosition
                            (
                            CameraPosition.Builder()
                                .target(startPositionn)
                                .zoom(15.5f)
                                .build()
                        )
                );

                isFirstPosition = false;

            } else {
                endPosition = LatLng(latLng.latitude, latLng.longitude);
                if ((startPositionn!!.latitude != endPosition!!.latitude) || (startPositionn!!.longitude != endPosition!!.longitude)) {
                    Log.e("asdhashd", "NOT SAME");
                    startBikeAnimation(startPositionn!!, endPosition!!);
                } else {
                    Log.e("dfsdfsd", "SAMME");
                }
            }
        } else {
            Log.d("UPDATEDRIVERONMAP", "inside run ELSE")
            mLatLngInterpolator = LatLngInterpolator.Linear()
            curentDriverMarker = mMap!!.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    .anchor(0.5f, 0.5f)
                    .flat(true)
            )
            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
        }

    }


    //Method for finding bearing between two points
    private fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        return 1f
    }


    companion object {
        lateinit var trackyour_ride_class: Fragment_HomePage
        private var curentDriverMarker: Marker? = null
        private val movingMarker: Marker? = null
        internal val REQUEST_LOCATION = 199
        internal var latLngInterpolator: LatLngInterpolator = LatLngInterpolator.Spherical()
        fun updateMap(latLng: LatLng) {
            MarkerAnimation.animateMarkerToICS(movingMarker, latLng, latLngInterpolator)
        }
    }

    private fun startBikeAnimation(start: LatLng, end: LatLng) {
        val handler = Handler()
        Log.e("Startbikeanimation", "Entered to me")
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.duration = 3000 // duration 3 second
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.addUpdateListener { animation ->
            try {
                val v = animation.animatedFraction

                lng = v * end!!.longitude + (1 - v) * start!!.longitude
                lat = v * end!!.latitude + (1 - v) * start!!.latitude

                val newPos = LatLng(lat, lng)
                curentDriverMarker!!.setPosition(newPos)
                curentDriverMarker!!.setAnchor(0.5f, 0.5f)
                curentDriverMarker!!.rotation = getBearing(start, end)
                mMap!!.moveCamera(
                    CameraUpdateFactory
                        .newCameraPosition(
                            CameraPosition.Builder()
                                .target(newPos)
                                .zoom(15.5f)
                                .build()
                        )
                )
                startPositionn = curentDriverMarker!!.getPosition();
            } catch (ex: Exception) {
                //I don't care atm..
            }
        }
        valueAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)

            }
        })
        valueAnimator.start()
//        firsttimepolyline = false
        Log.e("currentLocationn", currentLocationn.toString() + "-----" + startPositionn.toString())
        val runnable = Runnable {
            currentLocationn = start
            if (currentLocationn != null) {
                val getRoute = TrackRouteTask()
                try {
                    getRoute.setToAndFromLocationTrackride(dropLatLng!!, pickUpLatLng!!)
                    getRoute.execute()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        handler.postDelayed(runnable, 3000)
    }

    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    private inner class TrackRouteTask : AsyncTask<String, Void, String>() {
        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        private var endLocation: LatLng? = null
        private var currentLocation: LatLng? = null
        fun setToAndFromLocationTrackride(currentLocation: LatLng, endLocation: LatLng) {
            Log.e("UPDATELATLNG_INSIDEDROP", "" + currentLocation + "-----" + endLocation)
            this.currentLocation = currentLocation
            this.endLocation = endLocation
        }

        override fun onPreExecute() {
            Log.e("GETDROPROUTE", "GetDropRouteTask")
            currentLocationn = currentLocation
        }

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(
                endLocation,
                currentLocation,
                GMapV2GetRouteDirection.MODE_DRIVING
            )
            response = "Success"
            return response
        }

        override fun onPostExecute(result: String) {
            if (polyline != null) {
                polyline!!.remove()
            }
            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine =
                        PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }

                    // Adding ride_detail_trackride on the map
                    if (firsttimepolyline!!) {
                        // markerOptions!!.position(fromPosition!!)
                        markerOptions!!.draggable(true)
                        //mMap.addMarker(markerOptions);
                        firsttimepolyline = false
                        val markerWidth = 350
                        val markerHeight = 250
                        // destination marker
                        val destn_marker_view =
                            (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                                R.layout.custom_drop_marker,
                                null
                            )
                        val tv_marker_drop =
                            destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                        // tv_marker_drop.text = Sselected_DestinationLocation!!
                        val destn_bitmap = createBitmapFromLayout(destn_marker_view)
                        val destn_small_bitmap =
                            Bitmap.createScaledBitmap(
                                destn_bitmap,
                                markerWidth,
                                markerHeight,
                                false
                            )

                        mMap!!.addMarker(
                            MarkerOptions()
                                .position(currentLocation!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_sourceloc))
                        )

                        /*mMap!!.addMarker(
                            MarkerOptions()
                                .position(toPosition!!)
                                .icon(BitmapDescriptorFactory.fromBitmap(destn_bitmap))
                        )*/

                        curentDriverMarker = mMap!!.addMarker(
                            MarkerOptions()
                                .position(endLocation!!)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                        )
                        //Show path in
                        val builder = LatLngBounds.Builder()
                        builder.include(toPosition!!)
                        builder.include(toPosition!!)
                        val bounds = builder.build()
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                    }
                    polyline = mMap!!.addPolyline(rectLine)
                    markerOptions!!.position(toPosition!!)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun updateMapTrackRide(
        lat_decimal: Double,
        lng_decimal: Double,
        pick_lat_decimal: Double,
        pick_lng_decimal: Double
    ) {
        if (mMap != null) {
            mMap!!.clear()
        }
        Log.e(
            "UPDATELATLNG_INSIDE",
            "" + lat_decimal + "-----" + lng_decimal + "-----" + pick_lat_decimal + "-----" + pick_lng_decimal
        )
        dropLatLng = LatLng(lat_decimal, lng_decimal)
        pickUpLatLng = LatLng(pick_lat_decimal, pick_lng_decimal)

        Log.e("UPDATELATLNG_INSIDELATLNG", "" + dropLatLng + "-----" + pickUpLatLng)

        val draw_route_asyncTask = TrackRouteTask()
        draw_route_asyncTask.setToAndFromLocationTrackride(dropLatLng!!, pickUpLatLng!!)
        draw_route_asyncTask.execute()
    }

    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            // LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, activity!!)
        }
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()

    }

    override fun onResume() {
        super.onResume()
        ChatService.startUserAction(activity!!)
        /* try{
             ChatService.startUserAction(activity!!)
         }catch (e:Exception){
             e.printStackTrace()
         }*/

        startLocationUpdates()
    }

    //---------------AsyncTask to Draw PolyLine Between Two Point--------------
    inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document
        override fun onPreExecute() {
            Log.e("GETDROPROUTE", "GetRouteTask" + "" + fromPosition)
        }

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(
                toPosition,
                fromPosition,
                GMapV2GetRouteDirection.MODE_DRIVING
            )
            response = "Success"
            return response
        }

        override fun onPostExecute(result: String) {
            if (result.equals("Success", ignoreCase = true)) {
                // mMap.clear();
                try {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine =
                        PolylineOptions().width(10f).color(resources.getColor(R.color.blue_text_bg))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }
                    // Adding ride_detail_trackride on the map
                    val markerWidth = 350
                    val markerHeight = 250


                    val pickup_marker_view =
                        (activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                            R.layout.custom_pickup_marker,
                            null
                        )
                    val tv_marker_pickup =
                        pickup_marker_view.findViewById<TextView>(R.id.tv_marker_pickup)
                    tv_marker_pickup.text = address!!
                    val pickup_bitmap = createBitmapFromLayout(pickup_marker_view)
                    markerOptions!!.position(fromPosition!!)
                    markerOptions!!.draggable(true)
                    //mMap.addMarker(markerOptions);
                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(fromPosition!!)
                            .icon(BitmapDescriptorFactory.fromBitmap(pickup_bitmap))
                    )
                    curentDriverMarker = mMap!!.addMarker(
                        MarkerOptions()
                            .position(toPosition!!)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    )
                    //Show path in
                    val builder = LatLngBounds.Builder()
                    builder.include(fromPosition!!)
                    builder.include(toPosition!!)
                    val bounds = builder.build()
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 162))
                    markerOptions!!.position(toPosition!!)
                    polyline = mMap!!.addPolyline(rectLine)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

    override fun onInit(status: Int) {

        if (status == TextToSpeech.SUCCESS) {

            val result = tts.setLanguage(Locale.US)

            // tts.setPitch(5); // set pitch level

            // tts.setSpeechRate(2); // set speech speed rate

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language is not supported")
            } else {
                //btnSpeak.setEnabled(true)

                speakOut()
            }

        } else {
            Log.e("TTS", "Initilization Failed")
        }

    }

    private fun speakOut() {
        var text = ""
        if (voice_stg.equals("arrived")) {
            text = "Hey " + UserName + " Driver Arrived"

        } else if (voice_stg.equals("begin_trip")) {
            text = "Hi " + UserName + " Welcome to Ride hub"

        }

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null)
        /*tts.stop()
        tts.shutdown()*/

    }


    private fun getLocationFromAddress(context: Context, strAddress: String) {

        val coder = Geocoder(context)
        val p1: LatLng

        try {
            address_new = coder.getFromLocationName(strAddress, 1)
            val location = address_new[0]
            location.latitude
            location.longitude

            p1 = LatLng(location.getLatitude(), location.getLongitude())

            Selected_SourceLocation = address
            setpickuploc = "setpickuploc"

            Log.e("location_new", p1.toString())

            val lat_decimal = location.getLatitude()
            val lng_decimal = location.getLongitude()
            val pick_lat_decimal = MyCurrent_lat
            val pick_lng_decimal = MyCurrent_long
            //stoptimertask()
            updateGoogleMapTrackRide(
                pick_lat_decimal,
                pick_lng_decimal,
                lat_decimal,
                lng_decimal,
                ""
            )

            SselectedLatitude = pick_lat_decimal.toString()
            SselectedLongitude = pick_lng_decimal.toString()
            DselectedLatitude = lat_decimal.toString()
            DselectedLongitude = lng_decimal.toString()


            PostRequest(
                Iconstant.BookMyRide_url,
                pick_lat_decimal,
                pick_lng_decimal,
                lat_decimal,
                lng_decimal
            )

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    private fun CloseKeyboard(edittext: EditText) {
        val `in` = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }


    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("DATETIME", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }


    @SuppressLint("WrongConstant")
    private fun showNotification(
        message: String/*, intent: Intent*/,
        bundle_notification_id: String
    ) {

        val notificationManager =
            context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val groupChannel = NotificationChannel(
                BUNDLE_CHANNEL_ID,
                BUNDLE_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(groupChannel)
        }
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        val resultPendingIntent =
            PendingIntent.getActivity(
                context,
                BUNDLE_NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

        val notificationBuilder = NotificationCompat.Builder(activity!!, BUNDLE_CHANNEL_ID)
            .setGroup(bundle_notification_id)
            .setGroupSummary(true)
            .setSmallIcon(R.mipmap.ic_launcher_icon_round)
            .setContentTitle("Alert From Driver")
            .setContentText(message)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContentIntent(resultPendingIntent)

        notificationManager.notify(BUNDLE_NOTIFICATION_ID, notificationBuilder.build())
    }


    fun getWalletAmount(url: String) {

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        mRequest = ServiceRequest(context)
        mRequest!!.makeServiceRequest(
            url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("-------------amountWallet_Response----------------$response")


                    val Sstatus: String
                    try {
                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")

                            if (response_object.length() > 0) {

                                val currency_stg = response_object.getString("currency")
                                val current_balance_stg =
                                    response_object.getString("current_balance")


                                session!!.createWalletAmount(current_balance_stg)
                                Log.e("lalallal", current_balance_stg)
                            }
                        }
                    } catch (e: Exception) {

                    }

                }

                override fun onErrorListener() {
                }
            })
    }


}



