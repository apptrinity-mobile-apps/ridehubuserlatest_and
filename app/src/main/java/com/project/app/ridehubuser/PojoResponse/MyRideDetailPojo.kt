package com.project.app.ridehubuser.PojoResponse

class MyRideDetailPojo {

    private var carType: String? = null
    private var carNo: String? = null
    private var rideStatus: String? = null
    private var address: String? = null
    private var dropaddress: String? = null
    private var pickup: String? = null
    private var drop: String? = null
    private var rideDistance: String? = null
    private var timeTaken: String? = null
    private var waitTime: String? = null
    private var totalBill: String? = null
    private var totalPaid: String? = null
    private var couponDiscount: String? = null
    private var walletUsuage: String? = null
    private var tip_amount: String? = null
    private var currrencySymbol: String? = null
    private var rideId: String? = null
    private var displayStatus: String? = null
    private var doCancelAction: String? = null
    private var pay_status: String? = null
    private var LocationLat: String? = null
    private var LocationLong: String? = null
    private var doTrackAction: String? = null
    private var isFavLocation: String? = null
    private var distance_unit: String? = null
    private var vehicle_number: String? = null
    private var payment_type: String? = null
    private var points_usage: String? = null
    private var pickup_date: String? = null
    private var drop_date: String? = null
    private var grand_bill: String? = null
    private var total_bill: String? = null
    private var user_rating: String? = null
    private var driver_rating: String? = null
    private var ride_begin_time: String? = null
    private var ride_end_time: String? = null
    private var driver_name: String? = null
    private var driver_image: String? = null
    private var vehicle_model: String? = null
    private var image: String? = null

    fun getImage(): String? {
        return image
    }

    fun setImage(image: String) {
        this.image = image
    }



    fun getCarType(): String? {
        return carType
    }

    fun setCarType(carType: String) {
        this.carType = carType
    }

    fun getVehicle_model(): String? {
        return vehicle_model
    }

    fun setVehicle_model(vehicle_model: String) {
        this.vehicle_model = vehicle_model
    }

    fun getCarNo(): String? {
        return carNo
    }

    fun setCarNo(carNo: String) {
        this.carNo = carNo
    }

    fun getRideStatus(): String? {
        return rideStatus
    }

    fun setRideStatus(rideStatus: String) {
        this.rideStatus = rideStatus
    }

    fun getAddress(): String? {
        return address
    }

    fun setAddress(address: String) {
        this.address = address
    }

    fun getDropAddress(): String? {
        return dropaddress
    }

    fun setDropAddress(dropaddress: String) {
        this.dropaddress = dropaddress
    }

    fun getPickup(): String? {
        return pickup
    }

    fun setPickup(pickup: String) {
        this.pickup = pickup
    }

    fun getDrop(): String? {
        return drop
    }

    fun setDrop(drop: String) {
        this.drop = drop
    }

    fun getRideDistance(): String? {
        return rideDistance
    }

    fun setRideDistance(rideDistance: String) {
        this.rideDistance = rideDistance
    }

    fun getTimeTaken(): String? {
        return timeTaken
    }

    fun setTimeTaken(timeTaken: String) {
        this.timeTaken = timeTaken
    }

    fun getWaitTime(): String? {
        return waitTime
    }

    fun setWaitTime(waitTime: String) {
        this.waitTime = waitTime
    }

    fun getTotalBill(): String? {
        return totalBill
    }

    fun setTotalBill(totalBill: String) {
        this.totalBill = totalBill
    }


    fun getTotal_Bill(): String? {
        return total_bill
    }

    fun setTotal_Bill(total_bill: String) {
        this.total_bill = total_bill
    }

    fun getTotalPaid(): String? {
        return totalPaid
    }

    fun setTotalPaid(totalPaid: String) {
        this.totalPaid = totalPaid
    }

    fun getCouponDiscount(): String? {
        return couponDiscount
    }

    fun setCouponDiscount(couponDiscount: String) {
        this.couponDiscount = couponDiscount
    }

    fun getWalletUsuage(): String? {
        return walletUsuage
    }

    fun setWalletUsuage(walletUsuage: String) {
        this.walletUsuage = walletUsuage
    }

    fun getCurrrencySymbol(): String? {
        return currrencySymbol
    }

    fun setCurrrencySymbol(currrencySymbol: String) {
        this.currrencySymbol = currrencySymbol
    }

    fun getRideId(): String? {
        return rideId
    }

    fun setRideId(rideId: String) {
        this.rideId = rideId
    }

    fun getDisplayStatus(): String? {
        return displayStatus
    }

    fun setDisplayStatus(displayStatus: String) {
        this.displayStatus = displayStatus
    }

    fun getDoCancelAction(): String? {
        return doCancelAction
    }

    fun setDoCancelAction(doCancelAction: String) {
        this.doCancelAction = doCancelAction
    }

    fun getPay_status(): String? {
        return pay_status
    }

    fun setPay_status(pay_status: String) {
        this.pay_status = pay_status
    }

    fun getLocationLat(): String? {
        return LocationLat
    }

    fun setLocationLat(locationLat: String) {
        LocationLat = locationLat
    }

    fun getLocationLong(): String? {
        return LocationLong
    }

    fun setLocationLong(locationLong: String) {
        LocationLong = locationLong
    }

    fun getDoTrackAction(): String? {
        return doTrackAction
    }

    fun setDoTrackAction(doTrackAction: String) {
        this.doTrackAction = doTrackAction
    }

    fun getIsFavLocation(): String? {
        return isFavLocation
    }

    fun setIsFavLocation(isFavLocation: String) {
        this.isFavLocation = isFavLocation
    }

    fun getTip_amount(): String? {
        return tip_amount
    }

    fun setTip_amount(tip_amount: String) {
        this.tip_amount = tip_amount
    }

    fun getDistance_unit(): String? {
        return distance_unit
    }

    fun setDistance_unit(distance_unit: String) {
        this.distance_unit = distance_unit
    }

    fun getVehicle_number(): String? {
        return vehicle_number
    }

    fun setVehicle_number(vehicle_number: String) {
        this.vehicle_number = vehicle_number
    }

    fun getPayment_type(): String? {
        return payment_type
    }

    fun setPayment_type(payment_type: String) {
        this.payment_type = payment_type
    }

    fun getPoints_usage(): String? {
        return points_usage
    }

    fun setPoints_usage(points_usage: String) {
        this.points_usage = points_usage
    }

    fun getDrop_date(): String? {
        return drop_date
    }

    fun setDrop_date(drop_date: String) {
        this.drop_date = drop_date
    }

    fun getPickup_date(): String? {
        return pickup_date
    }

    fun setPickup_date(pickup_date: String) {
        this.pickup_date = pickup_date
    }
    fun getGrand_bill(): String? {
        return grand_bill
    }

    fun setGrand_bill(grand_bill: String) {
        this.grand_bill = grand_bill
    }

    fun getUser_rating(): String? {
        return user_rating
    }

    fun setUser_rating(user_rating: String) {
        this.user_rating = user_rating
    }

    fun getDriver_rating(): String? {
        return driver_rating
    }

    fun setDriver_rating(driver_rating: String) {
        this.driver_rating = driver_rating
    }


    fun getRidebegin_time(): String? {
        return ride_begin_time
    }

    fun setRidebegin_time(ride_begin_time: String) {
        this.ride_begin_time = ride_begin_time
    }

    fun getRideend_time(): String? {
        return ride_end_time
    }

    fun setRideend_time(ride_end_time: String) {
        this.ride_end_time = ride_end_time
    }



    fun setDriver_name(driver_name: String) {
        this.driver_name = driver_name
    }

    fun getDriver_name(): String? {
        return driver_name
    }

    fun setDriver_image(driver_image: String) {
        this.driver_image = driver_image
    }

    fun getDriver_image(): String? {
        return driver_image
    }



}