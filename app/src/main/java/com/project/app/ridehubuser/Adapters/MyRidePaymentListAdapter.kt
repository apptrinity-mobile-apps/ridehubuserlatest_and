package com.project.app.ridehubuser.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.project.app.ridehubuser.PojoResponse.PaymentListPojo
import com.project.app.ridehubuser.R
import java.util.*

class MyRidePaymentListAdapter(private val context: Context, private val data: ArrayList<PaymentListPojo>) : BaseAdapter() {
    private val mInflater: LayoutInflater

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        var reason: TextView? = null
        var image_pay_type: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.myride_payment_list_single, parent, false)
            holder = ViewHolder()
            holder?.reason = view.findViewById(R.id.myride_payment_list_textview) as TextView
            holder?.image_pay_type = view.findViewById(R.id.iv_payment_type) as ImageView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        if(data[position].getPaymentName().equals("Cash")){
            holder?.image_pay_type!!.setImageDrawable(context.resources.getDrawable(R.drawable.wallet_money_green))
        }else if(data[position].getPaymentName().equals("Swipe in cab")){
            holder?.image_pay_type!!.setImageDrawable(context.resources.getDrawable(R.drawable.wallet_card))
        }else{
            holder?.image_pay_type!!.setImageDrawable(context.resources.getDrawable(R.drawable.wallet_dollar_blue))
        }


        holder?.reason!!.setText(data[position].getPaymentName())
        return view
    }
}