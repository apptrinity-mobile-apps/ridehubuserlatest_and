package com.project.app.ridehubuser.Activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.countrycodepicker.CountryPicker
import com.countrycodepicker.CountryPickerListener
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.facebook.Util
import com.project.app.mylibrary.gps.GPSTracker
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.mylibrary.xmpp.ChatService
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.CountryDialCode
import com.project.app.ridehubuser.Utils.ImageFilePath
import com.project.app.ridehubuser.Utils.SessionManager
import com.project.app.ridehubuser.iconstant.Iconstant
import com.squareup.picasso.Picasso
import indo.com.ridehub_lyft_uber.HockeyApp.FragmentActivityHockeyApp
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

// TODO add services

class ProfilePageActivity : FragmentActivityHockeyApp() {

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var context: Context? = null
    private var session: SessionManager? = null
    private var back: ImageView? = null
    private var profile_icon: ImageView? = null
    private var UserID = ""
    private var UserName = ""
    private var UserMobileno = ""
    private var UserCountyCode = ""
    private var UserprofileImage = ""
    private var UserEmail = ""
    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog

    lateinit var iv_edit_profile_image: ImageView
    lateinit var cv_edit_profile_image: CardView
    lateinit var ll_first_name: LinearLayout
    lateinit var ll_last_name: LinearLayout
    lateinit var ll_phone_number: LinearLayout
    lateinit var ll_email: LinearLayout
    lateinit var ll_password: LinearLayout

    lateinit var tv_first_name: TextView
    lateinit var tv_last_name: TextView
    lateinit var tv_email: TextView
    lateinit var tv_password: TextView

    lateinit var ll_edit_first_name: LinearLayout
    lateinit var ll_edit_password: LinearLayout
    lateinit var ll_edit_last_name: LinearLayout
    lateinit var ll_edit_email: LinearLayout
    lateinit var ll_edit_phone: LinearLayout
    lateinit var iv_password_close: ImageView
    lateinit var iv_first_name_close: ImageView
    lateinit var iv_last_name_close: ImageView
    lateinit var iv_email_close: ImageView
    lateinit var iv_phone_close: ImageView
    lateinit var ll_save_last_name: LinearLayout
    lateinit var ll_save_first_name: LinearLayout
    lateinit var ll_save_password: LinearLayout
    lateinit var ll_save_email: LinearLayout
    lateinit var ll_save_phone: LinearLayout

    lateinit var tv_edit_phone_code: TextView
    lateinit var et_edit_phone: EditText
    lateinit var et_edit_first_name: EditText
    lateinit var et_edit_last_name: EditText
    lateinit var et_edit_old_password: EditText
    lateinit var et_edit_new_password: EditText
    lateinit var et_edit_re_password: EditText
    lateinit var et_edit_email: EditText
    lateinit var Str_countyCode: String
    lateinit var firstname_dialog: Dialog
    lateinit var lastname_dialog: Dialog
    lateinit var email_dialog: Dialog
    lateinit var phone_dialog: Dialog
    lateinit var password_dialog: Dialog

    lateinit var picker: CountryPicker
    private var gpsTracker: GPSTracker? = null

    private val editorWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable) {
            if (et_edit_new_password.text.isNotEmpty()) {
                et_edit_new_password.error = null
            }
            if (et_edit_re_password.text.isNotEmpty()) {
                et_edit_re_password.error = null
            }
            if (et_edit_old_password.text.isNotEmpty()) {
                et_edit_old_password.error = null
            }
        }
    }
    val REQUEST_IMAGE = 100
    lateinit var bitmap_data: Bitmap
    lateinit var bytearray_img: ByteArray
    lateinit var encodedImage: String
    var imageFileName = "test"
    var MY_CAMERA_REQUEST_CODE = 100

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_account)
        context = applicationContext
        initialize()
        lastname_dialog = Dialog(this@ProfilePageActivity)

        firstname_dialog = Dialog(this@ProfilePageActivity)
        phone_dialog = Dialog(this@ProfilePageActivity)
        email_dialog = Dialog(this@ProfilePageActivity)
        password_dialog = Dialog(this@ProfilePageActivity)

        lastname_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        firstname_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        phone_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        email_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        password_dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)

        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()

        //Start XMPP Chat Service
        try {
            ChatService.startUserAction(this@ProfilePageActivity)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (gpsTracker!!.canGetLocation() && gpsTracker!!.isgpsenabled()) {

            val MyCurrent_lat = gpsTracker!!.getLatitude()
            val MyCurrent_long = gpsTracker!!.getLongitude()

            val geocoder = Geocoder(this, Locale.getDefault())
            try {
                val addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1)
                if (addresses != null && addresses.isNotEmpty()) {

                    val Str_getCountryCode = addresses[0].countryCode
                    if (Str_getCountryCode.isNotEmpty() && Str_getCountryCode != null && Str_getCountryCode != "null") {
                        Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode)

                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        back!!.setOnClickListener {

            val intent = Intent(this, NavigationDrawer::class.java)
            startActivity(intent)
            // onBackPressed()
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finish()
        }

        // To open corresponding dialog
        ll_first_name.setOnClickListener {


            val view = LayoutInflater.from(this@ProfilePageActivity)
                .inflate(R.layout.edit_first_name, null)
            val Bt_Submit = view.findViewById(R.id.ll_save_first_name) as LinearLayout
            et_edit_first_name = view.findViewById(R.id.et_edit_first_name) as EditText
            val btn_close = view.findViewById(R.id.iv_first_name_close) as ImageView
            firstname_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            firstname_dialog!!.setContentView(view)
            firstname_dialog!!.show()
            //completejob_dialog!!.setView(view).show()
            Bt_Submit.setOnClickListener {
                if (et_edit_first_name.text.toString().isEmpty()) {
                    errorEdit(
                        et_edit_first_name,
                        resources.getString(R.string.register_label_alert_first_name)
                    )
                } else {
                    if (isInternetPresent!!) {
                        postRequest_editUserName(Iconstant.profile_edit_userName_url)
                    } else {
                        alert(
                            resources.getString(R.string.alert_nointernet),
                            resources.getString(R.string.alert_nointernet_message)
                        )
                    }
                }
            }
            btn_close.setOnClickListener { firstname_dialog!!.dismiss() }

        }

        ll_last_name.setOnClickListener {


            val view = LayoutInflater.from(this@ProfilePageActivity)
                .inflate(R.layout.edit_last_name, null)

            val Bt_Submit = view.findViewById(R.id.ll_save_last_name) as LinearLayout
            et_edit_last_name = view.findViewById(R.id.et_edit_last_name) as EditText
            val btn_close = view.findViewById(R.id.iv_last_name_close) as ImageView
            lastname_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            lastname_dialog!!.setContentView(view)
            lastname_dialog!!.show()
            //completejob_dialog!!.setView(view).show()
            Bt_Submit.setOnClickListener {
                if (et_edit_last_name.text.toString().isEmpty()) {
                    errorEdit(
                        et_edit_last_name,
                        resources.getString(R.string.register_label_alert_last_name)
                    )
                } else {
                    if (isInternetPresent!!) {
                        /*TODO save service call*/
                    } else {
                        alert(
                            resources.getString(R.string.alert_nointernet),
                            resources.getString(R.string.alert_nointernet_message)
                        )
                    }
                }
            }
            btn_close.setOnClickListener { lastname_dialog!!.dismiss() }


        }
        ll_phone_number.setOnClickListener {


            val view = LayoutInflater.from(this@ProfilePageActivity)
                .inflate(R.layout.edit_phone, null)
            val Bt_Submit = view.findViewById(R.id.ll_save_phone) as LinearLayout
            et_edit_phone = view.findViewById(R.id.et_edit_phone) as EditText
            tv_edit_phone_code = view.findViewById(R.id.tv_edit_phone_code) as TextView
            val btn_close = view.findViewById(R.id.iv_phone_close) as ImageView
            tv_edit_phone_code.setText(Str_countyCode)

            tv_edit_phone_code.setOnClickListener {
                picker.show(supportFragmentManager, "COUNTRY_PICKER")
            }

            picker.setListener(object : CountryPickerListener {
                override fun onSelectCountry(name: String, code: String, dialCode: String) {
                    picker.dismiss()
                    tv_edit_phone_code.setText(dialCode)
                    // close keyboard
                    val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mgr.hideSoftInputFromWindow(tv_edit_phone_code.windowToken, 0)
                }
            })
            phone_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            phone_dialog!!.setContentView(view)
            phone_dialog!!.show()
            //completejob_dialog!!.setView(view).show()
            Bt_Submit.setOnClickListener {
                if (!isValidPhoneNumber(et_edit_phone.text.toString())) {
                    errorEdit(
                        et_edit_phone,
                        resources.getString(R.string.register_label_alert_phoneNo)
                    )
                } else if (tv_edit_phone_code.text.toString().equals("code", ignoreCase = true)) {
                    errorEdit(
                        et_edit_phone,
                        resources.getString(R.string.register_label_alert_country_code)
                    )
                } else {

                    if (isInternetPresent!!) {
                        postRequest_editMobileNumber(Iconstant.profile_edit_mobileNo_url)
                    } else {
                        alert(
                            resources.getString(R.string.alert_nointernet),
                            resources.getString(R.string.alert_nointernet_message)
                        )
                    }
                }
            }
            btn_close.setOnClickListener { phone_dialog!!.dismiss() }
        }
        ll_email.setOnClickListener {


            /*val view = LayoutInflater.from(this@ProfilePageActivity)
                .inflate(R.layout.edit_email, null)
            val Bt_Submit = view.findViewById(R.id.ll_save_email) as LinearLayout
            et_edit_email = view.findViewById(R.id.et_edit_email) as EditText
            val btn_close = view.findViewById(R.id.iv_email_close) as ImageView
            email_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            email_dialog!!.setContentView(view)
            email_dialog!!.show()
            //completejob_dialog!!.setView(view).show()
            Bt_Submit.setOnClickListener {
                if (!isValidEmail(et_edit_email.text.toString())) {
                    errorEdit(
                        et_edit_email,
                        resources.getString(R.string.register_label_alert_email)
                    )
                } else {
                    if (isInternetPresent!!) {
                        *//*TODO save service call*//*
                    } else {
                        alert(
                            resources.getString(R.string.alert_nointernet),
                            resources.getString(R.string.alert_nointernet_message)
                        )
                    }
                }
            }
            btn_close.setOnClickListener { email_dialog!!.dismiss() }*/
        }
        ll_password.setOnClickListener {

            val view = LayoutInflater.from(this@ProfilePageActivity)
                .inflate(R.layout.edit_password, null)
            val Bt_Submit = view.findViewById(R.id.ll_save_password) as LinearLayout
            et_edit_old_password = view.findViewById(R.id.et_edit_old_password) as EditText
            et_edit_new_password = view.findViewById(R.id.et_edit_new_password) as EditText
            et_edit_re_password = view.findViewById(R.id.et_edit_re_password) as EditText
            val btn_close = view.findViewById(R.id.iv_password_close) as ImageView
            password_dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            password_dialog!!.setContentView(view)
            password_dialog!!.show()
            //completejob_dialog!!.setView(view).show()
            Bt_Submit.setOnClickListener {
                if (et_edit_old_password.text.toString().isEmpty()) {
                    errorEdit(
                        et_edit_old_password,
                        resources.getString(R.string.changepassword_label_alert_oldpassword)
                    )
                } else if (!isValidPassword(et_edit_new_password.text.toString())) {
                    errorEdit(
                        et_edit_new_password,
                        resources.getString(R.string.changepassword_label_alert_newpassword)
                    )
                } else if (!isValidPassword(et_edit_re_password.text.toString())) {
                    errorEdit(
                        et_edit_re_password,
                        resources.getString(R.string.changepassword_label_alert_newpassword)
                    )
                } else if (et_edit_new_password.text.toString() != et_edit_re_password.text.toString()) {
                    errorEdit(
                        et_edit_re_password,
                        resources.getString(R.string.changepassword_lable_confirm_notmatch_edittext)
                    )
                } else {
                    if (isInternetPresent!!) {
                        postRequest_changePassword(Iconstant.changePassword_url)
                    } else {
                        alert(
                            resources.getString(R.string.alert_nointernet),
                            resources.getString(R.string.alert_nointernet_message)
                        )
                    }
                }
            }
            btn_close.setOnClickListener { password_dialog!!.dismiss() }

        }

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {

        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                Toast.makeText(getApplicationContext(), "Permission Needed.", Toast.LENGTH_LONG)
                    .show();
            }
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), MY_CAMERA_REQUEST_CODE
            );
        }


    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initialize() {
        session = SessionManager(this@ProfilePageActivity)
        cd = ConnectionDetector(this@ProfilePageActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        tv_email = findViewById<TextView>(R.id.tv_email)
        profile_icon = findViewById<ImageView>(R.id.iv_profile_image)
        back = findViewById<ImageView>(R.id.iv_close_profile)
        iv_edit_profile_image = findViewById(R.id.iv_edit_profile_image)
        cv_edit_profile_image = findViewById(R.id.cv_edit_profile_image)
        ll_first_name = findViewById(R.id.ll_first_name)
        tv_first_name = findViewById(R.id.tv_first_name)
        ll_last_name = findViewById(R.id.ll_last_name)
        tv_last_name = findViewById(R.id.tv_last_name)
        ll_phone_number = findViewById(R.id.ll_phone_number)
        tv_phone_number = findViewById(R.id.tv_phone_number)
        tv_country_code = findViewById(R.id.tv_country_code)
        ll_email = findViewById(R.id.ll_email)
        ll_password = findViewById(R.id.ll_password)
        tv_password = findViewById(R.id.tv_password)

        /*ll_edit_first_name = findViewById(R.id.ll_edit_first_name)
        ll_edit_last_name = findViewById(R.id.ll_edit_last_name)
        ll_edit_password = findViewById(R.id.ll_edit_password)
        iv_password_close = findViewById(R.id.iv_password_close)
        iv_last_name_close = findViewById(R.id.iv_last_name_close)
        iv_first_name_close = findViewById(R.id.iv_first_name_close)
        ll_save_last_name = findViewById(R.id.ll_save_last_name)
        ll_save_first_name = findViewById(R.id.ll_save_first_name)
        ll_save_password = findViewById(R.id.ll_save_password)
        ll_edit_email = findViewById(R.id.ll_edit_email)
        iv_email_close = findViewById(R.id.iv_email_close)
        ll_save_email = findViewById(R.id.ll_save_email)
        ll_edit_phone = findViewById(R.id.ll_edit_phone)
        iv_phone_close = findViewById(R.id.iv_phone_close)
        ll_save_phone = findViewById(R.id.ll_save_phone)
        tv_edit_phone_code = findViewById(R.id.tv_edit_phone_code)

        et_edit_phone = findViewById(R.id.et_edit_phone)
        et_edit_first_name = findViewById(R.id.et_edit_first_name)
        et_edit_last_name = findViewById(R.id.et_edit_last_name)
        et_edit_old_password = findViewById(R.id.et_edit_old_password)
        et_edit_new_password = findViewById(R.id.et_edit_new_password)
        et_edit_re_password = findViewById(R.id.et_edit_re_password)
        et_edit_email = findViewById(R.id.et_edit_email)*/

        gpsTracker = GPSTracker(this@ProfilePageActivity)
        picker = CountryPicker.newInstance("Select Country")

        // get user data from session
        val user = session!!.getUserDetails()
        UserID = user[SessionManager.KEY_USERID].toString()
        UserName = user[SessionManager.KEY_USERNAME].toString()
        UserMobileno = user[SessionManager.KEY_PHONENO].toString()
        UserEmail = user[SessionManager.KEY_EMAIL].toString()
        UserCountyCode = user[SessionManager.KEY_COUNTRYCODE].toString()
        UserprofileImage = user[SessionManager.KEY_USERIMAGE].toString()


        Log.e("PROFILE", UserID + "----" + UserprofileImage)

        if (UserprofileImage!!.contains("https://ridehub.co/images/users/")) {
            Picasso.with(context).load(UserprofileImage)
                .placeholder(R.drawable.user_icon_default).into(profile_icon)
        } else {
            Picasso.with(context).load("https://ridehub.co/images/users/"+ UserprofileImage)
                .placeholder(R.drawable.user_icon_default).into(profile_icon)
        }


        tv_first_name.text = UserName
        tv_last_name.text = UserName
        tv_phone_number.text = UserMobileno
        tv_country_code.text = UserCountyCode.replace("+", "")
        tv_email.text = UserEmail
        tv_password.text = "********"


        /*Profile Pic Upload */

        iv_edit_profile_image.setOnClickListener {


            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                showImagePickerOptions()
            } else {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                    Toast.makeText(getApplicationContext(), "Permission Needed.", Toast.LENGTH_LONG)
                        .show();
                }
                requestPermissions(
                    arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), MY_CAMERA_REQUEST_CODE
                );
            }

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        //Checking the request code of our request
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                showImagePickerOptions()
                //intents you can use hear
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    //--------------Alert Method-----------
    private fun alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ProfilePageActivity)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(R.string.action_ok),
            View.OnClickListener {
                mDialog.dismiss()

                firstname_dialog.dismiss()
                lastname_dialog.dismiss()
                password_dialog.dismiss()
                email_dialog.dismiss()
                phone_dialog.dismiss()
            })
        mDialog.show()

    }

    //--------------Close KeyBoard Method-----------
    private fun closeKeyboard(editText: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            editText.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Logout from facebook-----------
    fun logoutFromFacebook() {
        Util.clearCookies(this@ProfilePageActivity)
        // your sharedPreference
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }

    // API Services
    //-----------------------Edit UserName Request-----------------
    private fun postRequest_editUserName(Url: String) {

        dialog = Dialog(this@ProfilePageActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_updating)

        println("---------------Edit Username Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["user_name"] = et_edit_first_name.text.toString()

        mRequest = ServiceRequest(this@ProfilePageActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------Edit Username Response-----------------$response")
                    var Sstatus = ""
                    var Smessage = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("response")

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        //ll_edit_first_name.visibility = View.GONE
                        session!!.setUserNameUpdate(et_edit_first_name.text.toString())
                        tv_first_name.text = et_edit_first_name.text.toString()
                        alert(
                            resources.getString(R.string.action_success),
                            resources.getString(R.string.profile_lable_username_success)
                        )
                    } else {
                        alert(resources.getString(R.string.action_error), Smessage)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    //-----------------------Edit MobileNumber Request-----------------
    private fun postRequest_editMobileNumber(Url: String) {
        dialog = Dialog(this@ProfilePageActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_updating)

        println("---------------Edit MobileNumber Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["country_code"] = "+" + tv_edit_phone_code.text.toString()
        jsonParams["phone_number"] = et_edit_phone.text.toString()
        jsonParams["otp"] = ""

        mRequest = ServiceRequest(this@ProfilePageActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------Edit MobileNumber Response-----------------$response")
                    var Sstatus = ""
                    var Smessage = ""
                    var Sotp = ""
                    var Sotp_status = ""
                    var Scountry_code = ""
                    var Sphone_number = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("response")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            Sotp = `object`.getString("otp")
                            Sotp_status = `object`.getString("otp_status")
                            Scountry_code = `object`.getString("country_code")
                            Sphone_number = `object`.getString("phone_number")
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        //ll_edit_phone.visibility = View.GONE
                        val intent = Intent(this@ProfilePageActivity, ProfileOtpPage::class.java)
                        intent.putExtra("Otp", Sotp)
                        intent.putExtra("Otp_Status", Sotp_status)
                        intent.putExtra("CountryCode", Scountry_code)
                        intent.putExtra("Phone", Sphone_number)
                        intent.putExtra("UserID", UserID)
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    } else {
                        alert(resources.getString(R.string.action_error), Smessage)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    //-----------------------Change Password Post Request-----------------
    private fun postRequest_changePassword(Url: String) {
        dialog = Dialog(this@ProfilePageActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_otp)

        println("-------------change password Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["password"] = et_edit_old_password.text.toString()
        jsonParams["new_password"] = et_edit_new_password.text.toString()

        mRequest = ServiceRequest(this@ProfilePageActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------change password Response----------------$response")

                    var Sstatus = ""
                    var Smessage = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("response")

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                    if (Sstatus.equals("1", ignoreCase = true)) {

                        val mDialog = PkDialog(this@ProfilePageActivity)
                        mDialog.setDialogTitle(resources.getString(R.string.action_success))
                        mDialog.setDialogMessage(resources.getString(R.string.changepassword_label_changed_success))
                        mDialog.setPositiveButton(
                            resources.getString(R.string.action_ok),
                            View.OnClickListener {
                                mDialog.dismiss()
                                // ll_edit_password.visibility = View.GONE
                                postRequest_Logout(Iconstant.logout_url)
                            })
                        mDialog.show()
                    } else {
                        alert(resources.getString(R.string.action_error), Smessage)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    //-----------------------Logout Request-----------------
    private fun postRequest_Logout(Url: String) {

        dialog = Dialog(this@ProfilePageActivity)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_logging_out)

        println("---------------LogOut Url-----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["device"] = "ANDROID"

        mRequest = ServiceRequest(this@ProfilePageActivity)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------LogOut Response-----------------$response")
                    var Sstatus = ""
                    var Sresponse = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Sresponse = `object`.getString("response")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                    if (Sstatus.equals("1", ignoreCase = true)) {
                        logoutFromFacebook()
                        session!!.logoutUser()
                        val local = Intent()
                        local.action = "com.app.logout"
                        this@ProfilePageActivity.sendBroadcast(local)

                        onBackPressed()
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    } else {
                        alert(resources.getString(R.string.action_error), Sresponse)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.enter, R.anim.exit)
    }

    companion object {
        lateinit var tv_phone_number: TextView
        lateinit var tv_country_code: TextView

        // validating Phone Number
        fun isValidPhoneNumber(target: CharSequence?): Boolean {
            return if (target == null || TextUtils.isEmpty(target) || target.length <= 9) {
                false
            } else {
                android.util.Patterns.PHONE.matcher(target).matches()
            }
        }

        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun updateMobileDialog(code: String, phone: String) {
            tv_phone_number.text = phone
            tv_phone_number.text = code.replace("+", "")
        }

    }


    private fun isValidPassword(pass: String): Boolean {
        return pass.length >= 6
    }

    private fun errorEdit(editText: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(this@ProfilePageActivity, R.anim.shake)
        editText.startAnimation(shake)

        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editText.error = ssbuilder
    }


    /*Profile Pic Utils*/


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /*if (resultCode == this) {
            return
        }*/

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)

                    // loading profile image from local cache
                    //loadProfile(uri.toString())
                    bitmap_data = bitmap
                    // val bm = BitmapFactory.decodeFile("/path/to/image.jpg")
                    val bm = BitmapFactory.decodeFile(ImageFilePath.getPath(this, uri))
                    val baos = ByteArrayOutputStream()
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                    bytearray_img = baos.toByteArray()

                    profile_icon!!.setImageBitmap(bitmap)

                    Log.e("uplaod_url", "-----")
                    UplaodPic()


                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }

    }

    fun UplaodPic() {

        profile_pic_API(Iconstant.profile_edit_photo_url, "", "", "", imageFileName)

    }

    fun profile_pic_API(
        pic_url: String,
        name_stg: String,
        phone_stg: String,
        password_stg: String,
        image_name_stg: String
    ) {


        encodedImage = ""
        encodedImage = Base64.encodeToString(bytearray_img, Base64.DEFAULT)


        val jsonParams = HashMap<String, String>()
        jsonParams["user_id"] = UserID
        jsonParams["name"] = name_stg
        jsonParams["phone"] = phone_stg
        jsonParams["password"] = password_stg
        jsonParams["image"] = encodedImage
        jsonParams["image_name"] = image_name_stg + ".jpg"
        println("--------------book offline-------------------$pic_url-------$jsonParams")
        mRequest = ServiceRequest(this@ProfilePageActivity)
        mRequest!!.makeServiceRequest(
            pic_url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("---------------Pic_Response-----------------$response")
                    var Suser_image = ""
                    var Spassword = ""
                    var Sphone_number = ""
                    var Suser_name = ""

                    try {
                        val json_obj = JSONObject(response)

                        val data_obj = json_obj.getJSONObject("data")



                        Suser_image = data_obj.getString("image")
                        Spassword = data_obj.getString("password")
                        Sphone_number = data_obj.getString("phone_number")
                        Suser_name = data_obj.getString("user_name")

                        Log.e("data_in", Suser_image)

                        session!!.Profile_editSession(Suser_name, Suser_image, Sphone_number)


                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }


                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })


        /*session!!.createLoginSession(
            Semail,
            Suser_id,
            Suser_name,
            Suser_image,
            Scountry_code,
            SphoneNo,
            Sreferal_code,
            Scategory,
            Subcategory,
            gcmId
        )*/


        /*val apiService = ApiInterface.create()
        val call = apiService.changePhotoApi(
            user_details.get(DllSessionManager.USER_ID).toString(),
            photo_selection,
            encodedImage,
            imageFileName+".jpg"
        )
        call.enqueue(object : Callback<UploadPhotoResponse> {
            override fun onResponse(
                call: Call<UploadPhotoResponse>,
                response: retrofit2.Response<UploadPhotoResponse>?
            ) {


                Log.e("profile_upload", response!!.body()!!.status
                        +"----"+response!!.body()!!.result)
                if (response!!.body()!!.status.equals("1") || response.body()!!.result.toString().equals("success")) {

                    ProfilePhotosAPI()
                }

            }

            override fun onFailure(call: Call<UploadPhotoResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })*/

    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object : ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@ProfilePageActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 400)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 400)

        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@ProfilePageActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }
}
