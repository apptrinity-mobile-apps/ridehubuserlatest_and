package com.project.app.ridehubuser.Activities

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.Utils.SessionManager

class SafetyCenterActivity : ActivityHockeyApp() {

    lateinit var iv_close_safety_center: ImageView
    lateinit var tv_set_trusted_contacts: TextView

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    lateinit var userID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_safety_center)

        initialize()
        val user = session!!.getUserDetails()
        userID = user[SessionManager.KEY_USERID].toString()

        iv_close_safety_center.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.slidedown, R.anim.slideup)
            finish()
        }
        tv_set_trusted_contacts.setOnClickListener { }

    }

    private fun initialize() {
        session = SessionManager(this@SafetyCenterActivity)
        cd = ConnectionDetector(this@SafetyCenterActivity)
        isInternetPresent = cd!!.isConnectingToInternet

        iv_close_safety_center = findViewById(R.id.iv_close_safety_center)
        tv_set_trusted_contacts = findViewById(R.id.tv_set_trusted_contacts)
    }

}