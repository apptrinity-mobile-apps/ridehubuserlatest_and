package com.project.app.ridehubuser.Activities

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.KeyEvent
import android.view.View.OnClickListener
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.project.app.mylibrary.dialog.PkDialog
import com.project.app.mylibrary.volley.ServiceRequest
import com.project.app.ridehubuser.HockeyApp.ActivityHockeyApp
import com.project.app.ridehubuser.R
import com.project.app.ridehubuser.Utils.ConnectionDetector
import com.project.app.ridehubuser.iconstant.Iconstant
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class ResetPassword : ActivityHockeyApp() {
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var Rl_back: ImageView? = null

    private var Et_email: EditText? = null
    private var Et_password: EditText? = null
    private var Bt_send: LinearLayout? = null
    private var Cb_showPwd: CheckBox? = null
    private var iv_show_password: ImageView? = null
    private var isPasswordShown: Boolean = false

    private var mRequest: ServiceRequest? = null
    internal lateinit var dialog: Dialog
    private var sEmail = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.resetpassword)
        initialize()

        Rl_back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(Rl_back!!.windowToken, 0)

            onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finish()
        }

        Et_email!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_email!!)
            }
            false
        }

        Et_password!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_password!!)
            }
            false
        }

        iv_show_password!!.setOnClickListener {
            if (isPasswordShown){
                iv_show_password!!.setImageResource(R.drawable.ic_hide)
                Et_password!!.transformationMethod = PasswordTransformationMethod()
                isPasswordShown = false
            }else{
                iv_show_password!!.setImageResource(R.drawable.ic_visible)
                Et_password!!.transformationMethod = HideReturnsTransformationMethod()
                isPasswordShown = true
            }
        }

        Cb_showPwd!!.setOnClickListener { v ->
            if ((v as CheckBox).isChecked) {
                Et_password!!.transformationMethod = null
            } else {
                Et_password!!.transformationMethod = PasswordTransformationMethod()
            }

            Et_password!!.setSelection(Et_password!!.text.length)
        }

        Bt_send!!.setOnClickListener {
            cd = ConnectionDetector(this@ResetPassword)
            isInternetPresent = cd!!.isConnectingToInternet

            if (!isValidEmail(Et_email!!.text.toString())) {
                Alert(
                    getResources().getString(R.string.alert_label_title),
                    getResources().getString(R.string.reset_password_email_label_enter_valid_email)
                )
            } else if (Et_password!!.text.toString().length == 0) {
                Alert(
                    getResources().getString(R.string.alert_label_title),
                    getResources().getString(R.string.reset_password_email_label_enter_valid_password)
                )
            } else if (!Et_email!!.text.toString().equals(sEmail, ignoreCase = true)) {
                Alert(
                    getResources().getString(R.string.alert_label_title),
                    getResources().getString(R.string.reset_password_email_label_enter_valid_email)
                )
            } else {
                if (isInternetPresent!!) {
                    PostRequest(Iconstant.reset_password_url)
                } else {
                    Alert(
                        getResources().getString(R.string.alert_label_title),
                        getResources().getString(R.string.alert_nointernet)
                    )
                }
            }
        }
    }

    private fun initialize() {
        Rl_back = findViewById(R.id.reset_password_header_back_layout) as ImageView
        Et_email = findViewById(R.id.reset_password_email_editText) as EditText
        Et_password = findViewById(R.id.reset_password_password_editText) as EditText
        Bt_send = findViewById(R.id.reset_password_submit_button) as LinearLayout
        Cb_showPwd = findViewById(R.id.reset_password_show_password_checkBox) as CheckBox
        iv_show_password = findViewById(R.id.iv_show_password) as ImageView

        val intent = getIntent()
        sEmail = intent.getStringExtra("Intent_email")

        Et_password!!.transformationMethod = PasswordTransformationMethod()
    }

    //--------------Close Keyboard Method-----------
    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(this@ResetPassword)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            getResources().getString(R.string.action_ok),
            OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }


    // -------------------------code for Forgot Password Post Request----------------------------------

    private fun PostRequest(Url: String) {

        dialog = Dialog(this@ResetPassword)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))

        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = Et_email!!.text.toString()
        jsonParams["password"] = Et_password!!.text.toString()

        mRequest = ServiceRequest(this@ResetPassword)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Reset Password reponse-------------------$response")

                    var Sstatus = ""
                    var Smessage = ""
                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        Smessage = `object`.getString("response")
                        if (Sstatus.equals("1", ignoreCase = true)) {

                            val mDialog = PkDialog(this@ResetPassword)
                            mDialog.setDialogTitle(getResources().getString(R.string.action_success))
                            mDialog.setDialogMessage(Smessage)
                            mDialog.setPositiveButton(
                                getResources().getString(R.string.action_ok),
                                OnClickListener {
                                    mDialog.dismiss()
                                    onBackPressed()
                                    finish()
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                })
                            mDialog.show()
                        } else {
                            Alert(getResources().getString(R.string.action_error), Smessage)
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    // close keyboard
                    val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    mgr.hideSoftInputFromWindow(Et_email!!.windowToken, 0)

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    //-----------------Move Back on pressed phone back button-------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(Rl_back!!.windowToken, 0)

            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            return true
        }
        return false
    }

    companion object {

        //----------------code to Check Email Validation----------
        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }
}
