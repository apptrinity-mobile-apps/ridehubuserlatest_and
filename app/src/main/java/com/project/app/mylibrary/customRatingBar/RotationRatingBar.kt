package com.project.app.mylibrary.customRatingBar

import android.content.Context
import android.util.AttributeSet
import android.view.animation.AnimationUtils
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.project.app.ridehubuser.R
import kotlin.math.ceil

/**
 * Created by nappannda on 2017/05/16.
 */

class RotationRatingBar : AnimationRatingBar {

    constructor(context: Context) : super(context)

    constructor(context: Context, @Nullable attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, @Nullable attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun emptyRatingBar() {
        // Need to remove all previous runnable to prevent emptyRatingBar and fillRatingBar out of sync
        if (mRunnable != null) {
            mHandler!!.removeCallbacksAndMessages(mRunnableToken)
        }

        var delay: Long = 0
        for (partialView in mPartialViews) {
            delay += 5
            mHandler!!.postDelayed({ partialView.setEmpty() }, delay)
        }
    }

    override fun fillRatingBar(rating: Float) {
        // Need to remove all previous runnable to prevent emptyRatingBar and fillRatingBar out of sync
        if (mRunnable != null) {
            mHandler!!.removeCallbacksAndMessages(mRunnableToken)
        }

        for (partialView in mPartialViews) {
            val ratingViewId = partialView.tag as Int
            val maxIntOfRating = ceil(rating.toDouble())

            if (ratingViewId > maxIntOfRating) {
                partialView.setEmpty()
                continue
            }

            mRunnable = getAnimationRunnable(rating, partialView, ratingViewId, maxIntOfRating)
            postRunnable(mRunnable!!, ANIMATION_DELAY)
        }
    }

    @NonNull
    private fun getAnimationRunnable(
        rating: Float,
        partialView: PartialView,
        ratingViewId: Int,
        maxIntOfRating: Double
    ): Runnable {
        return Runnable {
            if (ratingViewId.toDouble() == maxIntOfRating) {
                partialView.setPartialFilled(rating)
            } else {
                partialView.setFilled()
            }

            if (ratingViewId.toFloat() == rating) {
                val rotation = AnimationUtils.loadAnimation(context, R.anim.rotation)
                partialView.startAnimation(rotation)
            }
        }
    }

    companion object {

        // Control animation speed
        private const val ANIMATION_DELAY: Long = 15
    }
}